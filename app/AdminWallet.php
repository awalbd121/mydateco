<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminWallet extends Model
{
    public function receiver()
    { 
        return $this->belongsTo('App\User','receiver_id','id');
    }
    public function from_user()
    { 
        return $this->belongsTo('App\User','user_id','id');
    }
}
