<?php

namespace App\Console\Commands;

use App\Http\Controllers\SendPushNotification;
use App\Http\Controllers\AdminController;
use Carbon\Carbon;
use App\CustomPush;
use DB;

use Illuminate\Console\Command;

class customCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom:push';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Custom pushnotification from admin side';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $CustomPush = CustomPush::where('schedule_at','<=',\Carbon\Carbon::now()->addMinutes(5))->get();

        if(!empty($CustomPush)){
            foreach($CustomPush as $Push){
                CustomPush::where('id',$Push->id)->update(['schedule_at' => null ]);

                // sending push
                (new AdminController)->SendCustomPush($Push->id);
            }
        }
    }
}