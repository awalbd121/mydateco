<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

	protected $table = "countries";
	
    protected $fillable = [
       'code', 'name', 'dial_code', 'currency_name' , 'currency_symbol' ,'currency_code','status'
   ];
   
}
