<?php

use App\PromocodeUsage; 
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
use App\Notifications;
use App\Country;

function currency($value = '')
{
	if($value == ""){
		return Setting::get('currency')."0";
	}else{
		return Setting::get('currency').$value;
	}
}

function distance($value = '')
{
    if($value == ""){
        return "0".Setting::get('distance', 'Km');
    }else{
        return $value.Setting::get('distance', 'Km');
    }
}

function img($img){
	if($img == ""){
		return asset('main/avatar.jpg');
	}else if (strpos($img, 'http') !== false) {
        return $img;
    }else{
		return asset('storage/'.$img);
	}
}

function curl($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $return = curl_exec($ch);
    curl_close ($ch);
    return $return;
}

function user_new_notification_count($notifier_id,$user_id) {

    //dd($notifier_id);
    $user_new_notification_count = Notifications::selectRaw("COUNT(user_id) as user_notification")
      ->where('notifier_id',$notifier_id)
      ->where('user_id',$user_id)
      ->where('status','=','chat')
      ->where('read',0)
      ->groupBy('user_id')
      ->first();
    
    return @$user_new_notification_count->user_notification;
}

function country_list()
{
  return Country::all();
}

function country_name($country_id)
{
  return Country::where('id',$country_id)->first()->name;
}

function send_twilio_sms($mobile_number,$otp) {
            
    \Log::info("Twilio MObile".$mobile_number. '---' .$otp);

    $status = '';
   
    $message = "Mydate OTP : ". $otp ."";
    $accountSid = Setting::get('twilio_accountsid');
    $authToken = Setting::get('twilio_token');
    $twilioNumber = Setting::get('twilio_mobile');
   
    $client = new Client($accountSid, $authToken);
    try {
      $client->messages->create(
          $mobile_number,
          [
              "body" => $message,
              "from" => $twilioNumber
          ]);
      return $status = "success";
      
    }catch (TwilioException $e) {

      \Log::info($e->getMessage());
      dd($e);
      return $e->getMessage();
      // return response()->json(['error'=> $e->getMessage()]);
      Log::info(
          'Could not send SMS notification.' .
          ' Twilio replied with: ' . $e
          );
    }
}