<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Database\Seeder;

use App\Admin;
use App\User;
use App\Likes;
use App\UserPremium;
use App\CustomPush;
use App\Transactions;
use App\Settings;
use App\UserReport;
use App\Premium;
use App\AdminWallet;
use App\UserBlocked;
use Artisan;


use DB;
use Log;
use Auth;
use Hash;
use Storage;
use Setting;
use Redirect;
use Exception;
use Notification;
use Validator;

use Carbon\Carbon;
use App\Http\Controllers\SendPushNotification;
use App\Helpers\Helper;



class AdminController extends Controller
{    
	/**
    * Create a new controller instance.
    *
    * @return void
    */
  public function __construct()
  {
       $this->middleware('admin');  
  }
  public function dashboard(){
    //echo Setting::get('sitename');die;
    $data['users']=User::all();
    $data['males']=User::where('gender','male')->get();
    $data['females']=User::where('gender','female')->get();
    $data['premium']=UserPremium::get();
    //dd($data['premium']);
    return view('admin.dashboard',compact('data'));
  }
	public function adminSettings()
	{
		//echo Setting::get('sitename');die;
        $premium = Premium::where('is_default_trail',1)->first();
 		return view('admin.setting.site-setting',compact('premium'));
	}
  public function static_pages()
  {
    return view('admin.setting.static');
  }
  public function static_update(Request $request){
      $this->validate($request, [
              'privacy_content' => 'required',
              'terms_content' => 'required',
          ]);
      if($request->privacy_content){
        Setting::set('privacy_content', $request->privacy_content);
      }
      if($request->terms_content){
        Setting::set('terms_content', $request->terms_content);
      }      
      Setting::save();

      return back()->with('flash_success', 'Content Updated!');
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Provider  $provider
   * @return \Illuminate\Http\Response
   */
  public function settings_store(Request $request)
  {
    //dd($request->all());
    $this->validate($request,[
            'sitename' => 'required',
            'logo' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'favicon' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'period' => 'required',
            'duration' => 'required',
            'like_count' => 'required',
            'superlike' => 'required',
            'IOS_USER_ENV' => 'required',
            'IOS_USER_PASS' => 'required',
            //'pemfile' => 'mimes:pem',
        ]);
    
    foreach((array)$request->all() as $key => $value){
      if($key == 'IOS_USER_ENV'){
      $push = file_get_contents(base_path() . '/config/push-notification.php'); 
        if(Setting::get('IOS_USER_ENV')){
          $searchfor = Setting::get('IOS_USER_ENV');
          $change_content=str_replace("$searchfor", "$value",$push);
        }else{
          $searchfor = "env('IOS_USER_ENV')";
          $change_content=str_replace("$searchfor", "'$value'",$push);
        }
          file_put_contents(base_path() . '/config/push-notification.php', $change_content);
      } 
      if($key == 'IOS_USER_PASS'){
      $push = file_get_contents(base_path() . '/config/push-notification.php'); 
        if(Setting::get('IOS_USER_PASS')){
          $searchfor_pass = Setting::get('IOS_USER_PASS');
          $change_content1=str_replace("$searchfor_pass", "$value",$push);
        }else{
          $searchfor_pass = "env('IOS_USER_PASS')";
          $change_content1=str_replace("$searchfor_pass", "'$value'",$push);
        }
          file_put_contents(base_path() . '/config/push-notification.php', $change_content1);
      }
      if($key == 'facebook_status'){
        $push = file_get_contents(base_path() . '/config/push-notification.php'); 
          if(Setting::get('facebook_status')){
            $searchfor = Setting::get('facebook_status');
            $change_content=str_replace("$searchfor", "$value",$push);
          }else{
            $searchfor = "env('facebook_status')";
            $change_content=str_replace("$searchfor", "'$value'",$push);
          }
            file_put_contents(base_path() . '/config/push-notification.php', $change_content);
        } 
         //chmod($change_content, 0777); 
    }


    if($request->hasFile('logo')) {
        $logo = Helper::upload_picture($request->file('logo'));
        Setting::set('logo', $logo);
    }

    if($request->hasFile('favicon')) {
        $favicon = Helper::upload_picture($request->file('favicon'));
        Setting::set('favicon', $favicon);
    }

    if($request->has('monthly')){
      Setting::set('monthly', $request->monthly);
    }
    if($request->has('annual')){
      Setting::set('annual', $request->annual);
    }

    if($request->hasFile('site_email_logo')) {
        $site_email_logo = Helper::upload_picture($request->file('site_email_logo'));
        Setting::set('site_email_logo', $site_email_logo);
    }
    if($request->hasFile('pemfile')){
       $file_name = 'dateuser.pem';
        $request->file('pemfile')->move(app_path() . "/apns", $file_name);
        $local_url = $file_name;
        Setting::set('pemfile', ('/apns/' . $local_url));
    }
    if($request->has('demo_mode')){
      Setting::set('demo_mode', $request->demo_mode);
    }
    if($request->has('IOS_USER_ENV')){
      Setting::set('IOS_USER_ENV', $request->IOS_USER_ENV);
    }
    if($request->has('IOS_USER_PASS')){
      Setting::set('IOS_USER_PASS', $request->IOS_USER_PASS);
    }
    if($request->has('sitename')) {
        Setting::set('sitename', $request->sitename);
    }
    if($request->has('site_copyright')) {
        Setting::set('site_copyright', $request->site_copyright);
    }
    if($request->has('google_playstore_link')) {
        Setting::set('google_playstore_link', $request->google_playstore_link);
    }
    if($request->has('apple_app_store_link')) {
        Setting::set('apple_app_store_link', $request->apple_app_store_link);
    }
    if($request->has('website_link')) {
        Setting::set('website_link', $request->website_link);
    }
    if($request->has('currency')) {
        Setting::set('currency', $request->currency);
    }
    if($request->has('contact_email')) {
        Setting::set('contact_email', $request->contact_email);
    }
    if($request->has('contact_number')) {
        Setting::set('contact_number', $request->contact_number);
    }
    if($request->has('contact_text')) {
        Setting::set('contact_text', $request->contact_text);
    }
    if($request->has('contact_title')) {
        Setting::set('contact_title', $request->contact_title);
    }
    if($request->has('map_key')) {
        Setting::set('map_key', $request->map_key);
    } 
    if($request->has('fb_app_version')) {
        Setting::set('fb_app_version', $request->fb_app_version);
    }
    if($request->has('fb_app_id')) {
        Setting::set('fb_app_id', $request->fb_app_id);
    }
    if($request->has('fb_app_secret')) {
        Setting::set('fb_app_secret', $request->fb_app_secret);
    }  
    if($request->has('linkedin_app_id')) {
        Setting::set('linkedin_app_id', $request->linkedin_app_id);
    }
    if($request->has('linkedin_app_secret')) {
        Setting::set('linkedin_app_secret', $request->linkedin_app_secret);
    } 
    if($request->has('stripe_secret_key')) {
        Setting::set('stripe_secret_key', $request->stripe_secret_key);
    }
    if($request->has('stripe_public_key')) {
        Setting::set('stripe_public_key', $request->stripe_public_key);
    } 
    if($request->has('review')){
        Setting::set('review', $request->review);  
    }
    if($request->has('facebook_status')){
        Setting::set('facebook_status', $request->facebook_status);  
    }
    if($request->has('linkedin_status')){
        Setting::set('linkedin_status', $request->linkedin_status);  
    }
    if($request->has('facebook_account_kit')){
        Setting::set('facebook_account_kit', $request->facebook_account_kit);  
    }
    if($request->has('wallet_transaction')){
        Setting::set('wallet_transaction', strtoupper($request->wallet_transaction));  
    }
    if($request->has('premium_transaction')){
        Setting::set('premium_transaction', strtoupper($request->premium_transaction));  
    }
    if($request->has('referal_transaction')){
        Setting::set('referal_transaction', strtoupper($request->referal_transaction));  
    }
    if($request->has('referal_count')){
        Setting::set('referal_count', $request->referal_count);  
    }
    if($request->has('referal_amount')){
        Setting::set('referal_amount', $request->referal_amount);  
    }
    if($request->has('referal_status')){
        Setting::set('referal_status', $request->referal_status);  
    }if($request->has('demo_seeder')){
        Setting::set('demo_seeder', $request->demo_seeder);  
    }
    if($request->has('period')){
        Setting::set('package_period', $request->period);  
    }
    if($request->has('duration')){
        Setting::set('package_duration', $request->duration);  
    }
    if($request->has('like_count')){
        Setting::set('package_like_count', $request->like_count);  
    }
    if($request->has('superlike')){
        Setting::set('package_super_like', $request->superlike);  
    }
    //For seeder details
    if($request['demo_mode']==1)
    {
        if($request['demo_seeder']=='on')
        {
            $tables = DB::select('SHOW TABLES');
            foreach ($tables as $table) {
                $currentTable = array_values(get_object_vars($table))[0];
                if ($currentTable !== 'migrations')
                    DB::table($currentTable)->truncate();
            }
            //Update database seder
            Artisan::call('db:seed');
        }
    }
    Setting::save();
    // $premium = Premium::where('is_default_trail',1)->get()->count();
    // if($premium == 0){
    //     Premium::Create([
    //         'plan_name' => $request->plan_name,
    //         'period' => $request->period,
    //         'duration' => $request->duration,
    //         'price' => 0,
    //         'superlike' => $request->superlike,
    //         'like_count' => $request->like_count,
    //         'is_default_trail' => $request->is_default_trail
    //       ]);
    // }else{
    //   Premium::where('is_default_trail',1)->Update(['plan_name' => $request->plan_name,
    //         'period' => $request->period,
    //         'duration' => $request->duration,
    //         'price' => 0,
    //         'superlike' => $request->superlike,
    //         'like_count' => $request->like_count
    //         ]);
    // }
    
    return back()->with('flash_success','Settings Updated Successfully');
  }

	public function chat(Request $request){
    $user=$sender=$user_friends=array();
        $users = User::orderBy('created_at' , 'desc')->get();
    if($request->sender){
      $user=User::find($request->user);      
      $sender=User::find($request->sender);
      $request->request->add(['user_id'=>$user->id]);
      $user_friends=$this->user_friends($request);
    }
		$page='chat';
    	return view('admin.chat',compact('page','users','user','sender','user_friends'));
    }/*
    public function chat_list(){
		$users = User::orderBy('created_at' , 'desc')->get();
		//dd($user);
		$page='chat';
    	return view('admin.chat',compact('page','users'));
    }*/
  public function user_friends(Request $request){
    try{
        $user=$request->user_id;

        $match_lists=Likes::with('user')->where('like_id',$user)->Orwhere('user_id',$user)->where('status','2')->get(); 
        $match_list_user=array();   
        
        foreach ($match_lists as $match_list_find) { 

          if($match_list_find->user_id==$user){
              $match_list_user[]=Likes::with('user_like')->where('id',$match_list_find->id)->get();     
          }else{
              $match_list_user[]=Likes::with('user')->where('id',$match_list_find->id)->get();
          }
        }    

        $match_list=new Collection();
        foreach($match_list_user as $collection) {            
              $match_list=$match_list->merge($collection);
         }
        //dd($match_list);

        if($request->ajax()) {
            return response()->json($match_list);
          }else{
            return $match_list;
          }
    } 
    catch (Exception $e) {
        if($request->ajax()){
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }else{
            return back()->with('flash_error', 'Something went wrong');
        }
    }          
  }
  /**
     * user_transaction
     *
     * @return \Illuminate\Http\Response
     */
    public function user_transaction()
    {
        try {
             $wallet_transaction=Settings::where('key','wallet_transaction')->first(); 
             $premium=Settings::where('key','premium_transaction')->first(); 

             $user_wallet_transaction = $wallet_transaction['value'];
             $premium_transaction = $premium['value'];

             $payments= Transactions::with('from_user','receiver')->orderBy('id', 'DESC')->get();
            
            return view('admin.payment.user-transaction', compact('payments','user_wallet_transaction','premium_transaction'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }
     /**
     * user_transaction
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_transaction()
    {
        try {

             $payments= AdminWallet::with('from_user','receiver')->orderBy('id', 'DESC')->get();
             $admin_credit_amount = AdminWallet::select('id','amount','profit_status')->where('profit_status','Credit')->sum('amount');
             $admin_debit_amount= AdminWallet::select('id','amount','profit_status')->where('profit_status','Debit')->sum('amount');
             $admin_wallet_balance = $admin_credit_amount - $admin_debit_amount;
            return view('admin.payment.admin-transaction', compact('payments','admin_credit_amount','admin_wallet_balance'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }
    /**
     * user_premium
     *
     * @return \Illuminate\Http\Response
     */
    public function user_premium()

    {
        try {
             $user_premiums=UserPremium::with('premium','user')->orderBy('id','desc')->get();               
            
            return view('admin.payment.user-premium', compact('user_premiums'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }
    /**
     * user_matches
     *
     * @return \Illuminate\Http\Response
     */
    public function user_matches()
    {
        try {
             $likes=Likes::with('user','user_like')->orderBy('id','desc')->get();         
            
            return view('admin.user-matches', compact('likes'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('admin.account.profile');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function profile_update(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            //'mobile' => 'required',
            //'picture' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                       
        ]);

        try{

            $admin = Admin::find(Auth::guard('admin')->user()->id);
            $admin->name = $request->name;
            $admin->email = $request->email;
            //$admin->mobile = $request->mobile;

            if ($request->hasFile('picture')) {
                $admin->picture = Storage::url(Storage::putFile('admin/profile', $request->picture, 'public'));
            }

            //$admin->gender = $request->gender;
            $admin->save();

            return redirect()->back()->with('flash_success','Profile Updated');
        }

        catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        return view('admin.account.change-password');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password_update(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }

        $this->validate($request,[
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        try{

           $Admin = Admin::find(Auth::guard('admin')->user()->id);

            if(password_verify($request->old_password, $Admin->password))
            {
                $Admin->password = bcrypt($request->password);
                $Admin->save();

                return redirect()->back()->with('flash_success','Password Updated');
            }else{
                return redirect()->back()->with('flash_error','Incorrect Password');
            }
        }

        catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }        
    }  
    /**
   * push notification.
   *
   * @return \Illuminate\Http\Response
   */
  public function user_report(){

      try{
          $user_reports = UserReport::with('user','reporter')->orderBy('id','desc')->get();
          return view('admin.user_report',compact('user_reports'));
      }

      catch (Exception $e) {
           return back()->with('flash_error','Something Went Wrong!');
      }
  } 
  /**
   * push notification.
   *
   * @return \Illuminate\Http\Response
   */
  public function user_block(Request $request,$id){

      try{
          /*UserReport::where('report_id',$request->id)->delete();
          Like::where('like_id',$request->id)->delete();
          Like::where('user_id',$request->id)->delete();*/
          //$user = User::find($request->id)->delete();   
          $user_blocked=new UserBlocked;
          $user_blocked->user_id = $request->user_id;
          $user_blocked->block_id = $id;
          $user_blocked->status = "block";
          $user_blocked->save();

          return back()->with('flash_success','User blocked!');
      }

      catch (Exception $e) {
           return back()->with('flash_error','Something Went Wrong!');
      }
  }

  /**
   * push notification.
   *
   * @return \Illuminate\Http\Response
   */
  public function push(){

      try{
          $Pushes = CustomPush::orderBy('id','desc')->get();
          return view('admin.push',compact('Pushes'));
      }

      catch (Exception $e) {
           return back()->with('flash_error','Something Went Wrong!');
      }
  }


  /**
   * pages.
   *
   * @param  \App\Provider  $provider
   * @return \Illuminate\Http\Response
   */
  public function send_push(Request $request){

      $this->validate($request, [
              'send_to' => 'required|in:ALL,PREMIUM_USERS',
              'message' => 'required|max:100',
          ]);

      try{

          $CustomPush = new CustomPush;
          $CustomPush->send_to = $request->send_to;
          $CustomPush->message = $request->message;    

          if($request->has('schedule_date') && $request->has('schedule_time')){
              $CustomPush->schedule_at = date("Y-m-d H:i:s",strtotime("$request->schedule_date $request->schedule_time"));
          }

          $CustomPush->save();

          if($CustomPush->schedule_at == ''){
              $this->SendCustomPush($CustomPush->id);
          }

          return back()->with('flash_success', 'Message Sent to all '.$request->segment);
      }

      catch (Exception $e) {
           return back()->with('flash_error','Something Went Wrong!');
      }
  }
  public function SendCustomPush($CustomPush){

      try{

          \Log::notice("Starting Custom Push");

          $Push = CustomPush::findOrFail($CustomPush);

          if($Push->send_to == 'PREMIUM_USERS'){

              $Users =UserPremium::get();
              foreach ($Users as $key => $user) {
                  (new SendPushNotification)->sendPushToUser($user->user_id, $Push->message);
              }

          }elseif($Push->send_to == 'ALL'){

              $Users = User::all();
              foreach ($Users as $key => $user) {
                  (new SendPushNotification)->sendPushToUser($user->id, $Push->message);
              }
          }
      }

      catch (Exception $e) {
           return back()->with('flash_error','Something Went Wrong!');
      }
  }

}
