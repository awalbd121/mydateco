<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Route;
use Hash;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {        
        $this->middleware('guest')->except('logout');
    }
    public function index() {
        return view('layouts.login');
    }

    public function apiLogin(Request $request) {

        $this->validate($request, [

            'device_id' => 'required',
            'device_type' => 'required|in:android,ios',
            'device_token' => 'required',
            'username' => 'required',
            'password' => 'required',
            'client_id' => 'required',
            'client_secret' => 'required'
        ]);

        try {

            $userExits = User::where('email',$request->username)->first();
            if($userExits)
            {    
                
                $user = User::where('email',$request->username)->first();

                if(Hash::check($request->password,$user->password)) {

                    $update = User::where('email',$request->username)->first();
                    if($update){

                        $update->device_id = $request->device_id;
                        $update->device_token = $request->device_token;
                        $update->device_type = $request->device_type;
                        $update->save();
                    }
                    $tokenRequest = $request->create('/oauth/token', 'POST', $request->all());
                    $request->request->add([
                        "client_id"     => $request->client_id,
                        "client_secret" => $request->client_secret,
                        "grant_type"    => 'password',
                        "code"          => '*',
                    ]);

                    $response = Route::dispatch($tokenRequest);
                    $json = (array) json_decode($response->getContent());
                    $response->setContent(json_encode($json));
                    return $response; 

                } else {

                    return response()->json(['status' => 'false' , 'message' => 'Password Incorrect !!!' ],422);
                }
                
            } else {

                return response()->json(['status' => 'false' , 'message' => 'Invalid Email !!!' ],422);
            }

        } catch (Exception $e) {
           
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }
}
