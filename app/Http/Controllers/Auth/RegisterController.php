<?php

namespace App\Http\Controllers\Auth;
use Mail;
use App\User;
use App\UserPreferences;
use App\UserLocation;
use Setting;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use DateTime;
use substr;
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
use App\Settings;
use App\ReferralHistory;
use App\Transactions;
use App\AdminWallet;
use App\UserPremium;
use App\Http\Controllers\VideoRoomsController;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $setting_facebook = Settings::where('key','facebook_status')->first();
        $User= User::where('email',$data['email'])->whereIn('login_by',['facebook','linkedin'])->first();
        if($User){
            if(Setting::get('facebook_status')=='off' && $User->login_by=='facebook'){
            
                    return Validator::make($data, [
                        'first_name' => 'required|string|max:255',
                        'email' => 'required|string|email|max:255',
                        'username' => 'required',
                        'password' => 'required|string|min:6|confirmed',
                        'dob' => 'required',
                    ]);
            }
            if(Setting::get('linkedin_status')=='off' && $User->login_by=='linkedin'){
            
                    return Validator::make($data, [
                        'first_name' => 'required|string|max:255',
                        'email' => 'required|string|email|max:255',
                        'username' => 'required',
                        'password' => 'required|string|min:6|confirmed',
                        'dob' => 'required',
                    ]);
            }
        }else{
            return Validator::make($data, [
                'first_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'username' => 'required|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'dob' => 'required',
                'country_list' => 'required',
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $setting_facebook = Settings::where('key','facebook_status')->first();
        $User= User::where('email',$data['email'])->whereIn('login_by',['facebook','linkedin','google'])->first();
        $from = new DateTime($data['dob']);
        $to   = new DateTime('today');
        $age  = $from->diff($to)->y; 
        if($User){
            if(Setting::get('facebook_status')=='off' && $User->login_by=='facebook'){
            
                $User->password = bcrypt($data['password']);
                $User->first_name = $data['first_name'];
                $User->last_name = $data['last_name'];
                $User->gender = $data['gender'];
                $User->dob = $data['dob'];
                $User->dob = $age;
                $User->email = $data['email'];
                $User->save();
                return $User;
            
            }
            if(Setting::get('facebook_status')=='off' && $User->login_by=='linkedin'){
            
                $User->password = bcrypt($data['password']);
                $User->first_name = $data['first_name'];
                $User->last_name = $data['last_name'];
                $User->gender = $data['gender'];
                $User->dob = $data['dob'];
                $User->dob = $age;
                $User->email = $data['email'];
                $User->save();
                return $User;
            
            }
        }

        $from = new DateTime($data['dob']);
        $to   = new DateTime('today');
        $age  = $from->diff($to)->y;
        $install_code=isset($data['install_code'])?$data['install_code']:''; 
        $User = User::create([
            'username' => $data['username'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'gender' => $data['gender'],
            'dob' => $data['dob'],
            'age' => $age,
            'mobile' => $data['country_code'].$data['mobile'],
            'country' => $data['country_list'],
        // 'latitude' => $data['latitude'],
        // 'longitude' => $data['longitude'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'install_code' => $install_code,
            
        ]);
        $address = $this->getaddress($data['latitude'],$data['longitude']);
        /* $User_location = UserLocation::create([
            'user_id' => $User->id,
            'address' => $address,
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude'],
        ]); */

        $UserPreferences = new UserPreferences;
        $UserPreferences->user_id=$User->id;
        if($data['gender'] == 'male'){
        $UserPreferences->gender='female';  
        }else if($data['gender'] == 'female'){
            $UserPreferences->gender='male';  
        }else{
            $UserPreferences->gender='both';  
        }
        $age_max = $age+5;
        $UserPreferences->age_limit = $age.','.$age_max;
        $UserPreferences->address=json_encode([  ["country" => $address, "latitude" => $data['latitude'], "longitude" => $data['longitude']]  ]);
        $UserPreferences->distance = 20000;    
        // dd($UserPreferences);
        // echo '<pre>';print_r($UserPreferences);exit;
        $UserPreferences->save();

        //$invite_code=$data['first_name'].$data['last_name'].$User->id;
        //Generate 5 digit invite code as random
        $userplan = new UserPremium();
        $userplan->user_id = $User->id;
        $userplan->premia_id = 1;
        $userplan->status = "0";
        $userplan->reset_date = date('Y-m-d H:i:s');
        $userplan->created_at = date('Y-m-d H:i:s');
        $userplan->save();

        $invite_code= substr(md5($User->id),0,5);
        $user = User::where('id',$User->id)->update(['invite_code' => $invite_code]);

        //Create new referal code concept when user is login
        if($install_code!=null){
            $user_details = User::where('invite_code',$install_code)->first();
            $new_user_id = $User->id;
            $user_details_id = $user_details['id'];
            $user_wallet_balance = $user_details['wallet_balance'];
    
            $setting_referal_status = Settings::where('key','referal_status')->first();
            $setting_referal_count = Settings::where('key','referal_count')->first();
            $setting_referal_count_value = $setting_referal_count['value'];
            //For Referal history save record
            if($setting_referal_status['value']=='on'){
                $referral_history =new ReferralHistory;
                $referral_history->referrer_id=$user_details_id;
                $referral_history->referral_id=$new_user_id;
                $referral_history->status ='P';
                $referral_history->save();

                $referal_history = ReferralHistory::where('referrer_id',$user_details_id)->where('status','P')->count();
                if(count($referal_history)>0)
                {
                    if($referal_history ==(int)$setting_referal_count_value)
                    {
                        ReferralHistory::where('referrer_id',$user_details_id)->where('status', 'P')->update(['status' => 'C']);
                        $referal_transaction=Settings::where('key','referal_transaction')->first();
                        $referal_transaction_value = $referal_transaction['value'];
                        $referal_amount=Settings::where('key','referal_amount')->first();
                        $referal_amount_value = $referal_amount['value']; 
                        $transactions= Transactions::select('id')->orderBy('id', 'DESC')->first();
                        $transactions_id = ($transactions != null) ? $transactions->id + 1 : 1;
                        $referal ='REFERAL';
                        $random = str_pad($transactions_id, 4, '0', STR_PAD_LEFT);
                        $id = $transactions_id+1;
                        $alias_id = $referal_transaction_value.$referal."".$random;
                        $referal_status ='REFERAL';

                        //Trasanctions history save (Referal transactions saved)
                        $transactions =new Transactions;
                        $transactions->user_id=$user_details_id;
                        $transactions->status=$referal_status;
                        $transactions->payment_id = '';
                        $transactions->alias_id = $alias_id;
                        $transactions->profit_status = 'Credit';
                        $transactions->amount = $referal_amount_value;
                        $transactions->save();

                        //Admin user wallet updated
                        $admin_wallet =new AdminWallet;
                        $admin_wallet->user_id=$user_details_id;
                        $admin_wallet->status=$referal_status;
                        $admin_wallet->payment_id = '';
                        $admin_wallet->alias_id = $alias_id;
                        $admin_wallet->profit_status = 'Debit';
                        $admin_wallet->amount = $referal_amount_value;
                        $admin_wallet->save();
                        
                        //user wallet update
                        User::where('id',$user_details_id)->update(['wallet_balance' =>$user_wallet_balance+$referal_amount_value ]);

                    }
                }
            }
        }

        Mail::send('emails.user_registration_confirm',  ['user'=>$User], function($message)use($data) {
            $message->to($data['email'])->subject('Welcome to Mydate.');
            $message->from('postmaster@mails.mydateco.com','Mydate');
        });

        Mail::send('emails.user_registration_admin_confirm',  ['user'=>$User], function($message)use($data) {
            $message->to('Mydate@my.com')->subject('Welcome to Mydate.');
            $message->from('postmaster@mails.mydateco.com','Mydate');
        });

        return $User;
    }
    public function getaddress($lat,$lng)
    {   
	    $url = 'https://maps.googleapis.com/maps/api/geocode/json?key='.Setting::get('map_key','AIzaSyC808NpIO-MZ_C-nXv21zGFDKC1OUm_MkQ').'&latlng='.trim($lat).','.trim($lng).'&sensor=false';
	    $json = @file_get_contents($url);
	    $data=json_decode($json);
	    $status = $data->status;
	    if($status=="OK") return $data->results[0]->formatted_address;
	    else
	    return false;
    }

    public function OTP(Request $request)
    {   
        
        try {

            $data = $request->all();
            if($request->has('login_by')){                
               
                //dd($social_data);
                if($social_data){
                    return response()->json([
                    'error' => trans('form.socialuser_exist'),
                ], 422); 
                }
            }

            elseif(User::where('mobile',$data['mobile'])->first()){

                return response()->json([
                    'error' => 'Mobile Number Already Exist',
                ], 422); 
            }
           
            $newotp = rand(1000,9999);
            $data['otp'] = $newotp;
            $data['phone'] = $data['mobile'];   
            $data['message'] = 'Your Kazrr verification code is: '.$newotp;         
            $check =User::wheremobile($data['mobile'])->first();           

            if(count($check)>0) 
            {
                 return response()->json(['error' => 'Mobile Number Already Exist'], 500); 
            }   
            else
            {
                
                //(new VideoRoomsController)->sendSms($data);
                send_twilio_sms($data['mobile'],$newotp);
                return response()->json([
                    'message' => 'OTP Sent',
                    'otp' => $newotp
                ]);

            }
        } catch (Exception $e) {
            return response()->json(['error' => trans('form.whoops')], 200);
        }
    }
   
}
