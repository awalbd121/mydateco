<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio;
use Aloha\Twilio\TwilioInterface;
use Exception;


class CallController extends Controller
{

    public function __construct(TwilioInterface $twilio)
    {
        $this->twilio = $twilio;
    }
    
    public function Make_calls(Request $request)
    {

        try{

             $url = 'APcaba41a3e2ed44f9ab9d0690159fc5db';

         //    $this->twilio->call($request->phone, $url);

           //  die();

              $this->twilio->call($request->phone, function ($message) {
                    $message->say('Welcome to GloryTaxi');
                 
              });





              

           //  $this->twilio->dial('+919597625476');


             return response()->json(['message' => trans('api.call_connected_successfully')]);
        }
        catch(Exception $e)
        {
            print_r($e->getMessage());
            die();
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }


    }

    public function xml_data()
    {

        $content = '<Response>
        <Say voice="alice">Thanks for trying our documentation. Enjoy!</Say>
        <Dial><Number>+919087719073</Number></Dial>
        </Response>';

         return response($content, 200)
            ->header('Content-Type', 'text/xml');

    }

    public function get_token()
    {
      $sdk = Twilio::access_token();
      return response()->json(['access_token' => $sdk  ]);

    }

    public function dial_number(Request $request)
    {
        $dial = $request->phone;
        $dial = Twilio::call_dial($dial);
        return $dial;
    }
}
