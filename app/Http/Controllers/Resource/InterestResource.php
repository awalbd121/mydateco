<?php

namespace App\Http\Controllers\Resource;

use App\Interest;
use App\UserInterest;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Exception;
use Storage;
use Setting;

class InterestResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interests = Interest::orderBy('created_at' , 'desc')->get();
        return view('admin.interests.index', compact('interests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.interests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }*/

        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => '',
            'image' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
        ]);

        try{

            $interest = $request->all();
            if($request->hasFile('image')) {
                 //$interest['image'] = $request->image->store('interest');
                 $interest['image']  = Helper::upload_picture($request->image);
            }


            $interest = Interest::create($interest);

            return back()->with('flash_success','Interest Details Saved Successfully');

        } 

        catch (Exception $e) {
            return back()->with('flash_errors', 'Interest Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\interest  $interest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $interest = Interest::findOrFail($id);
            return view('admin.interests.interest-details', compact('interest'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\interest  $interest
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $interest = Interest::findOrFail($id);
            return view('admin.interests.edit',compact('interest'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\interest  $interest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        /*if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }*/

        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => '',
            'image' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
        ]);

        try {

            $interest = Interest::findOrFail($id);

            if($request->hasFile('image')) {
                //$interest->image = $request->image->store('uploads');
                $interest->image  = Helper::upload_picture($request->image);
            }

            $interest->name = $request->name;
            $interest->description = $request->description;
            $interest->save();

            return redirect()->route('admin.interest.index')->with('flash_success', 'Interest Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_errors', 'Interest Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\interest  $interest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Interest::find($id)->delete();
            UserInterest::where('interest_id',$id)->delete();
            return back()->with('message', 'Interest deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_errors', 'Interest Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function request($id){

        try{

            $requests = interestRequests::where('interest_requests.interest_id',$id)
                    ->RequestHistory()
                    ->get();

            return view('admin.request.request-history', compact('requests'));
        }

        catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }

    }

}
