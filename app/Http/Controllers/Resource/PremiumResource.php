<?php

namespace App\Http\Controllers\Resource;

use App\Premium;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Exception;
use Storage;
use Setting;
use DB;

class PremiumResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $premiums = Premium::orderBy('price' , 'asc')->get();
        //$premiums = Premium::where('is_default_trail','<>',1)->orderBy('price' , 'asc')->get();
        return view('admin.premiums.index', compact('premiums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.premiums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }*/

        $this->validate($request, [
            'plan_name' => 'required',
            'period' => 'required',
            'duration' => 'required',
            'superlike' => 'required',
            'price' => 'required',
            'like_count' => 'required',
        ]);

        try{

            $premium = $request->all();

            //$premium['video_call']=($request->video_call=='') ? 0 : $request->video_call;

            $premium = Premium::create($premium);

            return back()->with('flash_success','Premium Details Saved Successfully');

        } 

        catch (ModelNotFoundException $e) {
            dd(DB::getQuerylog());
            return back()->with('flash_errors', 'Premium Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\premium  $premium
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $premium = Premium::findOrFail($id);
            return view('admin.premiums.premium-details', compact('premium'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\premium  $premium
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $premium = Premium::findOrFail($id);
            return view('admin.premiums.edit',compact('premium'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\premium  $premium
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }*/
        $this->validate($request, [
            'plan_name' => 'required',
            'period' => 'required',
            'duration' => 'required',
            'superlike' => 'required',
            'price' => 'required',
        ]);

        try {

            $premium = Premium::findOrFail($id);

            //$premium->video_call=($request->video_call=='') ? 0 : $request->video_call;

            $premium->plan_name = $request->plan_name;
            $premium->description = $request->description;
            $premium->period = $request->period;
            $premium->duration = $request->duration;
            $premium->price = $request->price;
            $premium->superlike = $request->superlike;
            if($request->has('location_change')){
              $premium->location_change = $request->location_change;  
            }
            if($request->has('likes_me')){
                $premium->likes_me = $request->likes_me;  
            }
            $premium->skip_count = $request->skip_count; 
            $premium->save();

            return redirect()->route('admin.premium.index')->with('flash_success', 'Premium Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_errors', 'Premium Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\premium  $premium
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Premium::find($id)->delete();
            return back()->with('message', 'premium deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_errors', 'premium Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function request($id){

        try{

            $requests = premiumRequests::where('premium_requests.premium_id',$id)
                    ->RequestHistory()
                    ->get();

            return view('admin.request.request-history', compact('requests'));
        }

        catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }

    }

}
