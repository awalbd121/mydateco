<?php

namespace App\Http\Controllers\Resource;

use App\User;
use App\Likes;
use App\UserImages;
use App\UserInterest;
use App\UserPreferences;
use App\UserPremium;
use App\UserRecommend;
use App\UserReport;
use App\Card;
use App\Notifications;
use App\Transactions;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Exception;
use DateTime;
use Storage;
use Setting;
use DB;

class UserResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::with('premium')->orderBy('created_at','desc');
        // if($request->has('premium')){
        //     if($request->premium=='yes'){
        //         $users->has('premium');
        //     }else{
        //         $users->doesnthave('premium');
        //     }
        // }
        if($request->has('q')){
            $users->where('email',$request->q);
            $users->ORwhere('first_name',$request->q);
            $users->ORwhere('last_name',$request->q);
            $users->ORwhere('mobile',$request->q);

        }

        $users = $users->paginate(5);
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }*/

        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|unique:users,email|email|max:255',
            'mobile' => 'required|unique:users,mobile',
            'picture' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'password' => 'required|min:6|confirmed',
        ]);

        try{

            $data = $request->all();

            //$user['payment_mode'] = 'CASH';
            //$user['password'] = bcrypt($request->password);
            if($request->hasFile('picture')) {
                $data['picture']  = Helper::upload_picture($request->picture);
            }

            //$user = User::create($user);
            $from = new DateTime($data['dob']);
            $to   = new DateTime('today');
            $age  = $from->diff($to)->y;
            $install_code=isset($data['install_code'])?$data['install_code']:''; 
            $User = User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'gender' => $data['gender'],
                'dob' => date('m/d/Y',strtotime($data['dob'])),
                'age' => $age,
                'mobile' => $data['mobile'],
                'latitude' => $data['latitude'],
                'longitude' => $data['longitude'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'install_code' => $install_code,
                
            ]);
            $address = $this->getaddress($data['latitude'],$data['longitude']);
            /* $User_location = UserLocation::create([
                'user_id' => $User->id,
                'address' => $address,
                'latitude' => $data['latitude'],
                'longitude' => $data['longitude'],
            ]); */

            $UserPreferences = new UserPreferences;
            $UserPreferences->user_id=$User->id;
            if($data['gender'] == 'male'){
                $UserPreferences->gender='female';  
            }else if($data['gender'] == 'female'){
                $UserPreferences->gender='male';  
            }else{
                $UserPreferences->gender='both';  
            }
            $age_max = $age+5;
            $UserPreferences->age_limit = $age.','.$age_max;
            $UserPreferences->address=json_encode([  ["country" => $address, "latitude" => $data['latitude'], "longitude" => $data['longitude']]  ]);
            
            // dd($UserPreferences);
            // echo '<pre>';print_r($UserPreferences);exit;
            $UserPreferences->save();

            //$invite_code=$data['first_name'].$data['last_name'].$User->id;
            //Generate 5 digit invite code as random
            $invite_code= substr(md5($User->id),0,5);
            $user = User::where('id',$User->id)->update(['invite_code' => $invite_code]);

            return back()->with('flash_success','User Details Saved Successfully');

        } catch (Exception $e) {

            dd($e);
            return back()->with('flash_errors', 'User Not Found');
        }
    }

    public function getaddress($lat,$lng)
    {   
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?key='.Setting::get('map_key','AIzaSyC808NpIO-MZ_C-nXv21zGFDKC1OUm_MkQ').'&latlng='.trim($lat).','.trim($lng).'&sensor=false';
        $json = @file_get_contents($url);
        $data=json_decode($json);
        $status = $data->status;
        if($status=="OK") 
            return $data->results[0]->formatted_address;
        else
            return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::findOrFail($id);
            return view('admin.users.user-details', compact('user'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $user = User::findOrFail($id);
            return view('admin.users.edit',compact('user'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        /*if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }*/

        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'mobile' => '',
            'picture' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
        ]);

        try {

            $user = User::findOrFail($id);

            if($request->hasFile('picture')) {
                $user->picture  = Helper::upload_picture($request->picture);
            }

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->mobile = $request->mobile;
            $user->save();

            return redirect()->route('admin.user.index')->with('flash_success', 'User Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_errors', 'User Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::enableQueryLog();
            $user=User::find($id)->delete();
            // if($user){

            //     $user->email=$user->email.'-'.uniqid();  
            //     $user->username=$user->username.'-'.uniqid();
            //     $user->mobile=$user->mobile.'-'.uniqid();
            //     $user->save();
            // }
            
            Likes::where('user_id',$id)->orwhere('like_id',$id)->delete();
            UserInterest::where('user_id',$id)->delete();
            UserImages::where('user_id',$id)->delete();
            UserPreferences::where('user_id',$id)->delete();
            UserPremium::where('user_id',$id)->delete();
            UserReport::where('user_id',$id)->orwhere('report_id',$id)->delete();
            UserRecommend::where('user_id',$id)->orwhere('recommend_id',$id)->orwhere('friend_id',$id)->delete();
            Card::where('user_id',$id)->delete();
            Notifications::where('user_id',$id)->orwhere('notifier_id',$id)->delete();
            Transactions::where('user_id',$id)->orwhere('receiver_id',$id)->delete();            

            return back()->with('flash_success', 'User deleted successfully');
        } 
        catch (ModelNotFoundException $e) {
            dd(DB::getQueryLog());
            return back()->with('flash_errors', 'User Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function request($id){

        try{

            $requests = UserRequests::where('user_requests.user_id',$id)
                    ->RequestHistory()
                    ->get();

            return view('admin.request.request-history', compact('requests'));
        }

        catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }

    }

}
