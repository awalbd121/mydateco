<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Exception;
use Log;
use Setting;

class SendPushNotification extends Controller
{
	/**
     * New Ride Accepted by a Driver.
     *
     * @return void
     */
    public function RideAccepted($request){

    	return $this->sendPushToUser($request->user_id, trans('api.push.request_accepted'));
    }

    /**
     * Driver Arrived at your location.
     *
     * @return void
     */
    public function user_schedule($user){

        return $this->sendPushToUser($user, trans('api.push.schedule_start'));
    }

    /**
     * New Ride Accepted by a Driver.
     *
     * @return void
     */
    public function ProviderCancellRide($request){

        return $this->sendPushToUser($request->user_id, trans('api.push.provider_cancelled'));
    }

    /**
     * Driver Arrived at your location.
     *
     * @return void
     */
    public function Arrived($request){

        return $this->sendPushToUser($request->user_id, trans('api.push.arrived'));
    }

     /**
     * Driver Arrived at your location.
     *
     * @return void
     */
    public function Dropped($request){

        Log::info( trans('api.push.dropped').Setting::get('currency').$request->payment->total.' by '.$request->payment_mode);

        return $this->sendPushToUser($request->user_id, trans('api.push.dropped').Setting::get('currency').$request->payment->total.' by '.$request->payment_mode);
    }

    /**
     * Money added to user wallet.
     *
     * @return void
     */
    public function ProviderNotAvailable($user_id){

        return $this->sendPushToUser($user_id,trans('api.push.provider_not_available'));
    }


    /**
     * Money added to user wallet.
     *
     * @return void
     */
    public function WalletMoney($user_id, $money){

        return $this->sendPushToUser($user_id, $money.' '.trans('api.push.added_money_to_wallet'));
    }

    /**
     * Money charged from user wallet.
     *
     * @return void
     */
    public function ChargedWalletMoney($user_id, $money){

        return $this->sendPushToUser($user_id, $money.' '.trans('api.push.charged_from_wallet'));
    }

    /**
     * Sending Push to a user Device.
     *
     * @return void
     */
    public function sendPushToUser($user_id, $push_message){

    	try{

	    	$user = User::findOrFail($user_id);

            if($user->device_token != ""){

                \Log::info('sending push for user : '. $user->first_name ." - ".$user->device_token );

    	    	if($user->device_type == 'ios'){

    	    		return \PushNotification::app('IOSUser')
    		            ->to($user->device_token)
    		            ->send($push_message);

    	    	}elseif($user->device_type == 'android'){
    	    		
    	    		$push = \PushNotification::app('AndroidUser')
    		            ->to($user->device_token)
    		            ->send($push_message);
                    \Log::info('push : '. $push );
                    return $push;

    	    	}
            }

    	} catch(Exception $e){
    		return $e;
    	}

    }
    public function testpush(){
        $this->sendPushToUser(41,"Developer testing Message");

    }

    

    public function sendPushToUserVideo($provider_id, $push_message,$username,$roomname,$video=null){

        try{

            $user = User::findOrFail($provider_id);

            if($user->device_token != ""){

                \Log::info('sending push for user : '. $user->first_name);

                if($user->device_type == 'ios'){

                    $message = \PushNotification::Message($push_message,array(
                   'badge' => 1,
                   'push_handle' => "custom",
                   'sound' => 'example.aiff',
                   'actionLocKey' => $username,
                   'title' => 'video call',
                   'locKey' => 'localized key',
                   'locArgs' => array(
                       'localized args',
                       'localized args'
                   ),
                   'launchImage' => 'image.jpg','custom' => array('name' => $username,'room_id'=>$roomname)));                                                                  
                    return \PushNotification::app('IOSProvider')
                       ->to($provider->token)
                       ->send($message);

                    /*return \PushNotification::app('IOSUser')
                        ->to($user->device_token)
                        ->send($push_message,array('name' => $username,'room_id'=>$roomname));*/

                }elseif($user->device_type == 'android'){
                    
                    return \PushNotification::app('AndroidUser')
                        ->to($user->device_token)
                        ->send($push_message,array('name' => $username,'room_id'=>$roomname,'video'=>$video));

                }
            }

        } catch(Exception $e){
            return $e;
        }

    }

}
