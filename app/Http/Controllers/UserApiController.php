<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Collection;

use DB;
use Log;
use Auth;
use Hash;
use Storage;
use Setting;
use Exception;
use Notification;
use DateTime;
use substr;

use Carbon\Carbon;
use App\Http\Controllers\SendPushNotification;
use App\Notifications\ResetPasswordOTP;


use App\User;
use App\Likes;
use App\UserPreferences;
use App\UserImages;
use App\Transactions;
use App\Interest;
use App\UserInterest;
use App\Card;
use App\UserReport;
use App\Notifications;
use App\Country;
use App\UserRecommend;
use App\Premium;
use App\UserPremium;
use App\UserBlocked;
use App\UserLocation;
use App\Settings;
use App\AdminWallet;
use App\Subscription;

class UserApiController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function register(Request $request)
    {
        
        $this->validate($request, [
                'social_unique_id' => 'nullable|required_if:login_by,facebook,google','unique:users',
                'device_type' => 'required|in:android,ios',
                'device_token' => 'required',
                'device_id' => 'required',
                'login_by' => 'required|in:manual,facebook,google',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',                
                'mobile' => 'required|unique:users',
                'latitude' => 'nullable|numeric',
                'longitude' => 'nullable|numeric',
                'password' => 'required|min:6',
                'dob' => 'required|nullable',
                'install_code' => 'nullable',
                'country_id' => 'required',
            ]);
        //DB::enableQueryLog();
        $from = new DateTime($request['dob']);
        $to   = new DateTime('today');
        $age  = $from->diff($to)->y;

        try{           
            $User = $request->all();     
            $country_code = trim($request->country_code); 
            $mobile = trim($request->mobile);
            $User['password'] = bcrypt($request->password);
            $User['age'] = $age;
            $User['mobile'] = $country_code.$mobile;
            $User['country'] = $request->country_id;
            $User = User::create($User);
            $UserPreferences = new UserPreferences;
            $UserPreferences->user_id=$User->id;
            if($request->gender == 'male' || $request->gender == 'Male'){
                $UserPreferences->gender='female';  
            }else if($request->gender == 'female' || $request->gender == 'Female'){
                $UserPreferences->gender='male';  
            }else{
                $UserPreferences->gender='both';  
            }
            $age_max = $age+5;
            $UserPreferences->age_limit = $age.','.$age_max;
           // $UserPreferences->gender='both';
            $UserPreferences->distance = 1000;
            $UserPreferences->is_country_wise = 1;
            $UserPreferences->save();

            $userplan = new UserPremium();
            $userplan->user_id = $User->id;
            $userplan->premia_id = 1;
            $userplan->status = "0";
            $userplan->reset_date = date('Y-m-d H:i:s');
            $userplan->created_at = date('Y-m-d H:i:s');
            $userplan->save();

            $invite_code= substr(md5($User->id),0,5);
            $user = User::where('id',$User->id)->update(['invite_code' => $invite_code]);

            $userToken = $User->createToken($User->id)->accessToken;
            $refresh_token = $User->refreshToken;

                //dd($userToken);
            return response()->json([
                    "status" => true,
                    "token_type" => "Bearer",
                    "access_token" => $userToken,
                    "refresh_token" => $refresh_token,
                    //"expires_in" => '',
                    "user" =>$User
                ]);

            //return $User;
        } catch (ModelNotFoundException $e) {
             //dd(DB::getQueryLog());   
             return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function logout(Request $request)
    {
        try {
            //DB::enableQueryLog();
             $user_id=Auth::id();
            User::where('id',$user_id)->update(['device_id'=> '', 'device_token' => '']);
            //$request->user()->token()->revoke();

            return response()->json(['message' => "logout_success"]);
        } catch (ModelNotFoundException $e) {
            //dd(DB::getQueryLog());
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    public function country_list() {
    
        $countries = DB::table('countries')->select('id','name')->get();

        if(count($countries)>0)
          return response()->json(['countries' => $countries],200);
        else
          return response()->json(['message' => 'No country list'],422);
    }

    /**
     * Forgot Password.
     *
     * @return \Illuminate\Http\Response
     */

    public function forgotPassword(Request $request){

        $this->validate($request, [
                'email' => 'required|email|exists:users,email',
            ]);

        try{  
            
            $user = User::where('email' , $request->email)->first();

            $otp = mt_rand(100000, 999999);

            $user->password=bcrypt($otp);

            $user->save();
            Notification::send($user, new ResetPasswordOTP($otp));

            //$this->sendResetLinkEmail($request);

            return response()->json([
                'message' => 'New password sent to your email!',
                'otp' => $otp,
                'user' => $user              
            ]);

        }catch(Exception $e){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    public function forgotPasswordAPI(Request $request){

        $this->validate($request, [
                'email' => 'required|email|exists:users,email',
            ]);

        try{  
            
            DB::enableQueryLog();
            $user = User::where('email' , $request->email)->first();
            $otp = mt_rand(100000, 999999);
            $user->otp=$otp;

            $user->save();

            Notification::send($user, new ResetPasswordOTP($otp));

            //$this->sendResetLinkEmail($request);

            return response()->json([
                'message' => 'New password sent to your email!',
                'otp' => $otp,
                'email' => $request->email              
            ]);

        }catch(ModelNotFoundException $e){
                dd(DB::getQueryLog());
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }
    /**
     * Reset Password.
     *
     * @return \Illuminate\Http\Response
     */

    public function reset_password(Request $request){

        $this->validate($request, [
                'password' => 'required|confirmed|min:6',
                'otp' => 'required|numeric|exists:users,otp',
                'email' => 'required|email|exists:users,email',
            ]);

        try{
            $User = User::where('email',$request->email)->first();
            $User->password = bcrypt($request->password);
            $User->otp =0;
            $User->save();

            if($request->ajax()) {
                return response()->json(['message' => 'password_updated']);
            }else{
                return back()->with('flash_success', 'Password Updated');
            }

        }catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }
        }
    }

    /**
     * Change password.
     *
     * @return \Illuminate\Http\Response
     */

    public function changePassword(Request $request){

        $this->validate($request, [
                'password' => 'required|confirmed|min:6',
                'old_password' => 'required',
                'email' => 'required|email|exists:users,email',
            ]);

        $User = User::where('email',$request->email)->first();

        if(Hash::check($request->old_password, $User->password))
        {
            $User->password = bcrypt($request->password);
            $User->save();

            if($request->ajax()) {
                return response()->json(['message' => 'password_updated']);
            }else{
                return back()->with('flash_success', trans('user.alert.password_updated'));
            }

        } else {
            if($request->ajax()) {
                return response()->json(['error' => trans('user.alert.verify_password')]);
            }else{
                return back()->with('flash_error',trans('user.alert.verify_password'));
            }
        }

    }  
    /**
     * Edit the Profile dashboard.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function getProfile(Request $request)
    {
        $user_id=Auth::user()->id;
        try{
            if($request->all()){
                $user_updated=User::find($user_id);
                $user_preferences = UserPreferences::where('user_id' , $user_id)->first();
                
                if($request->has('device_id')){
                    $user_updated->device_id = $request->device_id;
                }if($request->has('device_type')){
                    $user_updated->device_type = $request->device_type;
                }if($request->has('device_token')){
                    $user_updated->device_token = $request->device_token;
                }if($request->has('latitude')){
                    $user_updated->latitude = $request->latitude;
                    $user_updated->longitude = $request->longitude;

                    $user_preferences->latitude = $request->latitude;
                    $user_preferences->longitude = $request->longitude;
                }if($request->has('address')){
                    $user_updated->address = $request->address;
                    $user_preferences->address = $request->address;
                }
                if ($request->has('language'))
                $user_updated->language = $request->language;

                $user_preferences->save();
                $user_updated->save();
            }            
            
            $user=User::with('user_images','user_interest.interest')->find($user_id);
            $interest=Interest::get();
            $user_premium=UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();
            $user['user_premium'] = $user_premium; 
            $user['user_country'] = Country::where('id',Auth::user()->country)->first();
            $data['user']=$user;
            $data['interest']=$interest; 
            $data['user_premium']=$user_premium; 
            if(count($data['user_premium']) > 0 && $data['user_premium']->premium->location_change == 1){
                $user['premium'] = 1;
                $data['location_change']=1;
            }else{
                $user['premium'] = 0;
                $data['location_change']=0;
            }   
   
            if($request->ajax()){
                return response()->json(['user' => $user,'interest' => $interest,'currency'=>Setting::get('currency','$'),'app_in_review'=>(int)Setting::get('review')]);
            }
            else{
               return $data; 
            }
        } catch (Exception $e) {
            if($request->ajax()){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something went wrong');
            }
        }  
        
    }   

    public function send_otp(Request $request)
    {
        $this->validate($request, [
                
            'mobile' => 'required|numeric',
            'country_code' => 'required',
                
        ]);
        
        try {
            $mobile = $request->country_code.$request->mobile;
            $is_mobile_exist = User::where('mobile',$mobile)->first();
            if(count($is_mobile_exist)>0) {

                return response()->json(['error' => 'Mobile Number Already Exist'], 422); 

            } else {

                $mobileno = $request->country_code.$request->mobile;
                $otp = $this->otp_generate();
                // $otp = 1234;

                // sendsms($mobileno,$otp);
                send_twilio_sms($mobileno,$otp);
                return response()->json(['otp' => $otp,'message' => 'OTP Sent']);  
            }  
        }
        catch (Exception $e) {

            dd($e);
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }     
    }

    public function user_email_verify(Request $request)
    {
        $this->validate($request, [
                
            'email' => 'required',
        ]);
        
        $email = $request->email;
        $is_email_exist = User::where('email',$email)->first();
        if(count($is_email_exist)>0) {

            return response()->json(['error' => 'Email Already Exist'], 422); 

        } else {
            
            return response()->json(['message' => 'Success'],200);  
        }
    }

    public function otp_generate()
    {
        $otp = mt_rand(1000, 9999);
    
        $count = User::where('otp',$otp)->count();
        if($count!=0)
        {
           $otp = $this->otp_generate();
        }

        return $otp;
    }
    /**
     * Update profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)   {

        //$picture = $request->file('picture');
        //dd($request->all());
        $this->validate($request, [
            'picture' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'image1' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'image2' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'image3' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'image4' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'image5' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'bio_video' => 'mimetypes:video/x-msvideo,video/x-ms-wmv,video/quicktime,video/mp4,video/3gpp|max:8388608',
            //'bio_video' => 'mimes:mp4,avi,wmv,mov',
        ]);
        if($request->has('username')){
            $this->validate($request, [
            'username' => 'unique:users,username',
            ]);
        }

        $user_id=Auth::user()->id;

    try {
            $user = User::with('user_images','user_interest.interest')->findOrFail($user_id);
            if(Auth::user()->dob){
                $from = new DateTime(Auth::user()->dob);
                $to   = new DateTime('today');
                $age1  = $from->diff($to)->y;
                $user->age = $age1;
            }

            if($request->has('username')){
                $user->username = $request->username;
            }if($request->has('first_name')){
                $user->first_name = $request->first_name;
            }if($request->has('last_name')){
                $user->last_name = $request->last_name;
            }if($request->has('dob')){
                $from = new DateTime($request->dob);
                $to   = new DateTime('today');
                $age  = $from->diff($to)->y;

                $user->dob = $request->dob;
                $user->age = $age;
            }if($request->has('mobile')){
                $user->mobile = $request->country_code.$request->mobile;
            }if($request->has('gender')){
                $user->gender = $request->gender;
            }if($request->has('about')){
                $user->about = $request->about;
            }if($request->has('work')){
                $user->work = $request->work;
            }if($request->show_age=='on'){
                $user->show_age =1;
            }else{
                $user->show_age =0;
            }if($request->show_distance=='on'){
                $user->show_distance =1;
            }else{
                $user->show_distance =0;
            }if($request->has('device_type')){
                $user->device_type = $request->device_type;
            }if($request->has('device_token')){
                $user->device_token = $request->device_token;
            }if($request->has('latitude')){
                $user->latitude = $request->latitude;
                $user->longitude = $request->longitude;

                $user_preferences = UserPreferences::where('user_id' , $user_id)->first();
                $user_preferences->latitude = $request->latitude;
                $user_preferences->longitude = $request->longitude;
                $user_preferences->address = $request->address;
                $user_preferences->save();

            }if($request->has('address')){
                $user->address = $request->address;
            }
            if($request->usr_interest == "" || $request->usr_interest != "") {

                UserInterest::where('user_id',$user_id)->delete();
            }
            if($request->has('interest')){    
            //dd($request->interest);            
                $user_interest=UserInterest::where('user_id',$user_id);
                if($user_interest->count() > 0) {
                    $user_interest->delete();
                }
                foreach ($request->interest as $interest) {
                    $user_interest = new UserInterest;
                    $user_interest->user_id=$user_id;
                    $user_interest->interest_id=$interest;
                    $user_interest->save();
                }               

            }
            if ($request->has('country_id'))
                $user->country = $request->country_id;
            if ($request->has('language'))
                $user->language = $request->language;

            if($request->file('picture')!=''){
                $file_name = time();
                $file_name .= rand();
                $ext = $request->file('picture')->getClientOriginalExtension();
                $request->file('picture')->move(public_path() . "/uploads", $file_name . "." . $ext);
                $local_url = $file_name . "." . $ext;
                $user->picture = url('/uploads/' . $local_url);
            }
            if($request->file('bio_video')!=''){
                $video_name = time();
                $video_name .= rand();
                $ext = $request->file('bio_video')->getClientOriginalExtension();
                $request->file('bio_video')->move(public_path() . "/uploads", $video_name . "." . $ext);
                $local_url = $video_name . "." . $ext;
                $user->bio_video = url('/uploads/' . $local_url);
            }           

            $user->save();   
            //dd($request->file('image1'));
            //User Images1
            if($request->file('image1') && $request->file('image1')!=''){

                $user_images = UserImages::where('user_id' , $user_id)->where('status',1);

                if($user_images->count() == 0) {
                    $user_images = new UserImages;
                    $user_images->user_id=$user_id;
                    $user_images->status=1;
                    $file_name = time();
                    $file_name .= rand();
                    $ext = $request->file('image1')->getClientOriginalExtension();
                    $request->file('image1')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;
                    $user_images->image = url('/uploads/' . $local_url);
                }else{
                    $user_images = $user_images->first();
                    $user_images->user_id=$user_id;
                    $user_images->status=1;
                    $file_name = time();
                    $file_name .= rand();
                    $ext = $request->file('image1')->getClientOriginalExtension();
                    $request->file('image1')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;
                    $user_images->image = url('/uploads/' . $local_url);
                }
                $user_images->save();

            }

            //User Images2
            if($request->file('image2') && $request->file('image2')!=''){
                $user_images = UserImages::where('user_id' , $user_id)->where('status',2);

                if($user_images->count() == 0) {
                    $user_images = new UserImages;
                    $user_images->user_id=$user_id;
                    $user_images->status=2;
                    $file_name = time();
                    $file_name .= rand();
                    $ext = $request->file('image2')->getClientOriginalExtension();
                    $request->file('image2')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;
                    $user_images->image = url('/uploads/' . $local_url);
                }else{
                    $user_images = $user_images->first();
                    $user_images->user_id=$user_id;
                    $user_images->status=2;
                    $file_name = time();
                    $file_name .= rand();
                    $ext = $request->file('image2')->getClientOriginalExtension();
                    $request->file('image2')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;
                    $user_images->image = url('/uploads/' . $local_url);
                }
                $user_images->save();

            }

            //User Images3
            if($request->file('image3') && $request->file('image3')!=''){
                $user_images = UserImages::where('user_id' , $user_id)->where('status',3);

                if($user_images->count() == 0) {
                    $user_images = new UserImages;
                    $user_images->user_id=$user_id;
                    $user_images->status=3;
                    $file_name = time();
                    $file_name .= rand();
                    $ext = $request->file('image3')->getClientOriginalExtension();
                    $request->file('image3')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;
                    $user_images->image = url('/uploads/' . $local_url);
                }else{
                    $user_images = $user_images->first();
                    $user_images->user_id=$user_id;
                    $user_images->status=3;
                    $file_name = time();
                    $file_name .= rand();
                    $ext = $request->file('image3')->getClientOriginalExtension();
                    $request->file('image3')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;
                    $user_images->image = url('/uploads/' . $local_url);
                }
                $user_images->save();

            }

            //User Images4
            if($request->file('image4') && $request->file('image4')!=''){
                $user_images = UserImages::where('user_id' , $user_id)->where('status',4);

                if($user_images->count() == 0) {
                    $user_images = new UserImages;
                    $user_images->user_id=$user_id;
                    $user_images->status=4;
                    $file_name = time();
                    $file_name .= rand();
                    $ext = $request->file('image4')->getClientOriginalExtension();
                    $request->file('image4')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;
                    $user_images->image = url('/uploads/' . $local_url);
                }else{
                    $user_images = $user_images->first();
                    $user_images->user_id=$user_id;
                    $user_images->status=4;
                    $file_name = time();
                    $file_name .= rand();
                    $ext = $request->file('image4')->getClientOriginalExtension();
                    $request->file('image4')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;
                    $user_images->image = url('/uploads/' . $local_url);
                }
                $user_images->save();

            }

            //User Images5
            if($request->file('image5') && $request->file('image5')!=''){
                $user_images = UserImages::where('user_id' , $user_id)->where('status',5);

                if($user_images->count() == 0) {
                    $user_images = new UserImages;
                    $user_images->user_id=$user_id;
                    $user_images->status=5;
                    $file_name = time();
                    $file_name .= rand();
                    $ext = $request->file('image5')->getClientOriginalExtension();
                    $request->file('image5')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;
                    $user_images->image = url('/uploads/' . $local_url);
                }else{
                    $user_images = $user_images->first();
                    $user_images->user_id=$user_id;
                    $user_images->status=5;
                    $file_name = time();
                    $file_name .= rand();
                    $ext = $request->file('image5')->getClientOriginalExtension();
                    $request->file('image5')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;
                    $user_images->image = url('/uploads/' . $local_url);
                }
                $user_images->save();              

            } 

            if($request->del1 == 1) {
                UserImages::whereuser_id(Auth::user()->id)->where('status',1)->delete();
            }
            if($request->del2 == 2) {
                UserImages::whereuser_id(Auth::user()->id)->where('status',2)->delete();
            }
            if($request->del3 == 3) {
                UserImages::whereuser_id(Auth::user()->id)->where('status',3)->delete();
            }
            if($request->del4 == 4) {
                UserImages::whereuser_id(Auth::user()->id)->where('status',4)->delete();
            }
            if($request->del5 == 5) {
                UserImages::whereuser_id(Auth::user()->id)->where('status',5)->delete();
            }

            $user = User::with('user_images','user_interest.interest')->findOrFail($user_id); 
            $interest=Interest::get();

            if($request->has('country_id'))
                $user['user_country'] = Country::where('id',$request->country_id)->first();
            else
                 $user['user_country'] = Country::where('id',Auth::user()->country)->first();

            if($request->ajax()) {
              return response()->json(['user' => $user,'interest' => $interest]);
            }else{
              return redirect('/profile')->with('flash_success', 'Profile updated successfully.');
            }
        }

        catch(ModelNotFoundException $e) 
        {
             dd(DB::getQueryLog()); 
            if($request->ajax()) {
               return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
               return back()->with('flash_error', 'Something went wrong while sending request. Please try again.');
            }
        }    
    }
    /**
     * Show the Discovery Setting dashboard.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function get_discover_settings(Request $request)   {
        try{
            DB::enableQueryLog();
            $User_preferences= UserPreferences::where('user_id', Auth::user()->id)->first();
            $User_preferences['user_locations']= UserPreferences::where('user_id', Auth::user()->id)->get();
            $user_premium=UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();

            if(count($user_premium) > 0 && $user_premium->premium->location_change == 1){
                $User_preferences['premium_location'] = 1;
            }else{
                $User_preferences['premium_location'] = 0;
            }
            if($request->ajax()) {
              return response()->json($User_preferences);
            }else{
              return $User_preferences;
            }
        } catch (ModelNotFoundException $e) {
            if($request->ajax()){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something went wrong');
            }
        }        
    }

    /**
     * Update Discover Settings.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Update Discover Settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_discover_settings(Request $request)   { 
        $this->validate($request, [
           'gender' => 'required',
           'age_limit' => 'required',
           // 'distance' => 'required'
        ]);

        $user_id=Auth::user()->id;
        //dd($request->all());
    try {
        if($request->has('location')){  

            // UserLocation::where('user_id',$user_id)->delete();
            // foreach($request->location as $key=>$value){
            //    $val_data[] = explode("&&",$value);
            // }
            // foreach($val_data as $lkey=>$loc_data){
            //     UserLocation::create(['user_id'=>$user_id,'address' => $loc_data[0], 'latitude' => $loc_data[1],'longitude'=>$loc_data[2]]
            //     );
            // }
            if(isset($request->notification_match) && ($request->notification_match=='on')){
                $notification_match =1;
            }else{
                $notification_match =0;
            }
            if(isset($request->dnd) && ($request->dnd=='on')){
                $dnd =1;
            }else{
                $dnd =0;
            }

            $UserPreferences = UserPreferences::where('user_id',$user_id)->first();
            //if(count($UserPreferences)>0){

                foreach($request['location'] as $location)
                {
                    $data = explode("&&",$location);
                    $val_data[] = ["country" => $data[0], "latitude" => number_format($data[1],7), "longitude" => number_format($data[2],7), 'id' =>'1'];
                }
               /* $UserPreferences->user_id = Auth::user()->id;
                $UserPreferences->gender = $request->gender;
                $UserPreferences->age_limit = $request->age_limit;
                $UserPreferences->distance = $request->distance;
                $UserPreferences->address = json_encode($val_data);

                $location = $request['location_value'];
                $selected_location_value= explode("&&",$location);
                $latitude = $request->latitude;
                $longitude = $request->longitude;

                if(count($selected_location_value)>2){
                $user_country_prefernces =  UserPreferences::where('user_id',$user_id)->orderBy('id','desc')->first();
                 $data = count($user_country_prefernces) > 0 ? $user_country_prefernces->address : [];

                 $new = new \stdClass();
                 $new->country = $latitude;
                 $new->latitude = $latitude;
                 $new->longitude = $longitude;
                 $new->status = 1;
                 $entries = [];
                 foreach($data as $key => $entry) {
                     $entry->status = 0;
                    if($entry->latitude == $new->latitude && $entry->longitude == $new->longitude) {
                        unset($data[$key]);
                        $entry->status = 1;
                    }

                    
                    $entry->id = $key + 1;
                    $entries[] = $entry;
                 }
                 $new->id = count($entries) + 1;
                 $entries[] = $new;
                 $pref = UserPreferences::where('user_id',$user_id)->first();
                 $pref->address = json_encode($data);
                 $pref->save();
                 return $pref;
                UserPreferences::where('user_id',$user_id)->orderBy('id','desc')->update(array('latitude' => $selected_location_value[1],'longitude' => $selected_location_value[2],'status'=>1));
                 User::where('id',$user_id)->update(array('latitude' => $selected_location_value[1],'longitude' => $selected_location_value[2]));
                 

            }

            }*/
            
            /*UserPreferences::create(['user_id'=>Auth::user()->id,
                                    'gender'=>$request->gender,
                                    'age_limit'=>$request->age_limit,
                                    'distance'=>$request->distance,
                                    'address' => json_encode($val_data)
            ]);*/
                $UserPreferences->user_id = Auth::user()->id;
                $UserPreferences->gender = $request->gender;
                $UserPreferences->age_limit = $request->age_limit;
                $UserPreferences->distance = $request->distance;
                if(!empty($request->latitude) && !empty($request->longitude)) {

                    $UserPreferences->latitude = $request->latitude[0];
                    $UserPreferences->longitude = $request->longitude[0];
                }
                $UserPreferences->dnd = $dnd;
                $UserPreferences->notification_match = $notification_match;
                $UserPreferences->is_country_wise = $request->country_wise;
                $UserPreferences->address = json_encode($val_data);
                $UserPreferences->save();

                if(!empty($request->latitude) && !empty($request->longitude)) {

                   User::where('id',Auth::user()->id)->update(['latitude' => $request->latitude[0],'longitude' => $request->longitude[0]]);
                }
            //For Save selected location lat and long..
            $location = $request['location_value'];
            $selected_location_value= explode("&&",$location);
            $latitude = $request->latitude;
            $longitude = $request->longitude;
          
            if(count($selected_location_value)>2){
                $user_country_prefernces =  UserPreferences::where('user_id',$user_id)->orderBy('id','desc')->first();
                 $data = count($user_country_prefernces) > 0 ? $user_country_prefernces->address : [];

                 $new = new \stdClass();
                 $new->country = $latitude;
                 $new->latitude = $latitude;
                 $new->longitude = $longitude;
                 $new->status = 1;
                 $entries = [];
                 foreach($data as $key => $entry) {
                     
                    if($entry->latitude == $new->latitude && $entry->longitude == $new->longitude) {
                        unset($data[$key]);
                    }

                    $entry->status = 0;
                    $entry->id = $key + 1;
                    $entries[] = $entry;
                 }
                 $new->id = count($entries) + 1;
                 $entries[] = $new;
                if(isset($request->notification_match) && ($request->notification_match=='on')){
                    $notification_match =1;
                }else{
                    $notification_match =0;
                }
                if(isset($request->dnd) && ($request->dnd=='on')){
                    $dnd =1;
                }else{
                    $dnd =0;
                }
                $pref = UserPreferences::where('user_id',$user_id)->first();
                $pref->notification_match = $notification_match;
                $pref->dnd = $dnd;
                $pref->is_country_wise = $request->country_wise;
                $pref->address = json_encode($data);
                $pref->save();
                 
                UserPreferences::where('user_id',$user_id)->orderBy('id','desc')->update(array('latitude' => $selected_location_value[1],'longitude' => $selected_location_value[2],'status'=>1));
                User::where('id',$user_id)->update(array('latitude' => $selected_location_value[1],'longitude' => $selected_location_value[2]));
                 

            }
        }
        elseif($request->has('country')){

            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $country = $request->country;

            $user_country_prefernces =  UserPreferences::where('user_id',$request->user_id)->orderBy('id','desc')->first();
            $data = count($user_country_prefernces) > 0 ? $user_country_prefernces->address : [];
                 $new = new \stdClass();
                 $new->country = $country;
                 $new->latitude = $latitude;
                 $new->longitude = $longitude;
                 $new->id = date('ymdHisu');
                 $new->status = 1;
                 $entries = [];
                 foreach($data as $key => $entry) {
                    if($entry->latitude == $new->latitude && $entry->longitude == $new->longitude && $entry->country == $new->country
                    && $entry->id == $new->id) {
                        unset($data[$key]);
                    }
                    $entry->status = 0;
                    $entry->id = $key + 1;
                    $entries[] = $entry;
                 }
                 if(isset($request->notification_match) && ($request->notification_match=='on')){
                    $notification_match =1;
                 }else{
                    $notification_match =0;
                 }
                 if(isset($request->dnd) && ($request->dnd=='on')){
                    $dnd =1;
                 }else{
                    $dnd =0;
                 }
                 $new->id = count($entries) + 1;
                 $entries[] = $new;
                 $pref = UserPreferences::where('user_id',$request->user_id)->first();
                 $pref->address = json_encode($entries);
                 $pref->dnd = $dnd;
                 $pref->notification_match = $notification_match;
                 $pref->is_country_wise = $request->country_wise;
                 $pref->save();
        }
        elseif($request->has('address')){

            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $country = $request->address;
            $is_country_wise = $request->country_wise;

            $user_country_prefernces =  UserPreferences::where('user_id',$user_id)->orderBy('id','desc')->first();
            $data = count($user_country_prefernces->address) > 0 ? $user_country_prefernces->address : [];
                 $new = new \stdClass();
                 $new->country = $country;
                 $new->latitude = $latitude;
                 $new->longitude = $longitude;
                 $new->id = date('ymdHisu');
                 $new->status = 1;
                 $entries = [];
                 foreach($data as $key => $entry) {
                    if($entry->latitude == $new->latitude && $entry->longitude == $new->longitude && $entry->country == $new->country
                    && $entry->id == $new->id) {
                        unset($data[$key]);
                    }
                    $entry->status = 0;
                    $entry->id = $key + 1;
                    $entries[] = $entry;
                 }
                 $new->id = count($entries) + 1;
                 $entries[] = $new;
                 $pref = UserPreferences::where('user_id',$user_id)->first();
                 $pref->address = json_encode($entries);
                 $pref->latitude = $request->latitude;
                 $pref->longitude = $request->longitude;
                 $pref->is_country_wise = $is_country_wise;
                 $pref->gender = $request->gender;
                 $pref->distance = $request->distance;

                if(isset($request->notification_match) && ($request->notification_match=='on')){
                    $notification_match =1;
                }else{
                    $notification_match =0;
                }
                $pref->notification_match = $notification_match;
                if(isset($request->notification_message) && ($request->notification_message=='on')){
                    $notification_message =1;
                }else{
                    $notification_message =0;
                }
                $pref->notification_message = $notification_message;
                if(isset($request->dnd) && ($request->dnd=='on')){
                    $dnd ='1';
                }else{
                    $dnd ='0';
                }
                $pref->dnd = $dnd;
                $pref->age_limit = $request->age_limit;
                $pref->status = 1;
                $pref->save();

                User::where('id',$user_id)->update(array('latitude' => $request->latitude,'longitude' => $request->longitude));
        }
        else{
            if(isset($request->notification_match) && ($request->notification_match=='on')){
                $notification_match =1;
            }else{
                $notification_match =0;
            }

            if(isset($request->notification_message) && ($request->notification_message=='on')){
                $notification_message =1;
            }else{
                $notification_message =0;
            }

            if(isset($request->dnd) && ($request->dnd=='on')){
                $dnd ='1';
            }else{
                $dnd ='0';
            }
            $addresss='';
            $is_country_wise = $request->country_wise;
            if($request->has('address')){
               $entries = [];
                $new = new \stdClass();
                 $new->country = $request->address;
                 $new->latitude = $request->latitude;
                 $new->longitude = $request->longitude;
                 $new->id = date('ymdHisu');
                 $new->status = 1;
                $entries[] = $new; 
                $addresss = json_encode($entries); 
            }
            UserPreferences::where('user_id',$user_id)->update(array('age_limit'=>$request->age_limit,'gender'=>$request->gender,'dnd'=>$dnd,'notification_match'=>$notification_match,'notification_message'=>$notification_message, 'address' => $addresss,'distance' => $request->distance,'is_country_wise' => $is_country_wise));
        }
            $user_preferences = UserPreferences::where('user_id',$user_id)
                            ->orderBy('id','desc')
                            ->first();
           
            if($request->ajax()) {
              return response()->json($user_preferences);
            }else{
              //   return back()->with('flash_success', 'Setting updated successfully.');
                return redirect('/dashboard')->with('flash_success', 'Setting updated successfully.');
            }
        }

        catch(ModelNotFoundException $e) 
        {   
            dd($e);
            if($request->ajax()) {
               return response()->json(['message' => trans('api.something_went_wrong')], 500);
            }else{
               return back()->with('flash_error', 'Something went wrong while sending request. Please try again.');
            }
        }    
    }
    /**
     * 
       *Delete the country details
     */
    public function deletecountry(Request $request)
    {
        $id = $request->id;
        $user_country_prefernces =  UserPreferences::where('user_id',$request->user_id)->orderBy('id','desc')->first();
        $data = count($user_country_prefernces) > 0 ? $user_country_prefernces->address : [];   
        $entries = []; 
            foreach($data as $entry) {
                if($entry->id != $id) {
                    $entries[] = $entry;
                }
            } 
            //  $entries[] = $new;
            //  $pref = UserPreferences::where('user_id',$request->user_id)->first();
            $pref = UserPreferences::where('user_id',$request->user_id)->orderBy('id','desc')->first();
            $pref->address = json_encode($entries);
            $pref->save();
            if($request->ajax()) {
                return response()->json($pref->address);
            }
    }
    
    /**
     * Show the likes.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function likes(Request $request)
    {
      $this->validate($request, [
           'like_id' => 'required',
           'status' => 'required',
        ]);

        $user_id=Auth::user()->id;
        $message='';
        $message='';

      try{
        $notifications=new Notifications;
        $notifications->user_id=$user_id;
        $notifications->notifier_id=$request->like_id;

        if($request->who_likes){

            $like=Likes::with('user')->where('user_id',$request->like_id)->where('like_id',$user_id)->first();
            $like->status = $request->status;  
            $like->save();

            $user = User::where('id',$request->like_id)->first();
            if($request->status=='2'){
               $notifications->status='friend';

               Notifications::where('user_id',$request->like_id)->where('notifier_id',$user_id)->update(['status' => 'friend']);

               (new SendPushNotification)->sendPushToUser($request->like_id, ''.$user->first_name.' '.$user->last_name.' got a match with '.Auth::user()->first_name.' '.Auth::user()->last_name.'');
            }
            $notifications->save(); 

            if($request->status=='2'){
                $message=$user->first_name.' '.$user->last_name.' got a match with '.Auth::user()->first_name.' '.Auth::user()->last_name;
                $status="SUPERLIKED";
            }else if($request->status=='3'){
                $message='You rejected '.$user->first_name.' '.$user->last_name.' as your friend';
                $status="DISLIKED";
            }
            return response()->json(['message'=> $message,'status'=> $status]);

                      
        }else{
            // DB::enableQueryLog();
            $like=Likes::with('user_like')
                    ->whereRaw('(user_id='.$user_id.' OR like_id='.$user_id.') AND (user_id='.$request->like_id.' OR like_id='.$request->like_id.')');

            //check superlike daily count
            $superlike_count=Likes::where('user_id',$user_id)->where('super_like',1)->whereDate('updated_at', DB::raw('CURDATE()'))->count();            

            $user_premium=UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();  
            if($user_premium){//check Premium account
                $super_like=$user_premium->premium->superlike;   
                $user_like_count=$user_premium->premium->like_count;
            } else{// Check trial account
                $premium=Premium::first(); 
                $super_like=$premium->superlike;
                $user_like_count=$premium->like_count;
              //  dd($user_like_count);
            }
             $total_like_count=Likes::where('user_id',$user_id)->where('status','1')->whereDate('updated_at', DB::raw('CURDATE()'))->count(); 
            //dd(gettype($user_like_count));
            //  if(($total_like_count >= $user_like_count) && ($request->status=='1')){  //&& ($user_like_count != 0)
            //     if(($superlike_count>=$super_like) && ($total_like_count >= $user_like_count)){
            //         $date = UserPremium::select('id','created_at')->where('user_id',Auth::id())->first();
            //         $premium_subscribed_date = $date->created_at;
            //         $premium_reset_date = date('Y-m-d H:i:s', strtotime('+24 hours', strtotime($premium_subscribed_date)));
            //         UserPremium::where('user_id',Auth::id())->update(['status' => 'hold','reset_date' => $premium_reset_date]);
            //     }
            //     $message="Your Today like limit reached, Get premium to get unlimited likes";
            //     $status="LIKE_REACHED_LIMIT";
            //  }else if(($superlike_count>=$super_like) && ($request->status=='2')){
            //     if(($superlike_count>=$super_like) && ($total_like_count >= $user_like_count)){
            //         $date = UserPremium::select('id','created_at')->where('user_id',Auth::id())->first();
            //         $premium_subscribed_date = $date->created_at;
            //         $premium_reset_date = date('Y-m-d H:i:s', strtotime('+24 hours', strtotime($premium_subscribed_date)));
            //         UserPremium::where('user_id',Auth::id())->update(['status' => 'hold','reset_date' => $premium_reset_date]);
            //     }
            //     $message="Your Today superlike limit reached, Get premium to continue";
            //     $status="SUPERLIKE_REACHED_LIMIT";
            // }
            // else{                
                //dd(DB::getQueryLog());
                if($like->count() == 0) {

                    $like1 = new Likes;
                    $like1->user_id = $user_id;
                    $like1->like_id = $request->like_id;
                    $like1->status = $request->status;               
                    if($request->status=='2'){
                        $like1->super_like = 1;
                        $notifications->status='superlike';
                        $notifications->save();
                        (new SendPushNotification)->sendPushToUser($request->like_id, ''.Auth::user()->first_name.' '.Auth::user()->last_name.' dropped a doubleheart on your profile');
                    }else if($request->status=='1'){
                        $notifications->status='like';
                        $notifications->save();
                        (new SendPushNotification)->sendPushToUser($request->like_id, ''.Auth::user()->first_name.' '.Auth::user()->last_name.' dropped a thumps up on your profile');
                    }
                    $like1->save();                    
                    $like=Likes::with('user_like')->where('id',$like1->id)->first();

                }  else {

                    $like = $like->first(); 
                    if($like->status=='2') {
                        $message='You and '.$like->user_like->first_name.' already friend';
                        $status="ALREADY_LIKED";
                    }else if($like->status==1 && $like->user_id==$user_id){
                        $message='You already liked '.$like->user_like->first_name;
                        $status="ALREADY_LIKED";
                    }else if(($like->user_id!=$user_id)&&($request->status=='1')){
                        $like->status ='2';
                        $like->super_like = 0;
                        $message='You and '.$like->user_like->first_name.' liked each other';
                        (new SendPushNotification)->sendPushToUser($request->like_id, 'you and '.Auth::user()->first_name.' '.Auth::user()->last_name.' dropped a thumps up to each other');
                        $status="EACH_LIKED";
                        $notifications->status='superlike';
                        $notifications->save();

                        $like->save(); 
                        $like_each=Likes::with('user')->where('id',$like->id)->first();

                    }else{
                        $like->status = $request->status ;                   
                        if($request->status=='2'){
                            $like->super_like = 1;
                            $notifications->status='superlike';
                            $notifications->save();
                            (new SendPushNotification)->sendPushToUser($request->like_id, ''.Auth::user()->first_name.' '.Auth::user()->last_name.' dropped a doubleheart on your profile');
                        }else{
                            $like->super_like = 0;
                            if($request->status=='1'){
                                $notifications->status='like';
                                $notifications->save();
                                (new SendPushNotification)->sendPushToUser($request->like_id, ''.Auth::user()->first_name.' '.Auth::user()->last_name.'dropped a thumps up on your profile');
                            }                       
                        } 
                        $like->save();                        
                    }                     
                    
                    
                    //print_r($like);       
                }
            // }            
        }
          
            if($message){
                $message=$message;
                $status=$status;
            }else if($request->status=='1'){
                $message='User Liked your profile';
                $status="LIKED";
            }else if($request->status=='2'){
                $message='User Superliked your profile';
                $status="SUPERLIKED";
            }else if($request->status=='3'){
                $message='User Disliked your profile';
                $status="DISLIKED";
            }else{
                $message='User status 0';
                $status="0";
            }

            //superlike count
            $superlike_count=Likes::where('user_id',$user_id)->where('super_like',1)->whereDate('updated_at', DB::raw('CURDATE()'))->count();
            $like_count_remain =$user_like_count-$total_like_count;

            $superlike_count_remain = $super_like - $superlike_count;
            if($request->next_id){
               if($request->like_id == $request->next_id){
                  $restore = Likes::where('user_id',$user_id)->where('status','3')->DELETE();
                    
                } 
                $next_user=User::with('user_images','user_interest.interest')->where('id',$request->next_id)->first();
                return response()->json(['message'=> $message,'status'=> $status,'like'=> $like,'next_user'=> $next_user,'superlike_count'=> $superlike_count,'like_count' => $total_like_count]);
            }      
            if($request->ajax()) {
                if($status=='EACH_LIKED'){
                    return response()->json([ 'like'=> $like_each,'message'=> $message,'status'=> $status,'superlike_count'=> $superlike_count,'like_count' => $total_like_count,'like_count' => $total_like_count,'like_count_remain' => $like_count_remain,'superlike_count_remain' => $superlike_count_remain]);
                }else{
                    return response()->json(['like'=> $like,'message'=> $message,'status'=> $status,'superlike_count'=> $superlike_count,'like_count' => $total_like_count,'like_count_remain' => $like_count_remain,'superlike_count_remain' => $superlike_count_remain]);
                }              
            }else{
                return back()->with('flash_success', $message);
                //return $message;
            }

      } 
      catch (ModelNotFoundException $e) {
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{
              return back()->with('flash_error', 'Something went wrong');
          }
      }          
    }
    public function skip(Request $request)
    {
       /* $this->validate($request, [
           'next_id' => 'required',
        ]);*/
        try{ 
             
            $user_premium=UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();  
            $superlike_count=Likes::where('user_id',Auth::id())->where('super_like',1)->whereDate('updated_at', DB::raw('CURDATE()'))->count();   
            $total_like_count=Likes::where('user_id',Auth::id())->where('status','1')->whereDate('updated_at', DB::raw('CURDATE()'))->count();
            // if($user_premium){//check Premium account
            //     $skip_count=$user_premium->premium->skip_count;   
            // } else{// Check trial account
            //     $premium=Premium::where('price',0)->first(); 
            //     $skip_count=$premium->skip_count;
            //     if($request->ajax()) {
            //         return response()->json(['next_user'=> 2,'like_count'=>$total_like_count,'superlike_count' => $superlike_count]);
            //     }
            // } 
            // $trashed_count = Likes::where('user_id',Auth::id())->onlyTrashed()->get()->count();
            // if($skip_count >= $trashed_count && $skip_count !=0){
                $previous_undo = Likes::where('user_id',Auth::id())->orderBy('id', 'DESC')->first();  
                if($previous_undo){
                    $like_id = $previous_undo->like_id;
                   $previous_undo->delete();
                   $next_user=User::with('user_images','user_interest.interest')->where('id',$like_id)->first();    
                }else{
                   $next_user= 0; 
                }
            // }else{
            //   $next_user= 1;   
            // }
            if($request->ajax()) {
              return response()->json(['next_user'=> $next_user,'like_count'=>$total_like_count,'superlike_count' => $superlike_count]);
            }else{
                return back()->with('flash_success', 'Successfully Skiped');
                //return $message;
            }
      } 
      catch (ModelNotFoundException $e) {
            //dd(DB::getQueryLog());
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{
              return back()->with('flash_error', 'Something went wrong');
          }
      }
    } 

    /**
     * Show the User list.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function findMatch(Request $request)
    {
      try{   
        if(Setting::get('demo_mode') =='1')  {
            $user=User::with('user_preferences')->where('id',Auth::id())->first();
            $user_premium=UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();
            if(count($user_premium) > 0 && $user_premium->premium->likes_me == 1){
                $likes_me = 1;
            }else{
                $likes_me = 0;
            }  
            //liked list
            $liked_user[]=0;
            $likes=Likes::where("id",'!=', '')
                   ->whereRaw("((user_id = ".Auth::id()." or like_id = ".Auth::id().") AND status='2') OR (user_id = ".Auth::id()." AND status='1')")->get();
            foreach($likes as $like){
                if($like->user_id==Auth::id()){
                    $liked_user[]=$like->like_id;
                }else{
                    $liked_user[]=$like->user_id;
                }            
            }

            $userblock=array();
            $userblocked=UserBlocked::where('status','block')->whereRaw('(user_id='.Auth::user()->id.' or block_id='.Auth::user()->id.')')->get();
            foreach ($userblocked as $key => $userblocks) {
                if($userblocks->user_id==Auth::user()->id){
                    $userblock[]=$userblocks->block_id;
                }else{
                    $userblock[]=$userblocks->user_id;
                }
            }

            //DB::enableQueryLog();
            DB::connection()->enableQueryLog();
            $type = $user->user_preferences->is_country_wise;
            $find_match=User::with('user_preferences','user_images','user_interest.interest','user_country')
                        ->select('*')
                        ->whereNotIn('id',$liked_user)
                        ->whereNotIn('id',$userblock)
                        ->where(function($query) use ($type, $request) {
                            if ($type == 1) {
                                return $query->where('country',Auth::user()->country);
                            }
                        }) 
                        // ->whereIn('gender',$gender);               
                        ->whereHas('user_preferences', function($q)
                        {
                            $q->where('user_preferences.dnd' , 0);
                        })

                     //->whereBetween('age', $age_limit)
                     ->take(100)->get()->except(Auth::id());
                     
            //check superlike daily count
            $superlike_reach=Likes::where('user_id',Auth::id())->where('super_like',1)->whereDate('updated_at', DB::raw('CURDATE()'))->count(); 
            $user_premium=UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();        
            if($user_premium){//check Premium account
                $super_like=$user_premium->premium->superlike;
                $user_like_count=$user_premium->premium->like_count;
            } else{// Check trial account
                $premium=Premium::first(); 
                $super_like=$premium->superlike;
                $user_like_count=$premium->like_count;
            }
            $today_like_reach=Likes::where('user_id',Auth::id())->where('status','1')->whereDate('updated_at', DB::raw('CURDATE()'))->count();
            $previous_undo = Likes::where('user_id',Auth::id())->orderBy('id', 'DESC')->first();
            if($previous_undo){
                $skip_pre = count($previous_undo);
            }else{
                $skip_pre = 0;
            }

            $data['superlike_count']=$super_like;
            $data['find_match']=$find_match;
            $data['superlike_reach']=$superlike_reach;
            $data['today_like_reach'] = $today_like_reach;
            $data['user_like_count'] = $user_like_count;
            $data['skip_pre'] =$skip_pre;
            if($request->ajax()) {
                return response()->json(['user' => $find_match,'superlike_count' => $super_like,'superlike_reach' => $superlike_reach,'app_in_review'=>(int)Setting::get('review'),'who_likes_me'=>$likes_me,'today_like_reach' => $today_like_reach ,'user_like_count'=>$user_like_count,'skip_pre' => $skip_pre]);
            }else{
                return $data;
            }
            
        }
        else{   

            $user=User::with('user_preferences','user_interest','user_locations','user_country')->where('id',Auth::id())->first();
            $user_premium=UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();
            if(count($user_premium) > 0 && $user_premium->premium->likes_me == 1){
                    $likes_me = 1;
                }else{
                    $likes_me = 0;
                }  
            $user_interests[]=0;
            foreach ($user->user_interest as $key => $user_interest) {
                $user_interests[]= $user_interest->interest_id;
            }
            // $latitude=$user->latitude;
            //$longitude=$user->longitude;
            // $distance=$user->user_preferences->distance ? : 10;
            $distance=$user->user_preferences->distance ? : 1000;
            $age_limit=explode(',', $user->user_preferences->age_limit);  
            if($user->user_preferences->gender=='both'){  
                $gender=['other','male','female'];
            }else{
                $gender=[$user->user_preferences->gender];
            } 
            //liked list
            $liked_user[]=0;
            $likes=Likes::where("id",'!=', '')
                    ->whereRaw("((user_id = ".Auth::id()." or like_id = ".Auth::id().") AND status='2') OR ((user_id = ".Auth::id()." or like_id = ".Auth::id().") AND status='1') OR ((user_id = ".Auth::id()." or like_id = ".Auth::id().") AND status='3')")->get();
            foreach($likes as $like){
                if($like->user_id==Auth::id()){
                    $liked_user[]=$like->like_id;
                }else{
                    $liked_user[]=$like->user_id;
                }            
            }

            $userblock=array();
            $userblocked=UserBlocked::where('status','block')->whereRaw('(user_id='.Auth::user()->id.' or block_id='.Auth::user()->id.')')->get();
            foreach ($userblocked as $key => $userblocks) {
                if($userblocks->user_id==Auth::user()->id){
                    $userblock[]=$userblocks->block_id;
                }else{
                    $userblock[]=$userblocks->user_id;
                }
            }

            DB::enableQueryLog();
                $type = $user->user_preferences->is_country_wise;
                $gender_type_request = $user->user_preferences->gender;
                $find_match=User::with('user_preferences','user_images','user_interest.interest','user_locations')
                            ->select('*')
                            ->whereNotIn('id',$liked_user)
                            ->whereNotIn('id',$userblock)
                            ->whereIn('gender',$gender)
                            ->where(function($query) use ($type, $request) {
                            	// Disable By Md. Abdul Awal
                                if ($type == 1) {
                                    return $query->where('country',Auth::user()->country);
                                }
                            });
                if(count($user_interests)>1){
                    $find_match->whereHas('user_interest', function($q) use($user_interests)
                    {
                        $q->whereIn('user_interests.interest_id' , $user_interests);
                    });
                }

            //if($latitude && $longitude){
                // $user_locations[]=0;
                // if(count($user->user_locations) != 0){
                //     $find_match->whereHas('user_locations', function($q) use ($user,$distance)
                //         {
                //             foreach($user->user_locations as $key => $user_location){
                //                 $longitude = $user_location->longitude;
                //                 $latitude = $user_location->latitude;
                //                 $q->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance");
                //             }    
                //         });
                    
                // } 
                $user_preferences[]=0;

               

                // Country Wise
                if($type == 1) {
  
                        $find_match->whereHas('user_preferences', function($q) use ($user,$distance)
                        {   

                            $latitude = Auth::user()->latitude;
                            $longitude = Auth::user()->longitude;

                            // Disable By Md. Abdul Awal
                            $q->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance");

                            // $user_preferences_address=json_encode($user->user_preferences['address']);
                            // if(is_array($user_preferences_address)){
                            //     foreach($user_preferences_address as $key => $location){
                            //         if(count($user_preferences_address)>1){
                            //             if($key==0){ 
                            //                 $latitude = $location->latitude;
                            //                 $longitude = $location->longitude;

                            //                 $q->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance");
                            //             }    
                            //             if($key>=1){                          
                            //                 $q->orWhereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance");
                            //             }
                                        
                            //         }
                            //         else{    
                            //             $latitude = $location->latitude;
                            //             $longitude = $location->longitude;                               
                            //             $q->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance");
                            //         }
                                    
                            //     }
                            // }    
                        });    
                } 
                $find_match=$find_match->whereHas('user_preferences', function($q)
                {
                	// Disable By Md. Abdul Awal
                    // $q->where('user_preferences.dnd' , 0);
                })
                ->whereBetween('age', $age_limit)
                ->take(100)->get()->except(Auth::id());
             //   dd('dhilips');
                foreach ($find_match as $key => $value) {
                                        
                    @$find_match['rewind'][$key] = Subscription::whereuser_id(Auth::user()->id)->wherematch_id($value->id)->whererewind(1)->first();
                }
                //dd('dhilip');

                 $rewind_count = Subscription::whereuser_id(Auth::user()->id)->whererewind(1)->count();
                 if($rewind_count != 0){
                   $likes = Likes::where('status',"3")->where('user_id',Auth::user()->id)->delete();
                 }
                //dd(DB::getQueryLog());
            //check superlike daily count
            $superlike_reach=Likes::where('user_id',Auth::id())->where('super_like',1)->whereDate('updated_at', DB::raw('CURDATE()'))->count(); 
            $user_premium=UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();        
            if($user_premium){//check Premium account
                $super_like=$user_premium->premium->superlike;
                $user_like_count=$user_premium->premium->like_count;
            } else{// Check trial account
                $premium=Premium::first(); 
                $super_like=$premium->superlike;
                $user_like_count=$premium->like_count;
            }
            $today_like_reach=Likes::where('user_id',Auth::id())->where('status','1')->whereDate('updated_at', DB::raw('CURDATE()'))->count(); 

            $previous_undo = Likes::where('user_id',Auth::id())->orderBy('id', 'DESC')->first();
            if($previous_undo){
                $skip_pre = count($previous_undo);
            }else{
                $skip_pre = 0;
            }
            $data['superlike_count']=$super_like;
            $data['find_match']=$find_match;
            $data['superlike_reach']=$superlike_reach;
            $data['today_like_reach'] = $today_like_reach;
            $data['user_like_count'] = $user_like_count;
            $data['skip_pre'] =$skip_pre;
            if($request->ajax()) {
                return response()->json(['user' => $find_match,'superlike_count' => $super_like,'superlike_reach' => $superlike_reach,'app_in_review'=>(int)Setting::get('review'),'who_likes_me'=>$likes_me,'today_like_reach' => $today_like_reach ,'user_like_count'=>$user_like_count,'skip_pre' => $skip_pre]);
            }else{
                return $data;
            }
        }       
      } 
      catch (ModelNotFoundException $e) {
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{            
            dd(DB::getQueryLog());
              return back()->with('flash_error', 'Something went wrong');
          }
      }          
    }

    /**
     * Show the User list.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function user(Request $request)
    {
      try{        
        $user=User::with('user_preferences','user_country')->where('id',Auth::id())->first();
        $latitude=$user->latitude;
        $longitude=$user->longitude;
        $distance=$user->user_preferences->distance;   
        if($user->user_preferences->gender=='both'){  
            $gender=['other','male','female'];
        }else{
            $gender=[$user->user_preferences->gender];
        } 

        DB::enableQueryLog();
        
        $find_match=User::with('user_preferences','user_images','user_interest.interest')
                    ->select('*')
                    ->selectRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) as distance")                     
                    ->where('id',$request->id) 
                    ->get();
                   //dd(DB::getQueryLog());
                   //dd($find_match);

        //check superlike daily count
        $superlike_reach=Likes::where('user_id',Auth::id())->where('super_like',1)->whereDate('updated_at', DB::raw('CURDATE()'))->count(); 
        $user_premium=UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();         
        if($user_premium){//check Premium account
            $super_like=$user_premium->premium->superlike;
            $user_like_count=$user_premium->premium->like_count;
        } else{// Check trial account
            $premium=Premium::first(); 
            $super_like=$premium->superlike;
            $user_like_count=$premium->like_count;
        }
        $today_like_reach=Likes::where('user_id',Auth::id())->where('status','1')->whereDate('updated_at', DB::raw('CURDATE()'))->count(); 

        $previous_undo = Likes::where('user_id',Auth::id())->orderBy('id', 'DESC')->first();
        if($previous_undo){
            $skip_pre = count($previous_undo);
        }else{
            $skip_pre = 0;
        }
        $data['superlike_count']=$super_like;
        $data['find_match']=$find_match;
        $data['superlike_reach']=$superlike_reach;
        $data['today_like_reach'] = $today_like_reach;
        $data['user_like_count'] = $user_like_count;
        $data['skip_pre'] =$skip_pre;
          if($request->ajax()) {
              return response()->json(['user' => $find_match,'superlike_count' => $superlike_count,'superlike_reach' => $superlike_reach]);
            }else{
              return $data;
            }
      } 
      catch (ModelNotFoundException $e) {
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{            
            dd(DB::getQueryLog());
              return back()->with('flash_error', 'Something went wrong');
          }
      }          
    }

    /**
     * Show the User list.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function matchList(Request $request)
    {
        //dd($request->all());
      try{
          $user=User::where('id',Auth::user()->id)->first();
          $latitude=$user->latitude;
          $longitude=$user->longitude;  
          $userblock=array();
          $userblocked=UserBlocked::where('status','block')->whereRaw('(user_id='.Auth::user()->id.' or block_id='.Auth::user()->id.')')->get();
          foreach ($userblocked as $key => $userblocks) {
              if($userblocks->user_id==Auth::user()->id){
                $userblock[]=$userblocks->block_id;
              }else{
                $userblock[]=$userblocks->user_id;
              }
          }
          //dd($userblock);
          DB::enableQueryLog();
          $match_lists=Likes::with('user')->whereRaw('(user_id='.Auth::user()->id.' AND status="2") OR (like_id='.Auth::user()->id.' AND status="2")')->orderBy('updated_at','DESC')->get();  
          //dd(DB::getQueryLog());   
          $match_list_user=array();   
         //dd($match_lists);
          foreach ($match_lists as $match_list_find) { 
            //dd($match_list_find->like_id);
            
            if($match_list_find->user_id==Auth::user()->id){                
                $match_list_user[]=Likes::with('user_like')->where('id',$match_list_find->id)->whereNotIn('like_id',$userblock)->orderBy('updated_at','DESC')->get();
               // $match_list_user['subscription'] = Subscription::whereuser_id(Auth::user()->id)->wherematch_id($match_list_find->like_id)->first();


             }else{
                $match_list_user[]=Likes::with('user')->where('id',$match_list_find->id)->whereNotIn('user_id',$userblock)->orderBy('updated_at','DESC')->get();
                                                 
            }
            //dd(DB::getQueryLog());  
          }  
        //  dd($match_list_user);  

          $match_list=new Collection();
          foreach($match_list_user as $collection) {            
                $match_list=$match_list->merge($collection);

                //dd($match_list);
           }
           $match_list->map(function ($Match) {
          
                $Match['user_distance'] = $this->friend_distance($Match);;
                $Match['chat_notification_count'] = $this->chat_notification_count($Match);
                $Match['subscription'] =Subscription::whereuser_id($Match->user_id)->wherematch_id($Match->like_id)->first();
                //dd($Match);
                return $Match;
            });
          //dd($match_list);
          

          if($request->ajax()) {
              return response()->json($match_list);
            }else{
              return $match_list;
            }
      } 
      catch (ModelNotFoundException $e) {
        return $e->getMessage();
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{
              return back()->with('flash_error', 'Something went wrong');
          }
      }          
    }

    // public function matchList(Request $request){
       
    //     $notifications = Notifications::orderBy('created_at','DESC')->where('user_id',Auth::user()->id)->where('status','chat')->get()->unique('notifier_id')->pluck('notifier_id');
    //     $likes = array();
    //     foreach ($notifications as $key => $value) {
    //        // echo $value->notifier_id;
    //         $liked_by_user = Likes::with('user')->where('user_id' , Auth::user()->id)->where('like_id' , $value)->get();
    //         $user_liked = Likes::with('user')->where('user_id' , $value)->where('like_id' , Auth::user()->id)->get();
    //         $likes[] = $liked_by_user->merge($user_liked);
            
    //     }
    //     return $likes;
    // }

    public function friend_distance($Match){

        $latitudeFrom = Auth::user()->latitude;
        $longitudeFrom = Auth::user()->longitude;
        if($Match->user){
            $latitudeTo    = $Match->user->latitude;
            $longitudeTo   = $Match->user->longitude;
        }
        else if($Match->user_like){
            $latitudeTo    = $Match->user_like->latitude;
            $longitudeTo   = $Match->user_like->longitude;
        }

        $theta    = $longitudeFrom - $longitudeTo;
        $dist    = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist    = acos($dist);
        $dist    = rad2deg($dist);
        $miles    = $dist * 60 * 1.1515;
        $unit = 'K';
        // Convert unit and return distance
        $unit = strtoupper($unit);
        if($unit == "K"){
            return round($miles * 1.609344, 2).' km';
        }elseif($unit == "M"){
            return round($miles * 1609.344, 2).' meters';
        }else{
            return round($miles, 2).' miles';
        }
    }

    public function chat_notification_count($Match) {

        $user_new_notification_count = Notifications::selectRaw("COUNT(user_id) as user_notification")
          ->where('notifier_id',Auth::user()->id)
          ->where('user_id',$Match->user->id)
          ->where('status','=','chat')
          ->where('read',0)
          ->groupBy('user_id')
          ->first();
        
        return @$user_new_notification_count->user_notification;
    }

    public function update_chat_notification(Request $request) {

        $notification = Notifications::where('user_id',$request->user_id)->where('notifier_id',Auth::user()->id)->update(['read' => 1]);

        return response()->json(['success' => 200]);
    }

    /**
     * Show the User list.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function whoLikesMe(Request $request)
    {
      try{
        DB::enableQueryLog();
            $likes_me=Likes::with('user')->with('user.user_images')->where('like_id',Auth::id())->where('status','1')->get()->except(Auth::id());
                    /*->whereHas('like', function($q)
                    {
                        $q->where('like.status' , '1');
                    })
                    ->get();*/                  

            $likes_me->map(function ($Match) {
                $Match['user_distance'] = $this->friend_distance($Match);;
                
                return $Match;
            });
            if($request->ajax()) {
            //dd(DB::getQueryLog());
              return response()->json($likes_me);
            }else{
              return $likes_me;
            }
      } 
      catch (ModelNotFoundException $e) {
        //dd(DB::getQueryLog());
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{
              return back()->with('flash_error', 'Something went wrong');
          }
      }          
    }

    /**
     * Show the Chat list.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function chat_list()
    {
      try{
          return User::all()->except(Auth::id());
      } 
      catch (Exception $e) {
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{
              return back()->with('flash_error', 'Something went wrong');
          }
      }          
    }
    /**
     * Show the Chat list.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function single_user(Request $request)
    {
      try{
          return User::with('user_images','user_interest.interest')->where('id',$request->user_id)->first();
      } 
      catch (Exception $e) {
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{
              return back()->with('flash_error', 'Something went wrong');
          }
      }          
    }
    /**
     * payment
     *
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
        //$data['matches'] = $this->matchList($request);
        $cards= (new CardController)->index();
        if($request->ajax()){
              return response()->json(['cards' => $cards]);
          }else{
              return $cards;
          }
    }
    /**
     * Payment Process.
     *
     * @return \Illuminate\Http\Response
     */
    public function payment_process(Request $request)   {

        $this->validate($request, [
           'status' => 'required',
           'amount' => 'required',
        ]);


       if($request->status=='PAID'){
            $this->validate($request, [
                'receiver_id' => 'required',
            ]);
       }

        $user_id=Auth::user()->id;
        $wallet =Auth::user()->wallet_balance;


        try {
            $user = User::where('id' , $user_id)->first();
            $transactions= Transactions::select('id')->orderBy('id', 'DESC')->first();
            $transactions_id = ($transactions != null) ? $transactions->id + 1 : 1;
            $share ='';
            $random = str_pad($transactions_id, 4, '0', STR_PAD_LEFT);
            $user_share_transaction = Setting::get('share_transaction','DATESHARE');
            $alias_id = $user_share_transaction.$share."".$random;
            $transactions =new Transactions;
            $transactions->user_id=$user_id;
            $transactions->alias_id = $alias_id;
            $transactions->status=$request->status;
            $transactions->amount = $request->amount;

            if($request->status=='ADDED'){
                $user->wallet_balance=$wallet+$request->amount;

                $msg='Wallet Added Successfully';
            }if($request->status=='PAID'){
                if($wallet>=$request->amount){
                    $user->wallet_balance=$wallet-$request->amount;
                    if($request->has('receiver_id')){
                        $transactions->receiver_id = $request->receiver_id;
                        $receiver = User::where('id' , $request->receiver_id)->first();
                        $receiver->wallet_balance=$receiver->wallet_balance+$request->amount;
                        $receiver->save();
                        

                        //Notification
                        $notifications=new Notifications;
                        $notifications->user_id=$user_id;
                        $notifications->notifier_id=$request->receiver_id;
                        $notifications->status='wallet';
                        $notifications->save();
                    } 
                    $msg='Amount transfered to '.$receiver->first_name.' successfully';
                    (new SendPushNotification)->sendPushToUser($request->receiver_id, ''.Auth::user()->first_name.' '.Auth::user()->last_name.' send amount to your wallet');   
                }else{
                    $msg="Wallet amount lesser than share amount, Boost your wallet";
                }            
            } 

            $transactions->save();
            $user->save();

            if($request->ajax()) {
               return response()->json(['message' => $msg,'user' => $user]);
            }else{
               return back()->with('flash_success', $msg);
            }
        }

        catch(ModelNotFoundException $e) 
        {
             dd(DB::getQueryLog());
            if($request->ajax()) {
               return response()->json(['message' => trans('api.something_went_wrong')], 500);
            }else{
               return back()->with('flash_error', 'Something went wrong while sending request. Please try again.');
            }
        }    
    }
    public function add_money(Request $request){
        //dd($request->all());
        $this->validate($request, [
                'amount' => 'required|integer',
                'card_id' => 'required|exists:cards,card_id,user_id,'.Auth::user()->id,
                'status' => 'required|in:ADDED',
            ]);

        try{
            
            $StripeWalletCharge = $request->amount * 100;

            \Stripe\Stripe::setApiKey(Setting::get('stripe_secret_key'));

            $Charge = \Stripe\Charge::create(array(
                  "amount" => $StripeWalletCharge,
                  "currency" => "usd",
                  "customer" => Auth::user()->stripe_cust_id,
                  "card" => $request->card_id,
                  "description" => "Adding Money for ".Auth::user()->email,
                  "receipt_email" => Auth::user()->email
                ));

            $update_user = User::find(Auth::user()->id);
            $update_user->wallet_balance += $request->amount;
            $update_user->save();

            Card::where('user_id',Auth::user()->id)->update(['is_default' => 0]);
            Card::where('card_id',$request->card_id)->update(['is_default' => 1]);
            //Transaction alias details

            $wallet_transaction=Settings::where('key','wallet_transaction')->first();
            $user_wallet_transaction = $wallet_transaction['value']; 
            $transactions= Transactions::select('id')->orderBy('id', 'DESC')->first();
            $transactions_id = ($transactions != null) ? $transactions->id + 1 : 1;
            $wallet ='WALLET';
            $random = str_pad($transactions_id, 4, '0', STR_PAD_LEFT);
            $id = $transactions_id+1;
            $alias_id = $user_wallet_transaction.$wallet."".$random;

            //Trasanctions history save
            $transactions =new Transactions;
            $transactions->user_id=Auth::user()->id;
            $transactions->status=$request->status;
            $transactions->payment_id = @$Charge->id;
            $transactions->alias_id = $alias_id;
            $transactions->profit_status = 'Credit';
            $transactions->amount = $request->amount;
            $transactions->save();

            //Admin user wallet updated
            $admin_wallet =new AdminWallet;
            $admin_wallet->user_id=Auth::user()->id;
            $admin_wallet->status=$request->status;
            $admin_wallet->payment_id = @$Charge->id;
            $admin_wallet->alias_id = $alias_id;
            $admin_wallet->profit_status = 'Credit';
            $admin_wallet->amount = $request->amount;
            $admin_wallet->save();
            
            $subscription = new Subscription;
            $subscription->user_id = Auth::user()->id;
            $subscription->match_id = $request->match_id;
            $subscription->subscription_amount = $request->amount;
            $subscription->rewind = @$request->rewind ?? 0;
            $subscription->save();

            //sending push on adding wallet money
           // (new SendPushNotification)->WalletMoney(Auth::user()->id,currency($request->amount));

            if($request->ajax()){
               return response()->json(['message' => 'Subscribed Successfully', 'user' => $update_user]); 
            }else{
                return Redirect('/dashboard')->with('flash_success',' Subscribed');
                //return back()->with('flash_success',$request->amount.' added to your wallet');
            }

        } catch(\Stripe\StripeInvalidRequestError $e){
            if($request->ajax()){
                 return response()->json(['error' => $e->getMessage()], 500);
            }else{
                return back()->with('flash_error',$e->getMessage());
            }
        } 

    }

    /**
     * transaction_history list.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function transaction_history(Request $request)
    {
      $user_id=Auth::user()->id;  
      try{
          DB::enableQueryLog();
          if($request->has('status')){
            if($request->status=="RECEIVED"){
                $transaction_historys= Transactions::where('status','PAID')->where('receiver_id',$user_id)->orderBy('id', 'DESC')->get(); 

            }elseif($request->status=="PAID"){
                $transaction_historys= Transactions::where('user_id',$user_id)->whereIN('status',['PAID','PREMIUM'])->orderBy('id', 'DESC')->get();  

            }else{
                $transaction_historys= Transactions::where('status',$request->status)->where('user_id',$user_id)->orderBy('id', 'DESC')->get(); 
            }
                        
          }else{
            $transaction_historys= Transactions::where('user_id',$user_id)->Orwhere('receiver_id',$user_id)->orderBy('id', 'DESC')->get();
          }
          
          //dd(DB::getQueryLog());
          $transaction_history_user=array();   
          
          foreach ($transaction_historys as $transaction_history) { 

            if($transaction_history->user_id==Auth::id()){
                $transaction_history_user[]=Transactions::with('receiver')->where('id',$transaction_history->id)->get();     
            }else{
                $transaction_history_user[]=Transactions::with('user')->where('id',$transaction_history->id)->get();
            }
          }    

          $transaction_historys_list=new Collection();
          foreach($transaction_history_user as $collection) {            
                $transaction_historys_list=$transaction_historys_list->merge($collection);
                //dd($match_list);
           }

          if($request->ajax()){
              return response()->json(['transaction_history' => $transaction_historys_list]);
          }else{
              return $transaction_historys_list;
          }
      } 
      catch (Exception $e) {
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{
              return back()->with('flash_error', 'Something went wrong');
          }
      }          
    }
    /**
     * user_report.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function user_report(Request $request)
    {
        $this->validate($request, [
                'report_id' => 'required|exists:users,id',
                'reason' => 'required|in:spam,bad_photo,bad_behaviour,bad_message',
            ]);
      
      try{
        $user_id=Auth::user()->id;  
        $user_report =new UserReport;
        $user_report->user_id=$user_id;
        $user_report->report_id=$request->report_id;
        $user_report->reason=$request->reason;
        $user_report->save();
        if($request->ajax()){
              return response()->json(['message'=>'User reported to admin','user_report' => $user_report]);
          }else{
              return back()->with('flash_success', 'User reported to admin');
          }
      }
      catch (Exception $e) {
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{
              return back()->with('flash_error', 'Something went wrong');
          }
      }
    }
    /**
     * notifications.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function notifications(Request $request)
    {        
      try{
        $user_id=Auth::user()->id;
        $notifications = Notifications::with('user','recommend.user')->where('notifier_id',$user_id)->where('status','<>','chat')->orderBy('id', 'DESC')->get();

        if($request->ajax()){
              return response()->json(['notifications' => $notifications]);
          }else{
              return $notifications;
          }
      }
      catch (Exception $e) {
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{
              return back()->with('flash_error', 'Something went wrong');
          }
      }
    }
    /**
     * User Recommend.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function user_recommend(Request $request)
    {        
        $this->validate($request, [
                'recommend_id' => 'required|exists:users,id',
                'friend_ids' => 'required',
            ]);
      
      try{
        $user_id=Auth::user()->id;  

        
        foreach ($request->friend_ids as $key => $friend_id) {
            //Notification
            $notification =new Notifications;
            $notification->user_id=$user_id;
            $notification->status='recommend';
            $notification->notifier_id=$friend_id;        
            $notification->save(); 

            //UserRecommend 
            $user_recommend =new UserRecommend;
            $user_recommend->user_id=$user_id;
            $user_recommend->recommend_id=$request->recommend_id;
            $user_recommend->notification_id=$notification->id; 
            $user_recommend->friend_id=$friend_id;        
            $user_recommend->save(); 
        }                    
        
        if($request->ajax()){
              return response()->json(['message'=>'User recommended successfully','user_recommend' => $user_recommend]);
          }else{
              return back()->with('flash_success', 'User recommended successfully');
          }
      }
      catch (ModelNotFoundException $e) {
        dd(DB::getQueryLog());
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{            
              return back()->with('flash_error', 'Something went wrong');
          }
      }
    }

    /**
     * premium
     *
     * @return \Illuminate\Http\Response
     */
    public function premium(Request $request)
    {
        $data['premiums']=$premiums = Premium::where('is_default_trail','<>',1)->orderBy('price' , 'asc')->get();
        $data['user_premium']=$user_premium=UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();
        $data['user_premium_expiry_date']=$user_premium_expiry_date=null;
        if($user_premium == null)
        {
            $user = User::select('created_at')->where('id',Auth::id())->first();
            $user_created_date =$user->created_at; 
            $reset_date = date('Y-m-d H:i:s', strtotime('+24 hours', strtotime($user_created_date)));

        }else{
            $userpremium = UserPremium::select('reset_date')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
            $reset_date =$userpremium->reset_date;
        }
        if($user_premium){
            //calculate expiry date
            $user_premium_date=$user_premium->created_at;
            $user_premium_period=$user_premium->premium->period;
            $user_premium_duration=$user_premium->premium->duration;
            if($user_premium_duration=='day'){
               $data['user_premium_expiry_date']=$user_premium_expiry_date= (new Carbon($user_premium->created_at))->addDays($user_premium->premium->period);
            }
            else if($user_premium_duration=='month'){           
               $data['user_premium_expiry_date']=$user_premium_expiry_date= (new Carbon($user_premium->created_at))->addMonths($user_premium->premium->period);
            }
            else if($user_premium_duration=='year'){ 
                $data['user_premium_expiry_date']=$user_premium_expiry_date= (new Carbon($user_premium->created_at))->addYears($user_premium->premium->period);     
            }
        }
        $superlike_count=Likes::where('user_id',Auth::id())->where('super_like',1)->whereDate('updated_at', DB::raw('CURDATE()'))->count();  
        $total_like_count=Likes::where('user_id',Auth::id())->where('status','1')->whereDate('updated_at', DB::raw('CURDATE()'))->count();
        if($request->ajax()){
              return response()->json(['premiums' => $premiums,'user_premium' => $user_premium,'expiry_date' => $user_premium_expiry_date,'like_count'=>$total_like_count,'superlike_count' => $superlike_count,'reset_date' => $reset_date,'monthly_subscripiton' => Setting::get('monthly' , 0),'annual_subscription' => Setting::get('annual' , 0)]);
        }else{
              return $data;
        }
    }
    /**
     * user_premium
     *
     * @return \Illuminate\Http\Response
     */
    public function user_premium(Request $request)
    {
        $this->validate($request, [
                'premium_id' => 'required|exists:premia,id',
            ]);
      
      try{
            $premium = Premium::where('id',$request->premium_id)->first();
            if($premium->price > 0.00){
            if($premium->price<=Auth::user()->wallet_balance){
                $user_premiums = new UserPremium;
                $user_premiums->user_id=Auth::id();
                $user_premiums->premia_id=$request->premium_id;
                $user_premiums->reset_date=date('Y-m-d H:i:s');         
                $user_premiums->save();

                $update_user = User::find(Auth::user()->id);
                $update_user->wallet_balance -= $premium->price;
                $update_user->save();

                //Transaction alias details

                $premium_setting=Settings::where('key','premium_transaction')->first(); 
                $premium_transaction = $premium_setting['value'];
                $transactions= Transactions::select('id')->orderBy('id', 'DESC')->first();

                $transactions_id = ($transactions != null) ? $transactions->id + 1 : 1;
                $premium_name ='PREMIUM';
                $random = str_pad($transactions_id, 4, '0', STR_PAD_LEFT);
                $alias_id = $premium_transaction.$premium_name."".$random;
                
                //Trasanctions history save//
                $transactions =new Transactions;
                $transactions->user_id=Auth::user()->id;
                $transactions->status='PREMIUM';
                $transactions->amount = $premium->price;
                $transactions->alias_id = $alias_id;
                $transactions->profit_status = 'Debit';
                $transactions->payment_id = $request->premium_id;
                $transactions->save();

                //Admin user wallet updated
                $admin_wallet =new AdminWallet;
                $admin_wallet->user_id=Auth::user()->id;
                $admin_wallet->receiver_id = 1;
                $admin_wallet->status='PREMIUM';
                $admin_wallet->amount = $premium->price;
                $admin_wallet->alias_id = $alias_id;
                $admin_wallet->profit_status = 'Debit';
                $transactions->payment_id = $request->premium_id;
                $admin_wallet->save();

                if($request->ajax()){
                      return response()->json(['user_premiums' => $user_premiums,'message' => 'SUCCESS']);
                  }else{
                      return $premiums;
                }
            }else{
                if($request->ajax()){
                      return response()->json(['user_premiums'=> null,'message' => 'BOOST_WALLET'],420);
                  }else{
                      return back()->with('flash_error', 'BOOST_WALLET');
                }
            }
        }else{
            return response()->json(['message' => 'Already in Trial pack.'],420); 
        }
            
        }
      catch (ModelNotFoundException $e) {
        dd(DB::getQueryLog());
            if($request->ajax()){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{            
                return back()->with('flash_error', 'Something went wrong');
            }
        }
    }
    /**
     * user_premium
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_account(Request $request)
    {
        try{
            $id=Auth::user()->id;
            $user=User::find($id)->delete();

            Likes::where('user_id',$id)->orwhere('like_id',$id)->delete();
            UserInterest::where('user_id',$id)->delete();
            UserImages::where('user_id',$id)->delete();
            UserPreferences::where('user_id',$id)->delete();
            UserPremium::where('user_id',$id)->delete();
            UserReport::where('user_id',$id)->orwhere('report_id',$id)->delete();
            UserRecommend::where('user_id',$id)->orwhere('recommend_id',$id)->orwhere('friend_id',$id)->delete();
            Card::where('user_id',$id)->delete();
            Notifications::where('user_id',$id)->orwhere('notifier_id',$id)->delete();
            Transactions::where('user_id',$id)->orwhere('receiver_id',$id)->delete();  
            
            if($request->ajax()){
                  return response()->json(['message' => 'REMOVED']);
              }else{
                  return back()->with('flash_success', 'REMOVED');
            }
        }
        catch (ModelNotFoundException $e) {
            dd(DB::getQueryLog());
            if($request->ajax()){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{            
                return back()->with('flash_error', 'Something went wrong');
            }
        }
    }
    /**
     * Payment Process.
     *
     * @return \Illuminate\Http\Response
     */
    public function send_push(Request $request)   {

        $this->validate($request, [
           'sender_id' => 'required',
           'message'   => 'required',
        ]);

        try {

            $user_id = Auth::user()->id;
            $like = Likes::whereRaw('(user_id='.$user_id.' OR like_id='.$user_id.') AND (user_id='.$request->sender_id.' OR like_id='.$request->sender_id.')')->first();

            $like_update = Likes::findOrFail($like->id);
            $like_update->updated_at = date('Y-m-d H:i:s');
            $like_update->save();

            //Notification
            $notifications=new Notifications;
            $notifications->user_id=Auth::user()->id;
            $notifications->notifier_id=$request->sender_id;
            $notifications->status='chat';
            $notifications->save();

            (new SendPushNotification)->sendPushToUser($request->sender_id, ''.Auth::user()->first_name.' '.Auth::user()->last_name.' sent you a message');
            return response()->json(['message' => 'SENT']);
        }
        catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'something_went_wrong'], 500);
        }
    }

    /**
     * Payment Process.
     *
     * @return \Illuminate\Http\Response
     */
    public function block_unblock(Request $request)   {

        $this->validate($request, [
           'block_id' => 'required|exists:users,id',
           'status'   => 'required',
        ]);

        try {
            //Notification
            $user_id=Auth::user()->id;
            $message='';
            $user_block=UserBlocked::where('user_id',$user_id)->where('block_id',$request->block_id)->first();
            if(count($user_block)>0){
                if($user_block->status==$request->status){
                    $message="User already ".$request->status."ed";
                }
                $user_block->status=$request->status;
                $user_block->save();
            }else{
                $user_block=new UserBlocked;
                $user_block->user_id=$user_id;
                $user_block->block_id=$request->block_id;
                $user_block->status=$request->status;
                $user_block->save();
            }

            if($message){
                $message=$message;
            }else{
                $message='User '.$request->status.' successfully';
            }
            

            /*(new SendPushNotification)->sendPushToUser($request->block_id, ''.Auth::user()->first_name.' '.Auth::user()->last_name.' '.$request->status.' you ');*/
            return response()->json(['message' => $message]);
            }
        catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'something_went_wrong'], 500);
        }
    }
    /**
     * Show the User list.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function bloked_list(Request $request)
    {
      try{ 
          $userblock=array();
          $userblocked=UserBlocked::where('status','block')->whereRaw('(user_id='.Auth::id().' or block_id='.Auth::id().')')->get();
          //dd($userblock);
          DB::enableQueryLog();
          $match_lists=Likes::with('user')->whereRaw('(user_id='.Auth::id().' AND status="2") OR (like_id='.Auth::id().' AND status="2")')->get();  
          //dd(DB::getQueryLog());   
          $match_list_user=array();   
          
          foreach ($userblocked as $match_list_find) { 
            
            if($match_list_find->user_id==Auth::id()){                
                $match_list_user[]=UserBlocked::with('user_block')->where('id',$match_list_find->id)->whereNotIn('like_id',$userblock)->get();      
                                            
            }else{
                $match_list_user[]=UserBlocked::with('user')->where('id',$match_list_find->id)->whereNotIn('user_id',$userblock)->get();                  
            }
            //dd(DB::getQueryLog());  
          }    

          $bloked_list=new Collection();
          foreach($match_list_user as $collection) {            
                $bloked_list=$bloked_list->merge($collection);
                //dd($match_list);
           }
          //dd($match_list);
          

          if($request->ajax()) {
              return response()->json($bloked_list);
            }else{
              return $bloked_list;
            }
      } 
      catch (ModelNotFoundException $e) {
        return $e->getMessage();
          if($request->ajax()){
              return response()->json(['error' => trans('api.something_went_wrong')], 500);
          }else{
              return back()->with('flash_error', 'Something went wrong');
          }
      }          
    } 
    public function facebook_status(Request $request)
    {
        try{
            $facebook_status=Settings::where('key','facebook_status')->first(); 
            if($facebook_status['value'] == 'off')
            {
                $facebook_value = false;
            }else{
                $facebook_value = true;
            }
            if($request->ajax()){
                return response()->json(['facebook_status' => $facebook_value,'message' => 'SUCCESS']);
            }
        }
        catch (ModelNotFoundException $e) {
            if($request->ajax()){
                return response()->json(['error' => 'Failure'], 500);
            }
        }
    }   
}
