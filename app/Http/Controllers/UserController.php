<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Route;

use App\User;
use App\UserPremium;
use App\UserPreferences;
use App\Settings;
use App\Transactions;
use App\Notifications;

use App\Http\Controllers\UserApiController;

use Setting;
use Redirect;
use Auth;
use Exception;
use Mail;
use DateTime;
use Carbon;


class UserController extends Controller
{
	protected $UserAPI;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserApiController $UserAPI)
    {
        $this->middleware('auth', ['except' => ['verifyCredentials','verifyRefercode']]);
        $this->UserAPI = $UserAPI;
    }

    /**
     * Show the Profile dashboard.
     *
     * @return \Illuminate\Http\Response
     */    
    public function home()
    { 
        $UserPreferences = UserPreferences::where('user_id',Auth::id())->first();
        if($UserPreferences){
            return Redirect('/dashboard');
        }else{
            return Redirect('/discover_settings');
        }

        //$user=$data['user'];
        //$user_images=$data['user_images'];
        return view('view_profile',compact('user'));
    }

    /**
     * Show the Profile dashboard.
     *
     * @return \Illuminate\Http\Response
     */    
    public function viewProfile(Request $request)
    { 
        $data = $this->UserAPI->getProfile($request); 
        $user=$data['user'];
        $interest=$data['interest'];
        $user_premium=$data['user_premium'];
        return view('view_profile',compact('user','interest','user_premium'));
    }

    /**
     * Edit the Profile dashboard.
     *
     * @return \Illuminate\Http\Response
     */    
    public function editProfile(Request $request)
    {	
        $data = $this->UserAPI->getProfile($request); 
        $user=$data['user'];
        $interests=$data['interest'];
        return view('edit_profile',compact('user','interests'));
    }
    /**
     * Update profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        return $this->UserAPI->updateProfile($request);
    }
    /**
     * Matches listuse
     *
     * @return \Illuminate\Http\Response
     */
    public function matches(Request $request)
    {
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        $match_lists = $this->UserAPI->matchList($request);
        //dd(Auth::user()->id);
       //dd($match_lists);
        return view('matches',compact('user','match_lists'));
    }
    /**
     * Discover settings
     *
     * @return \Illuminate\Http\Response
     */
    public function discover_settings(Request $request)
    { 
        $discover_settings = $this->UserAPI->get_discover_settings($request);   
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        $user_preferences = UserPreferences::where('user_id',Auth::id())
                            ->orderBy('id','desc')
                            ->first();
        $latitude  = $user_preferences['latitude']; 
        $longitude = $user_preferences['longitude'];
        $selected_country = '';
        if(count($user_preferences['address'])!=0)
        {   
            $user_preferences_address=json_encode($user_preferences['address']);
            if(is_array($user_preferences_address)){
                foreach($user_preferences_address as $key => $val){
                    if($val->latitude==$latitude && $val->longitude == $longitude){
                        $selected_country = $val->country;
                    }
                }
            }
            /*$geocode=file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=".Setting::get('map_key','AIzaSyDDCQMW-zn7-60euM-r8eeF15AIjs8oDfs') ."&latlng=".$latitude.",".$longitude."&sensor=false");
            $output_deals = json_decode($geocode);
            $selected_country = '';
            if(count($output_deals->results)!= ''){
                  $selected_country =$output_deals->results[0]->formatted_address; 
                // $selected_country =$output_deals->results[6]->address_components[4]->long_name;  
            }*/
        }
        return view('discover_settings',compact('discover_settings','user','selected_country'));
    }
    /**
     * Update Discover settings
     *
     * @return \Illuminate\Http\Response
     */
    public function update_discover_settings(Request $request)
    {    //return $request->all();
        return $this->UserAPI->update_discover_settings($request);
    }
    /**
     * Who likes me
     *
     * @return \Illuminate\Http\Response
     */
    public function who_likes_me(Request $request)
    {
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();
        $who_likes_me = $this->UserAPI->whoLikesMe($request);
        //dd($who_likes_me);
        return view('who_likes_me',compact('user','who_likes_me'));
    }
    /**
     * Chat list.
     *
     * @return \Illuminate\Http\Response
     */
    public function chat(Request $request)
    {        
        //$chat_users = $this->UserAPI->chat_list($request);
        $chat_users = $this->UserAPI->matchList($request); 
        //dd($chat_users);
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        return view('chat_list',compact('chat_users','user'));
    }
    /**
     * Chat User.
     *
     * @return \Illuminate\Http\Response
     */
    public function chat_user(Request $request)
    {        
        //$chat_users = $this->UserAPI->chat_user(); 
        $sender=User::with('user_images')->where('id',$request->id)->first();
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        return view('chat_user',compact('user','sender'));
    }
    /**
     * Help list.
     *
     * @return \Illuminate\Http\Response
     */

    /*public function help(Request $request)
    {
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        return view('help',compact('user'));
    }*/

    /**
     * Find Matches
     *
     * @return \Illuminate\Http\Response
     */
    public function find_matches(Request $request)
    {
        // dd($request);
        $user=Auth::user();
       // dd($user); 
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();
        // dd($user['user_premium']); 
       
        //date format
        $reset_date = strtotime($user['user_premium']['reset_date']);
        $data = $this->UserAPI->findMatch($request);
        $find_matchs =$data['find_match'];
        $superlike_reach =$data['superlike_reach'];
        $superlike_count=$data['superlike_count'];
        $today_like_reach = $data['today_like_reach'];
        $like_count = $data['user_like_count'];
        $skip_pre = $data['skip_pre'];
        $matches = $this->UserAPI->matchList($request);
        // dd($matches);
        // dd($find_matchs);
        return view('find_matches',compact('user','find_matchs','matches','superlike_reach','superlike_count','today_like_reach','like_count','skip_pre','reset_date'));
    } 

    public function pay_new(Request $request){
        $user=Auth::user(); 
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();

        return view('payment-new', compact('user'));
    }   
    /**
     * Find Matches
     *
     * @return \Illuminate\Http\Response
     */
    public function map_view(Request $request)
    {
        $user=Auth::user();  
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first();       
        $data = $this->UserAPI->findMatch($request);
        $find_matchs =$data['find_match'];
        $find_location =$find_matchs;
       // dd($find_location);
        $superlike_reach =$data['superlike_reach'];
        $superlike_count=$data['superlike_count'];
        $matches = $this->UserAPI->matchList($request);
        //dd($find_matchs);
        return view('map_view',compact('user','find_matchs','matches','superlike_reach','superlike_count'));
    }
    /**
     * Like
     *
     * @return \Illuminate\Http\Response
     */
    public function likes(Request $request)
    {
        
        $user=Auth::user();
        $likes = $this->UserAPI->likes($request); 
        return $likes;
    }

    public function update_notification(Request $request) {

        $notification = Notifications::where('user_id',$request->user_id)->where('notifier_id',$request->notifier_id)->update(['read' => 1]);

        if($request->ajax()) {

            return response()->json(['success' => 200,'notification' => $notification]);
        }  
    }
    /**
     * Like
     *
     * @return \Illuminate\Http\Response
     */
    public function skip(Request $request)
    {
        $likes = $this->UserAPI->skip($request); 
        return $likes;
    }
    /**
     * update_who_likes_me
     *
     * @return \Illuminate\Http\Response
     */
    public function update_likes(Request $request)
    {
        $user=Auth::user();
        $likes = $this->UserAPI->update_likes($request); 
        return $likes;
    }
    /**
     * Single_user
     *
     * @return \Illuminate\Http\Response
     */
    public function single_user(Request $request)
    {
        //$user=Auth::user();
        $single_user = $this->UserAPI->single_user($request); 
        return $single_user;
    }

    /**
     * payment
     *
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
       // dd($request->all());
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        $cards = $this->UserAPI->payment($request);   
        $matches = $this->UserAPI->matchList($request);  
        return view('payment',compact('user','matches','cards','request'));
    }
    /**
     * add_money
     *
     * @return \Illuminate\Http\Response
     */
    public function add_money(Request $request)
    {
        return $this->UserAPI->add_money($request);
    }
    /**
     * payment_process
     *
     * @return \Illuminate\Http\Response
     */
    public function payment_process(Request $request)
    {
        return $this->UserAPI->payment_process($request);
    }
    /**
     * payment
     *
     * @return \Illuminate\Http\Response
     */
    public function transaction_history(Request $request)
    {
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        $transactions = $this->UserAPI->transaction_history($request);
        //dd($transactions);
        return view('transaction_history',compact('user','transactions'));
    }
    /**
     * Notifications list.
     *
     * @return \Illuminate\Http\Response
     */
    public function notifications(Request $request)
    {
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        $notifications = $this->UserAPI->notifications($request);
        //dd($notifications);
        return view('notifications',compact('user','notifications'));
    }
    /**
     * Invite Friend.
     *
     * @return \Illuminate\Http\Response
     */
    public function invites(Request $request)
    {
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        $referal_amount = Transactions::where('user_id',Auth::id())->where('status','REFERAL')->where('profit_status','credit')
        ->sum('amount');
        $user_referal_count = User::where('install_code',Auth::user()->invite_code)->count();
        return view('invites',compact('user','referal_amount','user_referal_count'));
    }
    /**
     * Premium.
     *
     * @return \Illuminate\Http\Response
     */
    public function premium(Request $request)
    {
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        $data = $this->UserAPI->premium($request);
        $premiums = $data['premiums'];
        $user_premium = $data['user_premium'];

        return view('premium',compact('user','premiums','user_premium'));
    }
    /**
     * User Report
     *
     * @return \Illuminate\Http\Response
     */
    public function user_report(Request $request)
    {
        $user=Auth::user();
        $user_report = $this->UserAPI->user_report($request);
        return $user_report;
        //dd($transactions);
    }
    /**
     * User Recommend
     *
     * @return \Illuminate\Http\Response
     */
    public function user_recommend(Request $request)
    {
        $user=Auth::user();
        $user_recommend = $this->UserAPI->user_recommend($request);
        return $user_recommend;
    }
    /**
     * User Premium
     *
     * @return \Illuminate\Http\Response
     */
    public function user_premium(Request $request)
    {
        $user=Auth::user();
        $user_premium = $this->UserAPI->user_premium($request); 
       

        //Mail to premium changes
        $setting_sitename = Settings::where('key','sitename')->first();
        $Premium_mail = UserPremium::with('premium','user')->where('user_id',Auth::user()->id)->orderBy('id','Desc')->first()->toArray();
        
        $send_email_to= $Premium_mail['user']['email'];
        $data = array(
                        'user_name'=> $Premium_mail['user']['first_name'].' '.$Premium_mail['user']['last_name'],
                        'user_image'=> $Premium_mail['user']['picture'],
                        'plan_name'=>$Premium_mail['premium']['plan_name'],
                        'plan_duration'=>$Premium_mail['premium']['period'].' '.$Premium_mail['premium']['duration'],
                        'plan_price'=>$Premium_mail['premium']['price'],
                        'site_name'=>$setting_sitename['value'],
                        
                );
        Mail::send('emails.premium',$data, function ($message) use($send_email_to) {
            $message->from('developer@appoets.com', 'DateAround');
            $message->subject('Premium Package Changed');
            $message->to($send_email_to);
        });
        return $user_premium;
    }
    /**
     * User.
     *
     * @return \Illuminate\Http\Response
     */
    public function user(Request $request)
    {   
        $user=Auth::user();
        $user['user_premium'] = UserPremium::with('premium')->where('user_id',Auth::id())->orderBy('id','desc')->first(); 
        //date format
        $reset_date = strtotime($user['user_premium']['reset_date']);
        $data = $this->UserAPI->user($request);
        $find_matchs =$data['find_match'];
        $superlike_reach =$data['superlike_reach'];
        $superlike_count=$data['superlike_count'];
        $matches = $this->UserAPI->matchList($request);
        $today_like_reach = $data['today_like_reach'];
        $like_count = $data['user_like_count'];
        $skip_pre = $data['skip_pre'];
        $matches = $this->UserAPI->matchList($request);
        //dd($find_matchs);
        return view('single_user',compact('user','find_matchs','matches','superlike_reach','superlike_count','today_like_reach','like_count','skip_pre','reset_date'));

    }
    /**
     * send_push
     *
     * @return \Illuminate\Http\Response
     */
    public function send_push(Request $request)
    {
        $send_push = $this->UserAPI->send_push($request); 
        return $send_push;
    }  
    public function contactus(Request $request)
    {
        $user=Auth::user();
        $setting_email = Settings::where('key','contact_email')->first();
        $setting_mobile = Settings::where('key','contact_number')->first();
        return view('contactus',compact('user','setting_email','setting_mobile'));
    }  
    public function verifyCredentials(Request $request)
    {
        if($request->has("mobile")) {
            $Provider = User::where([['country',$request->country_code],['mobile', $request->mobile]])
            // ->where('user_type', 'NORMAL')
            ->first();

            if($Provider != null) {
                return response()->json(['message' => 'Already Mobile No Exist'], 422);
            }
        }

        if($request->has("email")) {
            $Provider = User::where('email', $request->input("email"))->first();
            if($Provider != null) {
                return response()->json(['message' => 'Already Email Exist'], 422);
            }
        }

        return response()->json(['message' => 'Data available']);

    }

    public function verifyRefercode(Request $request){

        if($request->has("refercode")) {
           $Refercode = User::where('invite_code',$request->refercode)
            // ->where('user_type', 'NORMAL')
            ->first();

            if($Refercode == null) {
                return response()->json(['message' => 'Invalid Refercode'], 422);
            }else{
                return response()->json(['message' => 'Data available']);
            }
        }
        

    }
    
}
