<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use Auth;

class Likes extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
	protected $fillable = [
        'user_id', 'like_id', 'status'
    ];
     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'created_at', 'updated_at'
    ];

    public function user()
    {   
        if($this->user_id==Auth::user()->id){     
            return $this->belongsTo('App\User','like_id','id');
        }else{
            return $this->belongsTo('App\User');
        }        
    }
    public function user_like()
    { 
        return $this->belongsTo('App\User','like_id','id');
    }
    

}
