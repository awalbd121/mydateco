<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notifications extends Model
{
	use SoftDeletes;
    public function user()
    {  	
    	return $this->belongsTo('App\User','user_id','id');      
    }
    public function recommend(){

        return $this->hasone('App\UserRecommend','notification_id','id');
    }
}
