<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Premium extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plan_name', 'description', 'period','duration','price', 'superlike', 'video_call','location_change','likes_me','skip_count','like_count','is_default_trail'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'created_at'
    ];
}
