<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use Auth;

class Transactions extends Model
{
    use SoftDeletes;
    public function user()
    {  	
    	if($this->user_id==Auth::id()){   	
    		return $this->belongsTo('App\User','receiver_id','id');
    	}else{
    		return $this->belongsTo('App\User');
    	}        
    }
    public function receiver()
    { 
        return $this->belongsTo('App\User','receiver_id','id');
    }
    public function from_user()
    { 
        return $this->belongsTo('App\User','user_id','id');
    }
}
