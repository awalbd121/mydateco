<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','password','first_name', 'last_name', 'gender','dob','age', 'email', 'password', 'mobile', 'country', 'picture','bio_video','about', 'work', 'show_age', 'show_distance',  'device_type','device_token','login_by', 'social_unique_id','device_id','address','latitude','longitude','wallet_balance', 'interest','install_code','invite_code','language','created_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
        //'created_at'
    ];
    public function premium()
    {
        return $this->hasOne('App\UserPremium','user_id','id');
    }
    public function user_preferences()
    {
        return $this->hasOne('App\UserPreferences');
    }
    public function user_locations(){
        return $this->hasmany('App\UserLocation');
    }
    public function like(){
        return $this->hasmany('App\Likes','id','like_id');
    }
    public function user_images(){
        return $this->hasmany('App\UserImages');
    }
    public function user_interest(){
        return $this->hasmany('App\UserInterest');
    }
    public function user_block(){
        return $this->hasOne('App\UserBlocked','user_id','id');
    }

    public function user_country()
    {
        return $this->belongsTo('App\Country','country','id');
    }
     


    /*public function blocked_userid()
    {
        return $this->hasMany('App\UserBlocked', 'user_id');
    }

    public function blocked_blockid()
    {
        return $this->hasMany('App\UserBlocked', 'block_id');
    }

    public function user_block() {
        return $this->blocked_userid->merge($this->blocked_blockid);
    }*/

}
