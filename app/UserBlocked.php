<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use Auth;

class UserBlocked extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
	protected $fillable = [
        'user_id', 'block_id', 'status'
    ];
     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'created_at', 'updated_at'
    ];

    public function user()
    {  	
    	return $this->belongsTo('App\User');        
    }
    public function user_block()
    { 
        return $this->belongsTo('App\User','block_id','id');
    }
}
