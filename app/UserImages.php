<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserImages extends Model
{
	use SoftDeletes;
    protected $table = 'user_images';

    // Relation between user_preferences model and user model
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
