<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPreferences extends Model
{
	use SoftDeletes;
    protected $table = 'user_preferences';
    protected $fillable = [
        'user_id','gender','age_limit', 'distance', 'address','latitude','longitude','notification_message', 'notification_match', 'dnd', 'category','is_country_wise',
    ];
    protected $appends = ['location'];

    // Relation between user_preferences model and user model
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function user_premia(){
    	return $this->hasmany('App\UserPremium','user_id','user_id');
    }

    public function getAddressAttribute($value) {
        return json_decode($value);
    }
    public function getLocationAttribute($value){
        if($this->address){            
            $country = json_decode($this->address[0]->country);
            return @$country;
        }       
    }
}
