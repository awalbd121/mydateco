<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPremium extends Model
{
	use SoftDeletes;
    //
    public function premium()
    {
        return $this->belongsTo('App\Premium','premia_id','id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
