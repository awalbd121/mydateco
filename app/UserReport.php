<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserReport extends Model
{
	use SoftDeletes;
    public function user()
    {  	
    	return $this->belongsTo('App\User','user_id','id');      
    }
    public function reporter()
    {  	
    	return $this->belongsTo('App\User','report_id','id');      
    }
}
