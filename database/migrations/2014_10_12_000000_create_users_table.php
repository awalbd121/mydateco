<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique()->nullable();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->enum('gender', ['male', 'female', 'other'])->default('male');
            $table->string('dob')->nullable();
            $table->string('age')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('mobile')->nullable();
            $table->string('country')->nullable();            
            $table->string('device_token')->nullable();
            $table->string('device_id')->nullable();
            $table->string('address')->nullable();
            $table->double('latitude', 15,8)->default(0);
            $table->double('longitude',15,8)->default(0);
            $table->enum('device_type',array('web','android','ios'))->default('web');
            $table->enum('login_by',array('manual','facebook','linkedin'))->default('manual');
            $table->string('social_unique_id')->nullable();
            $table->float('wallet_balance')->default(0);
            $table->string('interest')->nullable();
            $table->string('picture')->nullable();
            $table->string('bio_video')->nullable();
            $table->longtext('about')->nullable();
            $table->string('work')->nullable();
            $table->integer('show_age')->default(1);
            $table->integer('show_distance')->default(1);
            $table->mediumInteger('otp')->default(0);
            $table->string('stripe_cust_id')->nullable();
            $table->string('invite_code')->nullable();
            $table->string('install_code')->nullable();
            $table->rememberToken();
            $table->softDeletes(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
