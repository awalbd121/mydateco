<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_preferences', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('user_id');
            $table->enum('gender',array('male','female','both'))->default('both');
            $table->string('age_limit')->default('18,40');
            $table->integer('distance')->default(10);
            $table->text('address')->nullable();
            $table->double('latitude',15,8)->default(0);
            $table->double('longitude',15,8)->default(0);
            $table->integer('notification_message')->default(0);
            $table->integer('notification_match')->default(0);
            $table->integer('dnd')->default(0);
            $table->integer('category')->default(0);
            $table->integer('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_preferences');
    }
}
