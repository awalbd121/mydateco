<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('receiver_id')->default(0);
            $table->float('amount');
            $table->enum('status',array('ADDED','PAID','RECEIVED','PREMIUM','REFERAL'))->default('ADDED');
            $table->boolean('notify')->default(1);   
            $table->string('payment_id')->nullable(); 
            $table->string('alias_id','255')->nullable();
            $table->enum('profit_status',array('Credit','Debit'))->default('Credit');
            $table->softDeletes();                   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
