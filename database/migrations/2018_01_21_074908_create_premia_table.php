<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan_name');
            $table->string('description')->nullable();
            $table->integer('period')->default(0);
            $table->enum('duration',array('day','month','year'))->default('month');
            $table->float('price')->default(0);
            $table->integer('superlike')->default(0);
            $table->integer('video_call')->default(0);            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premia');
    }
}
