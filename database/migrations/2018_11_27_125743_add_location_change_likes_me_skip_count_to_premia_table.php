<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationChangeLikesMeSkipCountToPremiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('premia', function (Blueprint $table) {
            $table->integer('location_change')->default(0);
            $table->integer('likes_me')->default(0);
            $table->integer('skip_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('premia', function (Blueprint $table) {
            //
        });
    }
}
