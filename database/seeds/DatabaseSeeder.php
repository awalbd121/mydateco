<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(UserPreferencesTableSeeder::class);
        $this->call(AdminTableSeeder::class);
        $this->call(InterestTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(PremiaTableSeeder::class);        
    }
}
