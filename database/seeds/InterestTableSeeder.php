<?php

use Illuminate\Database\Seeder;

class InterestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interests')->truncate();
        DB::table('interests')->insert([
        [
            'name' => 'Reading',
        ],[
        	'name' => 'Shopping',
        ],[
        	'name' => 'Swimming',
        ],[
        	'name' => 'Movies',
        ],[
        	'name' => 'Travel',
        ]
        ]);
    }
}
