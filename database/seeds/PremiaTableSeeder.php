<?php

use Illuminate\Database\Seeder;

class PremiaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('premia')->truncate();
        DB::table('premia')->insert([
        [
            'plan_name' => 'Trial',
            'description' => '',
            'period' => '7',
            'duration' => 'day',
            'price' => '0',
            'superlike' => '5',
            'video_call' => '0',
            'location_change' => '1',
            'likes_me' => '1',
            'skip_count' => '5',
            'like_count' => '5',
            'is_default_trail' => '1',
        ],
        [
            'plan_name' => 'Premiun',
            'description' => '',
            'period' => '6',
            'duration' => 'month',
            'price' => '20',
            'superlike' => '5',
            'video_call' => '1',
            'location_change' => '1',
            'likes_me' => '1',
            'skip_count' => '5',
            'like_count' => '5',
            'is_default_trail' => '0',
        ],
        [
            'plan_name' => 'Garanty',
            'description' => '',
            'period' => '2',
            'duration' => 'year',
            'price' => '30',
            'superlike' => '10',
            'video_call' => '50',
            'location_change' => '1',
            'likes_me' => '1',
            'skip_count' => '5',
            'like_count' => '5',
            'is_default_trail' => '0',
        ],
        [
            'plan_name' => 'SuperPack',
            'description' => '',
            'period' => '1',
            'duration' => 'year',
            'price' => '40',
            'superlike' => '10',
            'video_call' => '50',
            'location_change' => '1',
            'likes_me' => '1',
            'skip_count' => '5',
            'like_count' => '5',
            'is_default_trail' => '0',
        ],
        ]);
    }
}
