<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        DB::table('settings')->insert([
            [
                'key' => 'sitename',
                'value' => 'MyDate'
            ],
            [
                'key' => 'logo',
                'value' => '/design/img/logo.png',
            ],
            [
                'key' => 'login_bg',
                'value' => '/design/img/logo.png',
            ],
            [
                'key' => 'favicon',
                'value' => '/design/img/icon.png',
            ],
            [
                'key' => 'home_tag',
                'value' => ''
            ],
            [
                'key' => 'site_introduction',
                'value' => ''
            ],
            [
                'key' => 'browser_key',
                'value' => ''
            ],
            [
                'key' => 'analytics_code',
                'value' => ''
            ],  
            [
                'key' => 'google_playstore_link',
                'value' => ''
            ], 
             [
                'key' => 'apple_app_store_link',
                'value' => ''
            ], 
            [
                'key' => 'website_link',
                'value' => ''
            ], 
            [
                'key' => 'like_limit_count',
                'value' => ''
            ],
            [
                'key' => 'superlike_limit_count',
                'value' => ''
            ], 
            [
                'key' => 'date_format',
                'value' => 'm d y'
            ],
            [
                'key' => 'contact_number',
                'value' => '9876543210'
            ],
            [
                'key' => 'contact_email',
                'value' => 'admin@xuber.com'
            ],            
            [
                'key' => 'contact_text',
                'value' => 'We will Contact you soon'
            ],
            [
                'key' => 'contact_title',
                'value' => 'MyDate Help'
            ],
            [
                'key' => 'privacy_content',
                'value' => ''
            ],
            [
                'key' => 'terms_content',
                'value' => ''
            ],
            [
                'key' => 'site_copyright',
                'value' => ''
            ],
            [
                'key' => 'currency',
                'value' => '$'
            ],
            [
                'key' => 'fb_app_version',
                'value' => '2'
            ],
            [
                'key' => 'fb_app_id',
                'value' => '119630332175381'
            ],
            [
                'key' => 'fb_app_secret',
                'value' => '47be5a7e2fce5c0f2733ed06211684fc'
            ],
            [
                'key' => 'map_key',
                'value' => 'AIzaSyC808NpIO-MZ_C-nXv21zGFDKC1OUm_MkQ'
            ],
            [
                'key' => 'linkedin_app_id',
                'value' => ''
            ],
            [
                'key' => 'linkedin_app_secret',
                'value' => ''
            ],
            [
                'key' => 'stripe_public_key',
                'value' => 'pk_test_2TdKhREUkWoY1iNI4m17FSn4'
            ],
            [
                'key' => 'stripe_secret_key',
                'value' => 'sk_test_RXJOlWVE7a3qeSj6pC0fUsiz'
            ],
            [
                'key' => 'facebook_status',
                'value' => '0'
            ],
        ]);
    }
}
