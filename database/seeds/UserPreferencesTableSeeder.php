<?php

use Illuminate\Database\Seeder;

class UserPreferencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_preferences')->truncate();
        DB::table('user_preferences')->insert([
        [
            'user_id' => '1',
            'gender' => 'both',
            'age_limit' => '18,40',
        ],[
        	'user_id' => '2',
            'gender' => 'both',
            'age_limit' => '18,40',
        ],[
        	'user_id' => '3',
            'gender' => 'both',
            'age_limit' => '18,40',
        ],[
            'user_id' => '4',
            'gender' => 'both',
            'age_limit' => '18,40', 
        ],[
            'user_id' => '5',
            'gender' => 'both',
            'age_limit' => '18,40',  
        ],[
            'user_id' => '6',
            'gender' => 'both',
            'age_limit' => '18,40',  
        ],[
            'user_id' => '7',
            'gender' => 'both',
            'age_limit' => '18,40',  
        ],[
            'user_id' => '8',
            'gender' => 'both',
            'age_limit' => '18,40',   
         ],[
            'user_id' => '9',
            'gender' => 'both',
            'age_limit' => '18,40', 
         ],[
            'user_id' => '10',
            'gender' => 'both',
            'age_limit' => '18,40',
        ]
        ]);
    }
}
