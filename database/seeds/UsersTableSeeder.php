<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
        [
            'first_name' => 'demo',
            'last_name' => 'demo',
            'gender' => 'male',
            'mobile' => '9876543210',
            'email' => 'admin@tranxit.com',
            'dob' => '08/11/1992',
            'age' => '26',
            'password' => bcrypt('123456'),
            'invite_code' => 'demodemo1',
            'latitude' => '13.05897720',
            'longitude' => '80.25420140',
        ],[
        	'first_name' => 'test',
            'last_name' => 'test',
            'gender' => 'female',
            'mobile' => '7896543210',
            'email' => 'demo@demo.com',
            'dob' => '08/11/1992',
            'age' => '26',
            'password' => bcrypt('123456'),
            'invite_code' => 'demodemo2',
            'latitude' => '13.05897720',
            'longitude' => '80.25420140',
        ],[
        	'first_name' => 'vino',
            'last_name' => 'k',
            'gender' => 'female',
            'mobile' => '9543217890',
            'email' => 'vino@demo.com',
            'dob' => '08/11/1992',
            'age' => '26',
            'password' => bcrypt('123456'),
            'invite_code' => 'demodemo3',
            'latitude' => '13.05897720',
            'longitude' => '80.25420140',
        ],[
            'first_name' => 'test1',
            'last_name' => 'k',
            'gender' => 'female',
            'mobile' => '9543217890',
            'email' => 'test1@demo.com',
            'dob' => '08/11/1992',
            'age' => '26',
            'password' => bcrypt('123456'),   
            'invite_code' => 'demodemo4', 
            'latitude' => '13.05897720',
            'longitude' => '80.25420140',
        ],[
            'first_name' => 'test2',
            'last_name' => 'k',
            'gender' => 'female',
            'mobile' => '9543217890',
            'email' => 'test2@demo.com',
            'dob' => '08/11/1992',
            'age' => '26',
            'password' => bcrypt('123456'), 
            'invite_code' => 'demodemo5', 
            'latitude' => '13.05897720',
            'longitude' => '80.25420140',  
        ],[
            'first_name' => 'test3',
            'last_name' => 'k',
            'gender' => 'male',
            'mobile' => '9543217890',
            'email' => 'test3@demo.com',
            'dob' => '08/11/1992',
            'age' => '26',
            'password' => bcrypt('123456'), 
            'invite_code' => 'demodemo6',  
            'latitude' => '13.05897720',
            'longitude' => '80.25420140', 
        ],[
            'first_name' => 'test4',
            'last_name' => 'k',
            'gender' => 'female',
            'mobile' => '9543217890',
            'email' => 'test4@demo.com',
            'dob' => '08/11/1992',
            'age' => '26',
            'password' => bcrypt('123456'), 
            'invite_code' => 'demodemo7',
            'latitude' => '13.05897720',
            'longitude' => '80.25420140',   
        ],[
            'first_name' => 'test5',
            'last_name' => 'k',
            'gender' => 'male',
            'mobile' => '9543217890',
            'email' => 'test5@demo.com',
            'dob' => '08/11/1992',
            'age' => '26',
            'password' => bcrypt('123456'), 
            'invite_code' => 'demodemo8',  
            'latitude' => '13.05897720',
            'longitude' => '80.25420140', 
         ],[
            'first_name' => 'test6',
            'last_name' => 'k',
            'gender' => 'female',
            'mobile' => '9543217890',
            'email' => 'test6@demo.com',
            'dob' => '08/11/1992',
            'age' => '26',
            'password' => bcrypt('123456'), 
            'invite_code' => 'demodemo9',
            'latitude' => '13.05897720',
            'longitude' => '80.25420140',   
         ],[
            'first_name' => 'test7',
            'last_name' => 'k',
            'gender' => 'male',
            'mobile' => '9543217890',
            'email' => 'test7@demo.com',
            'dob' => '08/11/1992',
            'age' => '26',
            'password' => bcrypt('123456'),
            'invite_code' => 'demodemo10',
            'latitude' => '13.05897720',
            'longitude' => '80.25420140', 
        ]
        ]);
    }
}
