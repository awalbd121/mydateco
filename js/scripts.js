$(document).ready(function() {

    var $window = $(window);

    function setCSS() {
        // var windowHeight = $(window).innerHeight();
        var windowHeight = $(window).height();
        var windowWidth = $(window).width();

        var navHeight = $(".navbar-default").outerHeight(true);

        var bodyHeight = windowHeight - navHeight;

        $('.body-height').css('height', bodyHeight);

        // var logLeft = $(".login-left").outerHeight(true);
        // var designSec = ($(window).height() - 200);

        // $('.login-right-img').css('height', logLeft);
        // $('.modal-left').css('height', windowHeight);
        // $('.modal-right').css('height', windowHeight);
        // // $('.comment-list').css('height', commentSec);
        // $('.design-sec').css('height', designSec);
        // $('#sel-designer-modal').css('height', windowHeight);
    };

    setCSS();
    $(window).on('load resize shown.bs.modal', function() {
        setCSS();
    });
});

//Match Slider
$(".match-slide").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    swipe: false,
    swipeToSlide: false,
    draggable: false,
    touchMove: false,
    speed: 500,
    fade: true,
    focusOnSelect: false
})

$(".match-slide").find(".slick-slide").on("click load", function() {
    $(".match-slide").slick("slickNext");
});

// Tinder Slider
$(document).ready(function() {

    $(".buddy").on("swiperight", function() {
        $(this).addClass('rotate-left').delay(700).fadeOut(1);
        $('.buddy').find('.status').remove();

        $(this).append('<div class="status like">Like!</div>');
        if ($(this).is(':last-child')) {
            $('.buddy:nth-child(1)').removeClass('rotate-left rotate-right').fadeIn(300);
        } else {
            $(this).next().removeClass('rotate-left rotate-right').fadeIn(400);
        }
    });

    $(".buddy").on("swipeleft", function() {
        $(this).addClass('rotate-right').delay(700).fadeOut(1);
        $('.buddy').find('.status').remove();
        $(this).append('<div class="status dislike">Dislike!</div>');

        if ($(this).is(':last-child')) {
            $('.buddy:nth-child(1)').removeClass('rotate-left rotate-right').fadeIn(300);
        } else {
            $(this).next().removeClass('rotate-left rotate-right').fadeIn(400);
        }
    });

});

// Toggle Switch
$("#switch1").toggleSwitch();
$("#switch2").toggleSwitch();
$("#switch3").toggleSwitch();
$("#switch4").toggleSwitch();
$("#switch5").toggleSwitch();
$("#all").toggleSwitch();
$("#country").toggleSwitch();