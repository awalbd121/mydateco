$(document).ready(function() {

    var $window = $(window);

    function setCSS() {
        // var windowHeight = $(window).innerHeight();
        var windowHeight = $(window).height();
        var windowWidth = $(window).width();

        var navHeight = $(".navbar-default").outerHeight(true);
        var logRight = $(".login-right").outerHeight(true);
        var chatHead = $(".chat-head").outerHeight(true);
        var chatFoot = $(".chat-foot").outerHeight(true);
        var rightFoot = $(".right-foot").outerHeight(true);
        var centerHead = $(".center-box-head").outerHeight(true);
        var logosecHeight = $(".logo").outerHeight(true);

        var chatTopBtm = chatHead + chatFoot;
        var chatTopBtmLogo = chatTopBtm + logosecHeight;
        // var chatTopBtmNav = chatTopBtm + navHeight;
        var chatBlock = windowHeight - chatTopBtm;
        var mdchatBlock = windowHeight - chatTopBtmLogo;
        var bodyHeight = windowHeight - navHeight;
        var centerContent = windowHeight - centerHead;
        var rightsecContent = bodyHeight - rightFoot;

        $('.body-height').css('height', bodyHeight);
        $('.login').css('min-height', windowHeight);
        $('.login-left').css('height', logRight);
        $('.chat-block').css('height', chatBlock);
        $('.md-chat-block').css('height', mdchatBlock);
        $('.center-height').css('height', centerContent);
        $('.right-sidebar-content').css('height', rightsecContent);
    };

    setCSS();
    $(window).on('load resize shown.bs.modal', function() {
        setCSS();
    });
});

function checkWidth(init) {
    /*If browser resized, check width again */
    if ($(window).width() < 768) {
        $('.chat-block').addClass('md-chat-block');
    } else {
        if (!init) {
            $('.chat-block').removeClass('md-chat-block');
        }
    }

    if ($(window).width() < 768) {
        $('.chat-block').addClass('md-chat-block');
    } else {
        if (!init) {
            $('.chat-block').removeClass('md-chat-block');
        }
    }
}

$(document).ready(function() {
    checkWidth(true);

    $(window).resize(function() {
        checkWidth(false);
    });
});

//Match Slider
$(".match-slide").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    swipe: false,
    swipeToSlide: false,
    draggable: false,
    touchMove: false,
    speed: 500,
    fade: false,
    focusOnSelect: false,
    autoplay: true
})

$(".match-slide").find(".slick-slide").on("click load", function() {
    $(".match-slide").slick("slickNext");
});



// Toggle Switch
$("#switch1").toggleSwitch();
$("#switch2").toggleSwitch();
$("#switch3").toggleSwitch();
$("#switch4").toggleSwitch();
$("#switch5").toggleSwitch();
$("#all").toggleSwitch();
$("#country").toggleSwitch();

//Match Slider
$(".premium-slider").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    swipe: false,
    swipeToSlide: false,
    draggable: false,
    touchMove: false,
    speed: 500,
    fade: false,
    focusOnSelect: false,
    autoplay: true
});

$(document).ready(function() {
    if ($('.ui-checkbox').hasClass('ui-checkbox')) {
        $('.ui-checkbox').removeClass('ui-checkbox')
    };
});

/*$(".recom-block").click(function() {
    //$('a.recom-block').removeClass('active')
    $(this).addClass('active');

});*/
$(".recom-block").click(function() {
    $(this).toggleClass('active');
});

$(document).ready(function() {
    $(".report-block").click(function() {
        if ($('.report-block').hasClass('active')) {
            $('.report-block').removeClass('active');
            $(this).addClass('active');
        };
    });
    toastr.options.timeOut = 5000; // 3s

});
$(".submenu-item").click(function() {
    jQuery(this).parent().toggleClass('has-submenu');
 });

