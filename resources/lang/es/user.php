<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during User for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'auth' => [
        //Login Page
        'login' => 'Login',
        'signup' => 'Signup',
        'login_title' => 'Login to Your Account',        
        'login_with' => 'OR Login with',
        'forgot_password' => 'Forgot Password',
        'email' => 'Your Email',
        'password' => 'Password',
        'submit' => 'Submit',

        //Signup page
        'location' => 'Location',  
        'signup_title' => 'Create Your Account',
      ],

      'menu' => [
        'view_profile' => 'View Profile',
        'find_matches' => 'Find Matches',
        'card_view' => 'Card View',
        'map_view' => 'Map View',
        'matches' => 'Matches',
        'who_likes_me' => 'Who likes Me',
        'discovery_settings' => 'Discovery Settings',
        'chats' => 'Chats',
        'payment' => 'Payment',
        'transaction_history' => 'Transaction History',
        'notifications' => 'Notifications',
        'invites' => 'Invites',
        'get_premium' => 'Get Premium',
        'privacy_policy' => 'Privacy Policy',
        'terms' => 'Terms',
        'terms_conditions' => 'Terms &amp; Conditions',
        'contact_us' => 'Contact Us',
        'logout' => 'Logout',
      ],        

      'user' => [
        //Profile 
        'profile' => 'Profile',
        'distance_visible' => 'Make My Distance Visible',
        'show_my_age' => 'Don\'t Show My Age',
        'control_profile' => 'Control Your Profile',
        'current_work' => 'Current Work',
        'gender' => 'Gender',
        'dob' => 'DOB',
        'phone_number' => 'Phone Number',
        'last_name' => 'Last Name',
        'first_name' => 'First Name',
        'expiry_date_at' => 'Expiry date at',
        'boost_your_account' => 'Boost your account',
        'account' => 'account',
        'account_detail' => 'Account Detail',
        'trial_account' => 'Trial account',
        'select_interest' => 'Select Interest',
        'upload_bio_video' => 'Upload Bio Video',
        'choose_file' => 'Choose File',
        'edit_profile' => 'Edit Profile',
        'location' => 'Location',
        'language' => 'Language',
        //sidebar
        'about' => 'About',
        'bio_video' => 'Bio Video',
        'interests' => 'Interests',
        'recommend' => 'Recommend',
        'report' => 'Report',
        //Report this user
        'report_this_user' => 'Report this user',
        'inappropriate_messages' => 'Inappropriate Messages',
        'inappropriate_photos' => 'Inappropriate Photos',
        'bad_offline_behaviour' => 'Bad Offline Behaviour',
        'feel_like_spam' => 'Feel like spam',
        
        //Recommended Friend
        'recommended_friend' => 'Recommended Friend',
        'select_your_friend' => 'Select Your Friend',
        'no_friend_found' => 'No Friend Found',
        'send_request' => 'Send Request',

        'view_user' => 'View User',
      ],

      'form' => [
        'close' => 'Close',
        'submit' => 'Submit',
        'save' => 'Save',
        'add' => 'Add',
        'share' => 'Share',
        'back' => 'Back',
        'get_pro' => 'Get Pro',
        'share_amount' => 'Share Amount',
        'get_now' => 'Get Now',
        'edit_profile' => 'Edit Profile',
        'cancel' => 'Cancel',
        'copy' => 'Copy'
      ],

      'matches' => [
        'match_title' => 'Matches List',
        'chat' => 'Chat',
        'not_found' => 'No :name Found',
        'who_likes_me_title' => 'Who likes Me',
        'away' => 'Away',

        //notifications
        'you_get' => 'You get',
        'recommend_from' => 'recommend from',
        'sent_message_you' => 'sent message to you',
        'sent_amount_you' => 'sent amount to your wallet',
        'your_profile' => 'your profile',

        //Invite
        'invite_friends' => 'Invite Friends',
        'invite' => 'Invite friends to :sitename',
        'invite_quote' => 'You will get :refer_money credits  to your account for each friends install the application',
        'sharing_url' => 'Your :sitename sharing URL',     
        'share_facebook' => 'Share URL to your Facebook account', 

        //Premium
        'get_plus' => 'Get :sitename Plus',

        //sidebar add
        'share_amount' => 'Share Amount To Your Friends',
        'share_amount_quote' => 'Share money to your friends from your wallet',
        'get_premium' => 'Get :sitename Premium',
        'get_premium_quote' => 'Get unlimited likes, no google ads etc',
        'get_plus' => 'Get :sitename Plus',
        'no_user_found' => 'No User found',

        //findmatch
      ],

      'setting' => [
        'setting_title' => 'Discover Settings',
        'max_distance' => 'Max Distance',
        'age_range' => 'Age Range',
        'looking_for' => 'Looking For',
        'dnd' => 'DND',
        'notifications' => 'Notifications',
      ],

      'chat' => [
        'chat_list' => 'Chats List',
        'chat' => 'Chat',
        'enter_message' => 'Enter Message Here',
        'download_file' => 'Download File',
        'admin' => 'Admin',
      ],

      'payment' => [
        'add_money' => 'Add Money',
        'share_amount' => 'Share Amount',
        'saved_card' => 'Saved Card',
        'available_balance' => 'Available Balance',
        'add_new_card' => 'Add new card',
        'view_history' => 'View History',
        'enter_amount' => 'Enter Amount',
        'select_card' => 'Select Card',
        'select_name' => 'Select Name',

        //History
        'all' => 'All',
        'paid' => 'Paid',
        'received' => 'Received',
        'added' => 'Added',
        'all_transaction' => 'All Transactions',
        'details' => 'Details',
        'amount' => 'Amount',
        'add_wallet' => 'Added to Wallet',
        'from_bank' => 'From Bank',
        'cash_sent' => 'Cash Sent',
        'to' => 'To',
        'cash_received' => 'Cash Received',
        'from' => 'From',
        'paid_subscription' => 'Paid for subscription',
        'getting_premium' => 'For getting premium of :sitename',

        //Premium
        'swipe_around' => 'Swipe around the World',
        'passport_anyware' => 'Passport to anywhere with :sitename plus',
        'stand_superlike' => 'Stand out with Super Likes',
        'superlike_3daily' => 'You can do 3 times super like a day to get a match',
        'get_plus' => 'Get :sitename Plus',
        'get_unlimited' => 'Get Unlimited Super likes & more',
      ],

      'alert' => [
        'logout_message' => 'Are you sure want to logout',
        'delete_card_message' => 'Are you sure want to delete this card',
        'select_friend' => 'Please select friend',
        'boost_wallet' => 'Please boost your wallet',
        'already_trial' => 'You are already in Trial pack',
        'superlike_limit_reached' => 'Your superlike limit reached, Get premium to continue',
        'boost_wallet' => 'Please boost your wallet',
        'already_trial' => 'You are already in Trial pack',

        //controller
        'verify_password' => 'Please verify your old password',
        'password_updated' => 'Password Updated',
        'select_friend' => 'Please select friend',
        'boost_wallet' => 'Please boost your wallet',
        'already_trial' => 'You are already in Trial pack',
        'superlike_limit_reached' => 'Your superlike limit reached, Get premium to continue',
        'boost_wallet' => 'Please boost your wallet',
        'already_trial' => 'You are already in Trial pack',
        'premium_account' => 'Congratulations! Your Premium Account is Activate'

      ],

      'contact_us' => [
        'help_support' => 'HELP / SUPPORT',
        'technical' => 'For all things technical and app-related',
        'reach_facsimile' => 'or reach us by facsimile at',
        'partnerships' => 'PARTNERSHIPS',
        'partnering' => 'Interested in partnering with :sitename?',
        'press' => 'Press',
        'success_stories' => 'Success Stories',
        'important_person' => 'Did you meet the most important person in your life on :sitename? Tell us about it',
      ],
      'admin' =>[
        'demomode' => '"CRUD Feature" has been disabled on the Demo Admin Panel. This feature will be enabled on your product which you will be purchasing, meahwhile if you have any queries feel free to contact our 24/7 support at info@appdupe.com.',
      ], 

      

];
