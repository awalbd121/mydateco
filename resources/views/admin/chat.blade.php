@extends('admin.layout.base')

@section('title', 'Chat')

@section('content')
<style type="text/css">
    /*=========================================== 
Chat Page CSS
===========================================*/

.chat-head {
    padding: 20px;
    border-bottom: 1px solid #e5e9ec;
}

.chat-block {
    overflow-y: auto;
    overflow-x: hidden;
}

.chat-block::-webkit-scrollbar-track {
    border-radius: 10px;
    background-color: #F5F5F5;
}

.chat-block::-webkit-scrollbar {
    width: 7px;
    background-color: #F5F5F5;
}

.chat-block::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
    background-color: #ccc;
}

.chat-img {
    height: 40px;
    width: 40px;
    border-radius: 100%;
    box-shadow: 0 2px 10px 0 rgba(146, 154, 160, 0.77);
    margin-right: 12px;
}

.chat-txt {
    padding: 15px;
    /*box-shadow: 0 2px 10px 0 rgba(146, 154, 160, 0.77);*/
    /*max-width: calc(100% - 40%);*/
    display: inline-block;
    font-size: 13px;
    color: #454545;
    -webkit-filter: drop-shadow(0 2px 3px rgba(203, 214, 219, .6));
    filter: drop-shadow(0 2px 3px rgba(203, 214, 219, .6));
}

.chat-left-txt {
    border-radius: 0px 20px 20px 20px;
    background: #f5f5f5;
}

.chat-left-txt.txt-only{chat
    background: #FB469C;chat
    color: #fff;
}

.chat-right-txt {
    border-radius: 20px 0px 20px 20px;
    background: #fff;
}

.chat-view-txt {
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    height: 20px;
}

.chat-det {
    max-width: calc(100% - 40%);chat
}

.chat-time {
    color: #999;
    font-size: 10px;
}

.chat-det-box {
    margin-bottom: 15px;
}

.msg-snd {
    border: 0;
    box-shadow: 0px -1px 8px 0px rgba(0, 0, 0, .2);
    border-radius: 0px;
    height: 47px;
}

#send {
    border: 0;
    box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, .3);
    font-size: 24px;
    padding: 10px;
    cursor: pointer;
}

.chat-sub-btn {
    width: 100%;
    /*padding: 10px 10px 10px 16px;*/
    border: 0;
}

.chat-send {    
    background-image: -moz-linear-gradient( 0deg, rgb(251, 102, 92) 0%, rgb(247, 69, 115) 100%);
    background-image: -webkit-linear-gradient( 0deg, rgb(251, 102, 92) 0%, rgb(247, 69, 115) 100%);
    background-image: -ms-linear-gradient( 0deg, rgb(251, 102, 92) 0%, rgb(247, 69, 115) 100%);
}

.attach {
    background: #bbb;
    background-image: none !important;
    cursor: pointer;
}

.upload-btn-wrapper {
    position: relative;
    overflow: hidden;
    display: inline-block;
    cursor: pointer;
}

.upload-btn {
    background-color: transparent;
    border: 0;
    /*padding: 8px 20px;*/
    color: #333;
    cursor: pointer;
}

.upload-btn-wrapper input[type=file] {
    /*font-size: 100px;*/
    position: absolute;
    left: 0;
    top: 0;
    opacity: 0;
    cursor: pointer;
}

.chat-download-icon {
    display: inline-block;
    padding: 4px 8px;
    background-color: #f4f4f4;
    background-image: -moz-linear-gradient( 0deg, rgb(251, 102, 92) 0%, rgb(247, 69, 115) 100%);
    background-image: -webkit-linear-gradient( 0deg, rgb(251, 102, 92) 0%, rgb(247, 69, 115) 100%);
    background-image: -ms-linear-gradient( 0deg, rgb(251, 102, 92) 0%, rgb(247, 69, 115) 100%);
    line-height: 0;
    color: #fff;
    margin-left: 5px;
    cursor: pointer;
}

.file-icon {
    font-size: 60px;
}

.send-img {
    height: 150px;
    width: 150px;
    display: block;
}
.bg-img {
    background-size: cover !important;
    background-position: center center !important;
    background-repeat: no-repeat !important;
}
</style>
<div class="content-area py-1">
    <div class="container-fluid">        
        <div class="box box-block bg-white">
            <h5 class="mb-1">Chat God View</h5>
            <form method="post" action="{{url('admin/chat')}}">
            {{csrf_field()}}
            <div class="form-group row">
                <label for="user_id" class="col-xs-12 col-form-label">Select User</label>
                <div class="col-xs-12">
                    <select class="form-control"  name="user" id="user_id" required="" onchange="get_chatter(this.value)">
                        <option value="">Select User</option>
                        @foreach($users as $user_list)
                        <?php 
                        $select='';
                        if(count($user)>0){
                            if($user_list->id==$user->id) $select='selected';
                        }?>
                        <option value="{{$user_list->id}}" {{$select}}>{{$user_list->first_name}} {{$user_list->last_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="chater_id" class="col-xs-12 col-form-label">Select Chater</label>
                <div class="col-xs-12">
                    <select class="form-control"  name="sender" id="chater_id" required="" onchange="">
                        <option value="">Select Match</option>
                        @if(count($user_friends)>0)
                        @foreach($user_friends as $user_friend)
                            @php
                            if($user_friend->has('user_like'))
                                $chat_user=$user_friend->user_like;               
                            else
                                $chat_user=$user_friend->user;                          
                            @endphp
                        <option value="{{$chat_user->id}}" @if($chat_user->id==$sender->id) {{'selected'}}@endif>{{$chat_user->first_name}} {{$chat_user->last_name}}</option>
                        @endforeach
                        @endif
                        
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="zipcode" class="col-xs-12 col-form-label"></label>
                <div class="col-xs-10">
                    <button class="btn btn-primary">View Chat</button>
                    <!-- <a href="" class="btn btn-default">Cancel</a> -->
                </div>
            </div>
            </form>
            @if(count($sender)>0)
            <div id="userchats">
                <div class="chat-block p-f-15">
                    <div v-for="chat in chatMessages">
                        <!-- Right Chat Starts -->
                        <div class="right-chat row m-0" v-if="chat.user=={{$user->id}}" >
                            @if($user->picture)
                                <div class="chat-img bg-img pull-right" style="background-image: url({{$user->picture}});"></div>
                            @else 
                                <div class="chat-img bg-img pull-right" style="background-image: url({{asset('design/img/user.png')}});"></div>
                            @endif
                            <div class="chat-det pull-right">
                                <div class="chat-det-box" v-if="chat.type == 'text'">
                                    <p class="chat-txt chat-right-txt">@{{chat.text}}</p>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>
                                <div class="chat-det-box" v-else-if="chat.type == 'image'">
                                    <p class="chat-txt chat-right-txt text-center">
                                        <span class="send-img bg-img" v-bind:style="{ backgroundImage: 'url(' + chat.url + ')' }"></span>
                                    </p>
                                    <a class="chat-download-icon" title="download" v-bind:href="chat.url" target="_blank" download=""><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div> 
                                <div class="chat-det-box" v-else-if="chat.type == 'video'"> 
                                    <video class="video_play" width="310" controls="true">
                                        <source v-bind:src="chat.url">Your browser doesn't support HTML5 video tag.
                                    </video> 
                                    <a class="chat-download-icon" title="download" v-bind:href="chat.url" target="_blank" download=""><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>
                                <div class="chat-det-box" v-else>
                                    <p class="chat-txt chat-right-txt text-center" >
                                        <i class="zmdi zmdi-file-text file-icon"></i>
                                        <br> Download File
                                    </p>
                                    <a title="download" v-bind:href="chat.url" target="_blank" download="" class="chat-download-icon"><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>                                   
                            </div>
                        </div>
                        <!-- Right Chat Box Ends -->
                        <!-- Left Chat Starts -->
                        <div class="left-chat row m-0" v-else>
                            @if($sender->picture)
                                <div class="chat-img bg-img pull-left" style="background-image: url({{$sender->picture}});"></div>
                            @else 
                                <div class="chat-img bg-img pull-left" style="background-image: url({{asset('design/img/user.png')}});"></div>
                            @endif
                            <div class="chat-det pull-left">
                                <div class="chat-det-box" v-if="chat.type == 'text'">
                                    <p class="chat-txt chat-left-txt txt-only">@{{chat.text}}</p>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>
                                <div class="chat-det-box" v-else-if="chat.type == 'image'">
                                    <p class="chat-txt chat-left-txt text-center">
                                        <span class="send-img bg-img" v-bind:style="{ backgroundImage: 'url(' + chat.url + ')' }"></span>
                                    </p>
                                    <a class="chat-download-icon" title="download" v-bind:href="chat.url" target="_blank" download=""><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div> 
                                <div class="chat-det-box" v-else-if="chat.type == 'video'"> 
                                    <video class="video_play" width="310" controls="true">
                                        <source v-bind:src="chat.url">Your browser doesn't support HTML5 video tag.
                                    </video> 
                                    <a class="chat-download-icon" title="download" v-bind:href="chat.url" target="_blank" download=""><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>
                                <div class="chat-det-box" v-else>
                                    <p class="chat-txt chat-left-txt text-center" >
                                        <i class="zmdi zmdi-file-text file-icon"></i>
                                        <br> Download File
                                    </p>
                                    <a title="download" v-bind:href="chat.url" target="_blank" download="" class="chat-download-icon"><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>                                   
                            </div>
                        </div>
                        <!-- Left Chat Box Ends -->                        
                    </div>
                </div>
                <!-- Chat Block Ends -->
                <!-- Chat Footer Starts -->
                <div class="chat-foot">
                    <form @submit.prevent="send" enctype="multipart/form-data">
                        <div class="input-group">
                        <!-- For chat text box!-->
                            <!-- <input type="text" class="form-control msg-snd" v-model="txt" placeholder="Enter Message Here"> -->
                            <!-- <div id="image-preview-small">
                                <label for="image-upload-small" id="image-label-small"><i class="zmdi zmdi-plus"></i></label>
                                <input type="file" name="image1" id="image-upload-small" v-model="file" @change="sendfile" />
                            </div> -->
                            <!-- <input type="file" name="image1" id="image-upload-small" v-model="file" @change="sendfile" /> -->
                            <!-- <span class="input-group-addon attach"> 
                                <div class="upload-btn-wrapper">
                                    <button class="upload-btn chat-sub-btn"><i class="ti-clip"></i></button>
                                    <input type="file" name="myfile" v-model="file" @change="sendfile"  />
                                </div>
                            </span>
                            <span class="input-group-addon " id="send">
                                <button type="submit" class="chat-send chat-sub-btn"><i class="ti-check"></i></button>
                            </span> -->
                        </div>
                    </form>                    
                </div>
                <!-- Chat Footer Ends -->
            </div>  
            @endif
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.3/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.1/vue.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.7.0/firebase.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script type="text/javascript">
    function get_chatter(user_id){
        var url    = "{{url('admin/user_friends')}}";        
        var token  = "{{csrf_token()}}";
        $.ajax({
            type: "POST",
            url : url,
            data: {'user_id' :user_id,'_token' :token},
            success: function(html)
            {
                var user_length=html.length;
                $('#chater_id').html('<option value="">select chater</option>');
                $.each(html, function(key,val) {             
                    //console.log(val);  
                    if(typeof val.user_like != 'undefined'){                        
                        var like_user=val.user_like;
                    }else{
                        var like_user=val.user;
                    }
                    $('#chater_id').append('<option value="'+like_user.id+'">'+like_user.first_name+' '+like_user.last_name+'</option>');       
                });
            }
        });   
    }
    <?php if(count($sender)>0){ ?>
    //console.log(sender);
    var config = {
        apiKey: "{{Setting::get('FIREBASE_API_KEY')}}",
        authDomain: "{{Setting::get('FIREBASE_AUTH_DOMAIN')}}",
        databaseURL: "{{Setting::get('FIREBASE_DATABASE_URL')}}",
        projectId: "{{Setting::get('FIREBASE_PROJECT_ID')}}",
        storageBucket: "{{Setting::get('FIREBASE_STORAGE_BUCKET')}}",
        messagingSenderId: "{{Setting::get('FIREBASE_MESSAGING_SENDERID')}}",
    };
    
    firebase.initializeApp(config);

    // Initialize Firebase
    var user={!! $user->id !!};
    var sender={!! $sender->id !!};
    if(parseInt(user)<parseInt(sender)){
        var room_name=user+'_chats_'+sender;
    }
    else{
        var room_name=sender+'_chats_'+user;
    }
    console.log(room_name);

    var database = firebase.database().ref(room_name);//.push();
    // Get the Storage service for the default app
    var storageRoom = firebase.storage();
    
    function writeData(text) {        
      database.push({
        text: text,
        type: 'text',
        timestamp: Date.now(),
        user: user,
        sender: sender,
        read: 0,
        admin:1,
      })
    };

    function writeFile(file) {
      var file_type =file.type.split('/'); 

      // Upload the image to Cloud Storage.
      var filePath = room_name+ '/' + Date.now()+file.name;
      storageRoom.ref(filePath).put(file).then(function(snapshot) {        

        // Get the file's Storage URI and update the chat message placeholder.
        var fullPath = snapshot.metadata.downloadURLs[0];
        database.push({
            url: fullPath,
            type: file_type[0],
            timestamp: Date.now(),
            user: user,
            sender: sender,
            read: 0,
            admin:1,
          })
        //return data.update({url: storageRoom.ref(filePath).toString()});
        
        }.bind(this)).catch(function(error) {
          console.error('There was an error uploading a file to Cloud Storage:', error);
        });
    };      
    function toBottom() {
        document.querySelector('.chat-block').scrollTop = 1000;      
    }
    
    new Vue({
        el: '#userchats',
        data: {
            chatMessages: [],
            txt: '',
            file: ''
        },
        mounted: function() {
            var self = this;
            var now = Date.now();
            database.on('child_added', function(data) {                
                //console.log(data.key, data.val(), moment(data.timestamp).fromNow());
                var dataval=data.val();
                console.log(dataval);
                
                self.chatMessages.push(dataval); 
               
              setTimeout(function() {
                toBottom();
              }, 0)
            });
        },
        methods: {
            send: function(e) {
                //console.log('send');
                let txt = this.txt.trim();
                if (txt) {
                    // send it
                    writeData(txt);
                    this.txt = '';
                }
            },
            sendfile: function(e) {
                $('#loader_id').show();
                var file = e.target.files || e.dataTransfer.files;
                console.log(file[0]);                
                if (file[0]) {
                    // send it
                    writeFile(file[0]);
                    this.file = '';
                }
            }
        }
    })
    <?php } ?>
</script>
@endsection

