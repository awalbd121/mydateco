@extends('admin.layout.base')

@section('title', 'Dashboard ')

@section('styles')
	<link rel="stylesheet" href="{{asset('main/vendor/jvectormap/jquery-jvectormap-2.0.3.css')}}">
@endsection

@section('content')
<div class="content-area py-1">
    <div class="container-fluid"> 
        <div class="row row-md">
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-danger"></span><i class="ti-rocket"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total No. of Users</h6>
					<h1 class="mb-1">{{$data['users']->count()}}</h1>
					<span class="tag tag-danger mr-0-5">{{Setting::get('sitename','MyDate')}}</span>
					<span class="text-muted font-90"></span>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-success"></span><i class="ti-bar-chart"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total No. of Males</h6>
					<h1 class="mb-1">{{$data['males']->count()}}</h1>
					<i class="fa fa-caret-up text-success mr-0-5"></i>
					@if($data['males']->count() != 0 && $data['users']->count() != 0)
						<span>{{round(($data['males']->count()/$data['users']->count()*100),2)}}% from total user</span>
					@else
						<span>0 total user</span>
					@endif
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-warning"></span><i class="ti-archive"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total No. of Females</h6>
					<h1 class="mb-1">{{$data['females']->count()}}</h1>
					<i class="fa fa-caret-down text-danger mr-0-5"></i><span>{{round(($data['females']->count()/$data['users']->count()*100),2)}}% from total user</span>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-primary"></span><i class="ti-view-grid"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total No. of Premium Users</h6>
					<h1 class="mb-1">{{$data['premium']->count()}}</h1>
					<span class="tag tag-primary mr-0-5">+{{$data['users']->count()-$data['premium']->count()}}</span>
					<span class="text-muted font-90">trial users</span>
				</div>
			</div>
		</div>		
	</div>
	<div class="box box-block bg-white">
		<div class="clearfix mb-1">
			<h5 class="float-xs-left">User Location</h5>
			<div class="float-xs-right">
				<button class="btn btn-link btn-sm text-muted" type="button"><i class="ti-close"></i></button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div id="world" style="height: 400px;"></div>
			</div>
		</div>
	</div>


    </div>
</div>

@endsection
@section('scripts')
	<script type="text/javascript" src="{{asset('main/vendor/jvectormap/jquery-jvectormap-2.0.3.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('main/vendor/jvectormap/jquery-jvectormap-world-mill.js')}}"></script>

	<script type="text/javascript">
		$(document).ready(function(){

		        /* Vector Map */
		    $('#world').vectorMap({
		        zoomOnScroll: false,
		        map: 'world_mill',
		        markers: [
		        @foreach($data['users'] as $user)
		        	{latLng: [{{$user->latitude}}, {{$user->longitude}}], name: '{{$user->first_name}}'},	
		        @endforeach
		        ],
		        normalizeFunction: 'polynomial',
		        backgroundColor: 'transparent',
		        regionsSelectable: true,
		        markersSelectable: true,
		        regionStyle: {
		            initial: {
		                fill: 'rgba(0,0,0,0.15)'
		            },
		            hover: {
		                fill: 'rgba(0,0,0,0.15)',
		            stroke: '#fff'
		            },
		        },
		        markerStyle: {
		            initial: {
		                fill: '#43b968',
		                stroke: '#fff'
		            },
		            hover: {
		                fill: '#3e70c9',
		                stroke: '#fff'
		            }
		        },
		        series: {
		            markers: [{
		                attribute: 'fill',
		                scale: ['#43b968','#a567e2', '#f44236'],
		                values: [200, 300, 600, 1000, 150, 250, 450, 500, 800, 900, 750, 650]
		            },{
		                attribute: 'r',
		                scale: [5, 15],
		                values: [200, 300, 600, 1000, 150, 250, 450, 500, 800, 900, 750, 650]
		            }]
		        }
		    });
		});
	</script>

@endsection

