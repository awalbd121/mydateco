<!-- Footer -->
<footer class="footer">
	<div class="container-fluid">
		<div class="row text-xs-center">
			<div class="col-sm-4 text-sm-left mb-0-5 mb-sm-0">
				2018 © {{Setting::get('sitename','MyDate')}}
			</div>
		</div>
	</div>
</footer>