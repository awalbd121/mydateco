<div class="site-sidebar">
	<div class="custom-scroll custom-scroll-light">
		<ul class="sidebar-menu">
			<li class="menu-title">Main</li>
			<li>
				<a href="{{url('admin/dashboard')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-anchor"></i></span>
					<span class="s-text">Dashboard</span>
				</a>
			</li>
			<li>
				<a href="{{url('admin/chat')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-comments"></i></span>
					<span class="s-text">Chat View</span>
				</a>
			</li>
			
			<li class="menu-title">Members</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-crown"></i></span>
					<span class="s-text">Users</span>
				</a>
				<ul>
					<li><a href="{{route('admin.user.index')}}">User List</a></li>
					<li><a href="{{route('admin.user.create')}}">Add New User</a></li>
				</ul>
			</li>
			<li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-palette"></i></span>
					<span class="s-text">Interests</span>
				</a>
				<ul>
					<li><a href="{{route('admin.interest.index')}}">Interest List</a></li>
					<li><a href="{{route('admin.interest.create')}}">Add New Interest</a></li>
				</ul>
			</li>
			<li class="menu-title">Subscription and Payments</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-infinite"></i></span>
					<span class="s-text">Subscription</span>
				</a>
				<ul>
					<li><a href="{{route('admin.premium.index')}}">Subscription List</a></li>
					<li><a href="{{route('admin.premium.create')}}">Add New Subscription</a></li>
				</ul>
			</li>
			<li>
				<a href="{{url('admin/admin-transaction')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-money"></i></span>
					<span class="s-text">Admin Transactions</span>
				</a>
			</li>
			<li>
				<a href="{{url('admin/user-transaction')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-money"></i></span>
					<span class="s-text">User Transactions</span>
				</a>
			</li>
			<li>
				<a href="{{url('admin/user-premium')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-money"></i></span>
					<span class="s-text">User Subscription</span>
				</a>
			</li>
			<li class="menu-title">Matches</li>
			<li>
				<a href="{{url('admin/matches')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-heart"></i></span>
					<span class="s-text">Match Details</span>
				</a>
			</li>
			<li class="menu-title">Settings</li>
			<li>
				<a href="{{url('admin/settings')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-settings"></i></span>
					<span class="s-text">Setting</span>
				</a>
			</li>
			<li>
				<a href="{{url('admin/static_pages')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-smallcap"></i></span>
					<span class="s-text">Static Pages</span>
				</a>
			</li>
			<li class="menu-title">Others</li>
			<li>
				<a href="{{url('admin/push')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-smallcap"></i></span>
					<span class="s-text">Custom Push</span>
				</a>
			</li>
			<li>
				<a href="{{url('admin/user-report')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-user"></i></span>
					<span class="s-text">User Report</span>
				</a>
			</li>			
			<li class="menu-title">Account</li>
			<li>
				<a href="{{url('admin/profile')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-user"></i></span>
					<span class="s-text">Account Settings</span>
				</a>
			</li>
			<li>
				<a href="{{url('admin/password')}}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-exchange-vertical"></i></span>
					<span class="s-text">Change Password</span>
				</a>
			</li>
			<li class="compact-hide">
				<a href="{{ url('/admin/logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
					<span class="s-icon"><i class="ti-power-off"></i></span>
					<span class="s-text">Logout</span>
                </a>

                <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
			</li>
			
		</ul>
	</div>
</div>