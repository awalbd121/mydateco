@extends('admin.layout.base')

@section('title', 'Add Interest ')


@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.interest.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Interest</h5>

            <form class="form-horizontal" action="{{route('admin.interest.store')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
				<div class="form-group row">
					<label for="name" class="col-xs-12 col-form-label">Interest Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('name') }}" name="name" required id="name" placeholder="Interest Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="description" class="col-xs-12 col-form-label">Interest Description</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('description') }}" name="description" id="description" placeholder="Interest Description">
					</div>
				</div>

				<div class="form-group row">
					<label for="image" class="col-xs-12 col-form-label">Interest Picture</label>
					<div class="col-xs-10">
						<input type="file" accept="image/*" name="image" class="dropify form-control-file" id="image" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-12 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Interest</button>
						<a href="{{route('admin.interest.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection
