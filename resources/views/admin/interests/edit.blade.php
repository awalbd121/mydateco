@extends('admin.layout.base')

@section('title', 'Update Interest ')
@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    	    <a href="{{ route('admin.interest.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Interest</h5>

            <form class="form-horizontal" action="{{route('admin.interest.update', $interest->id )}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	<input type="hidden" name="_method" value="PATCH">
				<div class="form-group row">
					<label for="name" class="col-xs-2 col-form-label">Interest Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $interest->name }}" name="name" required id="name" placeholder="Interest Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="description" class="col-xs-2 col-form-label">Interest Description</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $interest->description }}" name="description" required id="description" placeholder="Interest Description">
					</div>
				</div>


				<div class="form-group row">
					
					<label for="image" class="col-xs-2 col-form-label">Interest Image</label>
					<div class="col-xs-10">
					@if(isset($interest->image))
                    	<img style="height: 90px; margin-bottom: 15px; border-radius:2em;" src="{{$interest->image}}">
                    @endif
						<input type="file" accept="image/*" name="image" class="dropify form-control-file" id="image" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Interest</button>
						<a href="{{route('admin.interest.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection
