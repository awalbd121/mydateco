@extends('admin.layout.base')

@section('title', 'Interests')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                 @if(Setting::get('demo_mode') == 1)
                    <div class="col-md-12" style="height:50px;color:red;">
                                ** Demo Mode : @lang('user.admin.demomode')
                    </div>
                @endif
                <h5 class="mb-1">Interests
                @if(Setting::get('demo_mode', 0) == 1)
                    <span class="pull-right">(*personal information hidden in demo)</span>
                @endif 
                </h5>
                <a href="{{ route('admin.interest.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Interest</a>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Interest Name</th>
                            <th>Interest Description</th>
                            <th>Interest Image</th>
                            @if( Setting::get('demo_mode') == 0)
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($interests as $index => $interest)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$interest->name}}</td>
                            <td>{{$interest->description}}</td>
                            <td>{{$interest->image}}</td>
                            @if( Setting::get('demo_mode') == 0)
                            <td>
                                <form action="{{ route('admin.interest.destroy', $interest->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <!-- <a href="{{ url('interest.request', $interest->id) }}" class="btn btn-info"><i class="fa fa-search"></i> History</a> -->
                                    <a href="{{ route('admin.interest.edit', $interest->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                    <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                </form>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Interest Name</th>
                            <th>Interest Description</th>
                            <th>Interest Image</th>
                            @if( Setting::get('demo_mode') == 0)
                            <th>Action</th>
                            @endif
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection