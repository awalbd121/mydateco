
@extends('admin.adminLayout')

@section('title','Admin Dashboard')

@section('content')

<!-- BEGIN OFFCANVAS RIGHT -->
<div id="content">
    <!-- BEGIN BLANK SECTION -->
    <section>
        <div class="section-header">
            <ol class="breadcrumb">
                <li class="active">Chats</li>
            </ol>
        </div><!--end .section-header -->
        <div class="section-body">
        	<div class="row">
        		<div class="col-md-12">
        		<form class="form">
        		{{@csrf_field()}}       		
        			<div class="col-md-4">
	        			<select class="" required="" style="display: block;" onchange="get_userfriend(this.value)">
	        				<option>Select</option>
		        			@foreach($users as $user)
		        			<option value="{{$user->id}}">{{$user->first_name}}</option>
		        			@endforeach
	        			</select>	
        			</div> 
        			<div class="col-md-4">
	        			<select class="" required="" style="display: block;">
	        				<option>Select</option>
		        			@foreach($users as $user)
		        			<option value="{{$user->id}}">{{$user->first_name}}</option>
		        			@endforeach
	        			</select>	
        			</div>
        			<div>
        				<input class="btn" type="submit" value="View Chat">
        			</div>		
        		</form>
        		</div>
        		
        	</div>
        </div>
    </section>

    <!-- BEGIN BLANK SECTION -->
</div><!--end #content-->


@endsection

@section('scripts')
	
@endsection