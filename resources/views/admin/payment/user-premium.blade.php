@extends('admin.layout.base')

@section('title', 'Payment History ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                <h5 class="mb-1">User Subscription</h5>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>User Name</th>
                            <th>Subscription Name</th>
                            <th>Subscription Period</th>
                            <th>Subscription Price</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($user_premiums as $index => $user_premium)
                        <tr>
                            <td>{{$index+1 }}</td>
                            <td>{{@$user_premium->user->first_name}} {{@$user_premium->user->last_name}}</td>
                            <td>{{@$user_premium->premium->plan_name}}</td>
                            <td>{{@$user_premium->premium->period}} {{@$user_premium->premium->duration}}</td>
                            <td>{{currency(@$user_premium->premium->price)}}</td> 
                            <td>{{@$user_premium->created_at}}</td>                            
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>User Name</th>
                            <th>Subscription Name</th>
                            <th>Subscription Period</th>
                            <th>Subscription Price</th>
                            <th>Date</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
