@extends('admin.layout.base')

@section('title', 'Payment History ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                <h5 class="mb-1">User Transactions</h5>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Transaction ID</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Amount</th>
                            <th>Payment Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($payments as $index => $payment)
                        <tr>
                            <td>{{$index+1 }}</td>
                            <td>{{$payment->alias_id}}</td>
                            <td>{{$payment->from_user?$payment->from_user->first_name:''}} {{$payment->from_user?$payment->from_user->last_name:''}}</td>
                            <td>{{$payment->receiver?$payment->receiver->first_name.$payment->receiver->last_name:Auth::user()->name}} </td>
                            <td>
                                @if($payment->status =='ADDED')
                                    {{currency($payment->amount)}}
                                @else
                                    {{currency($payment->amount)}}
                                @endif
                            </td>
                            <td>{{$payment->status}}</td>                            
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Transaction ID</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Amount</th>
                            <th>Payment Status</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection