@extends('admin.layout.base')

@section('title', 'Update Subscription ')
@section('content')
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    	    <a href="{{ route('admin.premium.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Subscription</h5>

            <form class="form-horizontal" action="{{route('admin.premium.update', $premium->id )}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	<input type="hidden" name="_method" value="PATCH">			

				<div class="form-group row">
					<label for="plan_name" class="col-xs-2 col-form-label">Subscription Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $premium->plan_name }}" name="plan_name" required id="plan_name" placeholder="Subscription Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="description" class="col-xs-2 col-form-label">Subscription Description</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $premium->description }}" name="description" id="description" placeholder="Subscription Description">
					</div>
				</div>

				<div class="form-group row">
					<div class="col-xs-6 row">
						<label for="period" class="col-xs-4 col-form-label">Subscription Period</label>
						<div class="col-xs-8">
							<input class="form-control" type="number" value="{{ $premium->period }}" name="period" id="period" min="0" required>
						</div>
					</div>
					<div class="col-xs-6 row">
						<label for="duration" class="col-xs-4 col-form-label">Subscription Duration</label>
						<div class="col-xs-8">
							<select class="form-control" name="duration" required>
								<option value="">Select Duration</option>
								<option value="day" @if($premium->duration=='day') selected="" @endif>Day</option>
								<option value="month" @if($premium->duration=='month') selected="" @endif>Month</option>
								<option value="year" @if($premium->duration=='year') selected="" @endif>Year</option>
							</select>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<label for="price" class="col-xs-2 col-form-label">Subscription Price ({{Setting::get('currency','$')}}) @if($premium->price==0 && $premium->id==1) {{'(Non-editable)'}} @endif</label>
					<div class="col-xs-10">
					@if($premium->price==0 && $premium->id==1)				
						<input class="form-control" disabled="" type="number" value="{{ $premium->price }}" name="pre_price" id="price"  min="0" required>
						<input type="hidden" name="price" value="0">
					@else
						<input class="form-control" type="number" value="{{ $premium->price }}" name="price" id="price"  min="0" required>
					@endif
					</div>
				</div>

				<div class="form-group row">
					<label for="superlike" class="col-xs-2 col-form-label">Subscription Superlike Count</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{ $premium->superlike }}" name="superlike" id="superlike" min="0" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="skip_count" class="col-xs-2 col-form-label">Skip Count</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{ $premium->skip_count }}" name="skip_count" id="skip_count" min="0">
					</div>
				</div>
				
				<div class="form-group row ">
					<label class="col-form-label col-xs-2">Location Change</label>
					<div class="col-xs-10">
					<label class="switch">
						<input type="checkbox" name="location_change" id="location_change" value="1" @if($premium->location_change == 1) checked @endif>
						<span class="slider round"></span>
					</label>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-xs-2">Display who likes me?</label>
					<div class="col-xs-10">
					<label class="switch">
						<input type="checkbox" name="likes_me" id="likes_me" value="1" @if($premium->likes_me == 1) checked @endif>
						<span class="slider round"></span>
					</label>
					</div>
				</div>

				<!-- <div class="form-group row">
					<label for="video_call" class="col-xs-2 col-form-label">Subscription Videocall Count</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{ $premium->video_call }}" name="video_call" id="video_call"  min="0">
					</div>
				</div> -->

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Subscription</button>
						<a href="{{route('admin.premium.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection
