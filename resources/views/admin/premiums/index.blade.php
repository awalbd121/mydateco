@extends('admin.layout.base')

@section('title', 'Subscription')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                @if(Setting::get('demo_mode') == 1)
                <div class="col-md-12" style="height:50px;color:red;">** Demo Mode : @lang('user.admin.demomode')</div>
                @endif
                <h5 class="mb-1">Subscription
                    @if(Setting::get('demo_mode', 0) == 1)
                    <span class="pull-right">(*personal information hidden in demo)</span>
                    @endif 
                </h5>
                <a href="{{ route('admin.premium.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Subscriber</a>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Subscription Name</th>
                            <th>Subscription Duration</th>
                            <th>Subscription Price</th>
                            <th>DoubleHeart Limit</th>
                            @if( Setting::get('demo_mode') == 0)
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($premiums as $index => $premium)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$premium->plan_name}}</td>
                            <td>{{$premium->period}} {{$premium->duration}}</td>
                            <td>{{currency($premium->price)}}</td>
                            <td>{{$premium->superlike}}</td>
                            @if( Setting::get('demo_mode') == 0)
                            <td>
                                <form action="{{ route('admin.premium.destroy', $premium->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <!-- <a href="{{ url('premium.request', $premium->id) }}" class="btn btn-info"><i class="fa fa-search"></i> History</a> -->
                                    <a href="{{ route('admin.premium.edit', $premium->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                    <!-- @if($premium->price!=0 && $premium->id!=1)  --> 
                                    <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                   <!--  @endif -->
                                </form>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Subscription Name</th>
                            <th>Subscription Duration</th>
                            <th>Subscription Price</th>
                            <th>DoubleHeart Limit</th>
                            @if( Setting::get('demo_mode') == 0)
                            <th>Action</th>
                            @endif
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
