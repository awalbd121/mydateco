@extends('admin.layout.base')

@section('title', 'Site Settings ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">

			<h5 style="margin-bottom: 2em;">Site Settings</h5>

            <form class="form-horizontal" action="{{url('admin/settings_store')}}" method="POST" enctype="multipart/form-data" role="form">
            
            	{{csrf_field()}}
				<div class="form-group row">
					<label for="sitename" class="col-xs-2 col-form-label">Site Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('sitename', 'MyDate')  }}" name="sitename" required id="sitename" placeholder="Site Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="logo" class="col-xs-2 col-form-label">Site Logo</label>
					<div class="col-xs-10">
						@if(Setting::get('logo')!='')
	                    <img style="height: 90px; margin-bottom: 15px; border-radius:2em;" src="{{Setting::get('logo')}}">
	                    @endif
						<input type="file" accept="image/*" name="logo" class="dropify form-control-file" id="logo" aria-describedby="fileHelp">
					</div>
				</div>


				<div class="form-group row">
					<label for="favicon" class="col-xs-2 col-form-label">Site Icon</label>
					<div class="col-xs-10">
						@if(Setting::get('favicon')!='')
	                    <img style="height: 90px; margin-bottom: 15px; border-radius:2em;" src="{{Setting::get('favicon')}}">
	                    @endif
						<input type="file" accept="image/*" name="favicon" class="dropify form-control-file" id="favicon" aria-describedby="fileHelp">
					</div>
				</div>
				<div class="form-group row">
					<label for="pemfile" class="col-xs-2 col-form-label">Pemfile</label>
					<div class="col-xs-10">
						@if(Setting::get('pemfile')!='')
						{{Setting::get('pemfile')}}
						@endif
						<input type="file" name="pemfile" class="dropify form-control-file" id="pemfile" aria-describedby="fileHelp">
					</div>
				</div>
					<div class="form-group row">
					<label for="ios_user_mode" class="col-xs-2 col-form-label">Pemfile--Mode</label>
					<div class="col-xs-10">
						<select name="IOS_USER_ENV" class="form-control" id="ios_user_mode">
							<option @if(Setting::get('IOS_USER_ENV') == 'development') selected @endif value="development">development</option>
							<option @if(Setting::get('IOS_USER_ENV') == 'production') selected @endif value="production">production</option>
						</select>
					</div>
				</div>
					<div class="form-group row">
					<label for="ios_user_password" class="col-xs-2 col-form-label">Pemfile--Password</label>
					<div class="col-xs-10">
						<input type="text" name="IOS_USER_PASS" class="form-control" id="ios_user_password" value="{{Setting::get('IOS_USER_PASS','')}}">
					</div>
				</div>
				<hr>
				<div><label><b>TRAIL PACKAGE</b></label></div>
				<div class="form-group row">
				<input type="hidden" name="plan_name" value="Trial">
				<input type="hidden" name="is_default_trail" value="1">
					<label for="period" class="col-xs-2 col-form-label"> package period / <span id ="package_period">{{strtoupper(@$premium->duration)}}<span> </label>

					<div class="col-xs-4">
						<input type="number" name="period" class="form-control" id="period" value="{{$premium->period}}">
					</div>
					<label for="duration" class="col-xs-2 col-form-label">package duration</label>
					<div class="col-xs-4">
					<select class="form-control" name="duration" id="duration" required>
						<option value="">Select Duration</option>
						<option @if(@$premium->duration == "day") selected @endif value="day">Day</option>
						<option  @if(@$premium->duration == "month") selected @endif value="month">Month</option>
						<option @if(@$premium->duration == "year") selected @endif value="year">Year</option>
					</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="like_count" class="col-xs-2 col-form-label"> Package ThumpsUp count(/day)</label>
					<div class="col-xs-4">
						<input type="number" name="like_count" class="form-control" id="like_count" value="{{@$premium->like_count}}">
					</div>
					<label for="superlike" class="col-xs-2 col-form-label">Package DoubleHeart count</label>
					<div class="col-xs-4">
						<input type="number" name="superlike" class="form-control" id="superlike" value="{{@$premium->superlike}}">
					</div>
				</div>
				<hr>
				<div><label><b>SUBSCRIPTION AMOUNT</b></label></div>
				<div class="form-group row">
				
				
					<label for="period" class="col-xs-2 col-form-label"> Monthly Amount </label>

					<div class="col-xs-4">
						<input type="number" name="monthly" class="form-control" id="period" value="{{ Setting::get('monthly', '0')  }}">
					</div>
					<label for="duration" class="col-xs-2 col-form-label">Annual Amount</label>
					<div class="col-xs-4">
					 <input type="number" name="annual" class="form-control" id="period" value="{{ Setting::get('annual', '0')  }}">
					</div>
				</div>
				<hr>
					<div><label><b>REFERAL CODE</b></label></div>
					<div class="form-group row">
						<label for="like_count" class="col-xs-2 col-form-label"> Referal Code Count</label>
						<div class="col-xs-4">
							<input type="number" name="referal_count" class="form-control" id="referal_count" value="{{Setting::get('referal_count','')}}">
						</div>
						<label for="superlike" class="col-xs-2 col-form-label">Referal Code Amount</label>
						<div class="col-xs-4">
							<input type="number" name="referal_amount" class="form-control" id="referal_amount" value="{{Setting::get('referal_amount','')}}">
						</div>
						
					</div>
					<div class="form-group row">
							<label for="referal_status" class="col-xs-2 col-form-label">Referal Code Status</label>
							<div class="col-xs-4">
								<select name="referal_status" class="form-control" id="referal_status">
									<option @if(Setting::get('referal_status') == 'on') selected @endif value="on">ON</option>
									<option @if(Setting::get('referal_status') == 'off') selected @endif value="off">OFF</option>
								</select>
							</div>
					</div>
					
				<hr>
                <div class="form-group row">
                    <label for="site_copyright" class="col-xs-2 col-form-label">Copyright Content</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ Setting::get('site_copyright', '&copy; 2018 Appoets') }}" name="site_copyright" id="site_copyright" placeholder="Site Copyright">
                    </div>
                </div>

				<div class="form-group row">
					<label for="google_playstore_link" class="col-xs-2 col-form-label">Playstore link</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('google_playstore_link', '')  }}" name="google_playstore_link"  id="google_playstore_link" placeholder="Playstore link">
					</div>
				</div>

				<div class="form-group row">
					<label for="apple_app_store_link" class="col-xs-2 col-form-label">Appstore Link</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('apple_app_store_link', '')  }}" name="apple_app_store_link"  id="apple_app_store_link" placeholder="Appstore link">
					</div>
				</div>

				<div class="form-group row">
					<label for="website_link" class="col-xs-2 col-form-label">Website link</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('website_link', '')  }}" name="website_link"  id="website_link" placeholder="Website link">
					</div>
				</div>

				<div class="form-group row">
					<label for="currency" class="col-xs-2 col-form-label">Default Currency
                                 ( <strong>{{ Setting::get('currency', '$')  }} </strong>)
                            </label>
					<div class="col-xs-10">
						<select name="currency" class="form-control" required>
                            <option @if(Setting::get('currency') == "$") selected @endif value="$">US Dollar (USD)</option>
                            <option @if(Setting::get('currency') == "C$") selected @endif value="C$">Canadian Dollar (CAD)</option>
                            <option @if(Setting::get('currency') == "₹") selected @endif value="₹"> Indian Rupee (INR)</option>
                            <option @if(Setting::get('currency') == "د.ك") selected @endif value="د.ك">Kuwaiti Dinar (KWD)</option>
                            <option @if(Setting::get('currency') == "د.ب") selected @endif value="د.ب">Bahraini Dinar (BHD)</option>
                            <option @if(Setting::get('currency') == "﷼") selected @endif value="﷼">Omani Rial (OMR)</option>
                            <option @if(Setting::get('currency') == "£") selected @endif value="£">British Pound (GBP)</option>
                            <option @if(Setting::get('currency') == "€") selected @endif value="€">Euro (EUR)</option>
                            <option @if(Setting::get('currency') == "CHF") selected @endif value="CHF">Swiss Franc (CHF)</option>
                            <option @if(Setting::get('currency') == "ل.د") selected @endif value="ل.د">Libyan Dinar (LYD)</option>
                            <option @if(Setting::get('currency') == "B$") selected @endif value="B$">Bruneian Dollar (BND)</option>
                            <option @if(Setting::get('currency') == "S$") selected @endif value="S$">Singapore Dollar (SGD)</option>
                            <option @if(Setting::get('currency') == "AU$") selected @endif value="AU$"> Australian Dollar (AUD)</option>
                        </select>
					</div>
				</div>

				<div class="form-group row">
					<label for="stripe_secret_key" class="col-xs-2 col-form-label">Stripe Secret key</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('stripe_secret_key', '')  }}" name="stripe_secret_key"  id="stripe_secret_key" placeholder="Stripe Secret key">
					</div>
				</div>

				<div class="form-group row">
					<label for="stripe_public_key" class="col-xs-2 col-form-label">Stripe Publishable key</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('stripe_public_key', '')  }}" name="stripe_public_key"  id="stripe_public_key" placeholder="Stripe Publishable key">
					</div>
				</div>

				<div class="form-group row">
					<label for="map_key" class="col-xs-2 col-form-label">Google Map Key</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('map_key', '')  }}" name="map_key"  id="map_key" placeholder="Stripe Secret key">
					</div>
				</div>

				

				


				<div class="form-group row">
					<label for="contact_email" class="col-xs-2 col-form-label">Contact Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('contact_email', '')  }}" name="contact_email"  id="contact_email" placeholder="Contact Email">
					</div>
				</div>

				<div class="form-group row">
					<label for="contact_number" class="col-xs-2 col-form-label">Contact Number</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('contact_number', '')  }}" name="contact_number"  id="contact_number" placeholder="Contact Number">
					</div>
				</div>

				<div class="form-group row">
					<label for="contact_text" class="col-xs-2 col-form-label">Contact Text</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('contact_text', '')  }}" name="contact_text"  id="contact_text" placeholder="Contact text">
					</div>
				</div>

				<div class="form-group row">
					<label for="contact_title" class="col-xs-2 col-form-label">Contact title</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('contact_title', '')  }}" name="contact_title"  id="contact_title" placeholder="Contact title">
					</div>
				</div>

				<div class="form-group row">
					<label for="review" class="col-xs-2 col-form-label">App in Review</label>
					<div class="col-xs-1">
						<input class="form-control" type="checkbox" @if(Setting::get('review')==1) {{'checked'}}@endif name="review" value="1"  id="review">
					</div>
				</div>
				<div class="form-group row">
                    <label for="wallet_transaction" class="col-xs-2 col-form-label">Demo Mode</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="number" value="{{ Setting::get('demo_mode', '0') }}" name="demo_mode" id="demo_mode" placeholder="demo mode" min=0 max=1>
                    </div>
				</div>
				<div class="form-group row">
                    <label for="wallet_transaction" class="col-xs-2 col-form-label">Wallet Transaction</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{Setting::get('wallet_transaction','')}}" name="wallet_transaction" id="wallet_transaction" placeholder="Wallet Transaction">
                    </div>
				</div>
				<div class="form-group row">
                    <label for="referal_transaction" class="col-xs-2 col-form-label">Referal Transaction</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{Setting::get('referal_transaction','')}}" name="referal_transaction" id="referal_transaction" placeholder="Referal Transaction">
                    </div>
				</div>
				
				<div class="form-group row">
                    <label for="premium_transaction" class="col-xs-2 col-form-label">Subscription Transaction</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{strtoupper(Setting::get('premium_transaction','')) }}" name="premium_transaction" id="premium_transaction" placeholder="Wallet Transaction">
                    </div>
				</div>
				<div class="form-group row">
                    <label for="facebook_account_kit" class="col-xs-2 col-form-label">FaceBook Account Kit For Mobile Verification</label>
                    <div class="col-xs-10">
						<select name="facebook_account_kit" class="form-control" id="facebook_account_kit">
							<option @if(Setting::get('facebook_account_kit') == 'on') selected @endif value="on">ON</option>
							<option @if(Setting::get('facebook_account_kit') == 'off') selected @endif value="off">OFF</option>
						</select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="facebook_status" class="col-xs-2 col-form-label">FaceBook Status</label>
                    <div class="col-xs-10">
						<select name="facebook_status" class="form-control" id="facebook_status">
							<option @if(Setting::get('facebook_status') == 'on') selected @endif value="on">ON</option>
							<option @if(Setting::get('facebook_status') == 'off') selected @endif value="off">OFF</option>
						</select>
                    </div>
                </div>
                
               

				<div class="form-group row facebook_account_kit" @if(Setting::get('facebook_account_kit') == 'off') style="display:none" @endif>
					<label for="fb_app_version" class="col-xs-2 col-form-label">FB App Version</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('fb_app_version', '')  }}" name="fb_app_version"  id="fb_app_version" placeholder="FB App Version">
					</div>
				</div>
				

				<div class="form-group row facebook_status facebook_account_kit" @if(Setting::get('facebook_status') == 'off' && Setting::get('facebook_account_kit') == 'off') style="display:none" @endif >
					<label for="fb_app_id" class="col-xs-2 col-form-label">FB App ID</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('fb_app_id', '')  }}" name="fb_app_id"  id="fb_app_id" placeholder="FB App ID">
					</div>
				</div>

				<div class="form-group row facebook_status" @if(Setting::get('facebook_status') == 'off') style="display:none" @endif >
					<label for="fb_app_secret" class="col-xs-2 col-form-label">FB App Secret</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('fb_app_secret', '')  }}" name="fb_app_secret"  id="fb_app_secret" placeholder="FB App Secret">
					</div>
				</div>
				<div class="form-group row">
                    <label for="linkedin_status" class="col-xs-2 col-form-label">Linkedin Status</label>
                    <div class="col-xs-10">
						<select name="linkedin_status" class="form-control" id="linkedin_status">
							<option @if(Setting::get('linkedin_status') == 'on') selected @endif value="on">ON</option>
							<option @if(Setting::get('linkedin_status') == 'off') selected @endif value="off">OFF</option>
						</select>
                    </div>
                </div>
                <div class="form-group row linkedin_status"  @if(Setting::get('linkedin_status') == 'off') style="display:none" @endif>
					<label for="linkedin_app_id" class="col-xs-2 col-form-label">Linkedin App ID</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('linkedin_app_id', '')  }}" name="linkedin_app_id"  id="linkedin_app_id" placeholder="Linkedin App ID">
					</div> 
				</div>

				<div class="form-group row linkedin_status" @if(Setting::get('linkedin_status') == 'off') style="display:none" @endif >
					<label for="linkedin_app_secret" class="col-xs-2 col-form-label">Linkedin App Secret</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Setting::get('linkedin_app_secret', '')  }}" name="linkedin_app_secret"  id="linkedin_app_secret" placeholder="Linkedin App Secret">
					</div>
				</div>
				@if(Setting::get('demo_mode')=='1')
					<div class="form-group row">
						<label for="demo_seeder" class="col-xs-2 col-form-label">Demo Seeder</label>
						<div class="col-xs-10">
							<button type="submit" name ="demo_seeder" value ="on" id ="demo_seeder" class="btn btn-primary">Demo Seeder</button>
						</div>
					</div>
				@endif

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Site Settings</button>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
@endsection
@section('scripts')
<script>
	$(document).ready(function(){
		$('#duration').on('change', function() {
			$('#package_period').empty().append(this.value.toUpperCase());
		});
		$('#linkedin_status').on('change', function() {
			if($(this).val()=='on'){
				$('.linkedin_status').show();
			}else{
				$('.linkedin_status').hide();
			}
			$('#package_period').empty().append(this.value.toUpperCase());
		});
		$('#facebook_status').on('change', function() {
			if($(this).val()=='on'){
				$('.facebook_status').show();
			}else{
				$('.facebook_status').hide();
			}
			$('#package_period').empty().append(this.value.toUpperCase());
		});
		$('#facebook_account_kit').on('change', function() {
			if($(this).val()=='on'){
				$('.facebook_account_kit').show();
			}else{
				$('.facebook_account_kit').hide();
			}
			$('#package_period').empty().append(this.value.toUpperCase());
		});
	});
	$('#demo_seeder').click(function(){
		alert("Are you sure want to change the database as fresh");
	});
</script>
@endsection
