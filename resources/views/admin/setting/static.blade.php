@extends('admin.layout.base')

@section('title', 'Pages ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Privacy Policy Content</h5>

            <div className="row">
                <form action="{{ url('admin/static_update') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="privacy_content" id="myeditor">{{ Setting::get('privacy_content') }}</textarea>
                        </div>
                    </div>

                    <br>
                    <h5>Terms Content</h5>

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="terms_content" id="myeditor1">{{ Setting::get('terms_content') }}</textarea>
                        </div>
                    </div>
                    <br>                   

                    <div class="row">
                        <div class="col-xs-12 col-md-3">
                            <a href="{{ url('admin/dashboard') }}" class="btn btn-danger btn-block">Cancel</a>
                        </div>
                        <div class="col-xs-12 col-md-3 offset-md-6">
                            <button type="submit" class="btn btn-primary btn-block">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('myeditor');
    CKEDITOR.replace('myeditor1');
</script>
@endsection