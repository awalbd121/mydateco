@extends('admin.layout.base')

@section('title', 'Payment History ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                <h5 class="mb-1">User Matches</h5>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>User Name</th>
                            <th>Match Name </th>
                            <th>Match Status</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($likes) > 0 && $likes != '')
                        @foreach(@$likes as $index => $like)
                            <tr>
                                <td>{{$index+1 }}</td>
                                <td>{{@$like->user->first_name}} {{@$like->user->last_name}}</td>
                                <td>{{@$like->user_like->first_name}} {{@$like->user_like->last_name}}</td>
                                <td>@if(@$like->status=='1') {{'Thumbs Up'}}
                                @elseif(@$like->status=='2') @if(@$like->super_like=='1'){{'DoubleHeart'}} @else{{'Friends'}}@endif
                                @elseif(@$like->status=='3') {{'Thumbs Down'}}
                                @endif</td>
                                <td>{{@$like->updated_at}}</td>                            
                            </tr>
                        @endforeach
                    @else
                        No Records
                    @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>User Name</th>
                            <th>Match Name </th>
                            <th>Match Status</th>
                            <th>Date</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
