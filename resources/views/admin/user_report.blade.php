@extends('admin.layout.base')

@section('title', 'User Report')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                @if(Setting::get('demo_mode') == 1)
                <div class="col-md-12" style="height:50px;color:red;">
                            ** Demo Mode : @lang('user.admin.demomode')
                </div>
                @endif
                <h5 class="mb-1">User Transactions
                    @if(Setting::get('demo_mode', 0) == 1)
                    <span class="pull-right">(*personal information hidden in demo)</span>
                    @endif 
                </h5>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>User Name</th>
                            <th>Report User Name</th>
                            <th>Reason</th>
                            <th>Date</th>
                            @if( Setting::get('demo_mode') == 0)
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($user_reports as $index => $user_report)
                        <tr>
                            <td>{{$index+1 }}</td>
                            <td>{{$user_report->user->first_name}} {{$user_report->user->last_name}}</td>
                            <td>{{$user_report->reporter->first_name}} {{$user_report->reporter->last_name}}</td>
                            <td>{{$user_report->reason}}</td>
                            <td>{{$user_report->created_at}}</td> 
                            @if( Setting::get('demo_mode') == 0)
                            <td>
                                <form action="{{ url('admin/user-block', $user_report->report_id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="user_id" value="{{ $user_report->user_id }}">
                                    <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Block</button>
                                </form>
                            </td> 
                            @endif                           
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>User Name</th>
                            <th>Report User Name</th>
                            <th>Reason</th>
                            <th>Date</th>
                            @if( Setting::get('demo_mode') == 0)
                            <th>Action</th>
                            @endif
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection