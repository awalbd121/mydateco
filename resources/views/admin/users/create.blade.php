@extends('admin.layout.base')

@section('title', 'Add User ')


@section('content')


<link href="{{ asset('design/css/intlTelInput.css') }}" rel="stylesheet">  
<!-- Bootstrap Datepicker CSS -->
<!-- <link href="{{asset('design/css/bootstrap-datepicker.min.css')}}" rel="stylesheet"> -->
<script src="{{asset('design/js/intlTelInput.min.js')}}"></script>
<script src="{{asset('design/js/intlTelInput-jquery.min.js')}}"></script>
<!-- Bootstrap Datepicker JS -->
<!-- <script src="{{asset('design/js/bootstrap-datepicker.min.js')}}"></script> -->

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.user.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add User</h5>

            <form class="form-horizontal" action="{{route('admin.user.store')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
				<div class="form-group row">
					<label for="first_name" class="col-xs-12 col-form-label">First Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('first_name') }}" name="first_name" required id="first_name" placeholder="First Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="last_name" class="col-xs-12 col-form-label">Last Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('last_name') }}" name="last_name" required id="last_name" placeholder="Last Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="email" class="col-xs-12 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" required name="email" value="{{old('email')}}" id="email" placeholder="Email">
					</div>
				</div>

				<div class="form-group row">
					<label for="dob" class="col-xs-12 col-form-label">DOB</label>
					<div class="col-xs-10">
						<input class="form-control datepicker" type="date" required name="dob" value="" id="dob" placeholder="DOB" data-provide="datepicker">
					</div>
				</div>

				<div class="form-group row">
					<label for="gender" class="col-xs-12 col-form-label">Gender</label>
					<div class="col-xs-10">
						<!-- <input class="form-control" type="text" required name="dob" value="{{old('email')}}" id="dob" placeholder="Email" data-provide="datepicker"> -->
						<select name="gender" class="form-control">
							<option value="">Choose Gender</option>
							<option value="male">Male</option>
							<option value="female">Female</option>
							<option value="other">Other</option>
						</select>
					</div>
				</div>

<!-- 				<div class="form-group row">
					<label for="email" class="col-xs-12 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" required name="email" value="{{old('email')}}" id="email" placeholder="Email">
					</div>
				</div> -->

				<div class="form-group row">
					<label for="password" class="col-xs-12 col-form-label">Password</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password" id="password" placeholder="Password">
					</div>
				</div>

				<div class="form-group row">
					<label for="password_confirmation" class="col-xs-12 col-form-label">Password Confirmation</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password_confirmation" id="password_confirmation" placeholder="Re-type Password">
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-12 col-form-label">Picture</label>
					<div class="col-xs-10">
						<input type="file" accept="image/*" name="picture" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row" style ="padding-bottom:50px;">
					<label for="mobile" class="col-xs-12 col-form-label">Mobile</label>
					<!-- <div class="col-xs-2">
						<input class="form-control" value="" name="country_code" id="country_code" placeholder="">
					</div> -->
					<div class="col-xs-12">
						<input class="form-control" style = "padding-right:795px";  value="" name="mobile" id="mobile" placeholder="Mobile">
					</div>
				</div>
				@if(\Setting::get('referal_status') =='on')
                    <!-- <div class="form-group row">
                        <label for="install_code" class="col-xs-12 col-form-label">Referral code (optional)</label>
                        <input type="text" id="install_code" name="install_code" class="form-control" placeholder="Referral code" onblur="checkrefercode(this.id)" value="{{Session::get('refercode')}}">
                    </div>
                    <div class="col-md-12" style="padding-bottom: 10px;" id="refercode_verfication"></div> -->
                @endif
                <input type="hidden" name="latitude" id="latitude_location" value="13.05298479">
                <input type="hidden" name="longitude" id="longitude_location" value="80.2488487">
				<div class="form-group row">
					<label for="zipcode" class="col-xs-12 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add User</button>
						<a href="{{route('admin.user.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection
@section('scripts')

<script>
<!-- For Mobile Details!-->
	//For mobile number with date
	var input = document.querySelector("#mobile");
        window.intlTelInput(input,({
            // separateDialCode:true,
        }));
        $(".country-name").click(function(){
            var myVar = $(this).closest('.country').find(".dial-code").text();
            $('#mobile').val(myVar);

		});

    function checkrefercode(id){ //alert(id);
            var refercode = document.getElementById(id).value; //alert(refercode);
            $.post("{{url('/check-refercode')}}",{ _token: '{{csrf_token()}}', refercode : refercode })
            .done(function(data){ 
                $('#refercode_verfication').html('');
            })
            .fail(function(xhr, status, error) {
                console.log(error);
                $('#'+id).val("");
                $('#refercode_verfication').html('Invalid Refercode');
             });

    }
</script>
@endsection
