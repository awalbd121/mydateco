@extends('admin.layout.base')

@section('title', 'Users ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                @if(Setting::get('demo_mode') == 1)
                <div class="col-md-12" style="height:50px;color:red;">
                            ** Demo Mode : @lang('user.admin.demomode')
                </div>
                @endif
                <h5 class="mb-1">Users
                    @if(Setting::get('demo_mode', 0) == 1)
                    <span class="pull-right">(*personal information hidden in demo)</span>
                    @endif 
                </h5>
                
                <form class="form-inline" href="{{url('admin/user')}}">
                    <div class="form-group mb-2">
                        <label for="allsearch" class="sr-only">Search</label>
                        <input type="text" name="q" class="form-control-plaintext" id="allsearch" value="{{Request::get('q')}}" placeholder="Search..">
                    </div>
                    <!-- <div class="form-group mx-sm-3 mb-2">
                        <label for="premium" class="sr-only">User Type</label>
                        <select name="premium" class="form-control" id="premium" >
                            <option>Select</option>
                            <option value="yes" @if(Request::get('premium')=='yes') selected @endif  >Premium</option>
                            <option value="no" @if(Request::get('premium')=='no') selected @endif >Normal</option>
                        </select>
                    </div> -->
                    <button type="submit" class="btn btn-primary mb-2">Search</button>
                    <button type="reset" class="btn btn-primary mb-2" onClick="window.location.reload()"  >Reset</button>

                    <a href="{{ route('admin.user.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right mb-2"><i class="fa fa-plus"></i> Add New User</a>
                </form>
                
                
                <table class="table table-striped table-bordered dataTable" id="">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Wallet Amount</th>
                            @if( Setting::get('demo_mode') == 0)
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $index => $user)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$user->first_name}}</td>
                            <td>{{$user->last_name}}</td>
                            @if(Setting::get('demo_mode', 0) == 1)
                            <td>{{ substr($user->email, 0, 3).'****'.substr($user->email, strpos($user->email, "@")) }}</td>
                            @else
                            <td>{{$user->email}}</td>
                            @endif
                            @if(Setting::get('demo_mode', 0) == 1)
                            <td>+919876543210</td>
                            @else
                            <td>{{$user->mobile}}</td>
                            @endif
                            <td>{{currency($user->wallet_balance)}}</td>
                            @if( Setting::get('demo_mode') == 0)
                            <td>
                                <form action="{{ route('admin.user.destroy', $user->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <!-- <a href="{{ url('user.request', $user->id) }}" class="btn btn-info"><i class="fa fa-search"></i> History</a> -->
                                    <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                    <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                </form>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                    
                </table>
                {{ $users->appends(request()->input())->links() }}
            </div>
            
        </div>
    </div>
@endsection
@section('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
@endsection
@section('scripts')
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
@endsection