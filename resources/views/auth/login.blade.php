<!DOCTYPE html> 

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google-site-verification" content="iI3k0bcjSkbJ4nbksYufjUEE_lJuQIEvLM8zqR9Rd7o" />
        <title>{{Setting::get('sitename','MyDate')}}</title>

        <meta name="description" content="Kazrr">

        <meta name="author" content="Appoets">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link href="{{ asset('design/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link href="{{ asset('design/font-awesome/design/css/font-awesome.css') }}" rel="stylesheet">
        <!-- Material Icons CSS -->        
        <link href="{{asset('design/material-design-iconic-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="{{ asset('design/css/slick.css') }}" rel="stylesheet">
        <link href="{{ asset('design/css/slick-theme.css') }}" rel="stylesheet">
        <!-- Toggle Switch CSS -->
        <link href="{{ asset('design/css/ToggleSwitch.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('design/css/style.css') }}" rel="stylesheet">   
        <link rel="shortcut icon" type="image/png" href="{{Setting::get('favicon')}}"/>
        
        {{-- //propellerads ads code --}}
        <meta name="propeller" content="e4f11fc54c587aafd7a6690e8a55dcde">
        <script>(function(s,u,z,p){s.src=u,s.setAttribute('data-zone',z),p.appendChild(s);})(document.createElement('script'),'https://iclickcdn.com/tag.min.js',3860620,document.body||document.documentElement)</script>

        {{-- //Adsence Code --}}
        <script data-ad-client="ca-pub-3208805417662334" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <style type="text/css">
            .help-block{
                color: #d61d1d;
            }
           .login-right {
                padding-bottom: 100px;
            }
            .mt-10 {
                margin-top: 10px;
            }
            .gp {
    padding: 11px 20px;
    background: #ffffff;
    color: #ff4f5a;
    border-radius: 10px;
    margin-top: 12px;
    font-weight: 600;
}
            .google-icon {
                    width: 25px;
                    background: #fff;
                    padding: 5px;
                    margin-right: 10px;
            }
            body { padding: 2em; }


/* Shared */
.loginBtn {
  box-sizing: border-box;
  position: relative;
  /* width: 13em;  - apply for fixed size */
  margin: 0.2em;
  padding: 0 15px 0 46px;
  border: none;
  text-align: left;
  line-height: 34px;
  white-space: nowrap;
  border-radius: 0.2em;
  font-size: 16px;
  color: #FFF;
}
.loginBtn:before {
    content: "";
    box-sizing: border-box;
    position: absolute;
    top: 0;
    left: 11px;
    width: 51px;
    height: 100%;
}
.loginBtn:focus {
  outline: none;
}
.loginBtn:active {
  box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
}


/* Facebook */
.loginBtn--facebook {
  background-color: #4C69BA;
  background-image: linear-gradient(#4C69BA, #3B55A0);
  /*font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif;*/
  text-shadow: 0 -1px 0 #354C8C;
}
.loginBtn--facebook:before {
  border-right: #364e92 1px solid;
  background: url('design/img/icon_facebook.png') 6px 6px no-repeat;
}
.loginBtn--facebook:hover,
.loginBtn--facebook:focus {
  background-color: #5B7BD5;
  background-image: linear-gradient(#5B7BD5, #4864B1);
}


/* Google */
.loginBtn--google {
  /*font-family: "Roboto", Roboto, arial, sans-serif;*/
  background: #DD4B39;
}
.loginBtn--google:before {
  border-right: #BB3F30 1px solid;
  background: url('design/img/icon_google.png') 6px 6px no-repeat;
}
.loginBtn--google:hover,
.loginBtn--google:focus {
  background: #E74B37;
}
span.loginBtn.loginBtn--google {
    padding: 7px 15px 7px 46px;
}
span.loginBtn.loginBtn--facebook{
    padding: 7px 15px 7px 46px;
}
.cmn-btn{
    display:block;
}
        </style> 
    </head>

    <body>
    <div class="container login bg-img">
        <div class="row">
            <!-- Login Left Starts -->
            <div class="login-left-col col-md-6">
              {{--  <div class="login-left bg-img text-center">
                    <div class="log-overlay"></div>
                    <div class="log-left-content">
                        <h3 class="log-left-tit">{{Setting::get('sitename','MyDate')}}</h3>
                        <a href="{{url('/register')}}" class="log-left-link">@lang('user.auth.signup')</a>
                        <a href="{{url('/login')}}" class="log-left-link active">@lang('user.auth.login') </a>
                         @if(\Setting::get('facebook_status') =='on' || \Setting::get('linkedin_status') =='on' )
                        <div>
                            <div class="line">@lang('user.auth.login_with')</div>
                        </div>
                        @endif
                        
                        <div class="social-sec">
                            @if(\Setting::get('facebook_status') =='on')
                                <a href="{{ url('/auth/facebook') }}" class="social-icon"><i class="zmdi zmdi-facebook"></i></a>
                            @endif
                            @if(\Setting::get('linkedin_status') =='on')
                            <a href="{{ url('/auth/linkedin') }}" class="social-icon"><i class="zmdi zmdi-linkedin-box"></i></a>
                            @endif
                        </div>
                    </div>                    
                </div> --}}
                 <img src="{{asset('design/img/login-left.png')}}" class="img-responsive">
            </div>
            <!-- Login Left Ends -->
            <!-- Login Right Starts -->
            <div class="col-md-6 log-logo">
                <a href="{{url('/')}}" class="logo text-center">
                    <img src="{{asset('design/img/logo-with-text.jpg')}}">
                </a>
                <div class="login-right-col">
                    <div class="login-right">

                        <h5 class="log-tit text-center">Login</h5>
                        {!! Form::open(['url' => 'login']) !!}
                            <div class="form-group">
                               {{-- {{Form::label('email', 'Enter Your Email')}} --}}
                                {{Form::email('email','', ['class' =>'form-control','placeholder' => 'Enter Your Email','required'])}}

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                               {{-- {{Form::label('password', 'Enter Password')}} --}}
                                {{Form::password('password', ['class' => 'form-control','placeholder' => 'Enter Password','required'])}}

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            {{Form::hidden('latitude','', ['id' =>'latitude_location'])}}
                            {{Form::hidden('longitude','', ['id' =>'longitude_location'])}}
                         <a href="{{ url('password/reset') }}" class="forgot-link pull-right">@lang('user.auth.forgot_password')?</a>

                            <a href="{{url('/register')}}" class="forgot-link">Don't have account? Sign Up here </a><br>

                            <div class="col-md-12">
                             <!-- <a href="{{ url('/auth/google') }}"><span class="loginBtn loginBtn--google">
                            
                            </button></a>
                             <a href="{{ url('/auth/facebook') }}"> <span class="loginBtn loginBtn--facebook">
                             
                            </button></a>
 -->                            {{Form::submit('Submit', ['class' => 'cmn-btn'])}}
                                
                            </div>
                           

                            
                        {!! Form::close() !!}   
                        
                    </div>
                </div>
                <div class="login-logo">
                    <a href="{{url('/')}}" class="">
                        <img src="{{Setting::get('logo')}}" class="img-fluid" style="width:10%;">
                    </a>
                </div>
            </div>
            <!-- Login Right Ends -->
        </div>
    </div>
    <script src="{{ asset('design/js/jquery.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('design/js/bootstrap.min.js') }}"></script>
    <!-- Slick Slider JS -->
    <script src="{{ asset('design/js/slick.min.js') }}"></script>
    <!-- Toggle Switch JS -->
    <script src="{{ asset('design/js/ToggleSwitch.js') }}"></script>
    <!-- Scripts JS -->
    <script src="{{ asset('design/js/scripts.js') }}"></script>

    <script type="text/javascript">
    // function getLocation() {
    //     if (navigator.geolocation) {
    //         navigator.geolocation.getCurrentPosition(showPosition,showError);
    //     } else {
    //         $('#location_text').text( "Geolocation is not supported by this browser.");
    //     }
    // }
    // function showPosition(position) {
    //     console.log(position.coords.latitude +' '+position.coords.longitude); 
    //     $('#latitude_location').val(position.coords.latitude);
    //       $('#latitude_location_guest').val(position.coords.latitude);
    //       $('#longitude_location').val(position.coords.longitude);
    //       $('#longitude_location_guest').val(position.coords.longitude);
    //       $('#location_text').text('Your Location Updated');
    // }


    // function showError(error) {
    //     switch(error.code) {
    //         case error.PERMISSION_DENIED:
    //             $('#allow_location').show();
    //             //console.log("User denied the request for Geolocation.");
    //             $('#location_text').text('User denied the request for Geolocation.');
                
    //             break;
    //         case error.POSITION_UNAVAILABLE:
    //             console.log("Location information is unavailable.");
    //             $('#location_text').text('Location information is unavailable.');
    //             break;
    //         case error.TIMEOUT:
    //             console.log("The request to get user location timed out.");
    //             $('#location_text').text('The request to get user location timed out.');
    //             break;
    //         case error.UNKNOWN_ERROR:
    //             console.log("An unknown error occurred.");
    //             $('#location_text').text('An unknown error occurred.');
    //             break;
    //     }
    // }
    // getLocation();
    </script>

  </body>
</html>
