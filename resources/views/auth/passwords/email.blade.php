<!DOCTYPE html> 

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{Setting::get('sitename','MyDate')}}</title>

        <meta name="description" content="Kazrr">

        <meta name="author" content="Kazrr">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link href="{{ asset('design/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link href="{{ asset('design/font-awesome/design/css/font-awesome.css') }}" rel="stylesheet">
        <!-- Material Icons CSS -->
        <link href="{{ asset('design/material-design-iconic-font/design/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="{{ asset('design/css/slick.css') }}" rel="stylesheet">
        <link href="{{ asset('design/css/slick-theme.css') }}" rel="stylesheet">
        <!-- Toggle Switch CSS -->
        <link href="{{ asset('design/css/ToggleSwitch.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('design/css/style.css') }}" rel="stylesheet">    

        <style type="text/css">
            .mtb-20
            {
                margin: 20px 0;
            }
            .login-right-col {
                margin-top: 8%;
            }
            .login-right {
                padding-bottom: 70px;
            }
            .login-logo{
                display:none;
            }
        </style>  

    </head>

    <body>
    <div class="container login bg-img">
        <div class="row">
            <!-- Login Left Starts -->
            <div class="login-left-col col-md-6">
               {{-- <div class="login-left bg-img text-center" style="background-image: url({{asset('design/img/login-left-bg.jpg')}});">
                    <div class="log-overlay"></div>
                    <div class="log-left-content">
                        <h3 class="log-left-tit">{{Setting::get('sitename','MyDate')}}</h3>
                        <a href="{{url('/register')}}" class="log-left-link active">@lang('user.auth.signup')</a>
                        <a href="{{url('/login')}}" class="log-left-link active">@lang('user.auth.login') </a>
                    </div>
                    
                </div> --}}
                 <img src="{{asset('design/img/login-left.jpg')}}" class="img-responsive">
            </div>
            <!-- Login Left Ends -->
            <!-- Login Right Starts -->
            <div class="col-md-6">
                <div class="login-right-col ">
                <div class="login-right">
                     @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h5 class="log-tit">@lang('user.auth.forgot_password')</h5>
                    {!! Form::open(['url' => 'password/email']) !!}
                        <div class="form-group">
                         {{--  {{Form::label('email', 'E-Mail Address')}} --}} 
                            {{Form::email('email',old('email'), ['class' =>'form-control','placeholder' => 'Enter Your Email','id' => 'email','required' => 'required'])}}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                             {{Form::submit('Send Password Reset Link', ['class' => 'cmn-btn pull-right mtb-20'])}}
                        </div>

                       
                    {!! Form::close() !!}                       
                </div>
            </div>
            <div class="login-logo">
                    <a href="{{url('/')}}" class="">
                        <img src="{{Setting::get('logo')}}" class="img-fluid" style="width:10%;">
                    </a>
                </div>
            </div>
            <!-- Login Right Ends -->
        </div>
    </div>
    <script src="{{ asset('design/js/jquery.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('design/js/bootstrap.min.js') }}"></script>
    <!-- Slick Slider JS -->
    <script src="{{ asset('design/js/slick.min.js') }}"></script>
    <!-- Toggle Switch JS -->
    <script src="{{ asset('design/js/ToggleSwitch.js') }}"></script>
    <!-- Scripts JS -->
    <script src="{{ asset('design/js/scripts.js') }}"></script>

  </body>
</html>
