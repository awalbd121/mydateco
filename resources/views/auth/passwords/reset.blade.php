<!DOCTYPE html> 

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{Setting::get('sitename','Kazrr')}}</title>

        <meta name="description" content="Kazrr">

        <meta name="author" content="Kazrr">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link href="{{ asset('design/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link href="{{ asset('design/font-awesome/design/css/font-awesome.css') }}" rel="stylesheet">
        <!-- Material Icons CSS -->
        <link href="{{ asset('design/material-design-iconic-font/design/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="{{ asset('design/css/slick.css') }}" rel="stylesheet">
        <link href="{{ asset('design/css/slick-theme.css') }}" rel="stylesheet">
        <!-- Toggle Switch CSS -->
        <link href="{{ asset('design/css/ToggleSwitch.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('design/css/style.css') }}" rel="stylesheet">      

    </head>

    <body>
    <div class="container login bg-img">
        <div class="login-inner row">
            <!-- Login Left Starts -->
            <div class="login-left-col col-md-6">
               {{-- <div class="login-left bg-img text-center" style="background-image: url({{asset('design/img/login-left-bg.jpg')}});">
                    <div class="log-overlay"></div>
                    <div class="log-left-content">
                        <h3 class="log-left-tit">{{Setting::get('sitename','MyDate')}}</h3>
                        <a href="{{url('/register')}}" class="log-left-link active">@lang('user.auth.signup')</a>
                        <a href="{{url('/login')}}" class="log-left-link active">@lang('user.auth.login') </a>
                    </div>
                    
                </div> --}}
                 <img src="{{asset('design/img/login-left.jpg')}}" class="img-responsive">
            </div>
            <!-- Login Left Ends -->
            <!-- Login Right Starts -->
            <div class="login-right-col col-md-6">
                <div class="login-right">
                     @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h5 class="log-tit">Reset Password</h5>
                    {!! Form::open(['route' => 'password.request']) !!}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            {{Form::label('email', 'E-Mail Address')}}
                            {{Form::email('email',old('email'), ['class' =>'form-control','placeholder' => 'Enter Your Email','id' => 'email','required' => 'required'])}}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            {{Form::label('password', 'Password')}}
                            {{Form::password('password',['class' => 'form-control','placeholder' => 'Password','required'])}}

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            {{Form::label('password_confirmation', 'Confirm Password')}}
                            {{Form::password('password_confirmation',['class' => 'form-control','placeholder' => 'Confirm Password','required'])}}

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                                                     
                        </div>
                        {{Form::submit('Reset Password', ['class' => 'cmn-btn sub-btn'])}}
                    {!! Form::close() !!}  
                    
                </div>
            </div>
            <!-- Login Right Ends -->
        </div>
    </div>
    <script src="{{ asset('design/js/jquery.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('design/js/bootstrap.min.js') }}"></script>
    <!-- Slick Slider JS -->
    <script src="{{ asset('design/js/slick.min.js') }}"></script>
    <!-- Toggle Switch JS -->
    <script src="{{ asset('design/js/ToggleSwitch.js') }}"></script>
    <!-- Scripts JS -->
    <script src="{{ asset('design/js/scripts.js') }}"></script>

  </body>
</html>
