<!DOCTYPE html> 
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{Setting::get('sitename','Kazrr')}}</title>

        <meta name="description" content="Kazrr">

        <meta name="author" content="Kazrr">
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link href="{{ asset('design/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link href="{{ asset('design/font-awesome/design/css/font-awesome.css') }}" rel="stylesheet">
        <!-- Material Icons CSS -->
        <link href="{{asset('design/material-design-iconic-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="{{ asset('design/css/slick.css') }}" rel="stylesheet">
        <link href="{{ asset('design/css/slick-theme.css') }}" rel="stylesheet">
        <!-- Toggle Switch CSS -->
        <link href="{{ asset('design/css/ToggleSwitch.css') }}" rel="stylesheet">
        <!-- Bootstrap Datepicker CSS -->
        <link href="{{asset('design/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('design/css/style.css') }}" rel="stylesheet">  
        <!-- For mobile number -->
        <link href="{{ asset('design/css/intlTelInput.css') }}" rel="stylesheet">  

        {{-- //propellerads ads code --}}
        <meta name="propeller" content="e4f11fc54c587aafd7a6690e8a55dcde">
        <script>(function(s,u,z,p){s.src=u,s.setAttribute('data-zone',z),p.appendChild(s);})(document.createElement('script'),'https://iclickcdn.com/tag.min.js',3860620,document.body||document.documentElement)</script>

        {{-- //Adsence Code --}}
        <script data-ad-client="ca-pub-3208805417662334" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        
        <style type="text/css">
            .help-block{
                color: #d61d1d;
            }
            .datepicker{
                padding: 6px 12px;
            }
            /* Popup box BEGIN */
.hover_bkgr_fricc{
    background:rgba(0,0,0,.4);
    cursor:pointer;
    display:none;
    height:100%;
    position:fixed;
    text-align:center;
    top:0;
    width:100%;
    z-index:10000;
}
.hover_bkgr_fricc .helper{
    display:inline-block;
    height:100%;
    vertical-align:middle;
}
.hover_bkgr_fricc > div {
    background-color: #fff;
    box-shadow: 10px 10px 60px #555;
    display: inline-block;
    height: auto;
    max-width: 551px;
    min-height: 64px;
    vertical-align: middle;
    width: 35%;
    position: relative;
    border-radius: 8px;
    padding: 15px 5%;
}
.popupCloseButton {
    background-color: #fff;
    border-radius: 50px;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: 0px;
    right: 0px;
    font-size: 18px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
}
.popupCloseButton:hover {
    background-color: #ccc;
}
.trigger_popup_fricc {
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}
 p.text_content {
    font-weight: 600;
    font-size:1.5em;

}
.login-right {
    padding-bottom: 70px;
}
.login-right-col {
    padding-bottom: 25px;
}
/* Popup box BEGIN */ 
        </style>    
    </head>

    <body>
        @include('notification.notify')

        <div class="container  login bg-img">
        <div class="row">
            <!-- Login Left Starts -->
            <div class="login-left-col col-md-6 img">
               {{-- <div class="login-left bg-img text-center" style="background-image: url({{asset('design/img/login-left-bg.jpg')}});">
                    <div class="log-overlay"></div>
                    <div class="log-left-content">
                        <h3 class="log-left-tit">{{Setting::get('sitename','MyDate')}}</h3>
                        <a href="{{url('/register')}}" class="log-left-link active">@lang('user.auth.signup')</a>
                        <a href="{{url('/login')}}" class="log-left-link">@lang('user.auth.login') </a>
                       @if(\Setting::get('facebook_status') =='on' || \Setting::get('linkedin_status') =='on' )
                        <div>
                            <div class="line">@lang('user.auth.login_with')</div>
                        </div>
                        @endif
                        <div class="social-sec">
                            @if(\Setting::get('facebook_status') =='on')
                                <a href="{{ url('/auth/facebook') }}" class="social-icon"><i class="zmdi zmdi-facebook"></i></a>
                            @endif
                            @if(\Setting::get('linkedin_status') =='on')
                            <a href="{{ url('/auth/linkedin') }}" class="social-icon"><i class="zmdi zmdi-linkedin-box"></i></a>
                             @endif
                        </div>
                    </div>
                    
                </div> --}}

                 <img src="{{asset('design/img/login-left.png')}}" class="img-responsive">
            </div>
            <!-- Login Left Ends -->
            <!-- Login Right Starts -->
            <div class="col-md-6 log-logo">
                <a href="{{url('/')}}" class="logo text-center">
                    <img src="{{asset('design/img/logo-with-text.jpg')}}">
                </a>
                <div class="login-right-col ">
                    <div class="login-right">
                        <h5 class="log-tit text-center">@lang('user.auth.signup_title')</h5>
                        {!! Form::open(['url' => 'register' ,'id' => 'register']) !!}
                            <div id="first_step">
                                <div class="form-group">
                                    {{Form::label('mobile', 'Phone Number')}}
                                    <div class="row">
                                        <div class="col-xs-4">
                                            {{Form::text('country_code','', ['class' => 'form-control mobile_code','placeholder' => '','id'=> 'country_code']) }}  
                                        </div>
                                        <div class="col-xs-8">
                                     <!--        {{Form::text('mobile','', ['class' => 'form-control','placeholder' => 'Phone Number','id'=> 'phone_number','minlength'=>6,'maxlength'=>20, 'pattern'=> "[0-9]",'oninput'=>"numberOnly(this.id)"])}}  -->


                                             {{Form::number('mobile','', ['class' => 'form-control','placeholder' => 'Phone Number','id'=> 'phone_number', 'min' => 1])}} 

                                        </div>
                                    </div>
                                </div>


                                <div class=" exist-msg" id="mobile_registered" style="display: none;">
                                    <span class="help-block">
                                            <strong>Mobile number already exists!!</strong>
                                    </span>
                                </div>

                                <div class="">
                                    @if ($errors->has('phone_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone_number') }}</strong>
                                        </span>
                                    @endif
                                </div>


                                  <div class=" mobile_otp_verfication" style="display: none;">
                                    <input type="text" class="form-control" placeholder="OTP" name="otp" id="otp" value="">

                                    @if ($errors->has('otp'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('otp') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <input type="hidden" id="otp_ref"  name="otp_ref" value="" />
                                <input type="hidden" id="otp_phone"  name="phone" value="" />


                                <div class="" style="padding-bottom: 10px;" id="mobile_verfication"></div>
                                <div class="" id="verify_button" style="padding-bottom: 10px;">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="button" class="log-teal-btn small verify_btn cmn-btn sub-btn" onclick="smsLogin();" value="Verify Phone Number"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class=" exist-msg" id="otp_mobile_registered" style="display: none;">
                                    <span class="help-block">
                                            <strong style="color:#FFFFFF;">Mobile number already exists !!!</strong>
                                    </span>
                                </div>

                                  <div class=" mobile_otp_verfication" style="padding-bottom: 10px;display:none" id="mobile_otp_verfication">
                                    <input type="button" class="log-teal-btn small verify_btn cmn-btn sub-btn" onclick="checkotp();" value="Verify Otp"/>
                                    <input type="button" class="log-teal-btn small resend_btn cmn-btn sub-btn" onclick="smsLogin();" value="Resend Otp"/>
                                    <p class="print-error-msg" style="color: black;"></p>
                                </div>

                            </div>
                            <div id="second_step">
                                <div class="form-group">
                                    {{Form::label('first_name', 'First Name')}}
                                    {{Form::text('first_name','', ['class' => 'form-control text_feild','placeholder' => 'First Name','required'])}} 
                                </div>
                                <div class="form-group">
                                    {{Form::label('last_name', 'Last Name')}}
                                    {{Form::text('last_name','', ['class' => 'form-control text_feild','placeholder' => 'Last Name','required'])}} 
                                </div> 
                                <!-- <div class="form-group">
                                    {{Form::label('mobile', 'Phone Number')}}
                                    <div class="row">
                                        <div class="col-xs-4">
                                            {{Form::text('country_code','', ['class' => 'form-control mobile_code','placeholder' => '','id'=> 'country_code']) }}  
                                        </div>
                                        <div class="col-xs-8">
                                            {{Form::text('mobile','', ['class' => 'form-control','placeholder' => 'Phone Number','id'=> 'phone_num'])}} 
                                        </div>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    {{Form::label('gender', 'Gender')}}

                                    {{Form::select('gender',array(''=>'Choose','male'=>'Male','female'=>'Female','other'=>'Other'),'', ['class' => 'form-control'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('dob', 'DOB')}}
                                    {{Form::text('dob','', ['class' => 'form-control datepicker','placeholder' => 'DOB','data-provide' => 'datepicker'])}} 
                                </div>
                                <div class="form-group">
                                    {{Form::label('username', 'Username')}}
                                    {{Form::text('username','', ['class' => 'form-control','placeholder' => 'Username','required'])}} 

                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    {{Form::label('email', 'Your Email')}}
                                    {{Form::email('email','', ['class' => 'form-control','placeholder' => 'Your Email','required'])}} 

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    {{Form::label('password', 'Password')}}
                                    {{Form::password('password',['class' => 'form-control','placeholder' => 'Password','required'])}}

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>                        
                                <div class="form-group">
                                    {{Form::label('password_confirmation', 'Confirm Password')}}
                                    {{Form::password('password_confirmation',['class' => 'form-control','placeholder' => 'Confirm Password','required'])}}
                                </div>
                                <div class="form-group">
                                    <label>Country</label>
                                    <select name="country_list" id="country_list" class="form-control" required>
                                        <option value="">Choose Country</option>
                                        @foreach(country_list() as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if(\Setting::get('referal_status') =='on')
                                    <div class="form-group">
                                        {{Form::label('install_code', 'Referral code (optional)')}}
                                        <input type="text" id="install_code" name="install_code" class="form-control" placeholder="Referral code" onblur="checkrefercode(this.id)" value="{{Session::get('refercode')}}">
                                    </div>
                                     <div class="col-md-12" style="padding-bottom: 10px;" id="refercode_verfication"></div>
                                @endif
                                {{Form::hidden('latitude','13.05298479', ['id' =>'latitude_location'])}}
                                {{Form::hidden('longitude','80.2488487', ['id' =>'longitude_location'])}}
                                <div class="form-group">
                                    <strong>@lang('user.auth.location'): </strong>
                                    <em id="location_text">Waiting for your location</em>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="terms" name="terms">
                                    <strong>I am 18+ and accept terms and conditions</strong>
                                </div>
                               <input type="submit" name="submit" class="cmn-btn sub-btn" id="reg_btn">
                            </div>
                        {!! Form::close() !!}
                        
                    </div>
                </div>
                <div class="login-logo">
                    <a href="{{url('/')}}" class="">
                        <img src="{{Setting::get('logo')}}" class="img-fluid" style="width:10%;">>
                    </a>
                </div>
            </div>
            <!-- Login Right Ends -->
           
        </div>
    </div>

<!-- <div class="hover_bkgr_fricc">
    <span class="helper"></span>
    <div>
        <div class="popupCloseButton">X</div>
        <p class="text_content">Please allow your location</p>
    </div>
</div> -->

    <script src="{{ asset('design/js/jquery.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('design/js/bootstrap.min.js') }}"></script>
    <!-- Slick Slider JS -->
    <script src="{{ asset('design/js/slick.min.js') }}"></script>
    <!-- Toggle Switch JS -->
    <script src="{{ asset('design/js/ToggleSwitch.js') }}"></script>
    <!-- Scripts JS -->
    <script src="{{ asset('design/js/scripts.js') }}"></script>
    <!-- Bootstrap Datepicker JS -->
    <script src="{{asset('design/js/bootstrap-datepicker.min.js')}}"></script>
    
    <!-- For Mobile Details!-->
    <script src="{{asset('design/js/intlTelInput.min.js')}}"></script>
    <script src="{{asset('design/js/intlTelInput-jquery.min.js')}}"></script>
    <script src="https://sdk.accountkit.com/en_US/sdk.js"></script>
 
    <script type="text/javascript">
        $('#reg_btn').prop("disabled", true);
        $("#terms").click(function() {

            if($(this).prop("checked") == true) {

                var dob = $('#dob').val();
                dob = new Date(dob);
                var today = new Date();
                var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                if(age >= 18) {

                    $('#reg_btn').prop("disabled", false);
                } else {
                    $('#reg_btn').prop("disabled", true);
                } 
            } else if($(this).prop("checked") == false){
                $('#reg_btn').prop("disabled", true);
            }    
        });   
    function numberOnly(id) {
    // Get element by id which passed as parameter within HTML element event
    var element = document.getElementById(id);
    // Use numbers only pattern, from 0 to 9
    var regex = /[^0-9]/gi;
    // This removes any other character but numbers as entered by user
    element.value = element.value.replace(regex, "");
    }

    function checkrefercode(id){ //alert(id);
            var refercode = document.getElementById(id).value; //alert(refercode);
            $.post("{{url('/check-refercode')}}",{ _token: '{{csrf_token()}}', refercode : refercode })
            .done(function(data){ 
                $('#refercode_verfication').html('');
            })
            .fail(function(xhr, status, error) {
                console.log(error);
                $('#'+id).val("");
                $('#refercode_verfication').html('Invalid Refercode');
             });

    }

    //Account Kit Details
    AccountKit_OnInteractive = function(){
    AccountKit.init(
      {
        appId:{{\Setting::get('fb_app_id')}},
        state:"state", 
        version:"{{\Setting::get('fb_app_version')}}",
        fbAppEventsEnabled:true
      }
    );
  };

  function checkotp(){

        var otp = document.getElementById("otp").value;
        var my_otp = $('#otp_ref').val();
        if(otp){
            if(my_otp == otp){
                $(".print-error-msg").find("ul").html('');
                $('#mobile_otp_verfication').html("<p class='helper'> Please Wait... </p>");
                $('#phone_number').attr('readonly',true);
                $('#country_code').attr('readonly',true);
                $('.mobile_otp_verfication').hide();
                $('#second_step').fadeIn(400);
                $('#mobile_verfication').show().html("<p class='helper'> * Phone Number Verified </p>");
                my_otp='';
            }else{
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").find("ul").append('<li>Otp not Matched!</li>');
            }
        }
    }
   // phone form submission handler

       function smsLogin(){

        $('.exist-msg').hide();
        var countryCode = document.getElementById("country_code").value;
        var phoneNumber = document.getElementById("phone_number").value;
        $('#otp_phone').val(countryCode+''+phoneNumber);
        var csrf = $("input[name='_token']").val();;

            $.ajax({
                url: "{{url('/otp')}}",
                type:'POST',
                data:{ mobile : countryCode+''+phoneNumber,'_token':csrf ,phoneonly:phoneNumber},
                success: function(data) { 

                    if($.isEmptyObject(data.error)){
                        $('#otp_ref').val(data.otp);
                        $('.mobile_otp_verfication').show();
                        $('#mobile_verfication').hide();
                        $('#verify_button').hide();
                        $('#mobile_verfication').html("<p class='helper'> Please Wait... </p>");
                        $('#phone_number').attr('readonly',true);
                        $('#country_code').attr('readonly',true);
                        $(".print-error-msg").find("ul").html('');
                        $(".print-error-msg").find("ul").append('<li>'+data.message+'</li>');
                    }else{
                        
                        printErrorMsg(data.error);
                    }
                },
                error:function(jqXhr,status) { 

                    if(jqXhr.status === 422) {

                        $("#otp_mobile_registered").show();
                        $(".print-error-msg").show();
                        var errors = jqXhr.responseJSON;

                        $.each( errors , function( key, value ) { 
                            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
                        }); 
                    } 
                }

                });
    }
    function printErrorMsg (msg) { 

        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
       
        $(".print-error-msg").show();
       
        $(".print-error-msg").find("ul").append('<li><p>'+msg+'</p></li>');
        
    }
  //account hit ends here
    $("#second_step").hide();
    @if (count($errors) > 0)
        $("#second_step").show();
    @endif
    // Datepicker

    // var apiGeolocationSuccess = function(position) {
    // //alert("API geolocation success!\n\nlat = " + position.coords.latitude + "\nlng = " + position.coords.longitude);
    //     console.log(position.coords.latitude +' api '+position.coords.longitude); 
    //     $('#latitude_location').val(position.coords.latitude);
    //     $('#longitude_location').val(position.coords.longitude);
    //     $('#location_text').text('Your Location Updated');
    //     $('body').addClass('geolocation_hidden');
    // };


    // var tryAPIGeolocation = function() {
    //     jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key={{Setting::get('map_key','AIzaSyDDCQMW-zn7-60euM-r8eeF15AIjs8oDfs')}}", function(success) {
    //         apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
    
    //   })
    //   .fail(function(err) {
    //     $('#location_text').text("API Geolocation error! \n\n"+err);

    //   });
    // };

    // function getLocation() {
    //     if (navigator.geolocation) {
    //         navigator.geolocation.getCurrentPosition(showPosition,showError);
    //     } else {
    //         $('#location_text').text( "Geolocation is not supported by this browser.");
    //     }
    // }
    // function showPosition(position) {
    //     console.log(position.coords.latitude +' bro '+position.coords.longitude); 
    //     $('#latitude_location').val(position.coords.latitude);
    //     $('#longitude_location').val(position.coords.longitude);
    //     $('#location_text').text('Your Location Updated');
    // }

    // function showError(error) {
    //     switch(error.code) {
    //         case error.PERMISSION_DENIED:
    //             tryAPIGeolocation();
    //             $('#allow_location').show();
    //             console.log("User denied the request for Geolocation.");
    //             $('#location_text').text('User denied the request for Geolocation.');
    //             break;
    //         case error.POSITION_UNAVAILABLE:
    //             console.log("Location information is unavailable.");
    //             $('#location_text').text('Location information is unavailable.');
    //             break;
    //         case error.TIMEOUT:
    //             console.log("The request to get user location timed out.");
    //             $('#location_text').text('The request to get user location timed out.');
    //             break;
    //         case error.UNKNOWN_ERROR:
    //             console.log("An unknown error occurred.");
    //             $('#location_text').text('An unknown error occurred.');
    //             break;
    //     }
    // }
    // getLocation();
    $("#phone_num").on("keydown keyup change", function(){
            var charCode = event.keyCode;
            var value = $(this).val();
            if (charCode != 46 && charCode != 45 && charCode > 31
                && (charCode < 48 || charCode > 57)){
                return false;
            }
            else if (value.length > 9){
                return false;
            }else{
                return true;
            }

        });
    $(".text_feild").on("keydown keyup change", function(){
            var charCode = event.keyCode;
            //alert(charCode);
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)||charCode==8||charCode==9)
                    return true;
            return false;
        });
    var dt = new Date();
    dt.setFullYear(new Date().getFullYear()-18);
    $('.datepicker').datepicker({
        autoclose: true,
        //todayBtn: true,
        //todayHighlight: true,
        changeMonth: true,
        changeYear: true,
        endDate : dt
    }); 
    $(window).load(function () {
       $('.hover_bkgr_fricc').show();
        $('.popupCloseButton').click(function(){
            $('.hover_bkgr_fricc').hide();
        });
    });
    //For mobile number with date
        var input = document.querySelector("#country_code");
        window.intlTelInput(input,({
            // separateDialCode:true,
        }));
        $(".country-name").click(function(){
            var myVar = $(this).closest('.country').find(".dial-code").text();
            $('#country_code').val(myVar);
		});

    </script>
  </body>
</html>