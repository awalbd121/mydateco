@extends('layouts.user_header')

@section('content')
<style>
.notification-time {
    background: #f74573;
    height: 25px;
    width: 25px;
    border-radius: 100%;
    color: #ffffff;
    font-weight: 600;
    padding: 0px 8px;
    display: flex;
    position: relative;
    top: -7px;
}
</style>
<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height">
        <!-- Center Box Head Starts -->
        <div class="center-box-head p-f-20 row m-0">
            <h5 class="m-0">@lang('user.chat.chat_list')</h5>
        </div>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        @if(count($chat_users) > 0)
        <div class="center-box-content p-0">
            <div class="match-list">                
                    @foreach($chat_users as $key=>$chat_user)

                        <!-- Match List Block Starts -->
                        <a href="{{url('/chat_user',array('id' => $chat_user->user->id ))}}" class="match-list-block chat-list row m-0" data-ajax="false" data-userid="{{ $chat_user->user->id }}" data-notifyid="{{ Auth::user()->id }}">
                            <!-- Match List Block Left Starts -->
                            <div class="match-list-block-left col-md-6 p-l-0">
                                @if($chat_user->user->picture)
                                    <div class="match-list-img bg-img" style="background-image: url({{$chat_user->user->picture}});"></div>
                                @else 
                                    <div class="match-list-img bg-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                                @endif
                                <div class="match-list-details">
                                    <h6 class="match-list-tit">{{$chat_user->user->first_name}} {{$chat_user->user->last_name}}</h6>
                                    <p class="match-list-txt chat-view-txt" id="chat_txt_{{$key}}"></p>
                                </div>
                            </div>
                            <!-- Match List Block Left Ends -->
                            <!-- Match List Block Right Starts -->
                           
                            <div class="match-list-block-right col-md-6 text-right p-r-0">
                                @php $new_notification = user_new_notification_count(Auth::user()->id,$chat_user->user->id) @endphp
                                @if($new_notification != "")
                                <p class="notification-time" name="notification_time" id="new_notification_{{$key}}">{{ $new_notification }}</p>
                                @else
                                <p></p>
                                @endif
                                <p class="chat-time" id="chat_time_{{$key}}"></p>
                                <p class="bg-img" id="chat_new_{{$key}}" style="display: none;"> <img height="30" width="30" src="{{asset('design/img/new.png')}}"></p>
                            </div>
                            <!-- Match List Block Right Ends -->
                        </a>
                        <!-- Match List Block Ends -->
                        
                    @endforeach                                
            </div>
        </div>
        @else
            <div class="center-box-content notify-block">
                <div class="notify-block-inner">
                    <div class="notify-content">
                        <img src="{{asset('design/img/nolikes.png')}}">
                        <h6>@lang('user.matches.not_found',['name' => 'Chat'])</h6>
                    </div>
                </div>
            </div>
        @endif
        <!-- Center Box Content Ends -->
        <!-- Center Box Footer Starts -->
        <div class="center-box-foot">
        </div>
        <!-- Center Box Footer Ends -->
    </div>
</div>
<div class="col-md-3 p-0">
    <div class="right-sidebar body-height">
        <!-- Right Sidebar Content Starts -->
        <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">
            <h6>@lang('user.matches.invite',['sitename' => Setting::get('sitename')])</h6>
            <p>@lang('user.matches.invite_quote',['refer_money'=>Setting::get('currency').Setting::get('referal_amount')])</p>
            <a href="{{url('invites')}}" class="cmn-btn m-t-15">@lang('user.matches.invite_friends')</a>
        </div>
        <!-- Right Sidebar Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<!-- <div class="col-md-3 p-0">
    <div class="right-sidebar body-height"> -->
        <!-- Right Sidebar Content Starts -->
        <!-- <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">            
            <h6>@lang('user.matches.get_premium',['sitename'=>Setting::get('sitename')])</h6>
            <p>@lang('user.matches.get_premium_quote').</p>
            <a href="{{url('premium')}}" class="cmn-btn">@lang('user.form.get_now')</a>
        </div> -->
        <!-- Right Sidebar Content Ends -->
<!--     </div>
</div> -->
@endsection

@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.3/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.1/vue.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/3.7.0/firebase.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script type="text/javascript">
    $(document).on('click','.chat-list',function() {

        var notifier_id = $(this).attr('data-notifyid');
        var user_id = $(this).attr('data-userid');
        var url = "{{url('update/new/notification')}}";
        var token = "{{csrf_token()}}";   

        $.ajax({
            type: "POST",
            url : url,
            data: {'notifier_id':notifier_id,'user_id':user_id,'_token' :token},
            success: function(html)
            {
                
            }   
        });
    });

    // Initialize Firebase
    var chat_users={!! $chat_users !!};    
    var current_users={!! $user->id !!};

    function assign_value(key,data){
        if(data.type=='text'){
            $('#chat_txt_'+key).html(data.text);
        }else{
            $('#chat_txt_'+key).html('Uploaded '+data.type);
        }  
        if(data.read==0 && data.user!=current_users){    
            $('#chat_new_'+key).show();
        }     
        
        $('#chat_time_'+key).html(moment(data.timestamp).fromNow());
    }

    //console.log(sender);
    var config = {

        apiKey: "{{env('FIREBASE_API_KEY')}}",
        authDomain: "{{env('FIREBASE_AUTH_DOMAIN')}}",
        databaseURL: "{{env('FIREBASE_DATABASE_URL')}}",
        projectId: "{{env('FIREBASE_PROJECT_ID')}}",
        storageBucket: "{{env('FIREBASE_STORAGE_BUCKET')}}",
        messagingSenderId: "{{env('FIREBASE_MESSAGING_SENDERID')}}",
    };
    
    firebase.initializeApp(config);

    var database = firebase.database();//.push();  
    
    new Vue({
        el: '.match-list',
        data: {
            chatMessages: [],
            txt: '',
            file: '',
        },
        mounted: function() {
            var self = this;
            var now = Date.now();
            $.each(chat_users, function(key, value) {                
                var user=value.user_id
                var sender=value.like_id
                if(user<sender){
                    var room_name=user+'_chats_'+sender;
                }
                else{
                    var room_name=sender+'_chats_'+user;
                }
                
                database.ref(room_name).orderByChild('timestamp').on('child_added', function(data) {
                    console.log(data.val());
                    var dataval=data.val();  
                    assign_value(key,data.val());                    
                });
            }); 
            
        },
        methods: {

        }
    })
</script>
@endsection
