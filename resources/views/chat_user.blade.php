@extends('layouts.user_header')
@section('content')
<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box chat-center-box p-0">
        <!-- Center Box Content Starts -->
        <div class="center-box-content p-0">
            <!-- Chat Head Starts -->
            <div class="chat-head row m-0">
                <div class="col-xs-12 back-block p-0">
                    <a href="chat.html" class=""><i class="zmdi zmdi-long-arrow-left"></i> @lang('user.form.back')</a>
                </div>
                <div class="col-xs-12 p-0">
                    <a href="{{url('single-user',array('id'=>$sender->id))}}" >
                    @if($sender->picture)
                        <div class="match-list-img bg-img" style="background-image: url({{$sender->picture}});"></div>
                    @else 
                        <div class="match-list-img bg-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                    @endif
                    </a>
                    <div class="match-list-details">
                        <h6 class="match-list-tit">{{$sender->first_name}} {{$sender->last_name}}</h6>
                       <!--  <p class="match-list-txt">3KM @lang('user.matches.away')</p> -->
                    </div>
                </div>
            </div>
            <!-- Chat Head Ends -->
            <!-- Chat Block Starts -->
            <div id="userchat">
                <div class="chat-block p-f-15">
                    <div v-for="chat in chatMessages">
                        <!-- Admin Chat Starts -->
                        <div class="left-chat row m-0" v-if="chat.admin==1">
                            <!-- @if($sender->picture)
                                <div class="chat-img bg-img pull-left" style="background-image: url({{$sender->picture}});"></div>
                            @else 
                                <div class="chat-img bg-img pull-left" style="background-image: url({{asset('design/img/user.png')}});"></div>
                            @endif -->
                            <div class="pull-left about-txt">@lang('user.chat.admin')</div><br/>
                            <div class="chat-det pull-left">
                                <div class="chat-det-box" v-if="chat.type == 'text'">
                                    <p class="chat-txt chat-left-txt txt-only">@{{chat.text}}</p>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>
                                <div class="chat-det-box" v-else-if="chat.type == 'image'">
                                    <p class="chat-txt chat-left-txt text-center">
                                        <span class="send-img bg-img" v-bind:style="{ backgroundImage: 'url(' + chat.url + ')' }"></span>
                                    </p>
                                    <a class="chat-download-icon" title="download" v-bind:href="chat.url" target="_blank" download=""><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div> 
                                <div class="chat-det-box" v-else-if="chat.type == 'video'"> 
                                    <video class="video_play" width="310" controls="true">
                                        <source v-bind:src="chat.url">Your browser doesn't support HTML5 video tag.
                                    </video> 
                                    <a class="chat-download-icon" title="download" v-bind:href="chat.url" target="_blank" download=""><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>
                                <div class="chat-det-box" v-else>
                                    <p class="chat-txt chat-left-txt text-center" >
                                        <i class="zmdi zmdi-file-text file-icon"></i>
                                        <br> @lang('user.chat.download_file')
                                    </p>
                                    <a title="download" v-bind:href="chat.url" target="_blank" download="" class="chat-download-icon"><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>                                   
                            </div>
                        </div>
                        <!-- Admin Chat Box Ends -->  
                        <!-- Right Chat Starts -->
                        <div class="right-chat row m-0" v-else-if="chat.user=={{$user->id}}" >
                            <!-- @if($user->picture)
                                <div class="chat-img bg-img pull-right" style="background-image: url({{$user->picture}});"></div>
                            @else 
                                <div class="chat-img bg-img pull-right" style="background-image: url({{asset('design/img/user.png')}});"></div>
                            @endif -->
                            <div class="chat-det pull-right">
                                <div class="chat-det-box" v-if="chat.type == 'text'">
                                    <p class="chat-txt chat-right-txt">@{{chat.text}}</p>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>
                                <div class="chat-det-box" v-else-if="chat.type == 'image'">
                                    <p class="chat-txt chat-right-txt text-center">
                                        <span class="send-img bg-img" v-bind:style="{ backgroundImage: 'url(' + chat.url + ')' }"></span>
                                    </p>
                                    <a class="chat-download-icon" title="download" v-bind:href="chat.url" target="_blank" download=""><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div> 
                                <div class="chat-det-box" v-else-if="chat.type == 'video'"> 
                                    <video class="video_play" width="310" controls="true">
                                        <source v-bind:src="chat.url">Your browser doesn't support HTML5 video tag.
                                    </video> 
                                    <a class="chat-download-icon" title="download" v-bind:href="chat.url" target="_blank" download=""><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>
                                <div class="chat-det-box" v-else>
                                    <p class="chat-txt chat-right-txt text-center" >
                                        <i class="zmdi zmdi-file-text file-icon"></i>
                                        <br> @lang('user.chat.download_file')
                                    </p>
                                    <a title="download" v-bind:href="chat.url" target="_blank" download="" class="chat-download-icon"><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>                                   
                            </div>
                        </div>
                        <!-- Right Chat Box Ends -->
                        <!-- Left Chat Starts -->
                        <div class="left-chat row m-0" v-else>
                            <!-- @if($sender->picture)
                                <div class="chat-img bg-img pull-left" style="background-image: url({{$sender->picture}});"></div>
                            @else 
                                <div class="chat-img bg-img pull-left" style="background-image: url({{asset('design/img/user.png')}});"></div>
                            @endif -->
                            <div class="chat-det pull-left">
                                <div class="chat-det-box" v-if="chat.type == 'text'">
                                    <p class="chat-txt chat-left-txt txt-only">@{{chat.text}}</p>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>
                                <div class="chat-det-box" v-else-if="chat.type == 'image'">
                                    <p class="chat-txt chat-left-txt text-center">
                                        <span class="send-img bg-img" v-bind:style="{ backgroundImage: 'url(' + chat.url + ')' }"></span>
                                    </p>
                                    <a class="chat-download-icon" title="download" v-bind:href="chat.url" target="_blank" download=""><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div> 
                                <div class="chat-det-box" v-else-if="chat.type == 'video'"> 
                                    <video class="video_play" width="310" controls="true">
                                        <source v-bind:src="chat.url">Your browser doesn't support HTML5 video tag.
                                    </video> 
                                    <a class="chat-download-icon" title="download" v-bind:href="chat.url" target="_blank" download=""><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>
                                <div class="chat-det-box" v-else>
                                    <p class="chat-txt chat-left-txt text-center" >
                                        <i class="zmdi zmdi-file-text file-icon"></i>
                                        <br> @lang('user.chat.download_file')
                                    </p>
                                    <a title="download" v-bind:href="chat.url" target="_blank" download="" class="chat-download-icon"><i class="zmdi zmdi-download"></i></a>
                                    <p class="chat-time">@{{moment(chat.timestamp).fromNow()}}</p>
                                </div>                                   
                            </div>
                        </div>
                        <!-- Left Chat Box Ends -->                        
                    </div>
                </div>
                <!-- Chat Block Ends -->
                <!-- Chat Footer Starts -->
                <div class="chat-foot">
                    <form @submit.prevent="send" enctype="multipart/form-data">
                        <div class="input-group">
                            <input type="text" class="form-control msg-snd" v-model="txt" placeholder="@lang('user.chat.enter_message')" data-userid="{{ $sender->id }}" data-notifyid="{{ Auth::user()->id }}" id="message_send">
                            <span class="input-group-addon attach"> 
                                <div class="upload-btn-wrapper">
                                    <button class="upload-btn chat-sub-btn"><i class="zmdi zmdi-attachment-alt"></i></button>
                                    <input type="file" name="myfile" v-model="file" @change="sendfile"  />
                                </div>
                            </span>
                            <span class="input-group-addon " id="send">
                                <button type="submit" class="chat-send chat-sub-btn"><i class="zmdi zmdi-mail-send"></i></button>
                            </span>
                        </div>
                    </form>                    
                </div>
                <!-- Chat Footer Ends -->
            </div>            
        </div>
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<div class="col-md-3 p-0 hidden-md">
    <div class="right-sidebar">
        <!-- Right Sidebar Content Starts -->
        <div class="right-sidebar-content">
            <!-- Photos SEction Starts -->
            <div class="right-sec p-0">
                <div class="match-slide right-match-slide">
                    @php $image_length=count($sender->user_images);@endphp
                    @php $im=0; @endphp
                    @foreach($sender->user_images as $user_image)
                        <div class="match-slide-box m-0">
                            <div class="about-prof-img bg-img sidebar_image{{$im}}" style=" background-image: url({{$user_image->image}})"></div>
                        </div>
                        @php $im++;@endphp
                    @endforeach  
                    @for ($i = $image_length; $i < 5; $i++)
                        <div class="match-slide-box m-0">
                            <div class="about-prof-img bg-img sidebar_image{{$i}}" style=" background-image: url({{asset('design/img/user.png')}})"></div>
                        </div>
                    @endfor 
                </div>
            </div>
            <!-- Photo Section Ends -->
            <!-- About Section Starts -->
            <div class="right-sec about-sec">
                <h6 class="about-tit">{{$sender->first_name}}</h6>
                <p class="about-txt">{{$sender->about}}</p>
            </div>
            <!-- About Section Ends -->
            <!-- Video Section Starts -->
            <div class="right-sec video-sec m-t-15">
                <h6 class="about-tit">Bio Video</h6>
                <video width="310" controls="true">
                    <source src="{{$sender->bio_video}}" type="video/mp4">Your browser doesn't support HTML5 video tag.
                </video>
            </div>
            <!-- Video Section Ends -->
            <!-- Tags Section Starts -->
            <div class="right-sec tag-section m-t-15">
                <h6 class="about-tit">Interests</h6>
                <span class="tags">Reading</span>
                <span class="tags">Shopping</span>
                <span class="tags">Swimming</span>
                <span class="tags">Movies</span>
                <span class="tags">Cricket</span>
                <span class="tags">Travel</span>
            </div>
            <!-- Tag Section Ends -->
        </div>
        <!-- Right Sidebar Content Ends -->
    </div>
</div>
@endsection

@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.3/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.1/vue.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/3.7.0/firebase.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script type="text/javascript">

    $(document).on('click','#message_send',function() {

        var notifier_id = $(this).attr('data-notifyid');
        var user_id = $(this).attr('data-userid');
        var url = "{{url('update/new/notification')}}";
        var token = "{{csrf_token()}}";   

        $.ajax({
            type: "POST",
            url : url,
            data: {'notifier_id':notifier_id,'user_id':user_id,'_token' :token},
            success: function(html)
            {
                
            }   
        });
    });

  // Initialize Firebase
    var user={!! $user->id !!};
    var sender={!! $sender->id !!};
    if(user<sender){
        var room_name=user+'_chats_'+sender;
    }
    else{
        var room_name=sender+'_chats_'+user;
    }


    //console.log(sender);
    var config = {

        /*apiKey: "AIzaSyCkKe8E6AkcKLCpOGqvn86C71Lk8kTjguE",
        authDomain: "MyDate-d1a01.firebaseapp.com",
        databaseURL: "https://MyDate-d1a01.firebaseio.com",
        projectId: "MyDate-d1a01",
        storageBucket: "MyDate-d1a01.appspot.com",
        messagingSenderId: "99123424289"*/

        apiKey: "{{Setting::get('FIREBASE_API_KEY')}}",
        authDomain: "{{Setting::get('FIREBASE_AUTH_DOMAIN')}}",
        databaseURL: "{{Setting::get('FIREBASE_DATABASE_URL')}}",
        projectId: "{{Setting::get('FIREBASE_PROJECT_ID')}}",
        storageBucket: "{{Setting::get('FIREBASE_STORAGE_BUCKET')}}",
        messagingSenderId: "{{Setting::get('FIREBASE_MESSAGING_SENDERID')}}",
    };
    
    firebase.initializeApp(config);

    var database = firebase.database().ref(room_name);//.push();
    // Get the Storage service for the default app
    var storageRoom = firebase.storage();

    function send_push(message){
        var url    = "{{url('send_push')}}";
        var token  = "{{csrf_token()}}";
        $.ajax({
            type: "POST",
            url : url,
            data: {'sender_id' :sender,'message' :message,'_token' :token},
            success: function(html)
            {
                console.log(html);
            }
        });
    }
    
    function writeData(text) {        
      database.push({
        text: text,
        type: 'text',
        timestamp: Date.now(),
        user: user,
        sender: sender,
        read: 0,
      })
      send_push('message');
    };

    function writeFile(file) {
      var file_type =file.type.split('/'); 

      // Upload the image to Cloud Storage.
      var filePath = room_name+ '/' + Date.now()+file.name;
      storageRoom.ref(filePath).put(file).then(function(snapshot) {        

        // Get the file's Storage URI and update the chat message placeholder.
        var fullPath = snapshot.metadata.downloadURLs[0];
        database.push({
            url: fullPath,
            type: file_type[0],
            timestamp: Date.now(),
            user: user,
            sender: sender,
            read: 0,
          })
        //return data.update({url: storageRoom.ref(filePath).toString()});
        
        }.bind(this)).catch(function(error) {
          console.error('There was an error uploading a file to Cloud Storage:', error);
        });
        send_push(file_type[0]);
    };    
    function toBottom() {
        document.querySelector('.chat-block').scrollTop = 1000;
        //$('.video_play')[0].load();        
    }
    
    new Vue({
        el: '#userchat',
        data: {
            chatMessages: [],
            txt: '',
            file: ''
        },
        mounted: function() {
            var self = this;
            var now = Date.now();
            database.on('child_added', function(data) {                
                console.log(data.key, data.val(), moment(data.timestamp).fromNow());
                $('#loader_id').hide();
                var dataval=data.val();               
                if(dataval.read==0 && dataval.user==sender){                    
                    //console.log(dataval.read);   
                    database.child(data.key).update({read:1});                 
                }
                self.chatMessages.push(dataval);                           
               
              setTimeout(function() {
                toBottom();
              }, 0)
            });
        },
        methods: {
            send: function(e) {
                //console.log('send');
                let txt = this.txt.trim();
                if (txt) {
                    // send it
                    writeData(txt);
                    this.txt = '';
                }
            },
            sendfile: function(e) {
                $('#loader_id').show();
                var file = e.target.files || e.dataTransfer.files;
                console.log(file[0]);                
                if (file[0]) {
                    // send it
                    writeFile(file[0]);
                    this.file = '';
                }
            }
        }
    })
</script>
@endsection
