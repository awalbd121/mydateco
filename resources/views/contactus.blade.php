@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height p-f-15">
        <!-- Center Box Head Starts -->
        <div class="center-box-head row m-0">
            <h5 class="m-0">@lang('user.menu.contact_us')</h5>
        </div>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        <div class="center-box-content">
            <div class="cmn-content contact-content">
                <div class="contact-box">
                    <h6 class="contact-sub-tit">@lang('user.contact_us.help_support')</h6>
                    <p>@lang('user.contact_us.technical').</p>
                    <p><a href="help" class="theme-link">@lang('user.menu.contact_us')</a>@lang('user.contact_us.reach_facsimile') {{$setting_mobile['value']}}</p>
                </div>
                <div class="contact-box">
                    <h6 class="contact-sub-tit">@lang('user.contact_us.partnerships')</h6>
                    <p>@lang('user.contact_us.partnering',['sitename'=>Setting::get('sitename')])?</p>
                    <p><a href="#" class="theme-link">patner@MyDate.com</a></p>
                </div>
                <div class="contact-box">
                    <h6 class="contact-sub-tit">@lang('user.contact_us.press')</h6>
                    <p>@lang('user.contact_us.partnering',['sitename'=>Setting::get('sitename')])?</p>
                    <p><a href="#" class="theme-link">press@MyDate.com</a></p>
                </div>
                <div class="contact-box">
                    <h6 class="contact-sub-tit">@lang('user.contact_us.success_stories')</h6>
                    <p> @lang('user.contact_us.important_person',['sitename'=>Setting::get('sitename')]).</p>
                    <p><a href="#" class="theme-link">{{$setting_email['value']}}</a></p>
                </div>
            </div>
        </div>
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<div class="col-md-3 p-0">
    <div class="right-sidebar body-height">
        <!-- Right Sidebar Content Starts -->
        <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">            
            <!-- <h6>@lang('user.matches.get_premium',['sitename'=>Setting::get('sitename')])</h6> -->
          <!--  <p>@lang('user.matches.get_premium_quote').</p>
            <a href="{{url('premium')}}" class="cmn-btn">@lang('user.form.get_now')</a> -->
        </div>
        <!-- Right Sidebar Content Ends -->
    </div>
</div>
@endsection
