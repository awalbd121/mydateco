@extends('layouts.user_header')

@section('content')

@include('notification.user_notify')
<style type="text/css">
   #image-preview input {
    z-index: 1;
}
.close-btn {
    position: absolute;
    z-index: 1;
    background: #282627;
    padding: 3px;
    width: 22px;
    border-radius: 50%;
    height: 25px;
    left: 0;
}
.close-btn i {
    color: #fff;
}
</style>
<!-- Center Box Starts -->
<div class="col-md-9 p-0">
    <div class="center-box body-height p-f-20">
        <!-- Center Box Head Starts -->
        <form action="{{url('/editprofile')}}" method="post" enctype="multipart/form-data" > 
        {{csrf_field()}}
        <div class="center-box-head row m-0">
            <h5 class="m-0 pull-left">@lang('user.user.edit_profile')</h5>
            <input type="submit" name="save" class="cmn-btn pull-right" value="Save" />
        </div>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        <div class="center-box-content p-0">
            <div class="profile">               
                <!-- Upload Image Section Starts -->
                <div class="upload-img prof-sec">
                    <!-- Main Image Upload Starts -->
                    <div id="image-preview">

                        <a href="#" class="close-btn" id="rm1"><i class="zmdi zmdi-close"></i></a>
                        @if($user->picture)
                            <div id="bg_picture" class="bg-img edit-img" style="background-image: url({{$user->picture}});"></div>
                        @else 
                             <div id="bg_picture" class="bg-img edit-img"></div>
                        @endif 
                        <label for="image-upload" id="image-label">@lang('user.user.choose_file')</label>
                        <input type="file" name="picture" id="image-upload" />
                    </div>
                    <!-- Main Image Upload Ends -->
                    @php 
                    $image1=$image2=$image3=$image4='';
                    $user_interest=array();
                    if(count($user->user_interest)>0){
                        foreach($user->user_interest as $interest){
                            $user_interest[]=$interest->interest_id;
                        }
                    } else {
                        $user_interest[]="";
                    }
                    foreach($user->user_images as $user_image){
                        if($user_image->status==1) $image1=$user_image->image;
                        if($user_image->status==2) $image2=$user_image->image;
                        if($user_image->status==3) $image3=$user_image->image;
                        if($user_image->status==4) $image4=$user_image->image;
                        if($user_image->status==5) $image5=$user_image->image;
                    }
                    @endphp
                    <!-- Sub Image Section Starts -->
                    <div class="sub-img-sec">
                        <div class="sub-img-sec-inner row m-0">
                            <!-- Sub Image Upload Starts -->
                            <div class="col-md-3 p-l-0">
                                <div id="image-preview-small">
                                    <a href="#" class="close-btn" id="cls1"><i class="zmdi zmdi-close"></i></a>
                                    @if($image1)
                                        <div class="bg-img edit-img" id="edit_img1" style="background-image: url({{$image1}});"></div>
                                    @else 
                                         <div class="bg-img edit-img"></div>
                                    @endif
                                    <label for="image-upload-small" id="image-label-small"><i class="zmdi zmdi-plus"></i></label>
                                    <input type="file" name="image1" class="extra-picture" id="image-upload-small" value="{{$image1}}"/>
                                    <input type="hidden" name="del1" id="del1" value="">
                                </div>
                            </div>
                            <!-- Sub Image Upload Ends -->
                            <!-- Sub Image Upload Starts -->
                            <div class="col-md-3 p-l-0">
                               <div id="image-preview-small1">
                                    <a href="#" class="close-btn" id="cls2"><i class="zmdi zmdi-close"></i></a>
                                    @if($image2)
                                        <div class="bg-img edit-img" id="edit_img2" style="background-image: url({{$image2}});"></div>
                                    @else 
                                         <div class="bg-img edit-img"></div>
                                    @endif
                                    <label for="image-upload-small1" id="image-label-small1"><i class="zmdi zmdi-plus"></i></label>
                                    <input type="file" name="image2" class="extra-picture" id="image-upload-small1" value="{{$image2}}"/>
                                    <input type="hidden" name="del2" id="del2" value="">
                                </div>
                            </div>
                            <!-- Sub Image Upload Ends -->
                            <!-- Sub Image Upload Starts -->
                            <div class="col-md-3 p-l-0">
                                <div id="image-preview-small2">
                                    <a href="#" class="close-btn" id="cls3"><i class="zmdi zmdi-close"></i></a>
                                    @if($image3)
                                        <div class="bg-img edit-img" id="edit_img3" style="background-image: url({{$image3}});"></div>
                                    @else 
                                         <div class="bg-img edit-img"></div>
                                    @endif
                                    <label for="image-upload-small2" id="image-label-small2"><i class="zmdi zmdi-plus"></i></label>
                                    <input type="file" name="image3" class="extra-picture" id="image-upload-small2" value="{{$image3}}"/>
                                    <input type="hidden" name="del3" id="del3" value="">
                                </div>
                            </div>
                            <!-- Sub Image Upload Ends -->
                            <!-- Sub Image Upload Starts -->
                            <div class="col-md-3 p-l-0">
                                <div id="image-preview-small3">
                                    <a href="#" class="close-btn" id="cls4"><i class="zmdi zmdi-close"></i></a>
                                    @if($image4)
                                        <div class="bg-img edit-img" id="edit_img4" style="background-image: url({{$image4}});"></div>
                                    @else 
                                         <div class="bg-img edit-img"></div>
                                    @endif
                                    <label for="image-upload-small3" id="image-label-small3"><i class="zmdi zmdi-plus"></i></label>
                                    <input type="file" name="image4" class="extra-picture" id="image-upload-small3" value="{{$image4}}" />
                                    <input type="hidden" name="del4" id="del4" value="">
                                </div>
                            </div>
                            <!-- Sub Image Upload Ends -->
                        </div>
                    </div>
                    <!-- Sub Image Section Ends -->
                </div>
                <!-- Upload Image Section Ends -->
                <!-- Upload Video Starts -->
                <div class="upload-video prof-sec">
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.upload_bio_video')</h6>
                        <input type="file" id="demo" class="dropify" name="bio_video" data-max-file-size="10M" data-allowed-file-extensions="mp4 avi wmv mpg" data-errors-position="outside" >
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.first_name')</h6>
                        <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}" >
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.last_name')</h6>
                        <input type="text" class="form-control" name="last_name" value="{{$user->last_name}}" >
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.phone_number')</h6>
                        <input type="text" class="form-control" name="mobile" id="mobile" value="{{$user->mobile}}" >
                    </div>
                    <div class="error">

                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.dob')</h6>
                        <input type="text" class="form-control datepicker" name="dob" value="{{$user->dob}}" >
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.gender')</h6>
                        <select class="form-control" name="gender">
                            <option value="">Choose Options</option>
                            <option value="male" @if($user->gender=='male') {{'selected'}} @endif>Male</option>
                            <option value="female" @if($user->gender=='female') {{'selected'}} @endif>Female</option>
                            <option value="other" @if($user->gender=='other') {{'selected'}} @endif>Other</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">Country</h6>
                        <select class="form-control" name="country_id">
                            <option value="">Choose Options</option>
                            @foreach(country_list() as $country)
                                <option value="{{ $country->id }}" @if($user->user_country->id==$country->id) {{'selected'}} @endif>{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group" style="display:none;">
                        <h6 class="prof-tit">@lang('user.user.location')</h6>
                        <input type="text" class="form-control" name="address" id="location" value="{{$user->address}}" onFocus="geolocate()" onkeypress="return disableEnterKey(event);" >
                        <input type="hidden" name="latitude" id="lat" value="{{$user->latitude}}">
                        <input type="hidden" name="longitude" id="lng" value="{{$user->longitude}}">
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.about') {{$user->first_name}}</h6>
                        <textarea class="form-control" maxlength="255   " name="about" rows="3">{{$user->about}}</textarea>
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.current_work')</h6>
                        <input type="text" class="form-control" name="work" value="{{$user->work}}" >
                    </div>  
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.language')</h6>
                        <select class="form-control" name="language">
                            <option value="">Choose Options</option>
                            <option value="en" @if($user->language=='en') {{'selected'}} @endif>English</option>
                            <option value="ja" @if($user->language=='ja') {{'selected'}} @endif>Japanese</option>
                            <option value="es" @if($user->language=='es') {{'selected'}} @endif>Spanish</option>
                            <option value="zh-Hans" @if($user->language=='zh-Hans') {{'selected'}} @endif>Simplified Chinese</option>
                            <option value="zh-Hant" @if($user->language=='zh-Hant') {{'selected'}} @endif>Traditional Chinese</option>
                        </select>
                    </div>  
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.select_interest')</h6>
                        <!-- Interest Block Starts -->
                        @foreach($interests as $index=>$interest)
                        <div class="interest-block">
                            <input type="checkbox" name="interest[]" id="interest-{{$index}}" class="interest" value="{{$interest->id}}"  @if(in_array($interest->id,$user_interest)) {{'checked'}} @endif >
                            <label for="interest-{{$index}}">
                                <p class="interest-txt">{{$interest->name}}</p>
                            </label>
                        </div>
                        @endforeach
                        <input type="hidden" name="usr_interest" value="">
                        <!-- Interest Block Ends -->
                    </div>               
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.control_profile')</h6>
                        <div class="discovery-block">
                            <h6 class="dis-tit">@lang('user.user.show_my_age')</h6>
                            <input type="checkbox" name="show_age" id="switch2" @if($user->show_age==1) {{'checked'}} @endif>
                        </div>
                        <div class="discovery-block">
                            <h6 class="dis-tit">@lang('user.user.distance_visible')</h6>
                            <input type="checkbox" name="show_distance" id="switch3" @if($user->show_distance==1) {{'checked'}} @endif>
                        </div>
                    </div>
                </div>
                <!-- Upload Video Ends -->
                <div class="profile-foot m-t-15">
                    <a data-ajax="false" href="{{url('/profile')}}" class="cmn-btn">@lang('user.form.cancel')</a>
                    <button type="submit" class="cmn-btn">@lang('user.form.save')</button>
                </div>            
            </div>
        </div>
        </form>
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<!-- <div class="col-md-3 p-0">
    <div class="right-sidebar body-height"> -->
        <!-- Right Sidebar Content Starts -->
        <!-- <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">
            <h6>@lang('user.matches.get_premium',['sitename'=>Setting::get('sitename')])</h6>
            <p>@lang('user.matches.get_premium_quote').</p>
            <a href="{{url('premium')}}" class="cmn-btn">@lang('user.form.get_now')</a>
        </div> -->
        <!-- Right Sidebar Content Ends -->
<!--     </div>
</div> -->
    
@endsection

@section('scripts')
<!-- Upload File JS -->
    <script type="text/javascript" src="{{asset('design/js/dropify.js')}}"></script>
    <script type="text/javascript" src="{{asset('design/js/jquery.uploadPreview.min.js')}}"></script>
    <!-- Bootstrap Datepicker JS -->
    <script src="{{asset('design/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{Setting::get('map_key','AIzaSyDDCQMW-zn7-60euM-r8eeF15AIjs8oDfs')}}&libraries=places&callback=initAutocomplete" async defer></script>   
    
    <script type="text/javascript">
    var placeSearch, autocomplete;

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('location')),
            {types: ['geocode']});
        // fields in the form.
        autocomplete.addListener('place_changed', fillInlatlag);

      }
      function fillInlatlag() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        latvalue = place.geometry.location.lat();
        lngvalue = place.geometry.location.lng();
        $('#lat').val(latvalue);
        $('#lng').val(lngvalue);
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            console.log(geolocation);
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

    function disableEnterKey(e)
    {
        var key;
        if(window.e)
            key = window.e.keyCode; // IE
        else
            key = e.which; // Firefox

        if(key == 13)
            return e.preventDefault();
    }

    // Datepicker
    var dt = new Date();
    dt.setFullYear(new Date().getFullYear()-18);
    $('.datepicker').datepicker({
        autoclose: true,
        //todayBtn: true,
        //todayHighlight: true,
        changeMonth: true,
        changeYear: true,
        endDate : dt
    }); 

    $(document).ready(function() {
        $.uploadPreview({
            input_field: "#image-upload", // Default: .image-upload
            preview_box: "#image-preview", // Default: .image-preview
            label_field: "#image-label", // Default: .image-label
            label_default: "Choose File", // Default: Choose File
            label_selected: "Change File", // Default: Change File
            no_label: false // Default: false
        });
    });
    $(document).ready(function() {
        $.uploadPreview({
            input_field: "#image-upload-small", // Default: .image-upload
            preview_box: "#image-preview-small", // Default: .image-preview
            label_field: "#image-label-small", // Default: .image-label
            label_default: "Choose File", // Default: Choose File
            label_selected: "Change File", // Default: Change File
            no_label: false // Default: false
        });
        $.uploadPreview({
            input_field: "#image-upload-small1", // Default: .image-upload
            preview_box: "#image-preview-small1", // Default: .image-preview
            label_field: "#image-label-small1", // Default: .image-label
            label_default: "Choose File", // Default: Choose File
            label_selected: "Change File", // Default: Change File
            no_label: false // Default: false
        });
        $.uploadPreview({
            input_field: "#image-upload-small2", // Default: .image-upload
            preview_box: "#image-preview-small2", // Default: .image-preview
            label_field: "#image-label-small2", // Default: .image-label
            label_default: "Choose File", // Default: Choose File
            label_selected: "Change File", // Default: Change File
            no_label: false // Default: false
        });
        $.uploadPreview({
            input_field: "#image-upload-small3", // Default: .image-upload
            preview_box: "#image-preview-small3", // Default: .image-preview
            label_field: "#image-label-small3", // Default: .image-label
            label_default: "Choose File", // Default: Choose File
            label_selected: "Change File", // Default: Change File
            no_label: false // Default: false
        });
    });
    $('.dropify').dropify();
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#mobile").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                           
                if (!(keyCode >= 48 && keyCode <= 57)) {

                    $(".error").css("display", "inline");
                    return false;
                }else{
                    $(".error").css("display", "none");
                }
            });

            $(document).on('click','#cls1',function() {

                $("#edit_img1").css({'background-image': ''});
                $('#del1').attr("value","1");

            }); 

            


            $(document).on('click','#rm1',function() {

                $("#bg_picture").css({'background-image': ''});


            }); 

            $(document).on('click','#cls2',function() {

                $("#edit_img2").css({'background-image': ''});
                $('#del2').attr("value","2");
            }); 

            $(document).on('click','#cls3',function() {

                $("#edit_img3").css({'background-image': ''});
                $('#del3').attr("value","3");
            }); 

            $(document).on('click','#cls4',function() {

                $("#edit_img4").css({'background-image': ''});
                $('#del4').attr("value","4");
            }); 

            $(document).on('click','.interest',function() {

                if($(this).prop("checked") == true){
                    $(this).attr("checked",true);
                }
                else if($(this).prop("checked") == false){
                    $(this).attr("checked",false);
                }
            });
        }); 
    </script>
@endsection