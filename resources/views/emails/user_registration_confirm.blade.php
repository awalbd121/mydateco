<div style="width: 95%;border:1px #FB469C solid;padding: 0;margin: 0;overflow: hidden;">
	<div style="width: 100%;background-color: #FB469C;padding: 15px 10px 18px 10px;overflow: hidden;">
		<h1 style="color: #fff;margin: 0;"><img src="{{$message->embed(public_path().'/email_logo.png') }}" style="width: 60px;margin: 0;padding: 0;display: inline-block;float: left;margin-right: 20px;"> Mydate.</h1>
	</div>


	<div style="width: 100%;padding: 25px 10px;">
		<p>Dear {{$user->first_name.' '.$user->last_name}}</p>
		<p>Thank you for registering with Mydate.</p>
		<p>User Name: {{$user->username}}</p>
	</div>

	
	<div style="width: 100%;margin-top: 50px;margin-bottom: 25px;padding-left: 10px;">
		<p>Thanks & Regards</p>
		<p>Team Mydate.</p>
	</div>
	
	<div style="width: 100%;margin-top: 50px;margin-bottom: 0;padding: 15px 0;background-color: #FB469C;">
		<p style="margin: 0;padding: 0;text-align: center;color: #fff;">Copyright @ Mydate. All rights reserved.</p>
	</div>
</div>