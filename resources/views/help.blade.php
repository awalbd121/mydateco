@extends('layouts.content_header')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="staticContentBox">
	        	<h3>Test Help page Content !!!</h3>

	        	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacinia erat. Curabitur porta risus tempor facilisis maximus. Aliquam rutrum orci nisi, ac malesuada sem luctus a. Nam dui ligula, laoreet et magna quis, venenatis eleifend ligula. Maecenas pretium ac eros eget commodo. Vestibulum a risus elementum, tristique ligula at, auctor nisl. Pellentesque sit amet purus non libero vehicula tempus. Phasellus odio elit, tincidunt vitae fringilla vel, tempus suscipit nisi. Cras elementum congue gravida. Donec elit dui, vulputate sit amet porta vel, sodales vel nisi. Mauris euismod ante vel justo dignissim, sed ullamcorper quam egestas.</p>

				<p>Donec tortor nisi, laoreet sagittis vulputate nec, mollis sit amet augue. Sed placerat et velit id dignissim. Vivamus pharetra rhoncus tincidunt. Suspendisse consectetur ornare tortor at rhoncus. Vivamus cursus, erat non convallis ornare, erat erat dignissim augue, et faucibus libero ante id orci. Nunc efficitur maximus ipsum, sed interdum enim placerat aliquet. Etiam et metus eget magna pharetra tristique. Etiam dapibus volutpat arcu, sit amet consectetur mauris condimentum nec. Pellentesque ac leo vulputate nisl mollis suscipit vitae ut quam. Nulla facilisi. Sed id ex libero. Nullam blandit sit amet lorem quis faucibus.</p>

				<p>Phasellus cursus ex tortor, in sagittis urna laoreet vel. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam ut commodo purus, non euismod nunc. Cras commodo dolor a est placerat, nec porttitor orci finibus. Curabitur nisi urna, laoreet sit amet est a, iaculis maximus eros. Aliquam erat volutpat. Duis et tellus aliquam, consectetur ipsum eu, porttitor erat. Nam cursus erat molestie, facilisis lectus a, feugiat lacus. Mauris hendrerit pretium arcu nec ullamcorper. Vivamus a urna venenatis, mattis lectus sed, varius felis. Aenean condimentum posuere quam venenatis viverra.</p>

	        </div>
		</div>
	</div>
</div>
@endsection