@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height p-f-15">
        <!-- Center Box Head Starts -->
        <div class="center-box-head row m-0">
            <h5 class="m-0">@lang('user.matches.invite_friends')</h5>
        </div>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        <div class="center-box-content">
            <div class="invite-sec">
                <div class="text-center">
                    <div class="invite-img bg-img" style="background-image: url({{asset('design/img/banner.png')}});"></div>
                    <h6>@lang('user.matches.invite',['sitename'=>Setting::get('sitename')])</h6>
                    <p>@lang('user.matches.invite_quote',['refer_money' => Setting::get('currency').Setting::get('referal_amount')])</p>
                </div>
                <div class="form-group">
                    <label class="invite-label">@lang('user.matches.sharing_url',['sitename'=>Setting::get('sitename')])</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="refercode" value="{{Auth::user()->invite_code}}"  readonly="">
                         
                        <a href="javascript:void(0)" class="input-group-addon" onclick="myFunction()" >@lang('user.form.copy')</a>   
                    </div>
                    <small>and Share with Social login </small>
                    <div id="social-links">
                        <ul class="list-inline">
                            <li>
                                <a href="https://www.facebook.com/sharer/sharer.php?u=https://kazrr.com&summery=Register {{Setting::get('website_link')}} for unlimited match with chat" class="social-button " id=""><span class="fa fa-facebook-official"></span></a>
                            </li>
                          <!--   <li>
                                <a href="https://twitter.com/intent/tweet?text=Register {{Setting::get('website_link')}} for unlimited match with chat&amp;url=https://app.MyDate.com" class="social-button " id=""><span class="fa fa-twitter"></span></a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/share?url=https://app.MyDate.com" class="social-button " id=""><span class="fa fa-google-plus"></span></a>
                            </li> -->
                            <!-- <li>
                                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://app.MyDate.com&amp;title=my share text&amp;summary=Register {{Setting::get('website_link')}} for unlimited match with chat" class="social-button " id=""><span class="fa fa-linkedin"></span></a>
                            </li> -->
                            <li>
                                <a href="https://wa.me/?text=https://kazrr.com/" class="social-button " id=""><span class="fa fa-whatsapp"></span></a>
                            </li>    
                        </ul>
                    </div>
                       <!--  <input type="text" class="form-control" value="{{Setting::get('website_link')}}/?refercode={{Auth::user()->invite_code}}">
                        <a target="_blank" href="http://www.facebook.com/sharer.php?s=100&p[title]={{Setting::get('sitename')}} Registration&p[url]={{Setting::get('sitename')}}/&p[summary]=Register {{Setting::get('website_link')}} for unlimited match with chat&p[images][0]={{Setting::get('website_link')}}{{Setting::get('logo')}}" class="input-group-addon" id="basic-addon2">@lang('user.form.share')</a> -->
                    </div>
                </div>
                
            </div>
            <label class="invite-label">Refer Code:{{Auth::user()->invite_code}}</label><br>
            <!-- <label class="invite-label">Amount Gain By User:{{currency($referal_amount)}}</label><br> -->
            <!-- <label class="invite-label">No Of User Added':{{$user_referal_count }}</label> -->
        </div>
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<!-- <div class="col-md-3 p-0">
    <div class="right-sidebar body-height"> -->
        <!-- Right Sidebar Content Starts -->
        <!-- <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">            
            <h6>@lang('user.matches.get_premium',['sitename'=>Setting::get('sitename')])</h6>
            <p>@lang('user.matches.get_premium_quote').</p>
            <a href="{{url('premium')}}" class="cmn-btn">@lang('user.form.get_now')</a>
        </div> -->
        <!-- Right Sidebar Content Ends -->
<!--     </div>
</div> -->
@endsection
@section('styles')
<style type="text/css">
    .list-inline{display:block;}
    .list-inline li{display:inline-block;}
    .list-inline li:after{content:'|'; margin:0 10px;}
</style>
@endsection
@section('scripts')
<script src="{{ asset('js/share.js') }}"></script>
<script type="text/javascript">
  function myFunction() {
  /* Get the text field */
  var copyText = document.getElementById("refercode");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  //alert("Copied the text: " + copyText.value);
}  
</script>
@endsection
