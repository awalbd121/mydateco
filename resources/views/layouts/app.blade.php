<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MyDate</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="{{ asset('flamerui/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('flamerui/css/rangeslider.css')}}" rel="stylesheet">
    <link href="{{ asset('flamerui/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">        
    <link href="{{ asset('flamerui/css/slick.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('flamerui/css/slick-theme.css')}}" rel="stylesheet" type="text/css"/>
    <link href="http://kenwheeler.github.io/slick/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('flamerui/css/style.css') }}" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="{{Setting::get('favicon')}}"/>

    {{-- //propellerads ads code --}}
    <meta name="propeller" content="e4f11fc54c587aafd7a6690e8a55dcde">
    <script>(function(s,u,z,p){s.src=u,s.setAttribute('data-zone',z),p.appendChild(s);})(document.createElement('script'),'https://iclickcdn.com/tag.min.js',3860620,document.body||document.documentElement)</script>

    {{-- //Adsence Code --}}
    <script data-ad-client="ca-pub-3208805417662334" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    @yield('stylesheets')
</head> 

<body>
    <div class="full-outer">
        <div class="box-outer">
            <div id="mobile-app" class="card-box">
                <div class="full-load mobile-load" style="display:block">
                    <div class="full-load-outer">
                        <div class="full-load-inner">
                            <img src="{{asset('flamerui/img/logo.png')}}">

                            <div class="download">
                                
                                <a href="{{Setting::get('google_play')}}" target="_blank" class="app-link"><img src="{{asset('flamerui/img/playstore-icon.png')}}">
                                <a href="{{Setting::get('ios_app')}}" target="_blank" class="app-link"><img src="{{asset('flamerui/img/applestore-icon.png')}}"></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div id="get-flamer-popup" class="pay-load pay-popup">
                <div class="pay-load-outer">
                    <div class="pay-load-inner">
                        <div class="pay-outer">
                            <div class="pay-top">
                                <h5>Get {{Setting::get('sitename')}} Plus</h5>
                                <div class="pay-swiper pay-swipe">
                                    <div>
                                        <div class="img-outer">
                                            <img src="{{ asset('flamerui/img/redo.png') }}">
                                        </div>
                                        <span class="pay-txt">Rewind Your Lasy Swipe</span>
                                    </div>
                                    <div>
                                        <div class="img-outer">
                                            <img src="{{ asset('flamerui/img/location.png') }}">
                                        </div>
                                        <span class="pay-txt">Change Your Location</span>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="img-outer">
                                            <img src="{{ asset('flamerui/img/superlike.png') }}">
                                        </div>
                                        <span class="pay-txt">Get More Super Like</span>
                                    </div>
                                </div>
                            </div>
                            <form action="{{ route('UserPaymentPaypal') }}" method="post" name="payment" onsubmit="">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <a href="javascript: submitform()" class="pay-btn">Get it Now</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="menuxs">
                <span class="glyphicon glyphicon-align-left menu-xs-icon menu-xs aria-hidden="true"></span>
            
            <div class="left-box">
                <div class="top-logo">
                    @if(Setting::get('logo'))
                        <img src="{{Setting::get('logo')}}">
                    @else
                        <img src="{{asset('flamerui/img/logo.png')}}">
                    @endif
                </div> 
                <div class="side-btm smooth-scroll">           
                    <ul class="side-navv">
                        <li class="nav-profile">
                            <a href="{{route('UserProfile')}}" class="ui-link">
                                <div class="nav-avatar" style="background-image: url(@if ($user->picture) {{$user->picture}} @else flamerui/img/placeholder.png @endif)"></div>
                                <h5>{{ $user->name }}</h5>
                                <p>View Profile</p>
                            </a>
                        </li>
                        <li class="nav-list">
                            <a href="{{ route('UserSearch') }}" class="ui-link">
                                <img src="{{ asset('flamerui/img/flame.png') }}"><span class="side-nav-txt">Post</span>
                            </a>
                        </li>
                        <li class="nav-list">
                            <a href="{{ route('UserSettings')}}" class="ui-link">
                                <img src="{{ asset('flamerui/img/menu_appsettings.png') }}"><span class="side-nav-txt">Settings</span>
                            </a>
                        </li>
                        <li class="nav-list">
                            <a href="{{ route('UserFavorites')}}" class="ui-link">
                                <img src="{{ asset('flamerui/img/menu_match.png') }}"><span class="side-nav-txt">Favorite</span>
                            </a>
                        </li>
                        <!-- <li class="nav-list">
                            <a href="{{ route('UserMessages') }}" class="ui-link">
                                <img src="{{ asset('flamerui/img/actionbar_matches_active.png') }}"><span class="side-nav-txt">Chats</span>
                            </a>
                        </li> -->
                        <li class="nav-list">
                            <a href="{{ route('UserHelp') }}" class="ui-link">
                                <img src="{{ asset('flamerui/img/menu_support.png') }}"><span class="side-nav-txt">Help</span>
                            </a>
                        </li>

                        @if($user->user_type != 1)

                            <li class="nav-list" id="get-flamer-plus">
                                <a href="#" class="ui-link">
                                    <img src="{{ asset('flamerui/img/menu_plus.png') }}" /><span class="side-nav-txt">Get {{Setting::get('sitename')}} Plus</span>
                                </a>
                            </li>

                        @endif

                        <li class="nav-list">
                            <a href="{{ route('UserLogout') }}" class="ui-link">
                                <img src="{{ asset('flamerui/img/logout.png') }}"><span class="side-nav-txt">Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            
            </div>
            </div>
            @yield('content')
        </div>
    </div>

    <script src="{{ asset('flamerui/js/jquery.min.js') }}"></script>
    <script src="{{ asset('flamerui/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('flamerui/js/jquery.nicescroll.min.js') }}"></script>    
    <script src="{{ asset('flamerui/js/jquery.simple.thumbchanger.js') }}"></script>
    <script src="{{ asset('flamerui/js/rangeslider.min.js')}}"></script>
    <script src="{{ asset('flamerui/js/jquery-migrate-1.2.1.min.js')}}"></script>
    <script src="{{ asset('flamerui/js/slick.min.js')}}"></script>
    <script type="text/javascript">
        $('#menuxs').click(function(e){
            // e.preventDefault();
            $(this).find('div.left-box').toggleClass('show');
        });
    </script>
    <script>
        $(document).ready(function(){
            $('.pay-swiper').slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 1000,
            });
            $('#get-flamer-plus').click(function() {
                $('#get-flamer-popup').show();
            });
        });

        function submitform() {
            document.payment.submit();
        }

        $(document).ready(function(){
            $('#description').keypress(function (e) {
                var txt = String.fromCharCode(e.which);
                // console.log(txt + ' : ' + e.which);
                if(e.which != 0 && e.which != 8) {
                    if (!txt.match(/[A-Za-z0-9.,!@ ]/)) {
                        // console.log("false");
                        return false;
                    }
                }
            });
        });

    </script>

    @yield('mainScripts')

    <script src="{{ asset('flamerui/js/scripts.js') }}"></script>

    @yield('scripts')
</body>
</html>