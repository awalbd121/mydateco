<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{Setting::get('sitename','Datesuace')}}</title>
    <link rel="shortcut icon" type="image/png" href="{{ Setting::get('favicon') }}"/>
     <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{asset('design/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{asset('design/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <!-- Material Icons CSS -->
    <link href="{{asset('design/material-design-iconic-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
    <!-- Range Slider CSS -->
    <link href="{{asset('design/css/bootstrap-slider.min.css')}}" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="{{asset('design/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('design/css/slick-theme.css')}}" rel="stylesheet">
    <!-- Toggle Switch CSS -->
    <link href="{{asset('design/css/ToggleSwitch.css')}}" rel="stylesheet">
    <!-- Bootstrap Datepicker CSS -->
    <link href="{{asset('design/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{asset('design/css/style.css')}}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{asset('design/css/toastr.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('design/css/dropify.min.css')}}">
    <!-- Bootstrap Datepicker CSS -->
    <link href="{{asset('design/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">

    {{-- //propellerads ads code --}}
    <meta name="propeller" content="e4f11fc54c587aafd7a6690e8a55dcde">
    <script>(function(s,u,z,p){s.src=u,s.setAttribute('data-zone',z),p.appendChild(s);})(document.createElement('script'),'https://iclickcdn.com/tag.min.js',3860620,document.body||document.documentElement)</script>

    {{-- //Adsence Code --}}
    <script data-ad-client="ca-pub-3208805417662334" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <style type="text/css">
        header#header {
            background: #f5f5f5;
            
        }

        .main-content {
            padding-top: 50px;
        }

        .siteLogo {
            text-align: center;
        }

        .siteLogo a {
            display: inline-block;
            width: 75px;
        }

        .siteLogo a img {
            width: 100%;
        }

        .staticContentBox{
            margin-bottom: 50px;
            min-height: calc(100vh - 150px);
        }

        .staticContentBox h3 {
            margin: 0;
            font-size: 28px;
            font-weight: 600;
            border-bottom: 1px dashed #ddd;
            line-height: 1.5;
            padding-bottom: 10px;
            margin-bottom: 20px;
        }
       .staticContentBox p{
            margin: 0;
        }
        .staticContentBox p + p{
            margin-top: 20px;
        }
        footer{
            background: #1b2e35;
            text-align: center;
            color: #fff;
        }
        .copyright{
            padding-bottom: 20px;
            padding-top: 20px;
            font-size: 14px;
        }

    </style>
    
</head>
<body>

    <header id="header" class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="siteLogo">
                        <a href="{{url('/')}}" class="">
                            <img src="{{Setting::get('logo')}}" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
        </div>    
    </header><!-- /header -->
    <!-- Main Content Starts -->
    <div class="main-content">
        <div class="loader" style="display: none;" id="loader_id">
            <img src="{{asset('design/img/loader.gif')}}" >
        </div>
        @yield('content')
    </div>
    <footer>
        <div class="container">
            <div class="copyright">2020 All Right Reserved</div>
        </div>    
    </footer>
    <!-- Main Content Ends -->
    <script src="{{asset('design/js/jquery.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{asset('design/js/bootstrap.min.js')}}"></script>
    <!-- Slick Slider JS -->
    <script src="{{asset('design/js/slick.min.js')}}"></script>   
    <!-- Toggle Switch JS -->
    <script src="{{asset('design/js/ToggleSwitch.js')}}"></script>
    <!-- Scripts JS -->
    <script src="{{asset('design/js/scripts.js')}}"></script>
    <script src="{{asset('design/js/toastr.min.js')}}"></script>
    <!-- Bootstrap Datepicker JS -->
    <script src="{{asset('design/js/bootstrap-datepicker.min.js')}}"></script>

    
    @include('notification.userNotify')
    @yield('scripts')
</body>
</html>
