<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{Setting::get('sitename','Datesuace')}}</title>
    <link rel="shortcut icon" type="image/png" href="{{ Setting::get('favicon') }}"/>
     <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{asset('design/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{asset('design/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <!-- Material Icons CSS -->
    <link href="{{asset('design/material-design-iconic-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
    <!-- Range Slider CSS -->
    <link href="{{asset('design/css/bootstrap-slider.min.css')}}" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="{{asset('design/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('design/css/slick-theme.css')}}" rel="stylesheet">
    <!-- Toggle Switch CSS -->
    <link href="{{asset('design/css/ToggleSwitch.css')}}" rel="stylesheet">
    <!-- Bootstrap Datepicker CSS -->
    <link href="{{asset('design/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{asset('design/css/style.css')}}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{asset('design/css/toastr.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('design/css/dropify.min.css')}}">
    <!-- Bootstrap Datepicker CSS -->
    <link href="{{asset('design/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">

    {{-- //propellerads ads code --}}
    <meta name="propeller" content="e4f11fc54c587aafd7a6690e8a55dcde">
    <script>(function(s,u,z,p){s.src=u,s.setAttribute('data-zone',z),p.appendChild(s);})(document.createElement('script'),'https://iclickcdn.com/tag.min.js',3860620,document.body||document.documentElement)</script>

    {{-- //Adsence Code --}}
    <script data-ad-client="ca-pub-3208805417662334" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
    <!-- Main Content Starts -->
    <div class="main-content">
        <div class="loader" style="display: none;" id="loader_id">
            <img src="{{asset('design/img/loader.gif')}}" >
        </div>
        <div class="row m-0">
            <!-- Sidebar Starts -->
            <div class="col-md-3 p-0">
                <div class="sidebar body-height">
                    <div class="toggle-sec"  @if(count($user)==0) style="display: none;" @endif>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    @if(\Auth::user())
                    <div class="collapse navbar-collapse" id="menu-1">
                        <!-- Sidebar Head Starts -->
                        @if(count($user)>0)
                        <a href="{{url('/profile')}}" class="sidebar-head row m-0" data-ajax="false">
                            @if($user->picture)
                                <div class="prof-img bg-img pull-left" style="background-image: url({{$user->picture}});"></div>
                            @else 
                                <div class="prof-img bg-img pull-left" style="background-image: url({{asset('design/img/user.png')}});"></div>
                            @endif
                            
                            <div class="prof-details">
                                <h6 class="prof-name">{{$user->first_name}} {{$user->last_name}}</h6>
                                <p class="view-prof m-0">@lang('user.menu.view_profile')</p>
                            </div>
                        </a>
                        @endif
                        <!-- Sidebar Head Ends -->
                        <!-- Sidebar Content Starts -->
                        @php $current_route=Route::getCurrentRoute()->uri; @endphp
                        <div class="sidebar-content">
                        <div class="sidebar-content-top">
                            <div class="submenu @if($current_route == 'dashboard' || $current_route == 'mapview') {{'has-submenu'}} @endif">
                                <a href="{{url('/dashboard')}}" class="sidebar-menu-item submenu-item @if($current_route == 'dashboard' || $current_route == 'mapview') {{'active'}} @endif" data-ajax="false">
                                    <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-home"></i></span>
                                    <span class="sidebar-menu-txt">@lang('user.menu.find_matches')</span>
                                </a>
                                <!-- <div class="menu-drop">
                                    <a href="{{url('/dashboard')}}" class="sidebar-menu-item @if($current_route == 'dashboard') {{'active'}} @endif" data-ajax="false">
                                        <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-collection-image-o"></i></span>
                                        <span class="sidebar-menu-txt">@lang('user.menu.card_view')</span>
                                    </a>
                                    <a href="{{url('/mapview')}}" class="sidebar-menu-item @if($current_route == 'mapview') {{'active'}} @endif" data-ajax="false">
                                        <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-map"></i></span>
                                        <span class="sidebar-menu-txt">@lang('user.menu.map_view')</span>
                                    </a>
                                </div> -->
                            </div>
                            <!-- <a href="{{url('/dashboard')}}"  class="sidebar-menu-item @if($current_route == 'dashboard') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i  class="zmdi zmdi-home"></i></i></span>
                                <span class="sidebar-menu-txt">Find Matches - Card view</span>
                            </a>
                            <a href="{{url('/mapview')}}"  class="sidebar-menu-item @if($current_route == 'mapview') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i  class="zmdi zmdi-map"></i></i></span>
                                <span class="sidebar-menu-txt">Find Matches- Map view</span>
                            </a> -->
                            <a href="{{url('/matches')}}" class="sidebar-menu-item @if($current_route == 'matches') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-favorite"></i></span>
                                <span class="sidebar-menu-txt">@lang('user.menu.matches')</span>
                            </a>
                            <!-- @if(count($user->user_premium) > 0 && $user->user_premium->premium->likes_me == 1) -->
                            <!-- @endif -->
                            <a href="{{url('who_likes_me')}}" class="sidebar-menu-item @if($current_route == 'who_likes_me') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-thumb-up"></i></span>
                                <span class="sidebar-menu-txt">@lang('user.menu.who_likes_me')</span>
                            </a>
                            <a href="{{url('/discover_settings')}}" class="sidebar-menu-item @if($current_route == 'discover_settings') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-account"></i></span>
                                <span class="sidebar-menu-txt">@lang('user.menu.discovery_settings')</span>
                            </a>                            
                            <a href="{{url('/chat')}}" class="sidebar-menu-item @if($current_route == 'chat') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-comment-outline"></i></span>
                                <span class="sidebar-menu-txt">@lang('user.menu.chats')</span>
                            </a>
                            <!-- <a href="{{url('/payments')}}" class="sidebar-menu-item  @if($current_route == 'payments') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-money"></i></span>
                                <span class="sidebar-menu-txt">@lang('user.menu.payment')</span>
                            </a> 
                            <a href="{{url('/transaction_history')}}" class="sidebar-menu-item  @if($current_route == 'transaction_history') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-money"></i></span>
                                <span class="sidebar-menu-txt">@lang('user.menu.transaction_history')</span>
                            </a>-->
                            <a href="{{url('/notifications')}}" class="sidebar-menu-item  @if($current_route == 'notifications') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-notifications"></i></span>
                                <span class="sidebar-menu-txt">@lang('user.menu.notifications')</span>
                            </a>
                            <a href="{{url('/invites')}}" class="sidebar-menu-item  @if($current_route == 'invites') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-twitch"></i></span>
                                <span class="sidebar-menu-txt">@lang('user.menu.invites')</span>
                            </a>                            
                            
                            <!--<a href="{{url('/help')}}" class="sidebar-menu-item @if($current_route == 'help') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-flag"></i></span>
                                <span class="sidebar-menu-txt">Help</span>
                            </a> -->
                            @if(count($user)>0)
                            <!-- <a href="{{url('/premium')}}" class="sidebar-menu-item  @if($current_route == 'premium') {{'active'}} @endif" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-soundcloud"></i></span>
                                <span class="sidebar-menu-txt">@lang('user.menu.get_premium')</span>
                            </a> -->
                            
                            <a href="#" onclick="if(confirm('@lang('user.alert.logout_message')?')){ document.getElementById('logout-form').submit()};" class="sidebar-menu-item" data-ajax="false">
                                <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-mail-reply"></i></i></span>
                                <span class="sidebar-menu-txt">@lang('user.menu.logout')</span>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">  
                                    {{ csrf_field() }}
                                </form>
                            </a>
                            @endif
                        </div>
                        <div class="sidebar-content-foot">
                            <div class="tag-section text-center">
                                <a href="{{url('/privacy')}}" data-ajax="false"><span class="tags">@lang('user.menu.privacy_policy')</span></a>
                                <a href="{{url('/terms')}}" data-ajax="false"><span class="tags">@lang('user.menu.terms')</span></a>
                                <a href="{{url('/contactus')}}" data-ajax="false"><span class="tags">@lang('user.menu.contact_us')</span></a>
                            </div>
                            <div class="text-center copy-block">
                                <p>Copyrights &copy; {{Setting::get('sitename')}}</p>
                            </div>
                        </div>
                        <a href="{{url('/')}}" class="logo text-center">
                            <img src="{{Setting::get('logo')}}">
                        </a>

                        {{-- ///////////////////////// --}}
                        <style type="text/css">
                            .app-banner-sidebar{
                                width: 100%;
                                margin-top: 20px;
                                overflow: hidden;
                            }
                            .app_links{
                                width: 100%;
                                float: left;
                            }
                            .app_links a{
                                display: inline-block;
                                float: left;
                                max-width: 40%;
                                height: 36px;
                                overflow: hidden;
                            }
                            .app_links a.ios_app{
                                height: 29px;
                                margin-top: 3px;
                                margin-left: 5px;
                            }
                            .app_links a img{
                                display: inline-block;
                                height: 100%;
                                width: 100%;
                                overflow: hidden;
                            }
                            .app-banner-sidebar .app_link_content{
                                color: #FB469C;
                                padding-left: 17px;
                                margin-top: 50px;
                            }
                            .app-banner-sidebar .app_link_content strong{
                                font-size: 23px;
                            }
                            .app-banner-sidebar .app_link_content p{
                                font-size: 20px;
                            }
                        </style>
                        {{-- ///////////////////////// --}}

                        <div class="app-banner-sidebar">
                            <div class="row">
                                <div class="col-sm-8" style="padding-right: 0;">
                                    <div class="app_link_content">
                                        <strong><img src="{{Setting::get('logo')}}" style="display: inline-block;float: left;width: 30px;margin-top: 5px;margin-right: 5px;"> Mydate.</strong>
                                        <p style="margin-bottom: 0;">the world's best</p>
                                        <p style="margin-top: 0;line-height: 20px;">dating app</p>
                                        <div class="app_links">
                                            <a href="HTTPS://play.google.com/store/apps/details?id=com.app.mydatep">
                                                <img src="{{asset('android_app.png')}}">
                                            </a>

                                            <a class="ios_app" href="https://iTunes.apple.com/us/app/mydate/id1340317118?ls=1&mt=8">
                                                <img src="{{asset('ios_app.png')}}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <img src="{{asset('app-banner-sidebar.png')}}" style="max-width: 100%;max-height: 100%;margin-top: 20px;overflow: hidden;">
                                </div>
                            </div>
                            
                        </div><!--\.app-banner-sidebar-->

                        </div>
                        <!-- Sidebar Content Ends -->
                    </div>
                    @endif
                </div>
            </div>
            <!-- Sidebar Ends -->            
            @yield('content')
            
        </div>
    </div>
    <!-- Main Content Ends -->
    <script src="{{asset('design/js/jquery.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{asset('design/js/bootstrap.min.js')}}"></script>
    <!-- Slick Slider JS -->
    <script src="{{asset('design/js/slick.min.js')}}"></script>   
    <!-- Toggle Switch JS -->
    <script src="{{asset('design/js/ToggleSwitch.js')}}"></script>
    <!-- Scripts JS -->
    <script src="{{asset('design/js/scripts.js')}}"></script>
    <script src="{{asset('design/js/toastr.min.js')}}"></script>
    <!-- Bootstrap Datepicker JS -->
    <script src="{{asset('design/js/bootstrap-datepicker.min.js')}}"></script>

    
    @include('notification.userNotify')
    @yield('scripts')
</body>
</html>
