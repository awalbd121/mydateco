@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height">
        <!-- Center Box Head Starts -->
        <!-- <ul class="center-box-head row m-0 nav nav-tabs">
            <li role="presentation" class="col-xs-12 p-0 active">
                <a href="#map-view" class="center-box-head-block" aria-controls="map-view" role="tab" data-toggle="tab">@lang('user.menu.map_view')</a>
            </li>
        </ul> -->
        <ul class="center-box-head row m-0 nav nav-tabs find-match">
            <li role="presentation" class="col-xs-12 p-0">
                <!-- <a href="#card" class="center-box-head-block" aria-controls="card" role="tab" data-toggle="tab">@lang('user.menu.card_view')</a> -->
                <a href="{{url('/dashboard')}}" class="sidebar-menu-item " data-ajax="false">
                    <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-collection-image-o"></i></span>
                    <span class="sidebar-menu-txt">@lang('user.menu.card_view')</span>
                </a>
            </li>
            <li role="presentation" class="active col-xs-12 p-0">
                <a href="{{url('/mapview')}}" class="sidebar-menu-item" data-ajax="false">
                    <span class="pull-left sidebar-menu-icon"><i class="zmdi zmdi-map"></i></span>
                    <span class="sidebar-menu-txt">@lang('user.menu.map_view')</span>
                </a>
            </li>
            <!-- <li role="presentation" class="col-xs-6 p-0">
                <a href="#map-view" class="center-box-head-block" aria-controls="map-view" role="tab" data-toggle="tab">Map View</a>
            </li> -->
        </ul>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        <div class="center-box-content tab-content">  
        @if(count($find_matchs)>0)  
            @php $find_match_first=$find_matchs[0]; @endphp            
        @else
            <!-- @lang('user.matches.not_found',['name' => 'Matches']) -->
        @endif     
            <div role="tabpanel" class="tab-pane active" id="map-view">
                <div class="map-outer center-height">
                    <div class="" id="map" style="width: 100%; height: 100%;"></div>
                </div>
            </div>
            
        </div>
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<div class="col-md-3 p-0">
    @if(count($find_matchs)>0)
    <div class="right-sidebar body-height">
        <!-- Right Sidebar Content Starts -->        
        <div class="right-sidebar-content">
            <!-- Photos SEction Starts -->
            <div class="right-sec p-0">
                <div class="match-slide right-match-slide">
                @php $image_length=count($find_match_first->user_images);@endphp
                @php $im=0; @endphp
                @foreach($find_match_first->user_images as $user_image)
                    <div class="match-slide-box m-0">
                        <div class="about-prof-img bg-img sidebar_image{{$im}}" style=" background-image: url({{$user_image->image}})"></div>
                    </div>
                    @php $im++;@endphp
                @endforeach  
                @for ($i = $image_length; $i < 5; $i++)
                    <div class="match-slide-box m-0">
                        <div class="about-prof-img bg-img sidebar_image{{$i}}" style=" background-image: url({{asset('design/img/user.png')}})"></div>
                    </div>
                @endfor                      
                </div>
            </div>

            <div class="map-pop-img bg-img" style="background-image: url();"></div>
            <!-- Photo Section Ends -->
            <!-- About Section Starts -->
            <div class="right-sec about-sec">
                <h6 class="about-tit">@lang('user.user.about') <span id="sidebar_firstname">{{$find_match_first->first_name}}</span></h6>
                <p class="about-txt" id="sidebar_about">{{$find_match_first->about}}</p>
            </div>
            <!-- About Section Ends -->
            <!-- Video Section Starts -->
<!--             <div class="right-sec video-sec">
                <h6 class="about-tit">@lang('user.user.bio_video')</h6>
                <video id="sidebar_video" width="310" controls="true">
                    <source src="{{$find_match_first->bio_video}}" type="video/mp4">Your browser doesn't support HTML5 video tag.
                </video>
            </div> -->
            <!-- Video Section Ends -->
            <!-- Tags Section Starts -->
            <div class="right-sec tag-section" id="sidebar_interest">
                @if(count($find_match_first->user_interest)>0)
                <h6 class="about-tit">@lang('user.user.interests')</h6>                
                @foreach($find_match_first->user_interest as $user_interest)
                    <span class="tags">{{$user_interest->interest->name}}</span>
                @endforeach 
                @endif
            </div>
        </div>
        <!-- Tag Section Ends -->
        <div class="right-foot">
            <a href="#" class="right-foot-item" data-target="sidebar_video" data-toggle="modal">@lang('user.user.recommend')</a></a>
            <a href="#" class="right-foot-item" data-target="#report-modal" data-toggle="modal">@lang('user.user.report')</a>
            <input type="hidden" name="report_id" id="sidebar_report_id" value="{{$find_match_first->id}}">
            <input type="hidden" name="superlike_count" id="superlike_count" value="{{$superlike_reach}}">
            
        </div>
        @endif
        <!-- Right Sidebar Content Ends -->
    </div>
</div>

<!-- Report Modal Starts -->
<div id="report-modal" class="modal" data-easein="expandIn" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h5 class="modal-title">@lang('user.user.report_this_user')</h5>
            </div>
            <div class="modal-body p-0">
                <!-- Recommend List Starts -->
                <div class="recom-list">
                    <!-- Recommend Block Starts -->
                    <a href="javascript:void(0);" class="report-block active row" data-ajax="false" data-id="bad_message">
                        <div class="recom-block-left col-md-10 p-l-0">
                            <div class="recom-details m-0">
                                <h6><i class="zmdi zmdi-comment-outline recom-icon"></i> @lang('user.user.inappropriate_messages')</h6>
                            </div>
                        </div>
                        <div class="recom-block-right col-md-2 p-r-0 text-right">
                            <button class="recom-btn tick-btn"><i class="zmdi zmdi-check"></i></button>
                        </div>
                    </a>
                    <!-- Recommend Block Ends -->
                    <!-- Recommend Block Starts -->
                    <a href="javascript:void(0);" class="report-block row" data-ajax="false" data-id="bad_photo">
                        <div class="recom-block-left col-md-10 p-l-0">
                            <div class="recom-details m-0">
                                <h6><i class="zmdi zmdi-camera recom-icon"></i>@lang('user.user.inappropriate_photos') </h6>
                            </div>
                        </div>
                        <div class="recom-block-right col-md-2 p-r-0 text-right">
                            <button class="recom-btn tick-btn"><i class="zmdi zmdi-check"></i></button>
                        </div>
                    </a>
                    <!-- Recommend Block Ends -->
                    <!-- Recommend Block Starts -->
                    <a href="javascript:void(0);" class="report-block row" data-ajax="false" data-id="bad_behaviour">
                        <div class="recom-block-left col-md-10 p-l-0">
                            <div class="recom-details m-0">
                                <h6><i class="zmdi zmdi-cloud-off recom-icon"></i>@lang('user.user.bad_offline_behaviour') </h6>
                            </div>
                        </div>
                        <div class="recom-block-right col-md-2 p-r-0 text-right">
                            <button class="recom-btn tick-btn"><i class="zmdi zmdi-check"></i></button>
                        </div>
                    </a>
                    <!-- Recommend Block Ends -->
                    <!-- Recommend Block Starts -->
                    <a href="javascript:void(0);" class="report-block row" data-ajax="false" data-id="spam">
                        <div class="recom-block-left col-md-10 p-l-0">
                            <div class="recom-details m-0">
                                <h6><i class="zmdi zmdi-thumb-down recom-icon"></i>@lang('user.user.feel_like_spam') </h6>
                            </div>
                        </div>
                        <div class="recom-block-right col-md-2 p-r-0 text-right">
                            <button class="recom-btn tick-btn"><i class="zmdi zmdi-check"></i></button>
                        </div>
                    </a>
                    <!-- Recommend Block Ends -->                    
                </div>
                <!-- Recommend List Ends -->
            </div>
            <div class="modal-footer">
                <button class="white-btn two-btn" data-dismiss="modal">@lang('user.form.close')</button>
                <button onclick="user_report()" class="cmn-btn">@lang('user.form.submit')</button>
            </div>
        </div>
    </div>
</div>
<!-- Report Modal Ends -->

<!-- Recommend Modal Starts -->
<div id="recommend-modal" class="modal" data-easein="expandIn" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h5 class="modal-title">@lang('user.user.recommended_friend')</h5>
            </div>
            <div class="modal-body p-0">
                <h6 class="recom-tit">@lang('user.user.select_your_friend')</h6>
                <!-- Recommend List Starts -->
                <div class="recom-list">
                    @if(count($matches) > 0) 
                    <!-- Recommend Block Starts -->
                    @foreach($matches as $key=>$match_list)
		    @if($match_list->user)
                    <a href="javascript:void(0);" class="recom-block <?php if($key==0){echo 'active';}?> row" data-id="{{$match_list->user->id}}"> 
                        <div class="recom-block-left col-md-10 p-l-0">
                            @if($match_list->user->picture)
                                <div class="recom-img pull-left bg-img" style="background-image: url({{$match_list->user->picture}});"></div>
                            @else 
                                <div class="recom-img pull-left bg-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                            @endif
                            <div class="recom-details">
                                <h6>{{$match_list->user->first_name}} {{$match_list->user->last_name}}</h6>
                                <p>{{$match_list->user->description}}</p>
                            </div>
                        </div>
                        <div class="recom-block-right col-md-2 p-r-0 text-right">
                            <button class="recom-btn tick-btn"><i class="zmdi zmdi-check"></i></button>
                        </div>
                    </a>
		    @endif
                    @endforeach 
                    <!-- Recommend Block Ends -->
                    @else
                        <div class="notify-content">
                            <img src="{{asset('design/img/no-likes.png')}}">
                            <h6>@lang('user.user.no_friend_found')</h6>
                        </div>
                    @endif
                    
                </div>
                <!-- Recommend List Ends -->
            </div>
            <div class="modal-footer">
                <button class="white-btn two-btn" data-dismiss="modal">@lang('user.form.close')</button>
                <button onclick="send_recommend()" class="cmn-btn">@lang('user.user.send_request')</button>
            </div>
        </div>
    </div>
</div>
<!-- Recommend Modal Ends -->
@endsection

@section('scripts')
<!-- Map JS -->
<script src="https://maps.googleapis.com/maps/api/js?key={{Setting::get('map_key')}}&libraries=places"></script>
<script src="{{asset('design/js/jquery.googlemap.js')}}"></script>
<script type="text/javascript">

var map;
var infowindow;
initMap();

function initMap() {
  var find_matchs={!! $find_matchs !!}  

  var user_lat = {{ Auth::user()->latitude }};
  var user_lng = {{ Auth::user()->longitude }};

  var first_name = "{{ Auth::user()->first_name }}";
  var last_name = "{{ Auth::user()->last_name }}";

  if(find_matchs != null && find_matchs != "") {
    var myLatLng = {lat: find_matchs[0].latitude, lng: find_matchs[0].longitude};
  } else {
    var myLatLng = {lat: user_lat, lng: user_lng};
  }
  
  var map = new google.maps.Map(document.getElementById('map'), {

    zoom: 11,
    center: myLatLng,
    gestureHandling: 'none'
  });

    map.addListener("zoom_changed", function() {

        if(map.getZoom() < 13) {

             
        } else if(map.getZoom() > 13) {
            map.setOptions({zoom:11});
        } 
    });
   

  if(find_matchs == '' && user_lat != null && user_lng != null) {

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(user_lat, user_lng),
        map: map,
        title: first_name+' '+last_name,
    });

    var infowindow = new google.maps.InfoWindow({
          content: '<div class="map-content"><h6>'+first_name+' '+last_name+'</h6></div>'
    });

    marker.addListener('click', function() {

        infowindow.open(map, marker);          
    });
  }

  find_matchs.forEach(function(find_match) {
    //console.log(find_match.user_locations.length);
    if(find_match.picture){
        var pic=find_match.picture;
    }else{
        var pic='{{asset("design/img/user.png")}}';
    } 
    var pre_lat = find_match.latitude;
    var pre_long = find_match.longitude; 
    //console.log(pre_long);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(pre_lat, pre_long),
        map: map,
        title: find_match.first_name+' '+find_match.last_name,
        icon: {
            url: pic,
            scaledSize: new google.maps.Size(30, 30),
        }
    
    });

    var infowindow = new google.maps.InfoWindow({
          content: '<div class="map-content"><h6>'+find_match.first_name+' '+find_match.last_name+'</h6><div class="text-center"><a href="{{url("single-user")}}/'+find_match.id+'">@lang('user.user.view_user')</a></div></div>'
        });


    marker.addListener('click', function() {
            infowindow.open(map, marker);
            map_user_detail(find_match.id);          
        });
  
  
    /*if(find_match.user_locations.length != 0){
        for(var i=0; i<find_match.user_locations.length; i++){
            console.log(i);
            console.log(find_match.user_locations[i].latitude);
           var pre_lat = find_match.user_locations[i].latitude;
           var pre_long = find_match.user_locations[i].longitude; 
    console.log(pre_long);
    var marker = new google.maps.Marker({
    position: new google.maps.LatLng(pre_lat, pre_long),
    map: map,
    title: find_match.first_name+' '+find_match.last_name,
    icon: {
        url: pic,
        scaledSize: new google.maps.Size(30, 30),
        }
    
      });

    var infowindow = new google.maps.InfoWindow({
          content: '<div class="map-content"><h6>'+find_match.first_name+' '+find_match.last_name+'</h6><div class="text-center"><a href="{{url("single-user")}}/'+find_match.id+'">@lang('user.user.view_user')</a></div></div>'
        });


    marker.addListener('click', function() {
            infowindow.open(map, marker);
            map_user_detail(find_match.id);          
        });
      }
    }*/
  });

}
function map_user_detail(user){
    console.log(user);  
    var url    = "{{url('single_user')}}";
    var token  = "{{csrf_token()}}";

    $.ajax({
        type: "POST",
        url : url,
        data: {'user_id' :user,'_token' :token},
        success: function(html)
        {
            //console.log(html);
            //alert(html.user_like.bio_video);
            $('#sidebar_firstname').html(html.first_name);
            $('#sidebar_about').html(html.about);
            $('#sidebar_video source').attr('src',html.bio_video);
            $('#sidebar_video')[0].load();
            $('#sidebar_report_id').val(html.id);   
            var image_length=html.user_images.length;
            $.each(html.user_images, function(key,val) {             
                //alert(key+val.image);  
                $('.sidebar_image'+key).css('background-image', 'url(' + val.image + ')');       
            }); 
            for(var i=image_length; i<5; i++){
                $('.sidebar_image'+i).css('background-image', 'url(' +"{{asset('design/img/user.png')}} " +')');
            }  
            if(html.user_interest.length){
                $('#sidebar_interest').html('<h6 class="about-tit">@lang("user.user.interests")</h6>');
                $.each(html.user_interest, function(key,val) {                        
                    $('#sidebar_interest').append('<span class="tags">' + val.interest.name + '</span>');    
                });
            }else{
                $('#sidebar_interest').html('');
            }          
                      
        }
    });

}
function user_report(){
        var url    = "{{url('user_report')}}";
        var token  = "{{csrf_token()}}";
        var report_id    = $('#sidebar_report_id').val();
        var reason=$('a.report-block.active').data('id');
        $.ajax({
            type: "POST",
            url : url,
            data: {'report_id' :report_id,'reason' :reason,'_token' :token},
            success: function(html)
            {
                toastr.success(html.message);
                toastr.options.positionClass = "toast-top-right";
                
                $('#report-modal').modal('hide');
            }
        });   
}
function send_recommend(){
    var url    = "{{url('user_recommend')}}";
    var token  = "{{csrf_token()}}";
    var recommend_id    = $('#sidebar_report_id').val();
    //var friend_id    = $('a.recom-block.active').data('id');
    var friend_ids=[];
    $('a.recom-block.active').each(function(){
        friend_ids.push($(this).data('id'));
    });
    if(friend_ids.length>0){    
        $.ajax({
            type: "POST",
            url : url,
            data: {'recommend_id' :recommend_id,'friend_ids' :friend_ids,'_token' :token},
            success: function(html)
            {
                toastr.success(html.message);
                toastr.options.positionClass = "toast-top-right";
                $('sidebar_video').modal('hide');
            }
        });
    }else{
        alert('@lang("user.alert.select_friend")');
        return false;
    }
}


/*$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
    //google.maps.event.trigger(map, "resize");
    initMap();
});*/



function like_dislike_superlike(status,like_id){
    if(like_id){
        var like_id=like_id;
        var next_id=like_id;
    }else{
        var like_id=$(".buddy.active").attr('data-id');  

        if ($('.buddy.active').is(':last-child')) {
        var next_id=$(".buddy:nth-child(1)").attr('data-id');
        } else {
            var next_id=$(".buddy.active").next().attr('data-id');
        }
    }
      
    var url    = "{{url('likes')}}";
    var token  = "{{csrf_token()}}";   

    $.ajax({
        type: "POST",
        url : url,
        data: {'like_id':like_id,'next_id':next_id,'status' :status,'_token' :token},
        success: function(html)
        {
            console.log(html);
            $('#superlike_count').val(html.superlike_count);
            $('#sidebar_firstname').html(html.next_user.first_name);
            $('#sidebar_about').html(html.next_user.about);
            $('#sidebar_video source').attr('src',html.next_user.bio_video);
            $('#sidebar_video')[0].load();
            $('#sidebar_report_id').val(html.next_user.id);            
            var image_length=html.next_user.user_images.length;
            $.each(html.next_user.user_images, function(key,val) {             
                //alert(key+val.image);  
                $('.sidebar_image'+key).css('background-image', 'url(' + val.image + ')');       
            }); 
            for(var i=image_length; i<5; i++){
                $('.sidebar_image'+i).css('background-image', 'url(' +"{{asset('design/img/user.png')}} " +')');
            }  
            toastr.success(html.message);
            toastr.options.positionClass = "toast-top-right";    
            toastr.options.fadeOut = 2500;      
            
            
        }
    });
}
</script>
@endsection
