@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
            <div class="col-md-6 p-0">
                <div class="center-box body-height">
                    <!-- Center Box Head Starts -->
                    <div class="center-box-head p-f-20 row m-0">
                        <h5 class="m-0">@lang('user.matches.match_title')</h5>
                    </div>
                    <!-- Center Box Head Ends -->
                    <!-- Center Box Content Starts -->
                    @if(count($match_lists) > 0) 
                    <div class="center-box-content p-0">
                        <div class="match-list">
                            <!-- Match List Block Starts -->
                            @foreach($match_lists as $match_list)
                            <div class="match-list-block row m-0">
                                <!-- Match List Block Left Starts -->
                                <div class="match-list-block-left col-md-6 p-l-0">
                                    <a href="{{url('single-user',array('id'=>$match_list->user->id))}}" class="match-list-block-profile">                            
                                        @if($match_list->user->picture)
                                            <div class="match-list-img bg-img" style="background-image: url({{$match_list->user->picture}});"></div>
                                        @else 
                                            <div class="match-list-img bg-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                                        @endif
                                        <div class="match-list-details">
                                            <h6 class="match-list-tit">{{$match_list->user->first_name}} {{$match_list->user->last_name}}</h6>
                                            <p class="match-list-txt">{{$match_list->user_distance}} @lang('user.matches.away')</p>
                                        </div>
                                    </a>
                                </div>
                                <!-- Match List Block Left Ends -->
                                <!-- Match List Block Right Starts -->
                               
                              @if(!empty($match_list->subscription))
                                <div class="match-list-block-right col-md-6 text-right p-r-0">
                                    <a href="{{url('/chat_user',array('id' => $match_list->user->id ))}}" class="cmn-btn chat-btn">@lang('user.matches.chat')</a>
                                    <!-- <a href="#" class="cmn-btn chat-btn">Video Call</a> -->
                                </div>

                              @else
                                
                                <div class="match-list-block-right col-md-6 text-right p-r-0">
                                    <a href="{{url('payments')}}?node=superlike&match_id={{$match_list->user->id}}" class="cmn-btn chat-btn">Subscribe To Chat</a>
                                    
                                </div>
                              @endif  
                                <!-- MAtch List Block Right Ends -->
                            </div>
                            @endforeach 
                            <!-- Match List Block Ends -->
                        </div>
                    </div>
                    @else
                        <div class="center-box-content notify-block">
                            <div class="notify-block-inner">
                                <div class="notify-content">
                                    <img src="{{asset('design/img/nolikes.png')}}">
                                    <h6>@lang('user.matches.not_found',['name' => 'Matches'])</h6>
                                </div>
                            </div>
                        </div> 
                    @endif
                    <!-- Center Box Content Ends -->
                    <!-- Center Box Footer Starts -->
                    <div class="center-box-foot">
                    </div>
                    <!-- Center Box Footer Ends -->
                </div>
            </div>
            <!-- Center Box Ends -->
            <div class="col-md-3 p-0">
                <div class="right-sidebar body-height">
                    <!-- Right Sidebar Content Starts -->
                    <div class="right-sidebar-content banner text-center">
                        <img src="{{asset('design/img/banner.png')}}" class="banner-img">
                        <h6>@lang('user.matches.invite',['sitename' => Setting::get('sitename')])</h6>
                        <p>@lang('user.matches.invite_quote',['refer_money'=>Setting::get('currency').Setting::get('referal_amount')])</p>
                        <a href="{{url('invites')}}" class="cmn-btn m-t-15">@lang('user.matches.invite_friends')</a>
                    </div>
                    <!-- Right Sidebar Content Ends -->
                </div>
            </div>
@endsection
