
<!-- <link rel="stylesheet" type="text/css" href="{{asset('design/css/toastr.min.css')}}"> -->

<script src="{{asset('design/js/toastr.min.js')}}"></script>


@if(Session::has('flash_errors'))

    @if(is_array(Session::get('flash_errors')))

        <!-- <div class="alert alert-danger" style="margin: 0;"> -->

           <!--  <button type="button" class="close" data-dismiss="alert">×</button> -->

            <ul>
                @foreach(Session::get('flash_errors') as $errors)

                    @if(is_array($errors))

                        @foreach($errors as $error)

                            <li>
                                <script type="text/javascript">

                                    toastr.options.positionClass = "toast-top-right";

                                    toastr.error("<?php echo $error; ?>");

                                </script>
                            </li>
                        @endforeach

                    @else

                        <li> 
                            <script type="text/javascript">

                                toastr.options.positionClass = "toast-top-right";

                                toastr.error("<?php echo $errors; ?>");

                            </script> 
                        </li>
                    @endif

                @endforeach

            </ul>

        <!-- </div> -->

    @else
        <script type="text/javascript">

                toastr.options.positionClass = "toast-top-right";

                toastr.error("<?php echo Session::get('flash_errors'); ?>");

        </script>

    @endif

@endif

@if(Session::has('flash_error'))

    <script type="text/javascript">


        toastr.options.positionClass = "toast-top-right";

        toastr.error("<?php echo Session::get('flash_error'); ?>");

    </script>

@endif


@if(Session::has('flash_success'))

    <script type="text/javascript">

        //Toast({text: "<?php echo Session::get('flash_success'); ?>"}).show();

        toastr.options.positionClass = "toast-top-right";

        toastr.success("<?php echo Session::get('flash_success'); ?>");

    </script>

@endif
