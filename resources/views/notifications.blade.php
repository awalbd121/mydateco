@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height">
        <!-- Center Box Head Starts -->
        <div class="center-box-head m-0-head p-f-20 row m-0">
            <h5 class="m-0">@lang('user.menu.notifications')</h5>
        </div>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        @if(count($notifications) > 0)
            <div class="center-box-content p-0">
                <div class="match-list">
                    <!-- Match List Block Starts -->
                    @foreach($notifications as $notification)
                    <div class="match-list-block row m-0">
                        <!-- Match List Block Left Starts -->
                        <div class="match-list-block-left col-md-8 p-l-0">
                            <a href="{{url('single-user',array('id'=>$notification->user->id))}}" class="match-list-block-profile">
                                @if($notification->user->picture)
                                    <div class="match-list-img bg-img" style="background-image: url({{$notification->user->picture}});"></div>
                                @else 
                                    <div class="match-list-img bg-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                                @endif
                                @if(@$notification->status=='recommend')
                                <div class="match-list-details">
                                    <h6 class="match-list-tit">@lang('user.matches.you_get')
                                    <span class="theme-color"> {{@$notification->recommend->user->first_name}} {{@$notification->recommend->user->last_name}}</span>
                                     @lang('user.matches.recommend_from') <span class="theme-color"> {{$notification->user->first_name}} {{@$notification->user->last_name}}</span></h6>
                                     <p class="match-list-txt">{{ \Carbon\Carbon::parse(@$notification->created_at)->diffForHumans() }}</p>
                                </div>
                                @elseif($notification->status=='wallet')
                                <div class="match-list-details">
                                    <h6 class="match-list-tit"><span class="theme-color"> {{@$notification->user->first_name}} {{@$notification->user->last_name}}</span> @lang('user.matches.sent_amount_you').</h6>
                                    <p class="match-list-txt">{{ \Carbon\Carbon::parse(@$notification->created_at)->diffForHumans() }}</p>
                                </div>
                                @elseif($notification->status=='chat')
                                <div class="match-list-details">
                                    <h6 class="match-list-tit"><span class="theme-color"> {{@$notification->user->first_name}} {{@$notification->user->last_name}}</span> @lang('user.matches.sent_message_you').</h6>
                                    <p class="match-list-txt">{{ \Carbon\Carbon::parse(@$notification->created_at)->diffForHumans() }}</p>
                                </div>
                                @elseif($notification->status=='friend')
                                <div class="match-list-details">
                                    <h6 class="match-list-tit"><span class="theme-color"> {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} got a match with {{@$notification->user->first_name}} {{@$notification->user->last_name}}</span> </h6>
                                </div>
                                @else
                                <div class="match-list-details">
                                    <h6 class="match-list-tit"><span class="theme-color"> {{@$notification->user->first_name}} {{@$notification->user->last_name}}</span> Dropped a @if($notification->status == 'like')Thumps Up @else Double Hearts @endif @lang('user.matches.your_profile').</h6>
                                    <p class="match-list-txt">{{ \Carbon\Carbon::parse(@$notification->created_at)->diffForHumans() }}</p>
                                </div>
                                @endif
                            </a>
                        </div>
                        <!-- Match List Block Left Ends -->
                        <!-- Match List Block Right Starts -->
                        <div class="match-list-block-right col-md-4 text-right p-r-0">
                            @if($notification->status=='like')
                                <a href="{{url('who_likes_me')}}" class="cmn-btn chat-btn"><!-- <i class="zmdi zmdi-thumb-up"></i> --></a>
                            @elseif($notification->status=='chat')
                                <a href="{{url('/chat_user',array('id' => $notification->user_id ))}}" class="cmn-btn chat-btn"><!-- <i class="zmdi zmdi-comment-outline"></i> --></a>
                            @elseif($notification->status=='superlike')
                                <a href="{{url('/chat_user',array('id' => $notification->user_id ))}}" class="cmn-btn chat-btn"><!-- <i class="zmdi zmdi-star-outline"></i> --></a>
                            @elseif($notification->status=='wallet')
                                <a href="{{url('/transaction_history')}}" class="cmn-btn chat-btn"><!-- <i class="zmdi zmdi-balance-wallet"></i> --></a>
                            @elseif($notification->status=='recommend')
                                <a href="{{url('/dashboard')}}" class="cmn-btn chat-btn"><!-- <i class="zmdi zmdi-home"></i> --></a>
                            @else
                            
                            @endif                                
                            
                        </div>
                        <!-- Match List Block Right Ends -->
                    </div>
                    @endforeach
                    <!-- Match List Block Ends -->
                </div>
            </div>
        @else
            <div class="center-box-content notify-block">
                <div class="notify-block-inner">
                    <div class="notify-content">
                        <img src="{{asset('design/img/new-notify.png')}}">
                        <h6>@lang('user.matches.not_found',['name' => 'Notifications'])</h6>
                    </div>
                </div>
            </div> 
        @endif
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<div class="col-md-3 p-0">
    <div class="right-sidebar body-height">
        <!-- Right Sidebar Content Starts -->
        <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">
            <h6>@lang('user.matches.invite',['sitename' => Setting::get('sitename')])</h6>
            <p>@lang('user.matches.invite_quote',['refer_money'=>Setting::get('currency').Setting::get('referal_amount')])</p>
            <a href="{{url('invites')}}" class="cmn-btn m-t-15">@lang('user.matches.invite_friends')</a>
        </div>
        <!-- Right Sidebar Content Ends -->
    </div>
</div>
@endsection
