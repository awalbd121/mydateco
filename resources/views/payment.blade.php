@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height">
        <!-- Center Box Head Starts -->
        <ul class="center-box-head row m-0 nav nav-tabs tab-head-top">
            <li role="presentation" class="active col-xs-4 p-0">
                <a href="#add-money" class="center-box-head-block" aria-controls="add-money" role="tab" data-toggle="tab">@if($request->node == "refresh")Backward @else Subscribe @endif</a>
            </li>
            <!-- <li role="presentation" class="col-xs-4 p-0">
                <a href="#share-money" class="center-box-head-block" aria-controls="share-money" role="tab" data-toggle="tab">@lang('user.payment.share_amount')</a>
            </li>
            <li role="presentation" class="col-xs-4 p-0">
                <a href="#saved-card" class="center-box-head-block" aria-controls="share-money" role="tab" data-toggle="tab">@lang('user.payment.saved_card')</a>
            </li> -->
        </ul>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        <div class="center-box-content tab-content pay-content">
            <div role="tabpanel" class=" tab-pane active money-tab" id="add-money">
                <!-- Add Money Starts -->
                <div class="add-money">
                    <!-- Center Box Head Starts -->
                    <div class="center-box-head row m-0">
                        <h5 class="m-0 pull-left">@if($request->node == "refresh")Backward @else Subscribe @endif</h5>
                        <a href="#add-card-modal" class="cmn-btn pull-right" data-toggle="modal" data-target="#add-card-modal">@lang('user.payment.add_new_card')</a>
                    </div>
                    <!-- Center Box Head Ends -->
                    <div class="add-money-top text-center">
                        <img src="{{asset('design/img/add-money.png')}}">
                        <<!-- h6>@lang('user.payment.available_balance') {{currency($user->wallet_balance)}}</h6> -->
                    </div>
                    <div class="add-money-btm">
                        {!! Form::open(['url' => 'add_money']) !!}
                            <div class="form-group">
                                <select class="form-control" name="card_id" required="">
                                  @if(count($cards)>0)  
                                    <option value="">@lang('user.payment.select_card')</option>  
                                  @foreach($cards as $card)
                                    <option @if($card->is_default == 1) selected @endif value="{{$card->card_id}}">{{$card->brand}} **** **** **** {{$card->last_four}}</option>
                                  @endforeach
                                  @else
                                    <option value="">@lang('user.payment.add_new_card')</option> 
                                  @endif
                                </select>
                            </div>
                            <div class="form-group">
                               @if($request->node == 'refresh')
                                 {{Form::label('amount', 'Pay a small fee to reset your matches 15$')}}
                               @else
                                 {{Form::label('amount', 'Choose Your Premium Amount')}}
                               @endif
                                {{Form::hidden('amount','15',['class' => 'form-control','id' => 'wallet_amount','placeholder' => 'Enter Amount','required','min' => '1'])}} 
                                {{Form::hidden('match_id',24)}} 
                                @if($request->node == 'superlike')
                                
                                <div class="tags-money m-t-15">
                                    <a href="javascript:void(0)" onclick="select_money('wallet_amount','{{ Setting::get('monthly') }}')"><span class="tags">Monthly {{currency(Setting::get('monthly'))}}</span></a>
                                    <a href="javascript:void(0)" onclick="select_money('wallet_amount','{{ Setting::get('annual') }}')"><span class="tags">Annual {{currency(Setting::get('annual'))}}</span></a>
                                    
                                </div>
                                @endif
                            </div>

                            {{Form::hidden('status','ADDED')}}
                            {{Form::submit('Pay', ['class' => 'cmn-btn'])}}
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- Add Money Ends -->
            </div>
            <div role="tabpanel" class="tab-pane money-tab" id="share-money">
                <!-- Share Money Starts -->
                <div class="share-money">
                    <!-- Center Box Head Starts -->
                    <div class="center-box-head row m-0">
                        <h5 class="m-0 pull-left">@lang('user.payment.share_amount')</h5>
                        <a href="{{ url('/transaction_history' )}}" class="cmn-btn pull-right">@lang('user.payment.view_history')</a>
                    </div>
                    <!-- Center Box Head Ends -->
                    <div class="add-money-top text-center">
                        <img src="{{asset('design/img/share-money.png')}}">
                        <h6>@lang('user.payment.available_balance') {{currency($user->wallet_balance)}}</h6>
                    </div>
                    <div class="add-money-btm">
                        {!! Form::open(['url' => 'payment_process']) !!}
                            <div class="form-group">
                                {{Form::label('receiver_id', 'Select Name')}}
                                <select class="form-control" name="receiver_id" required="">
                                    <option value="">Select</option>
                                    @foreach($matches as $match)
                                        <option value="{{$match->user->id}}">{{$match->user->first_name}} {{$match->user->last_name}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            <div class="form-group">
                                {{Form::label('amount', 'Enter Amount')}}
                                {{Form::number('amount','', ['class' => 'form-control','id' => 'share_amount','placeholder' => 'Enter Amount','required','max'=>$user->wallet_balance,'min' => '1'])}} 
                                <div class="tags-money m-t-15">
                                    <a href="javascript:void(0)" onclick="select_money('share_amount',100)"><span class="tags">{{currency(100)}}</span></a>
                                    <a href="javascript:void(0)" onclick="select_money('share_amount',200)"><span class="tags">{{currency(200)}}</span></a>
                                    <a href="javascript:void(0)" onclick="select_money('share_amount',500)"><span class="tags">{{currency(500)}}</span></a>
                                    <a href="javascript:void(0)" onclick="select_money('share_amount',1000)"><span class="tags">{{currency(1000)}}</span></a>
                                </div>
                            </div>
                            {{Form::hidden('status','PAID')}}
                            {{Form::submit('Share', ['class' => 'cmn-btn'])}}
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- Share Money Ends -->
            </div>
            <div role="tabpanel" class="tab-pane money-tab" id="saved-card">
                <!-- Add Money Starts -->
                <div class="add-money">
                    <!-- Center Box Head Starts -->
                    <div class="center-box-head row m-0">
                        <h5 class="m-0 pull-left">@lang('user.payment.saved_card')</h5>
                        <a href="#add-card-modal" class="cmn-btn pull-right" data-toggle="modal" data-target="#add-card-modal">@lang('user.payment.add_new_card')</a>
                    </div>
                    <!-- Center Box Head Ends -->
                    @if(count($cards)>0)
                    <div class="add-money-btm">
                        <!-- Card Section Inner Starts -->
                        <div class="card-sec-inner row">
                        <img src="{{asset('design/img/loader.gif')}}" id="loader" style="display: none;" />
                            <!-- Card Block Starts -->                            
                             @foreach($cards as $card)
                                <div class="col-md-4 col-sm-6 col-xs-12" id="{{$card->card_id}}">
                                    <div class="card-block-outer">
                                        <input type="radio" name="fare" id="card-1" @if($card->is_default == 1) checked @endif >
                                        <label for="card-1">
                                            <div class="card-block">
                                                <p class="card-type">{{$card->brand}}</p>
                                                <p class="card-num card-txt">**** **** **** {{$card->last_four}}</p>
                                                <!-- <p class="valid card-txt">mm/yy</p> -->
                                                <div class="sel-icon1" onclick="delete_card('{{$card->card_id}}')">
                                                    <i class="fa fa-trash"></i>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            @endforeach                            
                        </div>                            
                            <!-- Card Block Ends -->
                        <!-- Card Section Inner Ends -->
                        <!-- <button class="cmn-btn">Submit</button> -->
                    </div>
                    @else
                       @lang('user.matches.not_found',['name' => 'Card']).
                    @endif
                </div>
                            <!-- Add Money Ends -->
            </div>
        </div>
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<div class="col-md-3 p-0">
    <div class="right-sidebar body-height">
        <!-- Right Sidebar Content Starts -->
        <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">
            <h6>@lang('user.matches.invite',['sitename' => Setting::get('sitename')])</h6>
            <p>@lang('user.matches.invite_quote',['refer_money'=>Setting::get('currency').Setting::get('referal_amount')])</p>
            <a href="{{url('invites')}}" class="cmn-btn m-t-15">@lang('user.matches.invite_friends')</a>
        </div>
        <!-- Right Sidebar Content Ends -->
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="add-card-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">@lang('user.payment.add_new_card')</h4>
            </div>
        {!! Form::open(['route' => 'card.store','id'=> 'payment-form']) !!}
        <div class="modal-body">
            <div class="add-money-btm" id="card-payment">                
                    <div class="form-group">
                        {{Form::label('name', 'Enter Name')}}
                         <input data-stripe="name" autocomplete="off" required type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        {{Form::label('card_number', 'Card Number')}}
                        <input data-stripe="number" type="text" id="card_number" required autocomplete="off" class="form-control" onkeypress="return isNumberKey(event)" maxlength="16" />
                    </div>
                    <div class="form-group">
                        {{Form::label('month', 'Enter Month')}}
                        <input type="text" required autocomplete="off" class="form-control" data-stripe="exp-month" placeholder="MM" onkeypress="return isNumberKey(event)" maxlength="2">
                    </div>
                    <div class="form-group">
                        {{Form::label('year', 'Enter Year')}}
                        <input type="text" data-stripe="exp-year" required autocomplete="off" class="form-control" placeholder="YY" onkeypress="return isNumberKey(event)" maxlength="2">
                    </div>
                    <div class="form-group">
                        {{Form::label('cvv', 'Enter CVV')}}
                        <input type="text" data-stripe="cvc" required autocomplete="off" class="form-control" placeholder="CVV" onkeypress="return isNumberKey(event)" maxlength="3">
                    </div>                
            </div>
        </div>
        <div class="modal-footer"> 
            <button type="button" class="cmn-btn two-btn" data-dismiss="modal">@lang('user.form.close')</button>
            <button type="submit" class="cmn-btn">@lang('user.form.save')</button>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        Stripe.setPublishableKey('{{Setting::get('stripe_public_key')}}');

         var stripeResponseHandler = function (status, response) {
            var $form = $('#payment-form');

            console.log(response);

            if (response.error) {
                // Show the errors on the form
                $form.find('.payment-errors').text(response.error.message);
                $form.find('button').prop('disabled', false);
                alert(response.error.message);
                $('#loader_id').hide();

            } else {
                // token contains id, last4, and card type
                var token = response.id;
                // Insert the token into the form so it gets submitted to the server
                $form.append($('<input type="hidden" id="stripeToken" name="stripe_token" />').val(token));
                jQuery($form.get(0)).submit();
            }
        };
                
        $('#payment-form').submit(function (e) {
            $('#loader_id').show();
            
            if ($('#stripeToken').length == 0)
            {
                console.log('ok');
                var $form = $(this);
                $form.find('button').prop('disabled', true);
                console.log($form);
                Stripe.card.createToken($form, stripeResponseHandler);
                return false;
            }
        });

    </script>
    <script type="text/javascript">        
        function isNumberKey(evt)
        {
           var charCode = (evt.which) ? evt.which : event.keyCode;
           if (charCode != 46 && charCode > 31
           && (charCode < 48 || charCode > 57))
               return false;            
           return true;
        }
        function delete_card(card_id){
            var con=confirm("@lang('user.alert.delete_card_message')?");            
            if(con==true){
                $('#loader_id').show();
                var url    = "{{url('card/destroy')}}";
                var token  = "{{csrf_token()}}";
                $.ajax({
                    type: "POST",
                    url : url,
                    data: {'card_id' :card_id,'_method' :'DELETE','_token' :token},
                    success: function(html)
                    {
                        $('#loader_id').hide();
                        $('#'+card_id).remove();
                        toastr.success(html.message);
                        toastr.options.positionClass = "toast-top-right";
                    }
                });
            }
               
        }
        function select_money(id,amount){
           // alert(id)
            $('#'+id).val(amount);    
        }
    </script>
@endsection
