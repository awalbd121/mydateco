@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height p-f-15">
        <!-- Center Box Head Starts -->
        <div class="center-box-head row m-0">
            <h5 class="m-0">Get Subscription</h5>
        </div>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        <div class="center-box-content">
            <!-- Premium Slider Starts -->
            <div class="premium-slider">
                <!-- Premium Slider Box Starts -->
                <div class="premium-slide-box text-center">
                    <h6 class="pre-tit">@lang('user.payment.swipe_around')!</h6>
                    <p class="pre-txt">@lang('user.payment.passport_anyware',['sitename'=>Setting::get('sitename')])!.</p>
                </div>
                <!-- Premium Slider Box Ends -->
                <!-- Premium Slider Box Starts -->
                <div class="premium-slide-box text-center">
                    <h6 class="pre-tit">@lang('user.payment.stand_superlike')</h6>
                    <p class="pre-txt">@lang('user.payment.superlike_3daily')!.</p>
                </div>
                <!-- Premium Slider Box Ends -->
                <!-- Premium Slider Box Starts -->
                <div class="premium-slide-box text-center">
                    <h6 class="pre-tit">Get Subscription</h6>
                    <p class="pre-txt">@lang('user.payment.get_unlimited')!.</p>
                </div>
                <!-- Premium Slider Box Ends -->
            </div>
            <!-- Premium Slider Ends -->
            <!-- Premium Options Starts -->
            <div class="premium-option row"> 
            @php 
            if(count($user_premium)>0)
                $user_premium_id=$user_premium->premia_id;
            else
                $user_premium_id=1;
            @endphp              
                <!-- Option Box Starts -->
                @foreach($premiums as $key=>$premium)
                <div class="col-md-4">                                
                    <div class="option-box text-center">
                        <input type="radio" name="premium_option" data-id="{{$premium->id}}" value="{{$premium->price}}" id="option-{{$key+1}}" @if($premium->id==$user_premium_id)checked @endif>
                        <label for="option-{{$key+1}}">
                            <div class="option-box-inner">
                                <div class="option-tit">{{$premium->period}}</div>
                                <p class="option-txt">{{$premium->duration}}</p>
                                <span class="discount-tag">@if($premium->price==0)Free @else {{currency($premium->price)}}@endif</span>
                            </div>
                        </label>
                    </div>
                </div>
                @endforeach
                <!-- Option Box Ends -->
            </div>
            <!-- Premium Options Ends -->
            <div class="premium-btm text-center">
                <a href="javascript:void(0);" onclick="get_premium()" class="cmn-btn  prmbtn">Get Subscription</a>
            </div>
        </div>
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<div class="col-md-3 p-0">
    <div class="right-sidebar body-height">
        <!-- Right Sidebar Content Starts -->
        <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">
            <h6>@lang('user.matches.invite',['sitename' => Setting::get('sitename')])</h6>
            <p>@lang('user.matches.invite_quote',['refer_money'=>Setting::get('currency').Setting::get('referal_amount')])</p>
            <a href="{{url('invites')}}" class="cmn-btn m-t-15">@lang('user.matches.invite_friends')</a>
        </div>
        <!-- Right Sidebar Content Ends -->
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    function get_premium(){
        
        var premium=$('input[name=premium_option]:checked');
        var premium_amount=premium.val();
        var premium_id=premium.data('id');
        var wallet={{Auth::user()->wallet_balance}};
        if(premium_amount>0){
            if(premium_amount<wallet){
                $("#loader_id").show();;
                var url    = "{{url('user_premium')}}";
                var token  = "{{csrf_token()}}";   

                $.ajax({
                    type: "POST",
                    url : url,
                    data: {'premium_id':premium_id,'premium_amount':premium_amount,'_token' :token},
                    success: function(html)
                    {
                        console.log(html);
                        if(html.message=='SUCCESS'){
                           toastr.success('@lang("user.alert.premium_account")'); 
                       }else{
                            toastr.success(html.message);
                       }
                        toastr.options.positionClass = "toast-top-right";  
                        $("#loader_id").hide();;   
                    }
                });
            }else{
                alert('@lang("user.alert.boost_wallet")');
            }
        }else{
            alert('@lang("user.alert.already_trial")');
        }
    }
</script>
@endsection
