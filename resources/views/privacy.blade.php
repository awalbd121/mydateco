@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height p-f-15">
        <!-- Center Box Head Starts -->
        <div class="center-box-head row m-0">
            <h5 class="m-0">@lang('user.menu.privacy_policy')</h5>
        </div>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        <div class="center-box-content">
            <div class="cmn-content privacy-content">
            {!!Setting::get('privacy_content')!!}
            </div>
        </div>
        <!-- Center Box Content Ends -->
    </div>
    </div>
<!-- Center Box Ends -->
<div class="col-md-3 p-0">
    <div class="right-sidebar body-height">
        <!-- Right Sidebar Content Starts -->
        <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">

            <h6>@lang('user.matches.invite',['sitename' => Setting::get('sitename')])</h6>
             @if(\Auth::user())
            <p>@lang('user.matches.invite_quote')</p>
             
            <a href="{{url('invites')}}" class="cmn-btn m-t-15">@lang('user.matches.invite_friends')</a>
            @endif
        </div>
        <!-- Right Sidebar Content Ends -->
    </div>
</div>
@endsection
