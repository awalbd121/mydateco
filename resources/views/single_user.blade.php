@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height">
        <!-- Center Box Head Starts -->
        <ul class="center-box-head row m-0 nav nav-tabs">
            <!-- <li role="presentation" class="active col-xs-12 p-0">
                <a href="#card" class="center-box-head-block" aria-controls="card" role="tab" data-toggle="tab">Card View</a>   
            </li> -->
            <!-- <li role="presentation" class="col-xs-6 p-0">
                <a href="#map-view" class="center-box-head-block" aria-controls="map-view" role="tab" data-toggle="tab">Map View</a>
            </li> -->
        </ul>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        <div class="center-box-content tab-content">        
            <div role="tabpanel" class=" tab-pane active p-30" id="card">
            @if(count($find_matchs)>0)
                <div id="matches-section" class="">
                    <!-- Match Block Starts -->                
                    @php $i=0;$find_match_first=''; @endphp
                    @foreach($find_matchs as $find_match)
                    @php
                        if($i==0){
                            $find_match_first=$find_match;
                        } 

                    @endphp
                    <div class="buddy @if($i==0) {{'active'}} @endif" data-id="{{$find_match->id}}" @if($i==0) {{'style=display:block'}} @endif>                        
                        <div class="match-img-sec">
                            @if($find_match->picture)
                                <div class="avatar bg-img" style=" background-image: url({{$find_match->picture }})"></div>
                            @else 
                                 <div class="avatar bg-img" style=" background-image: url({{asset('design/img/user.png')}})"></div>
                            @endif

                            <div class="match-name">
                                <h6>{{$find_match->first_name }} {{$find_match->last_name }}@if($find_match->show_age==0 && $find_match->age!=0) , {{$find_match->age }} @endif </h6>
                                <!-- @if($find_match->show_age==0 && $find_match->age!=0)
                                    <p>Age {{$find_match->age }}</p>
                                @endif -->
                                <p>Country : {{ $find_match['user_country']->name }}</p>
                                <p>User Preferred Distance :  {{round($find_match['user_preferences']->distance) }} KM </p>
                            </div>
                        </div>
                    </div>
                    @php $i++; @endphp
                    
                
                    <!-- Match Block Ends -->
                </div>
                <!-- <div class="match-foot text-center">                
                    <a href="#" onclick="skip()" class="match-btn refresh" title="Skip"><i class="zmdi zmdi-refresh"></i></a>
                    <a href="#" onclick="dislike()" class="match-btn big-match-btn close-btn" title="Dislike"><i class="zmdi zmdi-close"></i></a>
                    <a href="#" onclick="like()" class="match-btn big-match-btn like-btn" title="Like"><i class="zmdi zmdi-favorite"></i></a>
                    
                    <a href="#" onclick="superlike()" class="match-btn super-like" title="Superlike"><i class="zmdi zmdi-star"></i></i></a>
                </div> -->
                @endforeach
            @else
                @lang('user.matches.no_user_found')
            @endif
            </div>        
            <!-- <div role="tabpanel" class="tab-pane" id="map-view">
                <div class="map-outer center-height">
                    <div class="" id="map" style="width: 100%; height: 100%;"></div>
                </div>
            </div> -->
            
        </div>
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<div class="col-md-3 p-0">
    @if(count($find_matchs)>0)
    <div class="right-sidebar body-height">
        <!-- Right Sidebar Content Starts -->        
        <div class="right-sidebar-content">
            <!-- Photos SEction Starts -->
            <div class="right-sec p-0">
                <div class="match-slide right-match-slide">
                @php $image_length=count($find_match_first->user_images);@endphp
                @php $im=0; @endphp
                @foreach($find_match_first->user_images as $user_image)
                    <div class="match-slide-box m-0">
                        <div class="about-prof-img bg-img sidebar_image{{$im}}" style=" background-image: url({{$user_image->image}})"></div>
                    </div>
                    @php $im++;@endphp
                @endforeach  
                @for ($i = $image_length; $i < 4; $i++)
                    <div class="match-slide-box m-0">
                        <div class="about-prof-img bg-img sidebar_image{{$i}}" style=" background-image: url({{asset('design/img/user.png')}})"></div>
                    </div>
                @endfor  
                    <!-- <div class="match-slide-box">
                        <div class="about-prof-img bg-img sidebar_image1" style=" background-image: url({{asset('design/img/match-img-1/girl-2.jpg')}})"></div>
                    </div>
                    <div class="match-slide-box">
                        <div class="about-prof-img bg-img sidebar_image2" style=" background-image: url({{asset('design/img/match-img-1/girl-3.jpg')}})"></div>
                    </div>
                    <div class="match-slide-box">
                        <div class="about-prof-img bg-img sidebar_image3" style=" background-image: url({{asset('design/img/match-img-1/girl-1.jpg')}})"></div>
                    </div>
                    <div class="match-slide-box">
                        <div class="about-prof-img bg-img sidebar_image4" style=" background-image: url({{asset('design/img/match-img-1/girl-5.jpg')}})"></div>
                    </div> -->
                    
                </div>
            </div>

            <div class="map-pop-img bg-img" style="background-image: url();"></div>
            <!-- Photo Section Ends -->
            <!-- About Section Starts -->
            <div class="right-sec about-sec">
                <h6 class="about-tit">@lang('user.user.about') <span id="sidebar_firstname">
                 @if($find_match_first->first_name)
                    {{$find_match_first->first_name}}
                 @else
                       Not Available
                 @endif
                
                
                </span></h6>
                <p class="about-txt" id="sidebar_about">{{$find_match_first->about}}</p>
            </div>
            <!-- About Section Ends -->
            <!-- Video Section Starts -->
            <div class="right-sec video-sec">
                <h6 class="about-tit">@lang('user.user.bio_video')</h6>
                <video id="sidebar_video" width="310" controls="true">
                    <source src="{{$find_match_first->bio_video}}" type="video/mp4">Your browser doesn't support HTML5 video tag.
                </video>
            </div>
            <!-- Video Section Ends -->
            <!-- Tags Section Starts -->
             <div class="right-sec tag-section" id="sidebar_interest">
                @if(count($find_match_first->user_interest)>0)
                <h6 class="about-tit">@lang('user.user.interests')</h6>                
                @foreach($find_match_first->user_interest as $user_interest)
                    <span class="tags">{{$user_interest->interest->name}}</span>
                @endforeach 
                @endif
            </div>
        </div>
        <!-- Tag Section Ends -->
        <div class="right-foot">
            <a href="#" class="right-foot-item" data-target="#recommend-modal" data-toggle="modal">@lang('user.user.recommend')</a>
            <a href="#" class="right-foot-item" data-target="#report-modal" data-toggle="modal">@lang('user.user.report')</a>
            <input type="hidden" name="report_id" id="sidebar_report_id" value="{{$find_match_first->id}}">
            <input type="hidden" name="superlike_count" id="superlike_count" value="{{$superlike_reach}}">
            <input type="hidden" name="like_count" id="like_count" value="{{$today_like_reach}}">
            <input type="hidden" name="undo_val" id="undo_val" class="undo_val" value="{{$skip_pre}}">
            
            
        </div>
        @endif
        <?php  $image_val = url(asset('design/img/user.png')); ?>
        <!-- Right Sidebar Content Ends -->
    </div>
</div>

<!-- Report Modal Starts -->
<div id="report-modal" class="modal" data-easein="expandIn" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h5 class="modal-title">@lang('user.user.report_this_user')</h5>
            </div>
            <div class="modal-body p-0">
                <!-- Recommend List Starts -->
                <div class="recom-list">
                    <!-- Recommend Block Starts -->
                    <a href="javascript:void(0);" class="report-block active row" data-ajax="false" data-id="bad_message">
                        <div class="recom-block-left col-md-10 p-l-0">
                            <div class="recom-details m-0">
                                <h6><i class="zmdi zmdi-comment-outline recom-icon"></i> @lang('user.user.inappropriate_messages')</h6>
                            </div>
                        </div>
                        <div class="recom-block-right col-md-2 p-r-0 text-right">
                            <button class="recom-btn tick-btn"><i class="zmdi zmdi-check"></i></button>
                        </div>
                    </a>
                    <!-- Recommend Block Ends -->
                    <!-- Recommend Block Starts -->
                    <a href="javascript:void(0);" class="report-block row" data-ajax="false" data-id="bad_photo">
                        <div class="recom-block-left col-md-10 p-l-0">
                            <div class="recom-details m-0">
                                <h6><i class="zmdi zmdi-camera recom-icon"></i>@lang('user.user.inappropriate_photos') </h6>
                            </div>
                        </div>
                        <div class="recom-block-right col-md-2 p-r-0 text-right">
                            <button class="recom-btn tick-btn"><i class="zmdi zmdi-check"></i></button>
                        </div>
                    </a>
                    <!-- Recommend Block Ends -->
                    <!-- Recommend Block Starts -->
                    <a href="javascript:void(0);" class="report-block row" data-ajax="false" data-id="bad_behaviour">
                        <div class="recom-block-left col-md-10 p-l-0">
                            <div class="recom-details m-0">
                                <h6><i class="zmdi zmdi-cloud-off recom-icon"></i>@lang('user.user.bad_offline_behaviour') </h6>
                            </div>
                        </div>
                        <div class="recom-block-right col-md-2 p-r-0 text-right">
                            <button class="recom-btn tick-btn"><i class="zmdi zmdi-check"></i></button>
                        </div>
                    </a>
                    <!-- Recommend Block Ends -->
                    <!-- Recommend Block Starts -->
                    <a href="javascript:void(0);" class="report-block row" data-ajax="false" data-id="spam">
                        <div class="recom-block-left col-md-10 p-l-0">
                            <div class="recom-details m-0">
                                <h6><i class="zmdi zmdi-thumb-down recom-icon"></i>@lang('user.user.feel_like_spam') </h6>
                            </div>
                        </div>
                        <div class="recom-block-right col-md-2 p-r-0 text-right">
                            <button class="recom-btn tick-btn"><i class="zmdi zmdi-check"></i></button>
                        </div>
                    </a>
                    <!-- Recommend Block Ends -->                    
                </div>
                <!-- Recommend List Ends -->
            </div>
            <div class="modal-footer">
                <button class="white-btn two-btn" data-dismiss="modal">@lang('user.form.close')</button>
                <button onclick="user_report()" class="cmn-btn">@lang('user.form.submit')</button>
            </div>
        </div>
    </div>
</div>
<!-- Report Modal Ends -->

<!-- Recommend Modal Starts -->
<div id="recommend-modal" class="modal" data-easein="expandIn" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h5 class="modal-title">@lang('user.user.recommended_friend')</h5>
            </div>
            <div class="modal-body p-0">
                <h6 class="recom-tit">@lang('user.user.select_your_friend')</h6>
                <!-- Recommend List Starts -->
                <div class="recom-list">
                    @if(count($matches) > 0) 
                    <!-- Recommend Block Starts -->
                    @foreach($matches as $key=>$match_list)
		    @if($match_list->user)
                    <a href="javascript:void(0);" class="recom-block <?php if($key==0){echo 'active';}?> row" data-id="{{$match_list->user->id}}"> 
                        <div class="recom-block-left col-md-10 p-l-0">
                            @if($match_list->user->picture)
                                <div class="recom-img pull-left bg-img" style="background-image: url({{$match_list->user->picture}});"></div>
                            @else 
                                <div class="recom-img pull-left bg-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                            @endif
                            <div class="recom-details">
                                <h6>{{$match_list->user->first_name}} {{$match_list->user->last_name}}</h6>
                                <p>{{$match_list->user->description}}</p>
                            </div>
                        </div>
                        <div class="recom-block-right col-md-2 p-r-0 text-right">
                            <button class="recom-btn tick-btn"><i class="zmdi zmdi-check"></i></button>
                        </div>
                    </a>
		    @endif
                    @endforeach 
                    <!-- Recommend Block Ends -->
                    @else
                        <div class="notify-content">
                            <img src="{{asset('design/img/no-likes.png')}}">
                            <h6>@lang('user.user.no_friend_found')</h6>
                        </div>
                    @endif
                    
                </div>
                <!-- Recommend List Ends -->
            </div>
            <div class="modal-footer">
                <button class="white-btn two-btn" data-dismiss="modal">@lang('user.form.close')</button>
                <button onclick="send_recommend()" class="cmn-btn">@lang('user.user.send_request')</button>
            </div>
        </div>
    </div>
</div>
<!-- Recommend Modal Ends -->
<!-- Premium Modal Starts -->
<div id="premium-modal" class="modal" data-easein="expandIn" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
            </div>
            <div class="modal-body p-0">
                        
            </div>    
        </div>
    </div>
</div>
<!-- Premium Modal Ends -->
<!-- like superlike end details Modal Starts -->
<div id="like-superlike-modal" class="modal" data-easein="expandIn" tabindex="-1" style="background-color:rgba(0,0,0,.5)">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Next Reset Time</h5>
            </div>
            <div class="modal-body">
                <!-- Recommend List Starts -->
                <div class="recom-list">
                <div class='countdown' style = "padding-left:150px;" data-date="{{date('Y-m-d',$reset_date)}}" data-time="{{date('H:i',$reset_date)}}"></div> 
                                 
                </div>
                <!-- Recommend List Ends -->
            </div>
            <div class="modal-footer">
                <button class="cmn-btn ui-btn ui-shadow ui-corner-all" id ="time_close_button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--like superlike end details  Modal Ends -->
@endsection


@section('styles')
<style type="text/css">
    video#sidebar_video {
    width: 100%;
    height: auto;
}
.match-slide .slick-list{
    height: 300px;
}
</style>
@endsection

@section('scripts')
<!-- Map JS -->
<script src="https://maps.googleapis.com/maps/api/js?key={{Setting::get('map_key')}}&libraries=places"></script>
<script src="{{asset('design/js/jquery.googlemap.js')}}"></script>
<script src="{{asset('design/js/countdown.js')}}"></script>
<script type="text/javascript">

function user_report(){
        var url    = "{{url('user_report')}}";
        var token  = "{{csrf_token()}}";
        var report_id    = $('#sidebar_report_id').val();
        var reason=$('a.report-block.active').data('id');
        $.ajax({
            type: "POST",
            url : url,
            data: {'report_id' :report_id,'reason' :reason,'_token' :token},
            success: function(html)
            {
                toastr.success(html.message);
                toastr.options.positionClass = "toast-top-right";
                $('#report-modal').modal('hide');
            }
        });   
}
function send_recommend(){
    var url    = "{{url('user_recommend')}}";
    var token  = "{{csrf_token()}}";
    var recommend_id    = $('#sidebar_report_id').val();
    //var friend_id    = $('a.recom-block.active').data('id');
    var friend_ids=[];
    $('a.recom-block.active').each(function(){
        friend_ids.push($(this).data('id'));
    });
    if(friend_ids.length>0){    
        $.ajax({
            type: "POST",
            url : url,
            data: {'recommend_id' :recommend_id,'friend_ids' :friend_ids,'_token' :token},
            success: function(html)
            {
                toastr.success(html.message);
                toastr.options.positionClass = "toast-top-right";
                $('#recommend-modal').modal('hide');
            }
        });
    }else{
        alert('Please select friend');
        return false;
    }
}

</script>


<script src="{{asset('design/js/jquery.mobile-1.4.4.min.js')}}"></script>
@endsection
<div class="modal geolocation_hidden" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p>DateAround uses your location to find people  around you</p>
      </div>
      <div class="modal-footer">
        <button type="button" id = "geo_location_hidden" class="cmn-btn dis-btn">Allow</button>
      </div>
    </div>
  </div>
</div>
