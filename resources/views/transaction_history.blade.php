@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height p-f-15">
        <!-- Center Box Head Starts -->
        <div class="center-box-head row m-0">
            <h5 class="m-0 pull-left">@lang('user.menu.transaction_history')</h5>
            <a href="{{ url('/payments') }}" class="cmn-btn pull-right">@lang('user.form.back')</a>
        </div>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        <div class="center-box-content">
            <ul class="center-box-subhead row m-0 nav nav-tabs tab-head-top">
                <li role="presentation" class="active col-xs-3 p-0">
                    <a href="#all" class="center-box-head-block" aria-controls="add-money" role="tab" data-toggle="tab">@lang('user.payment.all')</a>
                </li>
                <li role="presentation" class="col-xs-3 p-0">
                    <a href="#paid" class="center-box-head-block" aria-controls="share-money" role="tab" data-toggle="tab">@lang('user.payment.paid')</a>
                </li>
                <li role="presentation" class="col-xs-3 p-0">
                    <a href="#received" class="center-box-head-block" aria-controls="share-money" role="tab" data-toggle="tab">@lang('user.payment.received')</a>
                </li>
                <li role="presentation" class="col-xs-3 p-0">
                    <a href="#added" class="center-box-head-block" aria-controls="share-money" role="tab" data-toggle="tab">@lang('user.payment.added')</a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- All Transaction Tab Starts -->
                <div role="tabpanel" class="tab-pane active history-tab" id="all">
                    <h6>@lang('user.payment.all_transaction')</h6>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>@lang('user.payment.details')</th>
                                <th>@lang('user.payment.amount')</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($transactions)>0)
                        @foreach($transactions as $transaction)   
                        @php 
                            $newDate = date("Y F d h:iA", strtotime($transaction->created_at));
                        @endphp                    
                        @if($transaction->status=="ADDED")
                            <tr>
                                <td scope="row">
                                    <div class="row m-0">
                                        <div class="details-left pull-left">
                                            <img src="{{asset('design/img/add-money.png')}}" class="table-img">
                                        </div>
                                        
                                        <div class="details-right">
                                            <p class="det-tit">@lang('user.payment.add_wallet')</p>
                                            <p class="det-txt">@lang('user.payment.from_bank')</p>
                                            <p class="det-txt">
                                                <i class="zmdi zmdi-long-arrow-down tran-icon receive-icon"></i> 
                                                {{$newDate}}
                                            </p>
                                        </div>
                                    </div>                                                
                                </td>
                                <td>{{currency($transaction->amount)}}</td>
                            </tr>                             
                        @elseif($transaction->status=="PAID" && $transaction->user_id==Auth::id())  
                            <tr>
                                <td scope="row">
                                    <div class="row m-0">
                                        <div class="details-left pull-left">
                                            <img src="{{asset('design/img/share-money.png')}}" class="table-img">
                                        </div>
                                        
                                        <div class="details-right">
                                            <p class="det-tit">@lang('user.payment.cash_sent')</p>
                                            <p class="det-txt">To: {{$transaction->user->first_name}} {{$transaction->user->last_name}}</p>
                                            <p class="det-txt">
                                                <i class="zmdi zmdi-long-arrow-up tran-icon sent-icon"></i> 
                                                {{$newDate}}
                                            </p>
                                        </div>
                                    </div>                                                
                                </td>
                                <td>{{currency($transaction->amount)}}</td>
                            </tr> 
                        @elseif($transaction->status=="PAID" && $transaction->receiver_id==Auth::id())  
                            <tr>
                                <td scope="row">
                                    <div class="row m-0">
                                        <div class="details-left pull-left">
                                            <img src="{{asset('design/img/receive-money.png')}}" class="table-img">
                                        </div>
                                        
                                        <div class="details-right">
                                            <p class="det-tit">@lang('user.payment.cash_received')</p>
                                            <p class="det-txt">From: {{$transaction->user->first_name}} {{$transaction->user->last_name}}</p>
                                            <p class="det-txt">
                                                <i class="zmdi zmdi-long-arrow-down tran-icon receive-icon"></i> 
                                                {{$newDate}}
                                            </p>
                                        </div>
                                    </div>                                                
                                </td>
                                <td>{{currency($transaction->amount)}}</td>
                            </tr> 
                        @elseif($transaction->status=="PREMIUM")  
                            <tr>
                                <td scope="row">
                                    <div class="row m-0">
                                        <div class="details-left pull-left">
                                            <img src="{{asset('design/img/share-money.png')}}" class="table-img">
                                        </div>
                                        
                                        <div class="details-right">
                                            <p class="det-tit">@lang('user.payment.paid_subscription')</p>
                                            <p class="det-txt">@lang('user.payment.getting_premium')</p>
                                            <p class="det-txt">
                                                <i class="zmdi zmdi-long-arrow-up tran-icon sent-icon"></i> 
                                                {{$newDate}}
                                            </p>
                                        </div>
                                    </div>                                                
                                </td>
                                <td>{{currency($transaction->amount)}}</td>
                            </tr>                       
                        @endif
                        @endforeach
                        @else
                            <tr><td scope="row">@lang('user.matches.not_found',['name' => 'Trasactions'])</td></tr>                            
                        @endif                     
                            
                            
                        </tbody>
                    </table>
                </div>
                <!-- All Transactions Tab Ends -->
                <!-- Paid Tab Starts -->
                <div role="tabpanel" class="tab-pane history-tab" id="paid">
                    <h6>Paid Transactions</h6>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>@lang('user.payment.details')</th>
                                <th>@lang('user.payment.amount')</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($transactions)>0)
                        @foreach($transactions as $transaction)   
                        @php 
                            $newDate = date("Y F d h:iA", strtotime($transaction->created_at));
                        @endphp                    
                        @if($transaction->status=="PAID" && $transaction->user_id==Auth::id())
                            <tr>
                                <td scope="row">
                                    <div class="row m-0">
                                        <div class="details-left pull-left">
                                            <img src="{{asset('design/img/share-money.png')}}" class="table-img">
                                        </div>
                                        
                                        <div class="details-right">
                                            <p class="det-tit">@lang('user.payment.cash_sent')</p>
                                            <p class="det-txt">To: {{$transaction->user->first_name}} {{$transaction->user->last_name}}</p>
                                            <p class="det-txt">
                                                <i class="zmdi zmdi-long-arrow-up tran-icon sent-icon"></i> 
                                                {{$newDate}}
                                            </p>
                                        </div>
                                    </div>                                                
                                </td>
                                <td>{{currency($transaction->amount)}}</td>
                            </tr>
                        @elseif($transaction->status=="PREMIUM")  
                            <tr>
                                <td scope="row">
                                    <div class="row m-0">
                                        <div class="details-left pull-left">
                                            <img src="{{asset('design/img/share-money.png')}}" class="table-img">
                                        </div>
                                        
                                        <div class="details-right">
                                            <p class="det-tit">@lang('user.payment.paid_subscription')</p>
                                            <p class="det-txt">@lang('user.payment.getting_premium')</p>
                                            <p class="det-txt">
                                                <i class="zmdi zmdi-long-arrow-up tran-icon sent-icon"></i> 
                                                {{$newDate}}
                                            </p>
                                        </div>
                                    </div>                                                
                                </td>
                                <td>{{currency($transaction->amount)}}</td>
                            </tr>  
                        @endif
                        @endforeach                           
                        @endif                             
                        </tbody>
                    </table>
                </div>
                <!-- Paid Tab Ends -->
                <!-- Received Tab Starts -->
                <div role="tabpanel" class="tab-pane history-tab" id="received">
                    <h6>Received Transactions</h6>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>@lang('user.payment.details')</th>
                                <th>@lang('user.payment.amount')</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($transactions)>0)
                        @foreach($transactions as $transaction)   
                        @php 
                            $newDate = date("Y F d h:iA", strtotime($transaction->created_at));
                        @endphp                    
                        @if($transaction->status=="PAID" && $transaction->receiver_id==Auth::id())
                            <tr>
                                <td scope="row">
                                    <div class="row m-0">
                                        <div class="details-left pull-left">
                                            <img src="{{asset('design/img/receive-money.png')}}" class="table-img">
                                        </div>
                                        
                                        <div class="details-right">
                                            <p class="det-tit">@lang('user.payment.cash_received')</p>
                                            <p class="det-txt">From: {{$transaction->user->first_name}} {{$transaction->user->last_name}}</p>
                                            <p class="det-txt">
                                                <i class="zmdi zmdi-long-arrow-down tran-icon receive-icon"></i> 
                                                {{$newDate}}
                                            </p>
                                        </div>
                                    </div>                                                
                                </td>
                                <td>{{currency($transaction->amount)}}</td>
                            </tr> 
                        @endif
                        @endforeach                                                  
                        @endif 
                        </tbody>
                    </table>
                </div>
                <!-- Received Tab Ends -->
                <!-- Added Tab Starts -->
                <div role="tabpanel" class="tab-pane active history-tab" id="added">
                    <h6>Added Transactions</h6>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>@lang('user.payment.details')</th>
                                <th>@lang('user.payment.amount')</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($transactions)>0)
                        @foreach($transactions as $transaction)   
                        @php 
                            $newDate = date("Y F d h:iA", strtotime($transaction->created_at));
                        @endphp                    
                        @if($transaction->status=="ADDED")
                            <tr>
                                <td scope="row">
                                    <div class="row m-0">
                                        <div class="details-left pull-left">
                                            <img src="{{asset('design/img/add-money.png')}}" class="table-img">
                                        </div>
                                        
                                        <div class="details-right">
                                            <p class="det-tit">@lang('user.payment.add_wallet')</p>
                                            <p class="det-txt">@lang('user.payment.from_bank')</p>
                                            <p class="det-txt">
                                                <i class="zmdi zmdi-long-arrow-down tran-icon receive-icon"></i> 
                                                {{$newDate}}
                                            </p>
                                        </div>
                                    </div>                                                
                                </td>
                                <td>{{currency($transaction->amount)}}</td>
                            </tr> 
                        @endif
                        @endforeach
                        @else
                            <tr><td scope="row">@lang('user.matches.not_found',['name' => 'Trasactions'])</td></tr>                            
                        @endif                             
                        </tbody>
                    </table>
                </div>
                <!-- Added Tab Ends -->
            </div>
        </div>
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<div class="col-md-3 p-0">
    <div class="right-sidebar body-height">
        <!-- Right Sidebar Content Starts -->
        <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">
            <h6>@lang('user.matches.invite',['sitename' => Setting::get('sitename')])</h6>
            <p>@lang('user.matches.invite_quote',['refer_money'=>Setting::get('currency').Setting::get('referal_amount')])</p>
            <a href="{{url('invites')}}" class="cmn-btn m-t-15">@lang('user.matches.invite_friends')</a>
        </div>
        <!-- Right Sidebar Content Ends -->
    </div>
</div>
@endsection
