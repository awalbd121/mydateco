@extends('layouts.user_header')

@section('content')
<!-- Center Box Starts -->
<div class="col-md-9 p-0">
    <div class="center-box body-height p-f-20">
        <!-- Center Box Head Starts -->
        <div class="center-box-head row m-0">
            <h5 class="m-0 pull-left">@lang('user.user.profile')</h5>
            <a href="{{url('/editprofile')}}" class="cmn-btn pull-right" data-ajax="false">@lang('user.form.edit_profile')</a>
        </div>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        <div class="center-box-content p-0">
            <div class="profile">
                <!-- Upload Image Section Starts -->
                <div class="upload-img prof-sec">
                    <!-- Main Image Upload Starts -->
                    <div id="">
                    @if($user->picture)
                        <div class="main-prof-img bg-img" style="background-image: url({{$user->picture}});"></div>
                    @else 
                         <div class="main-prof-img bg-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                    @endif                        
                    </div>
                    <!-- Main Image Upload Ends -->
                    @php 
                    $image1=$image2=$image3=$image4='';
                    if(count($user->user_images)>0){
                    foreach($user->user_images as $user_image){
                        if($user_image->status==1) $image1=$user_image->image;
                        if($user_image->status==2) $image2=$user_image->image;
                        if($user_image->status==3) $image3=$user_image->image;
                        if($user_image->status==4) $image4=$user_image->image;
                        if($user_image->status==5) $image5=$user_image->image;
                    }
                    }
                    @endphp
                    <!-- Sub Image Section Starts -->
                    <div class="sub-img-sec">
                        <div class="sub-img-sec-inner row m-0">
                            <!-- Sub Image Upload Starts -->
                            <div class="col-md-3 p-l-0">
                                @if($image1)
                                    <div class="bg-img edit-img" style="background-image: url({{$image1}});"></div>
                                @else 
                                     <div class="bg-img edit-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                                @endif
                            </div>
                            <!-- Sub Image Upload Ends -->
                            <!-- Sub Image Upload Starts -->
                            <div class="col-md-3 p-l-0">
                                @if($image2)
                                    <div class="bg-img edit-img" style="background-image: url({{$image2}});"></div>
                                @else 
                                     <div class="bg-img edit-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                                @endif
                            </div>
                            <!-- Sub Image Upload Ends -->
                            <!-- Sub Image Upload Starts -->
                            <div class="col-md-3 p-l-0">
                                @if($image3)
                                    <div class="bg-img edit-img" style="background-image: url({{$image3}});"></div>
                                @else 
                                     <div class="bg-img edit-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                                @endif
                            </div>
                            <!-- Sub Image Upload Ends -->
                            <!-- Sub Image Upload Starts -->
                            <div class="col-md-3 p-l-0">
                                @if($image4)
                                    <div class="bg-img edit-img" style="background-image: url({{$image4}});"></div>
                                @else 
                                     <div class="bg-img edit-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                                @endif
                            </div>
                            <!-- Sub Image Upload Ends -->
                        </div>
                    </div>
                    <!-- Sub Image Section Ends -->
                </div>
                <!-- Upload Image Section Ends -->
                <!-- Upload Video Starts -->
                <div class="upload-video prof-sec">
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.bio_video')</h6>
                        <video width="310" controls="true">
                        @if($user->bio_video)
                            <source src="{{$user->bio_video}}" type="video/mp4">Your browser doesn't support HTML5 video tag.
                        @else 
                             <source src="" type="video/mp4">Your browser doesn't support HTML5 video tag.
                        @endif
                            
                        </video>
                    </div>
                    <div class="form-group">
                    <!-- <a href="{{url('/premium')}}" class="cmn-btn pull-right" data-ajax="false">@lang('user.user.boost_your_account')</a> -->  
                        <!-- <h6 class="prof-tit">@lang('user.user.account_detail')</h6> -->

                        <!-- @if(count($user_premium)>0)
                            <p class="about-txt"><strong>{{$user_premium->premium->plan_name}} @lang('user.user.account') </strong>- @lang('user.user.expiry_date_at')
                            @if($user_premium->premium->duration=='day')
                                {{ Carbon\Carbon::parse($user_premium->created_at)->addDays($user_premium->premium->period)->format('d M Y') }}
                            @elseif($user_premium->premium->duration=='month')
                                {{ Carbon\Carbon::parse($user_premium->created_at)->addMonths($user_premium->premium->period)->format('d M Y') }}
                            @elseif($user_premium->premium->duration=='year')
                                {{ Carbon\Carbon::parse($user_premium->created_at)->addYears($user_premium->premium->period)->format('d M Y') }}
                            @endif
                             </p>
                        @else
                            <p class="about-txt"><strong>@lang('user.user.trial_account')</strong></p>
                        @endif    -->                                          
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.first_name')</h6>
                        <p class="about-txt">{{$user->first_name}}</p>
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.last_name')</h6>
                        <p class="about-txt">{{$user->last_name}}</p>
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.phone_number')</h6>
                        <p class="about-txt">{{$user->mobile}}</p>
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.dob')</h6>
                        <p class="about-txt">{{$user->dob}}</p>
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.gender')</h6>
                        <p class="about-txt">{{ucfirst($user->gender)}}</p>
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">Country</h6>
                        <p class="about-txt">{{ country_name(Auth::user()->country) }}</p>
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.about') {{$user->first_name}}</h6>
                        <p class="about-txt">{{$user->about}}</p>
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.current_work')</h6>
                        <p class="about-txt">{{$user->work}}</p>
                    </div>
                    <!-- <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.location')</h6>
                        <p class="about-txt">{{$user->address}}</p>
                    </div> -->
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.language')</h6>
                        <p class="about-txt">
                            @if($user->language == 'en') English @endif
                            @if($user->language == 'ja') Japanese @endif
                            @if($user->language == 'es') Spanish @endif
                            @if($user->language == 'zh-Hans') Simplified Chinese @endif
                            @if($user->language == 'zh-Hant') Traditional Chinese @endif
                        </p>
                    </div>
                    <div class="tag-section form-group">
                        <h6 class="prof-tit">@lang('user.user.interests')</h6>
                        @if(count($user->user_interest)>0)
                        @foreach($user->user_interest as $user_interest)
                            <span class="tags">{{$user_interest->interest->name}}</span>
                        @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <h6 class="prof-tit">@lang('user.user.control_profile')</h6>
                        <div class="discovery-block">
                            <h6 class="dis-tit">@lang('user.user.show_my_age')</h6>
                            <input type="checkbox" name="switch2" id="switch2" @if($user->show_age==1) {{'checked'}} @endif>
                        </div>
                        <div class="discovery-block">
                            <h6 class="dis-tit">@lang('user.user.distance_visible')</h6>
                            <input type="checkbox" name="switch3" id="switch3" @if($user->show_distance==1) {{'checked'}} @endif>
                        </div>
                    </div>
                </div>
                <!-- Upload Video Ends -->
            </div>
        </div>
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<!-- <div class="col-md-3 p-0">
    <div class="right-sidebar body-height"> -->
        <!-- Right Sidebar Content Starts -->
        <!-- <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">
            <h6>@lang('user.matches.get_premium',['sitename'=>Setting::get('sitename')])</h6>
            <p>@lang('user.matches.get_premium_quote').</p>
            <a href="{{url('premium')}}" class="cmn-btn">@lang('user.form.get_now')</a>
        </div> -->
        <!-- Right Sidebar Content Ends -->
<!--     </div>
</div> -->

@endsection
