@extends('layouts.user_header')

@section('content')

<!-- Center Box Starts -->
<div class="col-md-6 p-0">
    <div class="center-box body-height">
        <!-- Center Box Head Starts -->
        <div class="center-box-head p-f-20 row m-0">
            <h5 class="m-0">@lang('user.matches.who_likes_me_title')</h5>
        </div>
        <!-- Center Box Head Ends -->
        <!-- Center Box Content Starts -->
        @if(count($who_likes_me) > 0)      
        <div class="center-box-content p-0">
            <div class="match-list">
                <!-- Match List Block Starts -->                
                @foreach($who_likes_me as $who_likes)
                <div class="match-list-block row m-0 match_list_{{$who_likes->user_id}}">
                    <!-- Match List Block Left Starts -->
                    <div class="match-list-block-left col-md-6 p-l-0">
                        <a href="{{url('single-user',array('id'=>$who_likes->user->id))}}" class="match-list-block-profile">
                            @if($who_likes->user->picture)
                                <div class="match-list-img bg-img" style="background-image: url({{$who_likes->user->picture}});"></div>
                            @else 
                                <div class="match-list-img bg-img" style="background-image: url({{asset('design/img/user.png')}});"></div>
                            @endif
                            <div class="match-list-details">
                                <h6 class="match-list-tit">{{$who_likes->user->first_name}} {{$who_likes->user->last_name}}</h6>
                                <p class="match-list-txt">{{$who_likes->user_distance}} @lang('user.matches.away')</p>
                            </div>
                        </a>
                    </div>
                    <!-- Match List Block Left Ends -->
                    <!-- Match List Block Right Starts -->
                    <div class="match-list-block-right col-md-6 text-right p-r-0">
                        <a href="javascript::void(0);" onclick="like_dislike_superlike({{$who_likes->user_id}},3)" class="match-btn who-btn close-btn" title="Reject"  ><i class="zmdi zmdi-close"></i></a>
                        <a href="javascript::void(0);" onclick="like_dislike_superlike({{$who_likes->user_id}},2)" class="match-btn who-btn like-btn" title="Accept" ><i class="zmdi zmdi-favorite"></i></a>
                    </div>
                    <!-- MAtch List Block Right Ends -->
                </div>
                @endforeach                
                <!-- Match List Block Ends -->                
            </div>
        </div>
        @else
            <div class="center-box-content notify-block">
                <div class="notify-block-inner">
                    <div class="notify-content">
                        <img src="{{asset('design/img/nolikes.png')}}">
                        <h6>@lang('user.matches.not_found',['name' => 'Request'])</h6>
                    </div>
                </div>
            </div>
        @endif
        <!-- Center Box Content Ends -->
    </div>
</div>
<!-- Center Box Ends -->
<div class="col-md-3 p-0">
    <div class="right-sidebar body-height">
        <!-- Right Sidebar Content Starts -->
        <div class="right-sidebar-content banner text-center">
            <img src="{{asset('design/img/banner.png')}}" class="banner-img">
            <h6>@lang('user.matches.invite',['sitename' => Setting::get('sitename')])</h6>
            <p>@lang('user.matches.invite_quote',['refer_money'=>Setting::get('currency').Setting::get('referal_amount')])</p>
            <a href="{{url('invites')}}" class="cmn-btn m-t-15">@lang('user.matches.invite_friends')</a>
        </div>
        <!-- Right Sidebar Content Ends -->
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
function like_dislike_superlike(like_id,status){
    //var like_id=$(".buddy.active").attr('data-id');
    var url    = "{{url('likes')}}";
    var token  = "{{csrf_token()}}";    

    $.ajax({
        type: "POST",
        url : url,
        data: {'like_id':like_id,'status' :status,'_token' :token,'who_likes':1},
        success: function(html)
        {
            console.log(html);
            $('.match_list_'+like_id).remove();
            if(html.status=='DISLIKED'){
                toastr.error(html.message);
            }else{
                toastr.success(html.message);
            } 
            toastr.options.positionClass = "toast-top-right";
        }
    });
}
</script>

@endsection
