<?php
 Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return Redirect('admin/dashboard');
});

Route::get('/dashboard', 'AdminController@dashboard');

//Chat
Route::get('/chat', 'AdminController@chat');
Route::post('/chat', 'AdminController@chat');	
Route::post('/user_friends', 'AdminController@user_friends');

//Push
Route::get('/push', 'AdminController@push');
Route::post('/push', 'AdminController@send_push');

//User report
Route::get('/user-report', 'AdminController@user_report');
Route::post('/user-block/{id}', 'AdminController@user_block');

//Payment
Route::get('user-premium', 'AdminController@user_premium');
Route::get('user-transaction', 'AdminController@user_transaction');
Route::get('admin-transaction', 'AdminController@admin_transaction');

//Resource
Route::resource('user', 	'Resource\UserResource');
Route::resource('interest', 	'Resource\InterestResource');
Route::resource('premium', 	'Resource\PremiumResource');

//Settings
Route::get('/settings', 'AdminController@adminSettings');
Route::post('/settings_store', 'AdminController@settings_store');

Route::get('/static_pages', 'AdminController@static_pages');
Route::post('/static_update', 'AdminController@static_update');

//Accounts
Route::get('/profile', 'AdminController@profile');
Route::post('/profile_update', 'AdminController@profile_update');

Route::get('/password', 'AdminController@password');
Route::post('/password_update', 'AdminController@password_update');

//Matches
Route::get('/matches', 'AdminController@user_matches');
