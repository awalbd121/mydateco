<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
  //  return $request->user();
//});


Route::post('/register', 'UserApiController@register');
Route::post('/oauth/token' , 'Auth\LoginController@apiLogin');
Route::get('/country/list', 'UserApiController@country_list');
Route::post('/send/otp' , 'UserApiController@send_otp');
Route::post('/email/verify' , 'UserApiController@user_email_verify');
Route::post('/forgotPassword'  , 'UserApiController@forgotPasswordAPI');
Route::post('/changePassword'  , 'UserApiController@changePassword');
Route::post('/resetPassword'  , 'UserApiController@reset_password');

Route::post('/facebook','Auth\SocialLoginController@facebookViaAPI');
Route::post('/linkedin','Auth\SocialLoginController@linkedinViaAPI');
Route::post('/google', 'Auth\SocialLoginController@googleViaAPI');
Route::get('/facebook_status', 'UserApiController@facebook_status');

//Twillip VOIP

Route::post('dial', 'VideoRoomsController@dial_number');
//twilio access token
Route::get('/call/token', 'VideoRoomsController@voiceaccesstoken');



Route::get('/static_pages', function () {
	return response()->json(['privacy' => Setting::get('privacy_content'),'terms' => Setting::get('terms_content')]);
});


Route::group(['middleware' => ['auth:api']], function () {

	// user profile
	Route::get('/logout'  , 'UserApiController@logout');
	Route::post('/change_password' , 	'UserApiController@changePassword');
	Route::get('/get_profile' , 'UserApiController@getProfile');
	Route::post('/update_profile' , 'UserApiController@updateProfile');
	Route::get('/get_setting' , 'UserApiController@get_discover_settings');
	Route::post('/update_setting' , 'UserApiController@update_discover_settings');
	Route::post('/likes' , 'UserApiController@likes');
	Route::post('/update_likes' , 'UserApiController@update_likes');
	Route::get('/find_match' , 'UserApiController@findMatch');
	Route::get('/match_list' , 'UserApiController@matchList');
	Route::get('/who_likes_me' , 'UserApiController@whoLikesMe');
	Route::post('/add_money', 'UserApiController@add_money');
	Route::get('/card_list', 'UserApiController@payment');
	Route::post('/payment_process', 'UserApiController@payment_process');
	Route::post('/transaction_history', 'UserApiController@transaction_history');
	Route::post('/single_user' , 'UserApiController@single_user');
	Route::post('/user_report', 'UserApiController@user_report');
	Route::get('/notifications', 'UserApiController@notifications');
	Route::post('/update/chat/notification' ,'UserApiController@update_chat_notification');
	Route::post('/user_recommend', 'UserApiController@user_recommend');
	Route::get('/premium', 'UserApiController@premium');
	Route::post('/user_premium', 'UserApiController@user_premium');
	Route::get('/delete_account', 'UserApiController@delete_account');
	Route::post('/send_push', 'UserApiController@send_push');
	Route::post('/user_block', 'UserApiController@block_unblock');
	Route::get('/bloked_list', 'UserApiController@bloked_list');
	Route::post('/deletecountry', 'UserApiController@deletecountry');
	// Route::get('/facebook_status', 'UserApiController@facebook_status');


	// card
	Route::resource('card', 'CardController');

	Route::get('/video/access/token', 'VideoRoomsController@accesstoken');

});



