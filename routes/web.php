<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/otp', 'Auth\RegisterController@OTP');
Route::get('/', function () {
    //return view('welcome');
    $refercode=@$_GET['refercode'];
    Session::put('refercode',$refercode);
    return Redirect('/login');
});
/*Route::get('/home', function () {
    //return view('welcome');
    return Redirect('/dashboard');
});*/

//Static pages


Route::get('/clearconfig', function () {
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
});


Route::get('privacy', function () {
	$user=Auth::user();
	if($user)$user=$user;
	else $user=array();
    return view('privacy',compact('user'));
});
Route::get('terms', function () {
    $user=Auth::user();
	if($user)$user=$user;
	else $user=array();
    return view('terms',compact('user'));
});

Route::get('help', function () {
    
    $user=Auth::user();
    if($user)
      $user=$user;
    else 
      $user=array();
    return view('help',compact('user'));
});

// Route::get('contactus', function () {
//     $user=Auth::user();
// 	if($user)$user=$user;
// 	else $user=array();
//     return view('contactus',compact('user'));
// });

Route::get('testpush', 'SendPushNotification@testpush');

Auth::routes();

Route::group(['namespace' => 'Auth'], function() {
Route::post('/password/reset', 'ResetPasswordController@reset');
});

//Social login integration
Route::get('auth/facebook', 'Auth\SocialLoginController@redirectToFaceBook');
Route::get('auth/facebook/callback', 'Auth\SocialLoginController@handleFacebookCallback');
Route::get('auth/google', 'Auth\SocialLoginController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\SocialLoginController@handleGoogleCallback');
Route::get('auth/linkedin', 'Auth\SocialLoginController@redirectToLinkedin');
Route::get('auth/linkedin/callback', 'Auth\SocialLoginController@handleLinkedinCallback');
Route::post('account/kit', 'Auth\SocialLoginController@account_kit')->name('account.kit');

Route::get('/home', 'UserController@home');
Route::get('/dashboard', 'UserController@find_matches');
Route::get('/mapview', 'UserController@map_view');
Route::get('/profile', 'UserController@viewProfile');
Route::get('/editprofile', 'UserController@editProfile');
Route::post('/editprofile', 'UserController@updateProfile');
//ContactUs Details
Route::get('contactus', 'UserController@contactus');

Route::get('/matches', 'UserController@matches');
Route::get('/discover_settings', 'UserController@discover_settings');
Route::post('/discover_settings', 'UserController@update_discover_settings');
Route::get('/who_likes_me', 'UserController@who_likes_me');
Route::get('/chat', 'UserController@chat');
Route::get('/chat_user/{id}', 'UserController@chat_user');
//Route::get('/help', 'UserController@help');
Route::post('/likes' , 'UserController@likes');
Route::post('/update/new/notification' ,'UserController@update_notification');
Route::post('/skip' , 'UserController@skip');
Route::post('/update_likes' , 'UserController@update_likes');
Route::post('/single_user' , 'UserController@single_user');
Route::get('/payments', 'UserController@payment');
Route::post('/add_money', 'UserController@add_money');
Route::post('/payment_process', 'UserController@payment_process');
Route::get('/transaction_history', 'UserController@transaction_history');
Route::get('/notifications', 'UserController@notifications');
Route::get('/invites', 'UserController@invites');
Route::get('/premium', 'UserController@premium');
Route::post('/user_report', 'UserController@user_report');
Route::post('/user_recommend', 'UserController@user_recommend');
Route::post('/user_premium', 'UserController@user_premium');
Route::get('/single-user/{id}', 'UserController@user');
Route::post('/send_push', 'UserController@send_push');

Route::group(['middleware' => ['auth']], function () {
	Route::get('/logout' , 'Auth\LoginController@logout');
});

// card
Route::resource('card', 'CardController');


/*
|--------------------------------------------------------------------------
| Admin Authentication Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'admin'], function () {
  Route::get('/', 'AdminAuth\LoginController@showLoginForm');		
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
	
  // Route::group(['middleware' => ['admin']], function () {
	
  // });

});


Route::post('/usercredentials', 'UserController@verifyCredentials');
Route::post('/check-refercode', 'UserController@verifyRefercode');


Route::get('/pay-new', 'UserController@pay_new');
/*oute::get('/pay-new', function () {
   return view('payment-new');
});*/